#include <SPI.h>

// uncomment and recomment below to toggle between SD and SdFat
//#include <SD.h>
#include <SdFat.h>
SdFat SD; 

#define P(X) Serial.print(X)
uint8_t hexchar(uint8_t c)  { return "0123456789ABCDEF"[0x0F & c]; }
int chipselect = 2; 

File sdfile;
void setup()
{
    pinMode(chipselect, OUTPUT);
    delay(2000);
    bool bsucc = SD.begin(chipselect); 
    P("sdbegin "); 
    P(bsucc); 
    P("  "); 
    sdfile = SD.open("speed.txt", FILE_WRITE); 
    P("sdfile is "); 
    P(sdfile); 
    P("\n"); 
}

long mstampprev = 0; 
long n = 0; 
long maxd = 0; 
int multiple = 200; 
int ngapsline = 0; 
int ngapslineP = 17; 
int nbytessincegap = 0; 
long biggaptime = multiple*2; 
long sumnormaltimes = 0; 
long nnormaltimes = 0; 
long flushmultiple = 1000; 
void loop()
{
    long mstamp; 
    long mstampnext = ((int)(mstampprev/multiple + 1))*multiple; 
    do {
        mstamp = micros(); 
    } while (mstamp <= mstampnext); 
    
    for (int i = 32-4; i >= 0; i -= 4)
        sdfile.write(hexchar((mstamp>>i)&0x0f)); 
    long mwritetime = micros() - mstamp; 
    if (mwritetime > biggaptime) {
        if (ngapsline < ngapslineP) {
            P(" "); 
            P(mwritetime); 
            P("."); 
            P(nbytessincegap); 
        }
        nbytessincegap = 0; 
        ngapsline++; 
    } else {
        sumnormaltimes += mwritetime; 
        nnormaltimes++; 
    }
    
    nbytessincegap += 8; 
    
    long d = mstamp - mstampprev; 
    if (d > maxd)
        maxd = d; 
    n++; 
    if ((n % flushmultiple) == 0) {
        if (ngapsline > ngapslineP) {
            P(" ["); 
            P(ngapsline - ngapslineP); 
            P("]"); 
        }
        if (nnormaltimes != 0) {
            P(" ("); 
            P((float)sumnormaltimes/nnormaltimes); 
            P(")"); 
        }
        
        P("\n"); 
        P(n); 
        P(" "); 
        P(maxd); 
        maxd = 0; 
        ngapsline = 0; 
        sumnormaltimes = 0; 
        nnormaltimes = 0; 
        long mstamppreflush = micros(); 
        sdfile.flush(); 
        long mstampflushtime = micros() - mstamppreflush; 
        P(" F"); 
        P(mstampflushtime); 
    }
    mstampprev = mstamp; 
}

