// Copyright 2016 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef SDLOGGINGBASE_h
#define SDLOGGINGBASE_h

char hexchar(uint8_t c); 

class SDloggerBase : public Print
{
protected:
    void setlogfilename(int logfilenumber); 
    uint8_t chipselect; 
    uint8_t* sddata;   // accessed by 
    
public:
    uint8_t sdstate; // 0 uninitialized, 1 working, 0 dead, 3 read, 4 write, 5 write (dateset), 8 replay file
    char* logfilename; 
    int nlogfileslash;  // logfilename[nlogfileslash] == '/'
    int logfilenumber;  // logfilename[nlogfileslash+1:] == "NNN.TXT"
    
    // these are inisialized in opennextlogfileforwriting() rather than the constructor
    int32_t Nloglines; 
    int32_t Nloglinesfromserialbuff; 
    int32_t Nloglinesfailed; 
    int32_t Nlinessincelastflush; 
    int32_t Ncharssincelastflush; 
    uint32_t mstamplaststartline; 
    uint32_t mstamplastflush; 
    uint32_t mstamplastsuccess; 
    int pinLEDforflush; 
    SDloggerBase(uint8_t lchipselect, const char* dirname); 
    
    virtual void SetupSD()=0; 
    virtual void SDprintStats()=0;  // if you call this after log file is open, it drops the handle
    
    // almost totally a duplicate in Fat and SD, but riddled with use of SD object
    // bool opennextlogfileforwriting(char* pisodatetime, uint32_t mstamp); 
    // more important to remove the duplication stuff in writeline
    
    virtual void close()=0; 
    virtual void flush()=0; 
    virtual bool writestring(const char* str)=0;
    virtual void writeline(int offs)=0; 
    virtual bool opennextlogfileforwriting(const char* pisodatetime, uint32_t mstamp)=0; 
    virtual bool checkfornextflush(); 
    
    int startline(char cmd, uint32_t mstamp); 

    void writelogfileheader(uint32_t mstamp, const char* pisodatetime, int devicenumber);
    void writeformats(); 

    int buff8(int offs, int8_t val); 
    int buff16(int offs, int16_t val); 
    int buff24(int offs, int32_t val); 
    int buff32(int offs, int32_t val); 
    int buff16d10(int offs, uint16_t val); 
    
    void logacceleration (uint32_t mstamp, int16_t ax, int16_t ay, int16_t az, int16_t gx, int16_t gy, int16_t gz); 
    void logorient       (uint32_t mstamp, int16_t qw, int16_t qx, int16_t qy, int16_t qz, uint16_t calibstat); 
    void logorientA      (uint32_t mstamp, int16_t ax, int16_t ay, int16_t az, int16_t gx, int16_t gy, int16_t gz, int16_t qw, int16_t qx, int16_t qy, int16_t qz, uint16_t calibstat); 
    void logorientcalibrations(uint32_t mstamp, uint8_t calibstat, uint8_t* chdata, uint8_t length); 
    void loghumidity     (uint32_t mstamp, uint16_t rawHumidity, uint16_t rawTemperature); 
    void log7021humidity (uint32_t mstamp, uint16_t rawhumid, uint16_t rawtemp); 
    void log31humidity   (uint32_t mstamp, uint16_t rawHumidity, uint16_t rawTemperature); 
    void loggpscoeffs    (uint32_t mstamp, const char* isotimestamp, int32_t latdivisorE100, int32_t latdivisorN100, int32_t lngdivisorE100, int32_t lngdivisorN100, uint8_t devno); 
    uint8_t* loggpsabs   (uint32_t mstamp, uint32_t mstampmidnight, int32_t latminutes10000, int32_t lngminutes10000, int32_t altitude10, bool bbadgpschecksum, uint8_t devno); 
    void loggpsvel       (uint32_t mstamp, uint16_t velkph100, uint32_t veldegrees100, uint8_t devno); 
    void loggpsprec      (uint32_t mstamp, uint16_t horizontaldilutionofposition100, uint16_t verticaldilutionofposition100, uint8_t devno); 
    void logbfprs        (uint32_t mstamp, uint32_t prsreading); 
    void logwind         (uint32_t mstamp, uint32_t windcount, uint32_t windms); 
    void logtbarocoeffs  (uint32_t mstamp, uint16_t c1, uint16_t c2, uint16_t c3, uint16_t c4, uint16_t c5, uint16_t c6, uint16_t c7, uint16_t c8); 
    void logtbarometer   (uint32_t mstamp, int16_t temperature_raw, uint16_t pressure_raw); 
    void logpitot        (uint32_t mstamp, int32_t rawpresssum, uint16_t rawtemp, uint16_t windcountsum, uint32_t revtimesum); 
    void logbbarometer   (uint32_t mstamp, int32_t Pr, uint16_t temp100); 
    void logirlevel      (uint32_t mstamp, int16_t irt16, int16_t irtamb16); 
    void logdallas       (uint32_t mstamp, uint8_t i, int16_t temp16); 
    void logmrecord      (uint32_t mstamp, uint32_t Nlinessincelastflush, uint32_t Nmcount); 

    uint8_t* loglightlevel(uint32_t mstamp, int16_t lightlevel); 
    uint8_t* loghotwirewind(uint32_t mstamp, uint16_t hwrawwind, uint16_t hwrawtmp); 
    void loglakelevel    (uint32_t mstamp, char* isotimestamp, int16_t lightlevel, int16_t lakelevel); 
    void logserialbuff   (const char* buff, uint8_t bufflen); 
};

#endif
