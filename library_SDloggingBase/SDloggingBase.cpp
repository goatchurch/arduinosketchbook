// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include "SDloggingBase.h"

// have added line into SdVolume::cacheRawBlock() in sdVolume.cpp to stop crash in mkdir
// if (!sdCard_) return false; // added in to stop crashing in mkdir


void SDloggerBase::writeformats()
{
    writestring("Zt[ms]xyz[linacc]abc[gravacc]wxyz[quat]s[calibstat] orient\n"); 
    writestring("Yt[ms]s\"calibconsts\" orient calib\n"); 

    writestring("Rt[ms]d\"[isodate]\"e[latdE]n[latdN]f[lngdE]o[lngdN] GPS cooeffs\n"); 
    writestring("Qt[ms]u[ms midnight]y[lat600000]x[lng600000]a[alt] GPS\n"); 
    writestring("Vt[ms]v[kph100]d[deg100] GPS velocity\n"); 
    writestring("Pt[ms]h[dilute]v[dilute] GPS precision\n"); 

    writestring("Ft[ms]p[milibars] bluefly pressure\n"); 
    writestring("Dt[ms]i[num]r[temp16] dallas temperature\n"); 
    writestring("Wt[ms]w[micros5]n[count] wind meter\n"); 
    writestring("Ht[ms]r[rawhumid]a[rawtemp] Humidity21 meter\n"); 
    writestring("It[ms]r[raw-ir]a[tempamb] IR-sensor\n"); 
    writestring("St[ms]r[rawhumid]a[rawtemp] Humidity31 meter\n"); 
    writestring("Gt[ms]r[rawhumid]a[rawtemp] si7021Humidity meter\n"); 
    writestring("Lt[ms]l[brightness] Light cell\n"); 
    writestring("Ut[ms]d[dustiness] Dust sensor\n"); 
    writestring("Nt[ms]c[c1]d[c2]e[c3]f[c4]g[c5]h[c6]i[c7]j[c8] tbarometer coefs\n"); 
    writestring("Mt[ms]l[lines] File block summary\n"); 
    writestring("Ot[ms]r[rawtemp]q[rawpress] tbarometer\n"); 
    writestring("Bt[ms]p[Pr]c[temp100] bbarometer\n"); 
    writestring("Xt[ms]d[diffpressavg]r[rawtemp]n[windcount]w[revtime] px4pitot\n"); 
}


#define P(X) Serial.print(X)
        
char hexchar(uint8_t c)  { return "0123456789ABCDEF"[0x0F & c]; }

SDloggerBase::SDloggerBase(uint8_t lchipselect, const char* dirname) 
{
    chipselect = lchipselect; 
    pinMode(chipselect, OUTPUT); 
    sdstate = 0; 
    logfilenumber = 0; 
    logfilename = 0; 
    sddata = (uint8_t*)malloc(128*sizeof(uint8_t)); 

    if (dirname) {
        nlogfileslash = strlen(dirname);  
        logfilename = (char*)malloc((nlogfileslash + 9)*sizeof(char)); 
        strcpy(logfilename, dirname); 
        strcpy(logfilename+nlogfileslash, "/NNN.TXT"); 
    }
    logfilenumber = 0; 
    pinLEDforflush = -1; 
    Nloglines = 0; 
    Nloglinesfromserialbuff = 0; 
    Nloglinesfailed = 0; 
}



void SDloggerBase::setlogfilename(int logfilenumber) 
{
    if (logfilename) {
        logfilename[nlogfileslash+1] = logfilenumber/100 + '0'; 
        logfilename[nlogfileslash+2] = logfilenumber/10%10 + '0'; 
        logfilename[nlogfileslash+3] = logfilenumber%10 + '0';
    }
}

bool SDloggerBase::checkfornextflush()
{
    if ((Ncharssincelastflush > 40000) || (Nlinessincelastflush == 15000) || (mstamplaststartline - mstamplastflush > 10000)) {
long ms0 = millis();  
P("flush "); 
P(Ncharssincelastflush); 
        flush();  // resets the values 
P(" "); 
P(millis() - ms0);  
P("ms\n"); 
        //P("FLUSH!\n"); 
        //assert (Ncharssincelastflush == 0) && (Nlinessincelastflush == 0); 
        return true; 
    }
    return false; 
}


void SDloggerBase::writelogfileheader(uint32_t mstamp, const char* pisodatetime, int devicenumber)
{
    writestring("Logfile: "); 
    if (logfilename)
        writestring(logfilename); 
    if (pisodatetime != 0) {
        writestring("  Date: "); 
        writestring(pisodatetime); 
    }
    writestring("\n"); 
    if (devicenumber != -1) {
        writestring("Device number: "); 
        writestring(devicenumber == 0 ? "0" : "1"); 
        writestring("\n");
    }
    writestring("Compile date: "); 
    writestring(__DATE__); // sample "Dec  6 2009"
    writestring("  "); 
    writestring(__TIME__); // "12:34:56"
    writestring("\n"); 
    
    if (devicenumber != -1) {
        writeformats(); 
    }

    writestring("\n"); 
    Nloglines++; 
    flush(); 
    
/*    logfilenumber   
    long ts = Teensy3Clock.get();  // is there a set function too which we can use from the GPS value?
    isotimestampfromEpoch(ts); 
    Serial.println(GetIsotimestamp()); 
    Serial.println(epochfromisotimestamp()); 
*/
}

int SDloggerBase::startline(char cmd, uint32_t mstamp)
{
    mstamplaststartline = mstamp; 
    int offs = 0; 
    sddata[offs++] = cmd; 
    sddata[offs++] = 't'; 
    offs = buff32(offs, mstamp); 
    return offs; 
}


int SDloggerBase::buff8(int offs, int8_t val)
{
    sddata[offs++] = hexchar(val >> 4); 
    sddata[offs++] = hexchar(val); 
    return offs; 
}
int SDloggerBase::buff16(int offs, int16_t val)
{
    sddata[offs++] = hexchar(val >> 12); 
    sddata[offs++] = hexchar(val >> 8); 
    sddata[offs++] = hexchar(val >> 4); 
    sddata[offs++] = hexchar(val); 
    return offs; 
}

int SDloggerBase::buff16d10(int offs, uint16_t val)
{
    uint16_t d = 10000; 
    bool bwritedigit = false; 
    while (d != 0) {
        uint16_t dv = val/d; 
        bwritedigit = (bwritedigit || (dv != 0) || (d == 1)); 
        if (bwritedigit)
            sddata[offs++] = dv + '0'; 
        val -= dv * d; 
        d /= 10; 
    }
    return offs; 
}


int SDloggerBase::buff24(int offs, int32_t val)
{
    sddata[offs++] = hexchar(val >> 20); 
    sddata[offs++] = hexchar(val >> 16); 
    return buff16(offs, val); 
}
int SDloggerBase::buff32(int offs, int32_t val)
{
    offs = buff16(offs, val >> 16); 
    return buff16(offs, val); 
}


void SDloggerBase::logorient(uint32_t mstamp, int16_t qw, int16_t qx, int16_t qy, int16_t qz, uint16_t calibstat)
{
    int offs = startline('Z', mstamp); 
    sddata[offs++] = 'w'; 
    offs = buff16(offs, qw); 
    sddata[offs++] = 'x'; 
    offs = buff16(offs, qx); 
    sddata[offs++] = 'y'; 
    offs = buff16(offs, qy); 
    sddata[offs++] = 'z'; 
    offs = buff16(offs, qz); 
    sddata[offs++] = 'c'; 
    offs = buff16(offs, calibstat); 
    writeline(offs); 
}

void SDloggerBase::logacceleration(uint32_t mstamp, int16_t ax, int16_t ay, int16_t az, int16_t gx, int16_t gy, int16_t gz)
{
    int offs = startline('S', mstamp); 
    sddata[offs++] = 'x'; 
    offs = buff16(offs, ax); 
    sddata[offs++] = 'y'; 
    offs = buff16(offs, ay); 
    sddata[offs++] = 'z'; 
    offs = buff16(offs, az); 
    sddata[offs++] = 'a'; 
    offs = buff16(offs, gx); 
    sddata[offs++] = 'b'; 
    offs = buff16(offs, gy); 
    sddata[offs++] = 'c'; 
    offs = buff16(offs, gz); 
    writeline(offs); 
}

void SDloggerBase::logorientA(uint32_t mstamp, int16_t ax, int16_t ay, int16_t az, int16_t gx, int16_t gy, int16_t gz, int16_t qw, int16_t qx, int16_t qy, int16_t qz, uint16_t calibstat) 
{
    int offs = startline('Z', mstamp); 
    sddata[offs++] = 'x'; 
    offs = buff16(offs, ax); 
    sddata[offs++] = 'y'; 
    offs = buff16(offs, ay); 
    sddata[offs++] = 'z'; 
    offs = buff16(offs, az); 
    sddata[offs++] = 'a'; 
    offs = buff16(offs, gx); 
    sddata[offs++] = 'b'; 
    offs = buff16(offs, gy); 
    sddata[offs++] = 'c'; 
    offs = buff16(offs, gz); 
    sddata[offs++] = 'w'; 
    offs = buff16(offs, qw); 
    sddata[offs++] = 'x'; 
    offs = buff16(offs, qx); 
    sddata[offs++] = 'y'; 
    offs = buff16(offs, qy); 
    sddata[offs++] = 'z'; 
    offs = buff16(offs, qz); 
    sddata[offs++] = 's'; 
    offs = buff16(offs, calibstat); 
    writeline(offs); 
}


void SDloggerBase::logorientcalibrations(uint32_t mstamp, uint8_t calibstat, uint8_t* chdata, uint8_t length) 
{
    int offs = startline('Y', mstamp); 
    sddata[offs++] = 'c'; 
    offs = buff8(offs, calibstat); 
    sddata[offs++] = 's'; 
    sddata[offs++] = '"'; 
    for (uint8_t i = 0; i < length; i++) 
        offs = buff8(offs, chdata[i]); 
    sddata[offs++] = '"'; 
    writeline(offs); 
}

void SDloggerBase::loggpscoeffs(uint32_t mstamp, const char* isotimestamp, int32_t latdivisorE100, int32_t latdivisorN100, int32_t lngdivisorE100, int32_t lngdivisorN100, uint8_t devno)  
{
    int offs = startline('R', mstamp); 
    sddata[offs++] = 'd'; 
    sddata[offs++] = '"'; 
    while (*isotimestamp != 0)
        sddata[offs++] = *(isotimestamp++); 
    sddata[offs++] = '"'; 
    sddata[offs++] = 'e'; 
    offs = buff32(offs, latdivisorE100); 
    sddata[offs++] = 'n'; 
    offs = buff32(offs, latdivisorN100); 
    sddata[offs++] = 'f'; 
    offs = buff32(offs, lngdivisorE100); 
    sddata[offs++] = 'o'; 
    offs = buff32(offs, lngdivisorN100); 
    sddata[offs++] = 'A'+devno; 
    writeline(offs); 
    flush(); 
}

uint8_t* SDloggerBase::loggpsabs(uint32_t mstamp, uint32_t mstampmidnight, int32_t latminutes10000, int32_t lngminutes10000, int32_t altitude10, bool bbadgpschecksum, uint8_t devno)
{
    int offs = startline('Q', mstamp); 
    sddata[offs++] = 'u'; 
    offs = buff32(offs, mstampmidnight); 
    sddata[offs++] = 'y'; 
    offs = buff32(offs, latminutes10000); 
    sddata[offs++] = 'x'; 
    offs = buff32(offs, lngminutes10000); 
    sddata[offs++] = 'a'; 
    offs = buff16(offs, altitude10); 
    sddata[offs++] = 'A'+devno; 
    //if (bbadgpschecksum) {
    //    sddata[offs++] = 's'; 
    //    sddata[offs++] = '9'; 
    //}
    writeline(offs); 
    return sddata; 
}

void SDloggerBase::loggpsvel(uint32_t mstamp, uint16_t velkph100, uint32_t veldegrees100, uint8_t devno) 
{
    int offs = startline('V', mstamp); 
    sddata[offs++] = 'v'; 
    offs = buff16(offs, velkph100); 
    sddata[offs++] = 'd'; 
    offs = buff24(offs, veldegrees100); 
    sddata[offs++] = 'A'+devno; 
    writeline(offs); 
}

void SDloggerBase::loggpsprec(uint32_t mstamp, uint16_t horizontaldilutionofposition100, uint16_t verticaldilutionofposition100, uint8_t devno)
{
    int offs = startline('P', mstamp); 
    sddata[offs++] = 'h'; 
    offs = buff16(offs, horizontaldilutionofposition100); 
    sddata[offs++] = 'v'; 
    offs = buff24(offs, verticaldilutionofposition100); 
    sddata[offs++] = 'A'+devno; 
    writeline(offs); 
}

void SDloggerBase::logbfprs(uint32_t mstamp, uint32_t prsreading)  // bluefly barometer
{
    int offs = startline('F', mstamp); 
    sddata[offs++] = 'p'; 
    offs = buff24(offs, prsreading); 
    writeline(offs); 
}



void SDloggerBase::logwind(uint32_t mstamp, uint32_t windcount, uint32_t windms)
{
    int offs = startline('W', mstamp); 
    sddata[offs++] = 'w'; 
    if (windms >= 0x10000)
        windms = 0xFFFF; 
    offs = buff16(offs, windms);  // microseconds for 5 blade passes (one revolution)
    sddata[offs++] = 'n'; 
    offs = buff32(offs, windcount); 
    writeline(offs); 
}



void SDloggerBase::logdallas(uint32_t mstamp, uint8_t i, int16_t temp16)
{
    int offs = startline('D', mstamp); 
    sddata[offs++] = 'i'; 
    offs = buff8(offs, i); 
    sddata[offs++] = 'r'; 
    offs = buff16(offs, temp16); 
    writeline(offs); 
}

void SDloggerBase::logmrecord(uint32_t mstamp, uint32_t Nlinessincelastflush, uint32_t Nmcount)
{
    int offs = startline('M', mstamp); 
    sddata[offs++] = 'n'; 
    offs = buff32(offs, Nmcount); 
    sddata[offs++] = 'l'; 
    offs = buff32(offs, Nlinessincelastflush); 
    writeline(offs); 
}


uint8_t* SDloggerBase::loglightlevel(uint32_t mstamp, int16_t lightlevel) 
{
    int offs = startline('L', mstamp); 
    sddata[offs++] = 'l'; 
    offs = buff16(offs, lightlevel); 
    writeline(offs); 
    return sddata; 
}

uint8_t* SDloggerBase::loghotwirewind(uint32_t mstamp, uint16_t hwrawwind, uint16_t hwrawtmp) 
{
    int offs = startline('U', mstamp); 
    sddata[offs++] = 'w'; 
    offs = buff16(offs, hwrawwind); 
    sddata[offs++] = 'e'; 
    offs = buff16(offs, hwrawtmp); 
    writeline(offs); 
    return sddata; 
}


void SDloggerBase::loglakelevel(uint32_t mstamp, char* isotimestamp, int16_t lightlevel, int16_t lakelevel) 
{
    int offs = startline('K', mstamp); 
    sddata[offs++] = ','; 
    while (*isotimestamp != 0)
        sddata[offs++] = *(isotimestamp++); 
    sddata[offs++] = ','; 
    offs = buff16d10(offs, lightlevel); 
    sddata[offs++] = ','; 
    offs = buff16d10(offs, lakelevel); 
    writeline(offs); 
}

void SDloggerBase::logserialbuff(const char* buff, uint8_t bufflen)
{
    Nloglinesfromserialbuff++; 
    memcpy(sddata, buff, bufflen); 
    writeline(bufflen); 
}

void SDloggerBase::logirlevel(uint32_t mstamp, int16_t irt16, int16_t irtamb16) 
{
    int offs = startline('I', mstamp); 
    sddata[offs++] = 'r'; 
    offs = buff16(offs, irt16); 
    sddata[offs++] = 'a'; 
    offs = buff16(offs, irtamb16); 
    writeline(offs); 
}

void SDloggerBase::loghumidity(uint32_t mstamp, uint16_t rawHumidity, uint16_t rawTemperature) 
{
    int offs = startline('H', mstamp); 
    sddata[offs++] = 'r'; 
    offs = buff16(offs, rawHumidity); 
    sddata[offs++] = 'a'; 
    offs = buff16(offs, rawTemperature); 
    writeline(offs); 
}

void SDloggerBase::log31humidity(uint32_t mstamp, uint16_t rawHumidity, uint16_t rawTemperature)
{
    int offs = startline('S', mstamp); 
    sddata[offs++] = 'r'; 
    offs = buff16(offs, rawHumidity); 
    sddata[offs++] = 'a'; 
    offs = buff16(offs, rawTemperature); 
    writeline(offs); 
} 


void SDloggerBase::log7021humidity(uint32_t mstamp, uint16_t rawhumid, uint16_t rawtemp) 
{
    int offs = startline('G', mstamp); 
    sddata[offs++] = 'r'; 
    offs = buff16(offs, rawhumid); 
    sddata[offs++] = 'a'; 
    offs = buff16(offs, rawtemp); 
    writeline(offs); 
}

void SDloggerBase::logtbarometer(uint32_t mstamp, int16_t temperature_raw, uint16_t pressure_raw) 
{
    int offs = startline('O', mstamp); 
    sddata[offs++] = 'r'; 
    offs = buff16(offs, temperature_raw); 
    sddata[offs++] = 'q'; 
    offs = buff16(offs, pressure_raw); 
    writeline(offs); 
}

void SDloggerBase::logpitot(uint32_t mstamp, int32_t rawpresssum, uint16_t rawtemp, uint16_t windcountsum, uint32_t revtimesum) 
{
    int offs = startline('X', mstamp); 
    sddata[offs++] = 'd'; 
    offs = buff32(offs, rawpresssum); 
    sddata[offs++] = 'r'; 
    offs = buff16(offs, rawtemp); 
    sddata[offs++] = 'n'; 
    offs = buff16(offs, windcountsum); 
    sddata[offs++] = 'w'; 
    offs = buff32(offs, revtimesum); 
    writeline(offs); 
}

void SDloggerBase::logbbarometer(uint32_t mstamp, int32_t Pr, uint16_t temp100)
{
    int offs = startline('B', mstamp); 
    sddata[offs++] = 'p'; 
    offs = buff24(offs, Pr); 
    sddata[offs++] = 'c'; // uninteresting temperature of device when running hot
    offs = buff16(offs, temp100); 
    writeline(offs); 
}

void SDloggerBase::logtbarocoeffs(uint32_t mstamp, uint16_t c1, uint16_t c2, uint16_t c3, uint16_t c4, uint16_t c5, uint16_t c6, uint16_t c7, uint16_t c8) 
{
    int offs = startline('N', mstamp); 
    sddata[offs++] = 'c'; 
    offs = buff16(offs, c1); 
    sddata[offs++] = 'd'; 
    offs = buff16(offs, c2); 
    sddata[offs++] = 'e'; 
    offs = buff16(offs, c3); 
    sddata[offs++] = 'f'; 
    offs = buff16(offs, c4); 
    sddata[offs++] = 'g'; 
    offs = buff16(offs, c5); 
    sddata[offs++] = 'h'; 
    offs = buff16(offs, c6); 
    sddata[offs++] = 'i'; 
    offs = buff16(offs, c7); 
    sddata[offs++] = 'j'; 
    offs = buff16(offs, c8); 
    writeline(offs); 
}
