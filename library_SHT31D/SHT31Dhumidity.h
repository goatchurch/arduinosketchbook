
#ifndef SHT31DHUMIDITY_h
#define SHT31DHUMIDITY_h

#define SHT31_DEFAULT_ADDR    0x44
#define SHT31_MEAS_HIGHREP_STRETCH 0x2C06
#define SHT31_MEAS_MEDREP_STRETCH  0x2C0D
#define SHT31_MEAS_LOWREP_STRETCH  0x2C10
#define SHT31_MEAS_HIGHREP         0x2400
#define SHT31_MEAS_MEDREP          0x240B
#define SHT31_MEAS_LOWREP          0x2416
#define SHT31_READSTATUS           0xF32D
#define SHT31_CLEARSTATUS          0x3041
#define SHT31_SOFTRESET            0x30A2


class SHT31humidity 
{
public:
    uint16_t rawHumidity; 
    uint16_t rawTemperature; 
    void writeCommand(uint16_t cmd);
    boolean readstream(); 
    
    float GetTemperature(); 
    float GetHumidity(); 
    float GetDewpoint(); 
  
    uint16_t readStatus(void);
    uint8_t crc8(uint8_t b0, uint8_t b1);

    SHT31humidity();
};


#endif

