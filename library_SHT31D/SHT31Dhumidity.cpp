// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <Wire.h>
#include "SHT31Dhumidity.h"
#include <math.h>

SHT31humidity::SHT31humidity() 
{
}


boolean SHT31humidity::readstream() 
{
    writeCommand(0xE000);
    Wire.requestFrom(0x44, 6);
    if (Wire.available() != 6) 
      return false;
      
    byte msb, lsb, checksum;

    msb = Wire.read();
    lsb = Wire.read();
    checksum = Wire.read();
    if (checksum != crc8(msb, lsb)) 
        return false;
    rawTemperature = (((uint16_t)msb) << 8) | lsb;
    
    msb = Wire.read();
    lsb = Wire.read();
    checksum = Wire.read();
    if (checksum != crc8(msb, lsb)) 
        return false;
    rawHumidity = (((uint16_t)msb) << 8) | lsb;
    return true; 
}


float SHT31humidity::GetTemperature() 
{
      float stemp = rawTemperature;
      stemp *= 175;
      stemp /= 0xffff;
      stemp = -45 + stemp;
      return stemp;
}

float SHT31humidity::GetHumidity() 
{
    double shum = rawHumidity;
    shum *= 100;
    shum /= 0xFFFF;
    return shum; 
}

float SHT31humidity::GetDewpoint() 
{
    float tempC = GetTemperature(); 
    float humid = GetHumidity(); 
    float A = 8.1332, B = 1762.39, C = 235.66; 
    float svp = pow(10, (A - B/(tempC + C)))*133.322387415; 
    float pvp = svp*humid/100; 
    float dew = -C - B/(log10(pvp/133.322387415) - A); 
    return dew; 
}

uint16_t SHT31humidity::readStatus(void) 
{
  writeCommand(SHT31_READSTATUS);
  Wire.requestFrom(0x44, (uint8_t)3);
  uint16_t stat = Wire.read();
  stat <<= 8;
  stat |= Wire.read();
  //Serial.println(stat, HEX);
  return stat;
}





void SHT31humidity::writeCommand(uint16_t cmd) 
{
  Wire.beginTransmission(0x44);
  Wire.write(cmd >> 8);
  Wire.write(cmd & 0xFF);
  Wire.endTransmission();  
}

uint8_t SHT31humidity::crc8(uint8_t b0, uint8_t b1)
{
  const uint8_t POLYNOMIAL(0x31);
  uint8_t crc(0xFF);
  
  crc ^= b0;
  for ( int i = 8; i; --i ) 
    crc = ( crc & 0x80 ) ? (crc << 1) ^ POLYNOMIAL : (crc << 1);
    
  crc ^= b1;
  for ( int i = 8; i; --i ) 
    crc = ( crc & 0x80 ) ? (crc << 1) ^ POLYNOMIAL : (crc << 1);
    
  return crc;
}


