#include <Wire.h>
#include "SHT31Dhumidity.h"

#define P(X) Serial.print(X)

SHT31humidity sht31 = SHT31humidity();



void setup() 
{
  Serial.begin(9600);
  P("SHT31 test\n");

  Wire.begin();
  delay(1000); 
  P("SHT31 test 1\n");
  delay(1000); 
  P("SHT31 test 2\n");
  sht31.writeCommand(0x30A2);   // soft reset
  delay(1000); 
  P("SHT31 test 1\n");
  sht31.writeCommand(0x2737);   // read 10Hz?
  delay(1000); 
  P("SHT31 test 2\n");
}

int n = 0; 
void loop() 
{
  n++; 
  if (sht31.readstream()) {
    P(n); 
    P(","); 
    P(millis()); 
    P(","); 
    P(sht31.GetTemperature());
    P(","); 
    P(sht31.GetHumidity());
    P("\n");
  }
  delay(50);
}


