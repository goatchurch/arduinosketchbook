#include "Wire.h"
#include "simpledatetime.h"
#include "lightsensor.h"
  
#include "SDloggingBase.h"
//#include "SD.h"
//#include "SDlogging.h"
//SDlogger sdlogger(4, "OOL");  
#include "SPI.h"
#include "SdFat.h"
#include "SDloggingFat.h"
SDloggerFat sdlogger(4, "OOL");  


#define P(X) Serial.print(X)

SimpleDatetime simpledatetime; 
LightSensor lightsensor(A0, 8, 7, -1); // int lpinread, int lpinpower, int lpinLED, int lpinButton
int32_t Nloglinesfailedprev = 0; 
bool bfileopensucceeded = false; 

void setup()
{
    // wiring of I2C for Mega: SCL 21, SDA 20
    Wire.begin();
    Serial.begin(9600);

    // Wiring of SD on Arduino Mega:  CLK 52, DO 50, DI 51, 
    // Wiring of SD on Arduino Uno:   CLK 13, DO 12, DI 11,  (but there's not enough memory to run sdlogger)
    // Chip select: 4
    pinMode(53, OUTPUT); 
    sdlogger.SetupSD(); 
    sdlogger.pinLEDforflush = 7; 
    
    pinMode(A1, INPUT); // reads the pressure sensor current as voltage over a 250ohm resistor
    pinMode(9, OUTPUT); 
    digitalWrite(9, HIGH); 

    P("setup\n"); 
    sdlogger.SDprintStats(); 
    P(simpledatetime.pisotimestamp); 
    P("\n"); 

    simpledatetime.SetfromCompileTime(); 
    bfileopensucceeded = sdlogger.opennextlogfileforwriting(simpledatetime.pisotimestamp, millis()); 
    RTCdate(); 
    sdlogger.writelogfileheader(millis(), simpledatetime.pisotimestamp, -1); 
}

int n = 100; 
uint32_t Smillireadingstep = 6000; 
uint32_t Smillireadingsteplowpower = 12000; 
uint32_t Dnextlightmstamp = 0; 
uint32_t Smstampenterlowpower = 100000; 
void loop()
{
    uint32_t mstamp = millis(); 
    int32_t mtimestepNext = (int32_t)(Dnextlightmstamp - mstamp); // make it signed
    if (mtimestepNext < 50) {
        if (mtimestepNext > 1) {
            delay(mtimestepNext); 
        }
        
        
        RTCdate(); 
        int16_t lightlevel = lightsensor.ReadLight(); 
        uint16_t lakelevel = analogRead(A1); 

        sdlogger.loglakelevel(mstamp, simpledatetime.pisotimestamp, lightlevel, lakelevel); 

        if (lightlevel < 100) {
            Smstampenterlowpower = mstamp + 100000; 
        }
        bool binteractivemode = (mstamp < Smstampenterlowpower); 
        Dnextlightmstamp += (binteractivemode ? Smillireadingstep : Smillireadingsteplowpower); 
        
        P(simpledatetime.pisotimestamp); 
        P(" "); 
        P(lightlevel); 
        P(" "); 
        P(lakelevel); 
        if (binteractivemode) {
            if ((Nloglinesfailedprev != sdlogger.Nloglinesfailed) || (sdlogger.sdstate != 4)) {
                lightsensor.makeLEDsignal(40, 10, 0); 
                Nloglinesfailedprev = sdlogger.Nloglinesfailed; 
            }

            if (lakelevel == 0)  lakelevel = lightlevel; // when lakelevel is disconnected
            lightsensor.makeLEDsignal(500, lakelevel*5/1024, (lakelevel*50/1024) % 10); 
            P(" INTERACTIVE"); 
            P(" F"); 
            P(sdlogger.Nloglinesfailed); 
        }
        P("\n"); 
    }
}




void RTCdate()
{
    // Reset the register pointer
    Wire.beginTransmission(0x68);
    Wire.write(0x00);
    Wire.endTransmission();
  
    Wire.requestFrom(0x68, 7);
    uint8_t b0 = Wire.read(); 
    uint8_t b1 = Wire.read(); 
    uint8_t b2 = Wire.read(); 
    uint8_t b3 = Wire.read(); 
    uint8_t b4 = Wire.read(); 
    uint8_t b5 = Wire.read(); 
    uint8_t b6 = Wire.read(); 
    simpledatetime.SetfromRTC(b0, b1, b2, b3, b4, b5, b6); 
}


byte decToBcd(byte val){
  return ( (val/10*16) + (val%10) );
}

void setDateTime() {
  byte year  =       16; //0-99
  byte month =       1; //1-12
  byte monthDay =    29; //1-31
  byte weekDay =     5; //1-7
  byte hour =        13; //0-23
  byte minute =      39; //0-59
  byte second =      00; //0-59

  Wire.beginTransmission(0x68);
  Wire.write(0x00);

  Wire.write(decToBcd(second));
  Wire.write(decToBcd(minute));
  Wire.write(decToBcd(hour));
  Wire.write(decToBcd(weekDay));
  Wire.write(decToBcd(monthDay));
  Wire.write(decToBcd(month));
  Wire.write(decToBcd(year));

  Wire.write(0x00); //start 

  Wire.endTransmission();

}

