
#ifndef Si7021humidity_h
#define Si7021humidity_h



class SI7021humidity
{
public:
    uint16_t rawhumid; 
    uint16_t rawtemp; 
    
    void resetchip(); 
    bool checkchip(); 
    void checkstatus(); 
    
    void setmeasurementHT(bool bhold); // hold means it will hang, otherwise need to wait oneself
    bool fetchmeasurementHT(); 
    void setheater(uint8_t hheater); 
    
    float gethumidity(); 
    float gettemperature(); 

};


#endif

