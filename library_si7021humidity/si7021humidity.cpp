// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <Wire.h>
#include "si7021humidity.h"
#include <math.h>

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)

bool SI7021humidity::checkchip()
{
    // Read Electronic ID 1st Byte
    Wire.beginTransmission(0x40);
    Wire.write(0xFA);
    Wire.write(0x0F);
    Wire.endTransmission(false);
    Wire.requestFrom(0x40, 8);
    uint8_t sna3 = Wire.read();
    uint8_t crc3 = Wire.read();
    uint8_t sna2 = Wire.read();
    uint8_t crc2 = Wire.read();
    uint8_t sna1 = Wire.read();
    uint8_t crc1 = Wire.read();
    uint8_t sna0 = Wire.read();
    uint8_t crc0 = Wire.read();
    P("SNA "); PH(sna3); P(" "); PH(sna2); P(" "); PH(sna1); P(" "); PH(sna0); 

    // Read Electronic ID 1st Byte
    Wire.beginTransmission(0x40);
    Wire.write(0xFC);
    Wire.write(0xC9);
    Wire.endTransmission(false);
    Wire.requestFrom(0x40, 6);
    uint8_t snb3 = Wire.read();
    uint8_t snb2 = Wire.read();
    uint8_t crcb2 = Wire.read();
    uint8_t snb1 = Wire.read();
    uint8_t snb0 = Wire.read();
    uint8_t crcb0 = Wire.read();
    P("  SNB "); PH(snb3); P(" "); PH(snb2); P(" "); PH(snb1); P(" "); PH(snb0); 

    // Read Firmware Revision
    Wire.beginTransmission(0x40);
    Wire.write(0x84);
    Wire.write(0xB8);
    Wire.endTransmission(false);
    Wire.requestFrom(0x40, 1);
    uint8_t firmr = Wire.read();
    P("  firmware "); PH(firmr); 
    P("\n"); 
    
    return (snb3 == 0x15); 
}


void SI7021humidity::checkstatus()
{
    Wire.beginTransmission(0x40);
    Wire.write(0xE7);
    Wire.endTransmission(false);
    Wire.requestFrom(0x40, 1);
    uint8_t reg1 = Wire.read();
    
    Wire.beginTransmission(0x40);
    Wire.write(0x11);
    Wire.endTransmission(false);
    Wire.requestFrom(0x40, 1);
    uint8_t heater = Wire.read();
    
    P("MeasRes:"); PH(reg1 & 0x81); P(" VDD:"); PH(reg1 & 0x40);  P(" heater-on:"); PH(reg1 & 0x04);  P(" heater:"); P(heater & 0x0F); P("\n"); 
}

void SI7021humidity::setheater(uint8_t hheater)
{
    //assert hheater <= 15; 
    Wire.beginTransmission(0x40);
    Wire.write(0xE7);
    Wire.endTransmission(false);
    Wire.requestFrom(0x40, 1);
    uint8_t reg1 = Wire.read();

    reg1 = (hheater == 0 ? (reg1 & 0xFB) : (reg1 | 0x04)); 
    Wire.beginTransmission(0x40);
    Wire.write(0xE6);
    Wire.write(reg1);
    Wire.endTransmission();

    Wire.beginTransmission(0x40);
    Wire.write(0x11);
    Wire.endTransmission(false);
    Wire.requestFrom(0x40, 1);
    uint8_t heater = Wire.read();
    
    heater = ((heater & 0xF0) | hheater); 
    Wire.beginTransmission(0x40);
    Wire.write(0x51);
    Wire.write(heater);
    Wire.endTransmission();
}

void SI7021humidity::resetchip()
{
    Wire.beginTransmission(0x40);
    Wire.write(0xFE); 
    Wire.endTransmission();
}


// chip has the capability of holding on to the I2C bus for up to 20ms if requested for making the reading
void SI7021humidity::setmeasurementHT(bool bhold)
{
    Wire.beginTransmission(0x40);
    Wire.write(bhold ? 0xE5 : 0xF5); 
    Wire.endTransmission();
}

bool SI7021humidity::fetchmeasurementHT()
{    
    Wire.requestFrom(0x40, 2); 
    if (Wire.available() < 2) 
        return false; 
    uint16_t hmsb = Wire.read();
    uint16_t hlsb = Wire.read();
    //assert (hlsb & 0x03) == 2; 
    rawhumid = (hmsb << 8) | (hlsb & 0xFC);
    
    Wire.beginTransmission(0x40);
    Wire.write(0xE0);  // temperature measurement made as byproduct of humidity measurement
    Wire.endTransmission();
    Wire.requestFrom(0x40, 2); 
    if (Wire.available() < 2) 
        return false; 
    uint16_t tmsb = Wire.read();
    uint16_t tlsb = Wire.read();
    //assert (tlsb & 0x03) == 0; 
    rawtemp = (tmsb << 8) | (tlsb & 0xFC);
    return true;
}

float SI7021humidity::gethumidity()
{
    return ((125.0*rawhumid)/65536)-6; 
}

float SI7021humidity::gettemperature()
{
    return ((175.25*rawtemp)/65536)-46.85; 
}
