#include <Wire.h>
#include "si7021humidity.h"

// to do: check the heating/cooling curve when wind is applied to the chip
// (the temperature can go to 70C when heating element is on 15)
// the absolute humidity ought to be constant during this heating.
// there is hysteresis in terms of the humidity
// we need to find the temperature response curve in changes in wind temperature


bool bhold = false; // true means the fetchmeasurement is held by the device
int msdelay = 22;   // less than 20 doesn't work at high resultion
long heatercyclems = 10000*0; 

SI7021humidity sihumidity;

#define P(X) Serial.print(X)

void setup()
{
    Serial.begin(9600);   // open serial over USB at 9600 baud
    delay(1000); 
    Serial.println("hhh"); 
    delay(1000); 
    Serial.println("hhh2"); 
    delay(1000); 
    Serial.println("hhh3"); 

    Wire.begin();
    sihumidity.resetchip(); 
    delay(20); 
    sihumidity.checkchip();
    sihumidity.checkstatus();
}


byte heaterv = 0; 
void loop()
{
    if (heatercyclems != 0) {
        byte newheaterv = (millis() / heatercyclems) % 16; 
        if (newheaterv != heaterv) {
            heaterv = newheaterv; 
            sihumidity.setheater(heaterv); 
            sihumidity.checkstatus(); 
        }
    }

  
    long t0micros = micros(); 
    sihumidity.setmeasurementHT(bhold);
    delay(msdelay); 
    if (sihumidity.fetchmeasurementHT()) {
        if (heatercyclems != 0) {
            P("h"); 
            P(heaterv); 
            P(" "); 
        }
      
        P("ms: "); 
        P(micros() - t0micros); 
        
        P("  Temp:");
        P(sihumidity.gettemperature());
        P("C, ");
        
        P("Humidity:");
        P(sihumidity.gethumidity());
        P("%\n");
    } else {
        P("bad fetch\n"); 
    }
    
    delay(250);
    delay(750);
}

