// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef MLX90614irthermometer_h
#define MLX90614irthermometer_h

class IRthermometer
{
public:
    IRthermometer(); 
    float ReadIRtemperature(); 
    float ReadIRtemperatureAmbient(); 
    uint16_t ReadIRtemp16(uint8_t a); // a = 0x07 IR, 0x06 ambient
};

#endif
