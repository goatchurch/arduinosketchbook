// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"
#include "MLX90614irthermometer.h"
#include <Wire.h>

IRthermometer::IRthermometer()
{
}


uint16_t IRthermometer::ReadIRtemp16(uint8_t a) 
{
    uint16_t ret;
  
    Wire.beginTransmission(0x5A); 
    Wire.write(a); 
    Wire.endTransmission(false); // very important to use the "false"
    
    Wire.requestFrom((uint8_t)0x5A, (uint8_t)2);  // 3 if interested in CRC
    uint8_t b0 = Wire.read(); 
    uint8_t b1 = Wire.read(); 
    ret = b0 | (b1 << 8);
    //uint8_t pec = Wire.read();   // CRC not currently reproducable.
    return ret;
}

float IRthermometer::ReadIRtemperature() 
{ 
    uint16_t v = ReadIRtemp16(0x07); 
    if (v == 0xFFFF)
        return -999.0;
    return v*0.02 - 273.15; 
}

float IRthermometer::ReadIRtemperatureAmbient() 
{ 
    uint16_t v = ReadIRtemp16(0x06); 
    if (v == 0xFFFF)
        return -999.0;
    return v*0.02 - 273.15; 
}

