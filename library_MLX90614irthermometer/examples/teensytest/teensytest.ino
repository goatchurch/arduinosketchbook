#include <Wire.h>
#include "MLX90614irthermometer.h"

// see arduinotest version for inline code where development of config might be experimented upon

// Wiring: green wire to 19 SCL0, blue wire to 18 SDA0, red +, black ground.  
// https://learn.adafruit.com/using-melexis-mlx90614-non-contact-sensors/wiring-and-test

// with metal tab up sensor facing you, 
// topright: ground, bottomright: PWR
// topleft: SCL, bottomleft: SDA

// things to order: maxim IR sensor array
// the pitot pressure differential thing


#define P(X) Serial.print(X)

IRthermometer irthermometer;

void setup() 
{
    Serial.begin(9600);
    Wire.begin(); 
}

void loop() 
{
    delay(50);
    long x = micros(); 
    float t = irthermometer.ReadIRtemperature(); 
    long x1 = micros() - x;  // measure only the IR temperature reading
    P("  Object="); 
    P(t); 
    P("   "); 
    P(x1);   // about 0.50ms
    P("  Ambient="); 
    P(irthermometer.ReadIRtemperatureAmbient()); 
    P("\n"); 
}


