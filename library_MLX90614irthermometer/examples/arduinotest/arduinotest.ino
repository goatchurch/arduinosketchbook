#include <Wire.h>

#define P(X) Serial.print(X)

// https://learn.adafruit.com/using-melexis-mlx90614-non-contact-sensors/wiring-and-test
// www.adafruit.com/datasheets/MLX90614.pdf
// it would be useful to find how to configure this device for fast response

uint8_t Crc8(uint8_t inCrc, uint8_t inData)
{
    uint16_t data = inCrc ^ inData;
    data <<= 8; 
    for (uint8_t i = 0; i < 8; i++) {
        if (( data & 0x8000 ) != 0 )
            data = data ^ (0x1070U << 3);
        data = data << 1;
    }
    return data >> 8;
}

uint16_t read16(uint8_t a, uint8_t bprintv = false) 
{
    uint16_t ret;
  
    Wire.beginTransmission(0x5A); // start transmission to device 
    Wire.write(a); // sends register address to read from
    Wire.endTransmission(false); // end transmission
    
    Wire.requestFrom((uint8_t)0x5A, (uint8_t)3);// send data n-bytes read
    uint8_t b0 = Wire.read(); 
    uint8_t b1 = Wire.read(); 
    ret = b0 | (b1 << 8);
    uint8_t pec = Wire.read();
    if (bprintv) {
        P("\n"); 
        Serial.print(ret, HEX);  
        P(" "); 
        
        // check the CRC byte of which only the bottom 4 bits are meaningful
        Serial.print(pec & 0x0F, HEX);  
        P(".");
        Serial.print(Crc8(Crc8(0, b0), b1) & 0x0F, HEX); 
        P(" ");
    }
    return ret;
}

void setup() 
{
    Serial.begin(9600);
    Wire.begin(); 
}

void loop() 
{
    delay(50);
    long x1 = micros(); 
    uint16_t traw = read16(0x07); 
    x1 = micros() - x1; 
    float t = traw*0.02 - 273.15; 
    P("  Object = "); 
    P(t); 
    P("  microseconds="); 
    P(x1); 
    P("  Ambient = "); 
    P(read16(0x06)*0.02 - 273.15); 
    P("\n"); 
}


