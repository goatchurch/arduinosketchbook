// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef ADCTMP36_h
#define ADCTMP36_h

// for device http://www.adafruit.com/datasheets/ads1115.pdf (see cpp file for more details)
class ADCtmp36
{
public:
    int gainlevel; 
    float tmpforoled; 
    
    ADCtmp36(); 
    void SetupTMP36(int lgainlevel);
    int16_t read0(); 
};


#endif
