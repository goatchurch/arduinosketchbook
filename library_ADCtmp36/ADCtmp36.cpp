// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <Wire.h>
#include "ADCtmp36.h"

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)

// According to the volts produced by TMP36 and the 16 bit ADC
// The 2.048V range works up to 154degC
// The 1.024V range works up to 52degC  <-- this one, which is precise to 0.003degC
// The 0.512V range works up to 1.2degC


// http://www.adafruit.com/datasheets/ads1115.pdf


//float mvgain = 0.000125; 
// OS  MUX2 MUX1 MUX0       PGA2     PGA1     PGA0      MODE  
// DR2 DR1  DR0  COMP_MODE  COMP_POL COMP_LAT COMP_QUE1 COMP_QUE0

// 0x72E3; 
//DR2 DR1 DR0 = 000 -> 8 samples per second
//              100 -> 128 samples per second
//              110 -> 475 samples per second
//              111 -> 860 samples per second
//PGA2 PGA1 PGA0 = 001 -> 4.096V scale
//                 101 -> 0.256V scale
//MUX2 MUX1 MUX0 = 111 -> IN3 - GND
//MUX2 MUX1 MUX0 = 100 -> IN0 - GND

ADCtmp36::ADCtmp36()
{
}

int16_t ADCtmp36::read0() 
{
    Wire.requestFrom((uint8_t)0x48, (uint8_t)2);
    uint8_t r1 = Wire.read(); 
    uint8_t r2 = Wire.read(); 
    int16_t res = (r1 << 8) | r2; 
    
    float mvolts = 1000*(gainlevel == 8 ? 0.512 : 1.024) * res / 32767.0; 
    tmpforoled = (mvolts - 500)/10; 

    return res; 
}



void ADCtmp36::SetupTMP36(int lgainlevel)
{
    // config:
    // 0b*100**** 0b********  compare A0 to GND
    // 0b****011* 0b********  +/-1.024V range
    // 0b*******0 0b********  continuous operating mode
    // 0b******** 0b110*****  475 samples per second
    // 0b******** 0b******11  disable alert/rdy pin
    gainlevel = lgainlevel; 
    uint16_t config; 
    if (gainlevel == 8)
        config = 0x48C3;  // +/-0.512V range = Gain 8, 475 samples per second (can only read up to 1.2degC)
    else
        config = 0x4683;  // +/-1.024V range = Gain 4 (default), 128 samples per second (can read up to 52degC)
    
    Wire.beginTransmission(0x48);
    Wire.write((uint8_t)0x01); // config register
    Wire.write((uint8_t)(config >> 8));
    Wire.write((uint8_t)config);
    Wire.endTransmission();
    Wire.beginTransmission(0x48);
    Wire.write(0x00);
    Wire.endTransmission();

    delay(10); 
    Wire.requestFrom((uint8_t)0x48, (uint8_t)2);
    Wire.read(); 
    Wire.read(); 
    delay(10); 
    Wire.requestFrom((uint8_t)0x48, (uint8_t)2);
    Wire.read(); 
    Wire.read(); 
}


/* 
// including complete config version
int16_t read3reconfigurate() 
{
    int config = 0x72E3; 

    Wire.beginTransmission(0x48);
    Wire.write((uint8_t)0x01);
    Wire.write((uint8_t)(config >> 8));
    Wire.write((uint8_t)(config & 0xFF));
    Wire.endTransmission();
    delay(20); 
    
    Wire.beginTransmission(0x48);
    Wire.write(0x00); // ADS1015_REG_POINTER_CONVERT
    Wire.endTransmission();
    
    for (int i = 0; i < 3; i++) {
        Wire.requestFrom((uint8_t)0x48, (uint8_t)2);
        Wire.read(); 
        Wire.read(); 
        delay(3); 
    }
    
    Wire.beginTransmission(0x48);
    Wire.write(0x00); // ADS1015_REG_POINTER_CONVERT
    Wire.endTransmission();
    Wire.requestFrom((uint8_t)0x48, (uint8_t)2);
    uint8_t r1 = Wire.read(); 
    uint8_t r2 = Wire.read(); 

    return (r1 << 8) | r2; 
}
*/


