#include <Wire.h>
#include "ADCtmp36.h"

#define P(X) Serial.print(X)

ADCtmp36 adctmp36; 

void setup() 
{
    Serial.begin(9600);
    Serial.println("wire begin"); 
    Wire.begin(); 
    Serial.println("setting up"); 
    adctmp36.SetupTMP36(4); 
}

void loop() 
{
    delay(60);
      
    int16_t v = adctmp36.read0(); 
    //P(v); P("\n"); return;  // line 20

    
    P(millis()); 
    P(" raw=");   
    P(v);
    P(" "); 
    P(adctmp36.tmpforoled);
    P("C\n"); 
}

/*
import re
tlines = """
paste output from line 20 here
"""
vs = [int(tl)  for tl in tlines.split()  if tl]

fname = "/home/goatchurch/datalogging/permlogs/2015-05-04-bache.TXT"
#vs = [int(l[11:15], 16)  for l in open(fname)  if re.match("At[0-9A-F]{8}v[0-9A-F]{4}", l)]

#At0000253Dv4F20

#sendactivity("clearallcontours")
sendactivity("clearalltriangles")
vmin, vmax = min(vs), max(vs)
sendactivity("contours", contours=[[(i*0.002, (v-vmin)*0.02)  for i, v in enumerate(vs)]])
*/

