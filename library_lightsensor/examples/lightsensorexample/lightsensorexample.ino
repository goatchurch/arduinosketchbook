#include "Arduino.h"
#include "lightsensor.h"


// lpinreadlight, lpinpower, lpinLED, lpinButton
LightSensor lightsensor(14, 4, 3, 20); 
LightSensorGestures lightsensorgestures; 

#define P(X)  Serial.print(X)

void setup()
{
    Serial.begin(9600); 
    P("hi there"); 
    delay(1000); 
    P("hi there"); 
}

void loop()
{
#if 0

    int lc = lightsensor.LightChange(); 
    if (lc == 0)
        return; 
    int lg = lightsensorgestures.CommitLightChange(lc, millis()); 
    P(lc); 
    P(" "); 
    P(lg); 
    P("\n"); 
#else
    P(lightsensor.ReadLight()); 
    P(" "); 
    P(lightsensor.LightChange()); 
    P(" "); 
    P(digitalRead(lightsensor.pinButton)); 
    P(" "); 
    if (lightsensor.pinButton != -1)
        P(lightsensor.ReadButton()); 
    if (lightsensor.pinLED != -1)
        digitalWrite(lightsensor.pinLED, lightsensor.bButtonstate ? HIGH : LOW); 
    P(" "); 
    P(lightsensor.ReadButtonMillivolts()); 
    P("\n"); 
    delay(100); 
#endif   
}
    
