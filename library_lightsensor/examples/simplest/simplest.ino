
#define P(X) Serial.print(X)

void setup() 
{
    Serial.begin(9600); 
    pinMode(23, INPUT); 
    pinMode(22, OUTPUT); 
}

void loop() 
{
    P("light "); 
    digitalWrite(22, LOW); 
    P(analogRead(23));  
    digitalWrite(22, LOW); 
    P("  sw "); 
    P(analogRead(23)); 
    P("\n"); 
    delay(100); 
}
