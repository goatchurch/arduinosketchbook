// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"
#include "lightsensor.h"

//#define P(X) Serial.print(X)
#define P(X)


LightSensor::LightSensor(int lpinread, int lpinpower, int lpinLED, int lpinButton) 
{
    pinread = lpinread; 
    pinpower = lpinpower; 
    pinLED = lpinLED; 
    pinButton = lpinButton;
    if (pinread != -1)
        pinMode(pinread, INPUT); 
    if (pinpower != -1)
        pinMode(pinpower, OUTPUT); 
    if (pinLED != -1)
        pinMode(pinLED, OUTPUT); 
    if (pinButton != -1)
        pinMode(pinButton, INPUT); 
    bButtonstate = false; 
    nbuttonpresses = 0; 
    mstampbuttonrelease = 0; 
}
    
uint16_t LightSensor::ReadLight() 
{
    digitalWrite(pinpower, HIGH); 
    uint16_t res = analogRead(pinread); 
    digitalWrite(pinpower, LOW); 
    return res; 
};

int LightSensor::ReadButton() 
{
    uint32_t vres; 
    if (pinButton != -1)
        vres = analogRead(pinButton); // ARES=3.3V, R1=10K, R2=4.7K
    else if (pinread != -1)
        vres = analogRead(pinread)*2; // about 250 when depressed (not sure of the resistors) on the new flightlogger case
    else
        vres = 0;   
    
    if (vres > 200) {   // equiv to 2V on the power (on the known resistors), so can't happen
        if (bButtonstate)
            return 0; 
        bButtonstate = true; 
        if (millis() < mstampbuttonrelease + 100) {
            Serial.println("Suppressing double button click"); // could place these in an error message stack
            return 0; 
        } 
        nbuttonpresses++; 
        return 1;   // we only get it true once when it goes high
    }
    if (!bButtonstate)
        return 0; 
    bButtonstate = false; 
    mstampbuttonrelease = millis(); 
    return -1; 
};
    

uint16_t LightSensor::ReadFeatherMillivolts() 
{
    uint32_t vres = analogRead(pinButton);   // ARES=3.3V, R1=10K, R2=4.7K
    // vres/1024*3.3 * 20/10
    uint16_t volts1000 = (vres*33*200)/(1024); 
    return volts1000; 
}

uint16_t LightSensor::ReadButtonMillivolts() 
{
    uint32_t vres = analogRead(pinButton);
    // ARES=3.3V, R1=10K, R2=4.7K   vres/1024*3.3 * 14.7/4.7
    //uint16_t volts1000 = (vres*33*147*100)/(1024*47); 
    
    //vres/1024*3.3*2*1000
    uint16_t volts1000 = vres*3300*2/1024; // actually now it's two 22k resistors
    return volts1000; 
}


int LightSensor::LightChange()  // -1 dark;  0 no change;  1 bright
{
    int lightlevelcutoff = 700; 
    int newlightlevel = ReadLight();  // dark=1024, bright=0
    if ((currentlightchangestate == -1) && (newlightlevel > lightlevelcutoff-100))
        return 0; 
    if ((currentlightchangestate == 1) && (newlightlevel < lightlevelcutoff+100))
        return 0; 
    currentlightchangestate = (newlightlevel > lightlevelcutoff ? -1 : 1); 
    return currentlightchangestate; 
}


int LightSensorGestures::CommitLightChange(int lc, long mstamp) 
{
    if (lc == -1) {
        msdarkstamps[0] = msdarkstamps[1]; 
        msdarkstamps[1] = msdarkstamps[2]; 
        msdarkstamps[2] = mstamp; 
    }
    if (lc == 1) {
        long mgap5 = mstamp - msdarkstamps[0]; 
        if ((mgap5 > 500) && (mgap5 < 4000)) {
            P(msdarkstamps[1] - msdarkstamps[0]); P(" "); 
            P(mgap5*2/5);  P("   "); 
            P(msdarkstamps[2] - msdarkstamps[0]); P(" "); 
            P(mgap5*4/5);  P("   "); 
            P(mgap5);  P("   "); 
            P("\n"); 
            
            long mprec = mgap5/8; 
            if ((abs(msdarkstamps[1] - msdarkstamps[0] - mgap5*2/5) < mprec) && (abs(msdarkstamps[2] - msdarkstamps[0] - mgap5*4/5) < mprec)) {
                msdarkstamps[0] = 0; 
                msdarkstamps[1] = 0; 
                msdarkstamps[2] = 0; 
                return 1; 
            }
        }
    }
    return 0; 
}

void LightSensor::makeLEDflash(int duration) 
{
    int dur4 = duration/4; 
    for (int i = 1; i <= dur4; i++) {
        analogWrite(pinLED, 255*i/dur4); 
        delay(1); 
    }
    delay(duration-2*dur4); 
    for (int i = 1; i < dur4; i++) {
        delay(1); 
        analogWrite(pinLED, 255*(dur4-i)/dur4); 
    }
    analogWrite(pinLED, 0); 
}

void LightSensor::makeLEDsignal(int duration, int d0, int d1)
{
    for (int i = 0; i < d0; i++) {
        makeLEDflash(duration); 
        delay(duration*3/5); 
    }
    for (int i = 0; i < d1; i++) {
        makeLEDflash(duration/5); 
        delay(duration/2); 
    }
}




