// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef LIGHTSENSOR_h
#define LIGHTSENSOR_h

class LightSensor
{
public:
    int pinread; 
    int pinpower; 
    int pinLED; 
    int pinButton; 
    bool bButtonstate; 
    int nbuttonpresses; 
    long mstampbuttonrelease; 
    
    LightSensor(int lpinread, int lpinpower, int lpinLED, int lpinButton); 
    uint16_t ReadLight();  // between 0 for very bright and 1024 for dark
    int ReadButton();      // 1 depressed, -1 released, 0 stay the same
    uint16_t ReadButtonMillivolts(); // only works if button pressed
    uint16_t ReadFeatherMillivolts(); // pin A7
    
    int currentlightchangestate; 
    int LightChange();  // -1 dark;  0 no change;  1 bright
    
    void makeLEDflash(int duration); 
    void makeLEDsignal(int duration, int d0, int d1); 
};

class LightSensorGestures
{
public:
    long msdarkstamps[3]; 
    int CommitLightChange(int lc, long mstamp); 
};

#endif

