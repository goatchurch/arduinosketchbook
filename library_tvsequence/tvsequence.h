// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef HEADER_TVSEQUENCE
#define HEADER_TVSEQUENCE

#include <WString.h>

#define P(X) Serial.print(X)
#define PN Serial.println("")

#define TVINTS

// todo: 
// * code up fixed precision only and no floats in this example
// * convert the code to use consistent scalar or floats and switch on compilation
// * add in intermediate point for highest deviation (maxima and minima)
// * add a directional intermediate point on long series of no motion to guide the direction


#ifdef TVINTS
    typedef long scalar;
    typedef int rscalar;   // relative scalar
#else
    typedef float scalar;
    typedef float rscalar;
#endif

//#define DEBUG_ONLY( X )  X
#define DEBUG_ONLY( X )

int freeRam(); 

class WaveForm
{
public:
    long evtimeended; // seconds from power on
    int wavelength;   // seconds
    int waveclimb;    // seconds
    int light;        // seconds
    int wavemin;      // degrees10
    int wavemax;      // degrees10
    
    void Setval(float levtimeended, float lwavelength, float lwavemin, float lwaveclimb, float lwavemax, float llight); 
    String toString(); 
};

#define NWAVEFORMS 20
class WaveCatcher
{
public:
    WaveForm waveforms[NWAVEFORMS]; 
    int nwaveforms; 

    float thprev; 
    float vhprev; 
    float tlprev; 
    float vlprev; 
    float lighttime; 
    
    float vpdelta; 
    float tp, vp; 
    char state;  // d - descending, a - ascending
    
    WaveCatcher(); 
    void MarkPeak(float t, float v, char typ); 
    void GoutTV(float t, float v, char typ); 
};



class TVSequence
{
public:
    int tvarrsize; 
    int* tvarr;         // int[tvarrsize*2]
    int ntvupper; 
    int ntvlower;       // works backwards along the list
    
    int itn, ivn;       // (int)floor((tn - t0) * tfac + 0.5) 

    float t0, v0;       // origin offset for this segment
    float tn, vn;       // last entry for this segment
    float tfac, vfac;   // multiply factors for turning values into ints
    int ivlintol;       // line tolerance in v (after vfac factor)

    long ntvtotal;       // also potentially optional
    long ntvsegment;    // count of entries covered in this segment
    DEBUG_ONLY(int Dntvoutput);  // optional counter
    
    bool DeleteBackToConvexityTVabove(int it, int iv); 
    bool DeleteBackToConvexityTVbelow(int it, int iv); 
    bool ConsolidateTVabove(int it, int iv); 
    bool ConsolidateTVbelow(int it, int iv); 

    TVSequence(float ltfac, float lvfac, int ivlintol, int ltvarrsize=20); 
    ~TVSequence(); 
    void DumpState(); 
    
    void PointOut(float t, float v, int n); 
    
    void FirstTV(float t, float v, int lntv, char typ); 
    char AddTV(float t, float v); 
    
    void (*GoutTV)(float t, float v, char typ); // function pointer for output.  Syntactically easier to copy in than to use the constructor
    void GAddTV(float t, float v); 
    void GFinish(); 
};


#endif


