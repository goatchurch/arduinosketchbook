// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"
#include "tvsequence.h"

#define ASSERT( Expression )  { if (!(Expression))  { Serial.print(" ** Assert failed on line ");  Serial.println(__LINE__); } }
#define ASSERTM( Expression, M )  { if (!(Expression))  { Serial.print(" ** Assert ");  Serial.print(M); Serial.print(" failed on line ");  Serial.println(__LINE__); } }

int freeRam()
{
    extern int __heap_start, *__brkval; 
    int v; 
    return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}


void WaveForm::Setval(float levtimeended, float lwavelength, float lwavemin, float lwaveclimb, float lwavemax, float llight) 
{
    evtimeended = levtimeended; 
    wavelength = lwavelength; 
    wavemin = lwavemin*10; 
    waveclimb = lwaveclimb; 
    wavemax = lwavemax*10; 
    light = llight; 
}

String WaveForm::toString() 
{
    return String("w")+String(wavelength)+String("Tl")+String(wavemin)+String("Th")+String(wavemax)+String(light == 0 ? "" : "*"); 
}

WaveCatcher::WaveCatcher()
{
    vpdelta = 3; 
    state = 'd'; 
    nwaveforms = 0; 
}

void WaveCatcher::MarkPeak(float t, float v, char ptyp)
{
    DEBUG_ONLY(P(ptyp == 'h' ? "High peak at " : "Low peak at "); P(tp); P(" v="); P(vp); PN); 
    if (ptyp == 'l') {
        tlprev = t; 
        vlprev = v; 
        return; 
    }
    
    nwaveforms++; 
    if (nwaveforms == NWAVEFORMS)
        nwaveforms = 0; 
    waveforms[nwaveforms].Setval(t, t - thprev, vlprev, t - vlprev, v, lighttime);  
    lighttime = 0; 
    thprev = t; 
    vhprev = v; 
    P(waveforms[nwaveforms].toString());  PN; 
} 

void WaveCatcher::GoutTV(float t, float v, char typ)
{
    //Serial.print(ntvcount); Serial.print("/"); Serial.print(tvseq.ntvtotal); Serial.print(" t="); Serial.print(t); Serial.print("  Light: "); Serial.println(v); 
    if (state == 'a') {
        if (v > vp) {
            tp = t; 
            vp = v; 
        } else if (v < vp - vpdelta) { 
            MarkPeak(t, v, 'h'); 
            state = 'd'; 
            tp = t; 
            vp = v; 
        }
    } else {
        if (v < vp) {
            tp = t; 
            vp = v; 
        } else if (v > vp + vpdelta) {
            MarkPeak(t, v, 'l'); 
            state = 'a'; 
            tp = t; 
            vp = v; 
        }
    }
}



void DGoutTV(float t, float v, char typ)
{
    Serial.print("OutTV: ");  Serial.print(typ);  Serial.print(" "); Serial.print(t);  Serial.print("  ");  Serial.println(v);  
}

TVSequence::TVSequence(float ltfac, float lvfac, int livlintol, int ltvarrsize) 
{
    GoutTV = &DGoutTV; 
    tvarrsize = ltvarrsize; 
    tvarr = new int[tvarrsize*2]; 

    tfac = ltfac; 
    vfac = lvfac; 
    ivlintol = livlintol; 
    ntvtotal = 0; 
    DEBUG_ONLY(Dntvoutput = 0); 
    ntvsegment = 0; 
}

TVSequence::~TVSequence()
{
    delete tvarr; 
}

void TVSequence::FirstTV(float t, float v, int lntv, char typ) 
{
    //Serial.print("OutTV: ");  Serial.print(ntvoutput);  Serial.print(":");  Serial.print(lntv);  Serial.print("  ");  Serial.print(t);  Serial.print("  ");  Serial.println(v);  
    (*GoutTV)(t, v, typ); 
    DEBUG_ONLY(Dntvoutput++); 
    t0 = t; 
    v0 = v; 
    ntvupper = 0; 
    ntvlower = 0; 
    ntvtotal = lntv; 
    ntvsegment = 1; 
}

// this is the only arithmetical calculation, and it works for fixed point because it has a mult and a div
int Vinterp(int itm, int it, int iv)
{
    //ASSERTM((itm != 0), "VinterprgZ"); 
    ASSERTM(((itm >= 0) && (itm <= it)), String("Vinterprg ")+String(itm)+String(" ")+String(it)+String(" ")+String(iv)); 
    // interpolation ratios to match: ivm / itm == iv / it
    if (it == 0)  
        return iv; 
    return itm * iv / it; // could capture rounding errors up by operating at a factor of (x*2 +1)/2
    
}


bool TVSequence::DeleteBackToConvexityTVabove(int it, int iv)
{
    bool bres = false; 
    while (ntvupper > 0) {
        int im1 = ntvupper - 1; 
        int itp = 0; 
        int ivp = 0; 
        if (ntvupper != 1) {
            int im2 = ntvupper - 2; 
            itp = tvarr[im2*2]; 
            ivp = tvarr[im2*2 + 1]; 
        }
        int ivm = ivp + Vinterp(tvarr[im1*2] - itp, it - itp, iv - ivp); 
        if (ivm < tvarr[im1*2 + 1])
            break; 
        ntvupper--; 
        bres = true; 
    }
    return bres; 
}


bool TVSequence::DeleteBackToConvexityTVbelow(int it, int iv)
{
    bool bres = false; 
    while (ntvlower > 0) {
        int im1 = tvarrsize - ntvlower; 
        int itp = 0; 
        int ivp = 0; 
        if (ntvlower != 1) {
            int im2 = tvarrsize - ntvlower + 1; 
            itp = tvarr[im2*2]; 
            ivp = tvarr[im2*2 + 1]; 
        }
        int ivm = ivp + Vinterp(tvarr[im1*2] - itp, it - itp, iv - ivp); 
        if (tvarr[im1*2 + 1] < ivm)
            break; 
        ntvlower--; 
        bres = true; 
    }
    return bres; 
}


bool TVSequence::ConsolidateTVabove(int it, int iv)
{
    // append n point onto lower list if convex
    ntvlower++; 
    ASSERT(ntvlower + ntvupper <= tvarrsize); 
    tvarr[(tvarrsize - ntvlower)*2] = itn; 
    tvarr[(tvarrsize - ntvlower)*2 + 1] = ivn; 
    if (!DeleteBackToConvexityTVbelow(it, iv)) { 
        // look for tolerance deviation in lower list (which is reversed in the array)
        for (int i = tvarrsize - ntvlower; i < tvarrsize; i++) {
            int ivdiff = Vinterp(tvarr[i*2], it, iv) - tvarr[i*2 + 1]; 
            ASSERTM(ivdiff >= 0, String("ivdiff >= 0 ")+String(ivdiff)); 
            if (ivdiff > ivlintol)
                return false; 
        }
    }
    DeleteBackToConvexityTVabove(it, iv); 
    return true; 
}

bool TVSequence::ConsolidateTVbelow(int it, int iv)
{
    // append to tail end of upper list
    ntvupper++; 
    ASSERT(ntvlower + ntvupper <= tvarrsize); 
    tvarr[(ntvupper - 1)*2] = itn; 
    tvarr[(ntvupper - 1)*2 + 1] = ivn; 
    
    if (!DeleteBackToConvexityTVabove(it, iv)) {
        // look for tolerance deviation in upper list
        for (int i = ntvupper - 1; i >= 0; i--) {
            int ivdiff = tvarr[i*2 + 1] - Vinterp(tvarr[i*2], it, iv); 
            ASSERT(ivdiff >= 0); 
            if (ivdiff > ivlintol)
                return false; 
        }
    }
    
    // delete back to convexity in lower list (which is reversed in the array)
    DeleteBackToConvexityTVbelow(it, iv); 
    return true; 
}


char TVSequence::AddTV(float t, float v)
{
    ntvtotal++; 
    ASSERT(ntvsegment != 0); 
    float fit = (t - t0) * tfac + 0.5; 
    if ((fit >= 32767) && (ntvsegment != 1))
        return false;      // overflow in time int
    
    int it = (int)fit; // should be unsigned
    int iv = (int)floor((v - v0) * vfac + 0.5); 
    DEBUG_ONLY(Serial.print("    it="); Serial.print(it); Serial.print(" iv="); Serial.print(iv);  Serial.print("  ntvsegment="); Serial.println(ntvsegment)); 
    if (ntvsegment == 1) {
        tn = t; 
        vn = v; 
        itn = it; 
        ivn = iv;  
        ntvsegment = 2; 
        return true; 
    }
    ASSERT(t >= tn); 
    
    ntvsegment++; 
    if (ivlintol == -1)
        return false;      // all points 
    if (ivlintol == 0) {
        if ((iv != ivn) || (iv != 0))
            return false;  // just where there is a change in v
        tn = t; 
        vn = v; 
        return true; 
    }
    
    if (ntvlower + ntvupper == tvarrsize) {
        DumpState(); 
        return false;     // overflow on memory
    }
    
    // non-trivial linear tolerance case
    char bcontinuesequence; 
    int livn = Vinterp(itn, it, iv); 
    if (livn >= ivn) 
        bcontinuesequence = ConsolidateTVabove(it, iv); 
    else
        bcontinuesequence = ConsolidateTVbelow(it, iv); 
    DEBUG_ONLY(Serial.print("    ntvupper="); Serial.print(ntvupper); Serial.print(" ntvlower="); Serial.print(ntvlower); Serial.print(" "); Serial.println((int)bcontinuesequence)); 
    if (!bcontinuesequence)
        return false; 
        
    tn = t; 
    vn = v; 
    itn = it; 
    ivn = iv; 
    ASSERTM(ntvlower + ntvupper <= ntvsegment - 2, "upperlowertoohigh");  // includes start and end points
    if (!(ntvlower + ntvupper <= ntvsegment - 2))
        { DumpState();  while (true); }
    return true; 
}
        
void TVSequence::GAddTV(float t, float v)
{
    DEBUG_ONLY(Serial.print("  AddTV: ");  Serial.print(ntvtotal);  Serial.print(": ");  Serial.print(t);  Serial.print("  ");  Serial.println(v));  
    if (ntvsegment == 0)
        return FirstTV(t, v, 1, 'f');  
    if (AddTV(t, v)) 
        return; 
        
    // place in this trailing segment
    FirstTV(tn, vn, ntvtotal - 1, 'm');  
    AddTV(t, v);  
}

void TVSequence::GFinish()
{
    DEBUG_ONLY(DumpState()); 
    FirstTV(tn, vn, ntvtotal, 'e'); 
    ntvsegment = 0; 
}

void TVSequence::DumpState() 
{
    Serial.println(""); 
    Serial.print("ntvsegment="); Serial.println(ntvsegment); 
    Serial.print("t0v0 "); Serial.print(t0); Serial.print(" "); Serial.println(v0); 
    Serial.print("tnvn "); Serial.print(tn); Serial.print(" "); Serial.println(vn); 
    Serial.print("itnvn "); Serial.print(itn); Serial.print(" "); Serial.println(ivn); 
    for (int i = 0; i < ntvupper; i++) {
        Serial.print("upper "); Serial.print(i); Serial.print(": "); Serial.print(tvarr[i*2]); Serial.print(" "); Serial.println(tvarr[i*2+1]); 
    }
    for (int i = 0; i < ntvlower; i++) {
        Serial.print("lower "); Serial.print(i); Serial.print(": "); Serial.print(tvarr[(tvarrsize-i-1)*2]); Serial.print(" "); Serial.println(tvarr[(tvarrsize-i-1)*2+1]); 
    }
    Serial.println(""); 
}




