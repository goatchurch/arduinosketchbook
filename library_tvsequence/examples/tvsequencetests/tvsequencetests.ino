// (C) 2014 Julian Todd <julian@goatchurch.org.uk>  freesteel.co.uk
// This code is free.  See the COPYING.WTFPL for more details.

#include "tvsequence.h"

int ntvcount = 0; 
void GoutTV(float t, float v, char typ)
{
    DEBUG_ONLY(Serial.print("OutTV: ");  Serial.print(typ);  Serial.print(" "); Serial.print(ntvcount); Serial.print(": "); Serial.print(t);  Serial.print("  ");  Serial.println(v));  
    ntvcount++; 
}


void TestThining(int testno, float* tvdata, int tvdatalen, float tfac, float vfac, int ivlintol, int expectedntvout)
{
    for (int sg = -1; sg < 3; sg += 2) {  // reflect in v to check answers agree
        Serial.print("--Testno: "); Serial.print(testno);  Serial.print("sg");  Serial.println(sg); 
        TVSequence tvseq(tfac, vfac, ivlintol); 
        tvseq.GoutTV = &GoutTV; 
        ntvcount = 0; 
        for (int i = 0; i < tvdatalen; i++) {
            tvseq.GAddTV(tvdata[i*2], tvdata[i*2 + 1]*sg); 
        }
        tvseq.GFinish(); 
        if (ntvcount != expectedntvout) {
            Serial.print("Fail ");  Serial.println(testno); 
            Serial.print("ivlintol: ");  Serial.println(ivlintol); 
            Serial.println("\n"); 
        }
    }
}


float tvdata0[] = { 10.0, 20.0,  12.0, 20.0,  13.0, 20.0,  15.0, 20.0,  19.0, 20.0,   22.0, 30.0,  26.0, 30.0,  28.0, 30.0 }; 
float tvdata1[] = { 2, 2,  4, 2.9,  6, 4,  8, 5.2,  10, 6,  11, 5,  12, 4,  18, -2,  20, -4 }; 
float tvdata2[] = { 0, 88,  302,  87,  604,  87,  926,  88,  1249,  89,  1572,  87,  1898,  89,  2222,  87, 
    2547,  89,  2871,  87,  3196,  88,  3521,  88,  3846,  88,  4171,  88,  4496,  88,  4822,  88, 
    5147,  89,  5474,  87,  5799,  89,  6125,  88,  6450,  88,  6775,  88,  7100,  88,  7426,  88, 
    7752,  87,  8079,  89,  8406,  88,  8731,  88,  9058,  89,  9383,  87 }; 
float tvdata3[] = { 0, 0,  1, 10,   2, 15,  4, 18,  6, 20,  8, 21,  10, 21,  12, 20,  14, 18,  16, 15,  18, 10,  20, 0 }; 
    
void setup(void)
{
    Serial.begin(9600); 
Serial.println("hi1"); 
delay(3000); 
Serial.println("hi2"); 


    TestThining(2, tvdata0, 5, 1.0, 1.0, -1, 5);  // every point output case
    TestThining(3, tvdata0, 5, 1.0, 1.0, 0, 2);   // direct horizontal lines case
    TestThining(4, tvdata0, 8, 1.0, 1.0, 0, 4);   // direct horizontal lines case with a step

    TestThining(13, tvdata0, 5, 1.0, 1.0, 1, 2); 
    TestThining(14, tvdata0, 8, 1.0, 1.0, 1, 4); 
    TestThining(15, tvdata0, 8, 1.0, 1.0, 10, 2); 
    TestThining(16, tvdata0, 8, 1.0, 1.0, 5, 4); 
    TestThining(20, tvdata1, 9, 1, 1, 0, 9); 
    TestThining(21, tvdata1, 9, 1, 1, 1, 3); 
    TestThining(22, tvdata1, 9, 1, 100, 18, 4); // verify the geometry
    TestThining(222, tvdata2, 30, 0.1, 1, 10, 2); 

    TestThining(43, tvdata3, 12, 10.0, 10.0, 300, 2); 
    TestThining(44, tvdata3, 12, 10.0, 10.0, 100, 3); 
return; 
}


void loop()
{
}

