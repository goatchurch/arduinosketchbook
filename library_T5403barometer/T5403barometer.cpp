// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <Wire.h>
#include "T5403barometer.h"

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)

int16_t T5403Barometer::getData(uint8_t location)
{
    uint8_t byteLow, byteHigh;
    Wire.beginTransmission(0x77); 
    Wire.write(location);
    Wire.endTransmission();    // Transmit data
    Wire.requestFrom(0x77,2);

    while(Wire.available()){
        byteLow = Wire.read(); // receive low byte 
        byteHigh = Wire.read(); // receive high byte
    }
    return (byteHigh << 8)|(byteLow);
}

void T5403Barometer::sendCommand(uint8_t location, uint8_t command)
{
    Wire.beginTransmission(0x77); 
    Wire.write(location);
    Wire.write(command);
    Wire.endTransmission();
}

void T5403Barometer::T5403SetUp()
{
    c1 = (uint16_t)getData(0x8E);  
    c2 = (uint16_t)getData(0x90);  
    c3 = (uint16_t)getData(0x92);  
    c4 = (uint16_t)getData(0x94);  
    c5 = getData(0x96);  
    c6 = getData(0x98);  
    c7 = getData(0x9A);  
    c8 = getData(0x9C);  
    bstate = 0; 
    mstamp = millis(); 
}
    

void T5403Barometer::RequestPressure()
{
    sendCommand(0xF1, 0x03); 
    mstamp = millis(); 
    bstate = 1; 
}
    
bool T5403Barometer::valuesready()
{
    if (bstate == 1) {
        if (millis() < mstamp + 6) 
            return false; 
        temperature_raw = getData(0xF5);
        mstamp = millis(); 
        bstate = 1; 
        
        sendCommand(0xF1, (0x11 << 3)|(0x01)); 
        mstamp = millis(); 
        bstate = 2; 
        return false; 
    }
    if (bstate == 2) {
        if (millis() < mstamp + 68) 
            return false; 
        pressure_raw = getData(0xF5);
        bstate = 0;
        mstamp = millis(); 
        return true; 
    }
    return false; 
}

float T5403Barometer::GetTemperature() 
{ 
    return (((((c1 * temperature_raw) >> 8) + (c2 << 6)) * 100) >> 16)*0.01;
}

int32_t T5403Barometer::GetPressure() 
{ 
    int32_t s = ((((c5 * temperature_raw) >> 15) * temperature_raw) >> 19) + c3 + ((c4 * temperature_raw) >> 17); 
    int32_t o = ((((c8 * temperature_raw) >> 15) * temperature_raw) >> 4) + ((c7 * temperature_raw) >> 3) + (c6 * 0x4000);
    return (s * pressure_raw + o) >> 14;
}


/* Python code for reading
c = rl.Cpl["N"]
c1, c2, c3, c4 = c["c"]&0xFFFF, c["d"]&0xFFFF, c["e"]&0xFFFF, c["f"]&0xFFFF
c5, c6, c7, c8 = c["g"], c["h"], c["i"], c["j"]
def GetPressure(temperature_raw, pressure_raw):
    s = ((((c5 * temperature_raw)  >> 15) * temperature_raw) >> 19) + c3 + ((c4 * temperature_raw) >> 17) 
    o = ((((c8 * temperature_raw) >> 15) * temperature_raw) >> 4) + ((c7 * temperature_raw) >> 3) + (c6 * 0x4000);
    return (s * pressure_raw + o) >> 14
cont = [(pl["t"]/60000, (GetPressure(pl["r"], pl["p"]&0xFFFF)-100000)/100+50)  for pl in rl.plines  if pl["C"] == "O"]
sendactivity("contours", contours=[cont], materialnumber=3)
*/
