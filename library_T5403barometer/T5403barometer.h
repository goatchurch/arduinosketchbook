// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef T5403BAROMETER_h
#define T5403BAROMETER_h

// https://www.sparkfun.com/products/12039
class T5403Barometer
{
public:
    int32_t c1, c2, c3, c4, c5, c6, c7, c8;
    int16_t temperature_raw; 
    uint16_t pressure_raw;
    
    long mstamp; 
    uint8_t bstate; // 0 not reading, 1 waiting on temperature, 2 waiting on pressure
    bool valuesready(); 
    
    void T5403SetUp(); // sets those c-values
    void sendCommand(uint8_t location, uint8_t command); 
    int16_t getData(uint8_t location); 

    void RequestPressure(); 
    float GetTemperature();
    int32_t GetPressure();
};

#endif

