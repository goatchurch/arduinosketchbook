#include <Wire.h>
#include "T5403barometer.h"

#define P(X) Serial.print(X)

T5403Barometer barometer;

void setup()
{
    Serial.begin(9600);
    delay(1000); 
    Serial.println("T5403barometer Example!");
    delay(1000); 
    Serial.println("T5403barometer Example!");
    Wire.begin(); 
    barometer.T5403SetUp(); 
}

void loop()
{
    barometer.RequestPressure(); 
    while (!barometer.valuesready())
        ; 
    P("Pressure: "); 
    P(barometer.GetPressure()); 
    P("  temperature: "); 
    P(barometer.GetTemperature()); 
    P("C\n"); 
    delay(50);
}

