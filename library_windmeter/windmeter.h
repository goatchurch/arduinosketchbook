// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef WINDMETER_h
#define WINDMETER_h

#define USE_WINDCHANGEINTERRUPT_FILTER_BOGUS_RISINGS 1
#define WIND_BLADEDISCARDS_PER_REV 6

class WindReceiver
{
public:
    volatile uint32_t revcount; 
    volatile uint32_t revtime;  
    // ~80000/revtime = metres per second
    volatile uint32_t microbladetimeL;  
    volatile uint32_t microbladetimes[5]; 
    volatile int bladecount; // up to 5
#if WIND_BLADEDISCARDS_PER_REV
    volatile int Dprevmicrobladediscards; 
    volatile int microbladediscards; 
#endif

#if USE_WINDCHANGEINTERRUPT_FILTER_BOGUS_RISINGS
    volatile bool bwindpinstate; 
#endif
    void windinterrupt(); 
    
public:
    int windpin; 
    WindReceiver(int lwindpin); 
    void SetWindInterrupts();  
}; 


#endif

