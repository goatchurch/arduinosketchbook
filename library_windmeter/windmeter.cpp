// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include "windmeter.h"

// interrupts can only be attached to global functions, so this dereferences into an object
WindReceiver* pwindreceiver1 = 0; 
void windinterrupt1()  {  pwindreceiver1->windinterrupt();  }

void WindReceiver::windinterrupt() 
{
    #if USE_WINDCHANGEINTERRUPT_FILTER_BOGUS_RISINGS
        bool lbstate = digitalRead(windpin); 
        if (lbstate == bwindpinstate)
            return; 
        bwindpinstate = lbstate; 
        if (!bwindpinstate)
            return; 
    #endif

    uint32_t microbladetime = micros(); 
    microbladetimes[bladecount] = microbladetime - microbladetimeL; 
    microbladetimeL = microbladetime; 
    if (bladecount != 4) {
        bladecount++; 
        return; 
    }
    uint32_t lrevtime = microbladetimes[0] + microbladetimes[1] + microbladetimes[2] + microbladetimes[3] + microbladetimes[4]; 
    
    // filter out any bogus short ones
#if WIND_BLADEDISCARDS_PER_REV
    int idiscardblade; 
    uint32_t lowerbladetime = lrevtime / 8; 
    if (microbladetimes[0] < lowerbladetime)
        idiscardblade = 0; 
    else if (microbladetimes[1] < lowerbladetime)
        idiscardblade = 1; 
    else if (microbladetimes[2] < lowerbladetime)
        idiscardblade = 2; 
    else if (microbladetimes[3] < lowerbladetime)
        idiscardblade = 3; 
    else if (microbladetimes[4] < lowerbladetime)
        idiscardblade = 4; 
    else
        idiscardblade = -1; 
        
    if (microbladediscards == WIND_BLADEDISCARDS_PER_REV)
        idiscardblade = -1; 

    if (idiscardblade != -1) {
        microbladetimes[idiscardblade] = microbladetimes[4];
        microbladediscards++; 
        return; 
    }
    Dprevmicrobladediscards = microbladediscards; 
    microbladediscards = 0; 
#endif
    revtime = lrevtime; 
    revcount++; 
    bladecount = 0; 
}


WindReceiver::WindReceiver(int lwindpin)
{
    windpin = lwindpin; 
    revcount = 0; 
    revtime = 0; 
    bladecount = 0; 
    microbladediscards = 0; 
}

void WindReceiver::SetWindInterrupts()
{
    pwindreceiver1 = this; 
    pinMode(windpin, INPUT);   
#if USE_WINDCHANGEINTERRUPT_FILTER_BOGUS_RISINGS
    attachInterrupt(windpin, windinterrupt1, CHANGE);
#else
    attachInterrupt(windpin, windinterrupt1, RISING);
#endif
}




