#include "Arduino.h"
#include "windmeter.h"

WindReceiver wr(21); 
#define P(X) Serial.print(X)

void setup()
{
    Serial.begin(9600); 
    wr.SetWindInterrupts(); 
    delay(2000); 
    P("beginning\n");     
    delay(2000); 
    P("again\n");     
}

long prevmstamp = 0;  
long prevwindcount = 0;  
long nblankcounts = 0; 
void loop()                     // run over and over again
{
    long mstamp = millis(); 
    long windcount = wr.revcount; 
    if (windcount == prevwindcount) {
         nblankcounts++; 
         if ((nblankcounts % 1000000) == 0) {
             P("blank cycles "); 
             P(nblankcounts); 
             P("\n"); 
         }
         return;
    }
    nblankcounts = 0; 
    prevwindcount = windcount; 
    if (wr.Dprevmicrobladediscards != 0) {
        P(" DISCARDS "); 
        P(wr.Dprevmicrobladediscards); 
        P("\n"); 
    }
    
    if (mstamp / 100 != prevmstamp / 100) {
        P(windcount); 
        P(" "); 
        P(wr.revtime); 
        P("\n"); 
        prevmstamp = mstamp; 
    }
}


