#include <SPI.h>
#include "Adafruit_BLE_UART.h"
#include "adafruitBLE.h"

#define P(X) Serial.print(X)

// note #define ble_system.h PREVENT_BITORDER_LSBFIRST to make it compatible with the non-transactional OLED
AdafruitBLE BTLEserial(5, 6, 22);

void setup(void)
{
    Serial.begin(9600);
    delay(2000); // to get a chance to open the serial console
    Serial.println("Adafruit Bluefruit Low Energy nRF8001 Print echo demo");
    BTLEserial.SetupBLE(); 
}



int n = 0; 
void loop()
{
    char rd = BTLEserial.readBLEintobuffer(); 
    if (rd == 'x') {
        ;
    } else if (rd == 'r') {
        P("gReceived: "); 
        P((char*)BTLEserial.blebuffer); 
        P("\n"); 
    } else if (rd == 'a') {
        P("*START\n"); 
    } else if (rd == 'c') {
        ("*CONNECT\n"); 
    } else if (rd == 'd') {
        P("*DISCONNECT\n"); 
    }

    if (Serial.available()) {
        Serial.setTimeout(100); // 100 millisecond timeout
        String s = Serial.readString();
        BTLEserial.writeBLEstr(s);  
    }
  
    if ((++n % 500000) == 0)
        { Serial.print(n); Serial.println("hi there"); }
}
