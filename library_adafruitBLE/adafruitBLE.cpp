// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include "Adafruit_BLE_UART.h"
#include "adafruitBLE.h"

AdafruitBLE::AdafruitBLE(int8_t req, int8_t rdy, int8_t rst) :
    Adafruit_BLE_UART(req, rdy, rst)
{
    blebuffer = (uint8_t*)malloc(24*sizeof(uint8_t)); 
    blebuffer[21] = 0; 
    aci_evt_opcode_t laststatus = ACI_EVT_DISCONNECTED;
}

void AdafruitBLE::SetupBLE()
{ 
    Adafruit_BLE_UART& BTLEserial = *this; 
    BTLEserial.setDeviceName("B_Fxpt"); 
    P("entering BTLEserial.begin\n"); 
    BTLEserial.begin();
    P("finished BTLEserial.begin\n"); 
}

char AdafruitBLE::readBLEintobuffer()
{
    Adafruit_BLE_UART& BTLEserial = *this; 
    BTLEserial.pollACI();
    aci_evt_opcode_t status = BTLEserial.getState();
    if (status != laststatus) {
        laststatus = status; 
        if (status == ACI_EVT_DEVICE_STARTED) {
            Serial.println("** Advertising started");
            return 's'; 
        } else if (status == ACI_EVT_CONNECTED) {
            Serial.println("* Connected");
            return 'c'; 
        } else if (status == ACI_EVT_DISCONNECTED) {
            Serial.println("* Disconnected or advertising timed out");
            return 'd'; 
        }
        return 'u'; 
    }
    if (status != ACI_EVT_CONNECTED)
        return 'x'; 
        
    // Lets see if there's any data for us!
    if (BTLEserial.available() == 0)
       return 'x'; 
       
    nblebuffer = 0; 
    while ((nblebuffer < 20) && BTLEserial.available())
        blebuffer[nblebuffer++] = (char)BTLEserial.read();
    
    // assume no linefeeds in middle of record and lf is not being used to denote end of record (which would allow for concattenation)
    beollinefeed = ((nblebuffer != 0) && (blebuffer[nblebuffer-1] == '\n'));
    if (beollinefeed) 
        nblebuffer--; 

    blebuffer[nblebuffer] = 0; 
    P("Received from ble: \""); 
    P((char*)blebuffer); 
    P("\"\n"); 
    return 'r'; 
}

void AdafruitBLE::writeBLEstr(String s) 
{
    s.getBytes(blebuffer, 20);
    char sendbuffersize = min(20, s.length()); 
    P("\n* Sending -> \""); 
    P((char *)blebuffer); 
    P("\"\n");
    // write the data
    this->write(blebuffer, sendbuffersize);
}

// general purpose thing here
int formatnumber(char* bbuffer, int nc, int nmax, int32_t num, int frac100)
{
    if ((num < 0) && (nc < nmax)) {
        bbuffer[nc++] = '-'; 
        num = -num; 
    }
    int32_t hdig = 10000; 
    while ((hdig != 1) && (num < hdig)) 
        hdig /= 10; 
    while ((hdig != 0) && (nc < nmax)) {
        bbuffer[nc++] = ((num / hdig) % 10) + '0'; 
        hdig /= 10; 
    }
    if ((frac100 != 0) && (nc < nmax)) {
        bbuffer[nc++] = '.'; 
        if (nc < nmax) {
            bbuffer[nc++] = ((frac100 / 10) % 10) + '0'; 
            frac100 = frac100 % 10; 
            if ((nc < nmax) && (frac100 != 0))
                bbuffer[nc++] = frac100 + '0'; 
        }
    }
    return nc; 
}

void AdafruitBLE::sendBLEnumber(char* str, int32_t num, int32_t frac100) 
{
    int nc = strlen(str); 
    if (nc < 18) {
        strcpy((char*)blebuffer, str); 
    } else {
        blebuffer[0] = '#'; 
        nc = 1; 
    }
    nc = formatnumber((char*)blebuffer, nc, 20, num, frac100); 
    P("\n* Sending -> \""); 
    blebuffer[nc] = '\0'; 
    P((char*)blebuffer); 
    P("\"\n");
    // write the data
    this->write(blebuffer, nc);
}

