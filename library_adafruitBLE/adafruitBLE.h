// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef ADAFRUITBLE_h
#define ADAFRUITBLE_h

#undef P
#define P(X) Serial.print(X)

class AdafruitBLE : public Adafruit_BLE_UART
{
public: 
    uint8_t* blebuffer;  // 24 bytes but 0 terminated string
    uint8_t nblebuffer; 
    bool beollinefeed;   // true if final character in buffer was linefeed (which was trimmed)
    
    aci_evt_opcode_t laststatus; 
    
    AdafruitBLE(int8_t req, int8_t rdy, int8_t rst);
    void SetupBLE(); 
    char readBLEintobuffer(); 
    void writeBLEstr(String s); 
    void sendBLEnumber(char* str, int32_t num, int32_t frac100);  
}; 


#endif
