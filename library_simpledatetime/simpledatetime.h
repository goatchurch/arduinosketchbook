// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef SIMPLEDATETIME_h
#define SIMPLEDATETIME_h

class SimpleDatetime
{
public:
    char* pisotimestamp;  //    strcpy(pisotimestamp, "2010-01-01T16:07:18"); 
    
    SimpleDatetime(); 
    void SetfromEpoch(long t);   // should have coincell to work
    void SetfromCompileTime(const char* cdate, const char* ctime); 
    void SetfromRTC(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, uint8_t b4, uint8_t b5, uint8_t b6); 
    void SetfromBCD(uint8_t byteseconds, uint8_t byteminutes, uint8_t bytehours, uint8_t bytedate, uint8_t bytemonth, uint8_t byteyear); 
    void GetfromBCD(uint8_t* bsectoyears); 
    long GetEpochfromisotimestamp(); 
};

#endif
