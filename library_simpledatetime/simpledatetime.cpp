// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"
#include "simpledatetime.h"

SimpleDatetime::SimpleDatetime() 
{
    pisotimestamp = (char*)malloc(20*sizeof(char)); 
    strcpy(pisotimestamp, "2010-01-01T16:07:18"); 
}

void set2digits(char* p, uint8_t val)
{
    *(p++) = '0'+(val / 10); 
    *p = '0'+(val % 10); 
}

const uint8_t daysInMonth[] = { 31,28,31,30,31,30,31,31,30,31,30,31 };

void SimpleDatetime::SetfromEpoch(long t)
{
    t -= 946684800L;  // SECONDS_FROM_1970_TO_2000 
    set2digits(pisotimestamp + 17, t % 60);
    t /= 60; 
    set2digits(pisotimestamp + 14, t % 60);
    t /= 60; 
    set2digits(pisotimestamp + 11, t % 24);

    uint16_t days = t / 24;
    uint8_t leap;
    int y = 0; 
    while (true) { 
        leap = y % 4 == 0;
        if (days < 365 + leap)
            break;
        days -= 365 + leap;
        y++; 
    }
    set2digits(pisotimestamp + 2, y); 
    int m = 1; 
    while (true) {
        uint8_t daysPerMonth = daysInMonth[m - 1];
        if (leap && m == 2)
            ++daysPerMonth;
        if (days < daysPerMonth)
            break;
        days -= daysPerMonth;
        m++; 
    }
    set2digits(pisotimestamp + 5, m); 
    set2digits(pisotimestamp + 8, days + 1); 
}

/*Wire.requestFrom(DS1307_ADDRESS, 7);
  uint8_t ss = bcd2bin(Wire._I2C_READ() & 0x7F);
  uint8_t mm = bcd2bin(Wire._I2C_READ());
  uint8_t hh = bcd2bin(Wire._I2C_READ());
  Wire._I2C_READ();
  uint8_t d = bcd2bin(Wire._I2C_READ());
  uint8_t m = bcd2bin(Wire._I2C_READ());
  uint16_t y = bcd2bin(Wire._I2C_READ()) + 2000;
*/
void SimpleDatetime::SetfromRTC(uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, uint8_t b4, uint8_t b5, uint8_t b6)
{
    SetfromBCD(b0&0x7F, b1&0x7F, b2&0x3F, b4&0x3F, b5&0x1F, b6); 
}

void SimpleDatetime::SetfromBCD(uint8_t byteseconds, uint8_t byteminutes, uint8_t bytehours, uint8_t bytedate, uint8_t bytemonth, uint8_t byteyear)
{
    pisotimestamp[2] = '0'+(byteyear/16); 
    pisotimestamp[3] = '0'+(byteyear%16); 
    pisotimestamp[5] = '0'+(bytemonth/16); 
    pisotimestamp[6] = '0'+(bytemonth%16); 
    pisotimestamp[8] = '0'+(bytedate/16); 
    pisotimestamp[9] = '0'+(bytedate%16); 
    pisotimestamp[11] = '0'+(bytehours/16); 
    pisotimestamp[12] = '0'+(bytehours%16); 
    pisotimestamp[14] = '0'+(byteminutes/16); 
    pisotimestamp[15] = '0'+(byteminutes%16); 
    pisotimestamp[17] = '0'+(byteseconds/16); 
    pisotimestamp[18] = '0'+(byteseconds%16); 
}

void SimpleDatetime::GetfromBCD(uint8_t* bsectoyears)
{
    bsectoyears[0] = (pisotimestamp[17]-'0')*16 + (pisotimestamp[18]-'0'); 
    bsectoyears[1] = (pisotimestamp[14]-'0')*16 + (pisotimestamp[15]-'0'); 
    bsectoyears[2] = (pisotimestamp[11]-'0')*16 + (pisotimestamp[12]-'0'); 
    bsectoyears[3] = (pisotimestamp[8]-'0')*16 + (pisotimestamp[9]-'0'); 
    bsectoyears[4] = (pisotimestamp[5]-'0')*16 + (pisotimestamp[6]-'0'); 
    bsectoyears[5] = (pisotimestamp[2]-'0')*16 + (pisotimestamp[3]-'0'); 
}


// simpledatetime.SetfromCompileTime(__DATE__, __TIME__); 
void SimpleDatetime::SetfromCompileTime(const char* cdate, const char* ctime) 
{
    // sample input: cdate = "Dec  6 2009", ctime = "12:34:56"
    //const char* cdate = __DATE__; // must be external of picks up datetime of this object file!
    //const char* ctime = __TIME__; 
    
    for (int i = 0; i < 8; i++)
        pisotimestamp[i + 11] = ctime[i]; 
    pisotimestamp[2] = cdate[9]; 
    pisotimestamp[3] = cdate[10]; 
    pisotimestamp[8] = (cdate[4] == ' ' ? '0' : cdate[4]); 
    pisotimestamp[9] = cdate[5]; 
        
    // Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec 
    uint8_t m; 
    switch (cdate[0]) {
        case 'J': m = cdate[1] == 'a' ? 1 : m = cdate[2] == 'n' ? 6 : 7; break;
        case 'F': m = 2; break;
        case 'A': m = cdate[2] == 'r' ? 4 : 8; break;
        case 'M': m = cdate[2] == 'r' ? 3 : 5; break;
        case 'S': m = 9; break;
        case 'O': m = 10; break;
        case 'N': m = 11; break;
        case 'D': m = 12; break;
    }
    set2digits(pisotimestamp + 5, m); 
}

long SimpleDatetime::GetEpochfromisotimestamp()
{
    int y = atoi(pisotimestamp + 2); 
    int m = atoi(pisotimestamp + 5); 
    atoi(pisotimestamp + 8) - 1; 

    long days = atoi(pisotimestamp + 8) - 1;
    for (uint8_t i = 1; i < m; ++i)
        days += daysInMonth[i - 1];
    if (m > 2 && y % 4 == 0)
        ++days;
    days += 365*y + (y + 3)/4;
    return ((days * 24L + atoi(pisotimestamp + 11))*60 + atoi(pisotimestamp + 14))*60 + atoi(pisotimestamp + 17) + 946684800L;
}


