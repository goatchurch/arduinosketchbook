#include "simpledatetime.h"

SimpleDatetime simpledatetime; 

const int ledPin = 13;

#define P(X) Serial.print(X)


long pctime = 0; 
void setup() {
    pinMode(ledPin, OUTPUT);
    Serial.begin(9600); 

    if (Serial.find("T")) {
        pctime = Serial.parseInt();
    }
    
    // set from compile time
    simpledatetime.SetfromCompileTime(); 
    Teensy3Clock.set(simpledatetime.GetEpochfromisotimestamp());  

    digitalWrite(ledPin, HIGH);   // set the LED on
    delay(2000); 
    P("Compile datetime: "); 
    P(__DATE__); 
    P(" ");
    P(__TIME__); 
    P("\n"); 
    digitalWrite(ledPin, LOW);    // set the LED off
}


void loop() {
    long ts = Teensy3Clock.get();  // is there a set function too which we can use from the GPS value?
    simpledatetime.SetfromEpoch(ts); 
    //Serial.println(GetIsotimestamp()); 
    //Serial.println(epochfromisotimestamp()); 
    //P("pctime="); P(pctime); P("\n"); 
 
    P("ts="); 
    P(ts); 
    P("  iso="); 
    P(simpledatetime.pisotimestamp); 
    P("  backts="); 
    P(simpledatetime.GetEpochfromisotimestamp()); 
    P("\n"); 
    
    digitalWrite(ledPin, HIGH);   // set the LED on
    delay(1000);                  // wait for a second
    digitalWrite(ledPin, LOW);    // set the LED off
    delay(1000);                  // wait for a second
}


