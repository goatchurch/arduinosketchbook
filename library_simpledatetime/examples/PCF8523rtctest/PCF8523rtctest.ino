#include <stdio.h>
#include <stdarg.h>
#include <Wire.h>
#include "simpledatetime.h"

SimpleDatetime simpledatetime; 

bool readrtc(SimpleDatetime& sdt) 
{
    Wire.beginTransmission(0x68);
    Wire.write((byte)0x03);  
    uint8_t r = Wire.endTransmission();
    if (r != 0) {
        Serial.print(" endtrans="); 
        Serial.println(r); 
        return false; 
    }
    
    Wire.requestFrom(0x68, 7);
    uint8_t b3 = Wire.read(); 
    uint8_t b4 = Wire.read(); 
    uint8_t b5 = Wire.read(); 
    uint8_t b6 = Wire.read(); 
    uint8_t b7 = Wire.read(); 
    uint8_t b8 = Wire.read(); 
    uint8_t b9 = Wire.read(); 
    if ((b3 & 0x80) == 1)   // clock stopped
        return false; 
    /*Serial.print(" b3="); Serial.print(b3, HEX); 
    Serial.print(" b4="); Serial.print(b4, HEX); 
    Serial.print(" b5="); Serial.print(b5, HEX); 
    Serial.print(" b6="); Serial.print(b6, HEX); 
    Serial.print(" b7="); Serial.print(b7, HEX); 
    Serial.print(" b8="); Serial.print(b8, HEX); 
    Serial.print(" b9="); Serial.print(b9, HEX); Serial.print("  "); */

    sdt.SetfromBCD(b3&0x7F, b4&0x7F, b5&0x3F, b6&0x3F, b8&0x1F, b9); 
    return true; 
}

bool settimefromcompiletime()
{
    simpledatetime.SetfromCompileTime(__DATE__, __TIME__); 
    uint8_t bsectoyears[6]; 
    simpledatetime.GetfromBCD(bsectoyears); 

    Wire.beginTransmission(0x68);
    Wire.write((byte)0x03);  
    Wire.write((byte)bsectoyears[0]); 
    Wire.write((byte)bsectoyears[1]); 
    Wire.write((byte)bsectoyears[2]); 
    Wire.write((byte)bsectoyears[3]); 
    Wire.write((byte)0); 
    Wire.write((byte)bsectoyears[4]); 
    Wire.write((byte)bsectoyears[5]); 
    uint8_t r = Wire.endTransmission();
    return (r == 0); 
}

bool bSetTime = false; 
int secsdelay = 5; 

void setup()
{
    Serial.begin(9600);   
    Serial.println("hi there"); 
    Wire.begin(); 

    if (bSetTime)
        Serial.println(settimefromcompiletime() ? "successful settimefromcompiletime" : "FAIL settimefromcompiletime"); 
    
    for (int i = 0; i < secsdelay; i++) {
        Serial.println(i); 
        delay(1000); 
    }

    Serial.print("CCompiled at: "); 
    simpledatetime.SetfromCompileTime(__DATE__, __TIME__); 
    Serial.print(simpledatetime.pisotimestamp); 
    Serial.print(" read time at "); 
    Serial.print(secsdelay); 
    Serial.print(" secsdelay: "); 
    readrtc(simpledatetime); 
    Serial.println(simpledatetime.pisotimestamp); 
}

void loop()
{
    if (readrtc(simpledatetime))
        Serial.println(simpledatetime.pisotimestamp); 
    else
        Serial.println("read failed"); 
    delay(500); 


}
