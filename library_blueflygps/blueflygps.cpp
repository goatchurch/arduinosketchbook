// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

// http://www.doarama.com/view/439860

#include "Arduino.h"
#include "blueflygps.h"

// defs of commands: http://www.adafruit.com/datasheets/PMTK_A11.pdf
// Defs of data records http://www.adafruit.com/datasheets/GlobalTop-FGPMMOPA6H-Datasheet-V0A.pdf

// should change the settings to use GGA and VTG instead of RMC (once verified that RMC is smoothing)
// then also use the SBAS setting and a 5Hz update for that one option

#undef P
#define P(X)  Serial.print(X) 
#undef P6
#define P6(X)  Serial.print(X, 8) 
#undef PH
#define PH(X)  Serial.print(X, HEX) 

BlueflyGpsData::BlueflyGpsData(HardwareSerial* lserial, int lgpsfixinterval, bool lbaregps, bool lbloud) : 
    serial(lserial), gpsfixinterval(lgpsfixinterval), baregps(lbaregps), bloud(lbloud)
{
    currentstate = 0; 
    BRB = 0; 
    Nprsreading = 0; 
    Ngpsreading = 0; 
    devno = (baregps ? 0 : 1); 

    nrec = 0; 
    nrecblock = -1; 
    recchecksum = 0; 
    linenumber = 0; 
    npoststarblocks = 0; 
    mstampchangestate = 0; 
    
    isotimestamp = (char*)malloc(32*sizeof(char)); 
    strcpy(isotimestamp, "2099-99-99T99:99:99.999"); // isotimestamp[2] == '9' means it's invalid and not been set by a GPRMC record
    mstampmidnight = 0; 

    latminutes10000 = 0;
    lngminutes10000 = 0; 
    velkph100 = 0;
    veldegrees100 = 0;
    altitude10 = 0; 
}

long atohex(char* c) 
{
    long res = 0; 
    while (*c != 0) {
        res = res*0x10; 
        if (*c <= '9')
            res += (*c - '0'); 
        else if (*c <= 'F')
            res += (*c - 'A' + 10); 
        else
            res += (*c - 'a' + 10); // handle possibility of lower case hex values
        c++; 
    }
    return res; 
}

long atoint(char* c) 
{
    long res = 0; 
    while (*c != 0) {
        res = res*10 + (*c - '0'); 
        c++; 
    }
    return res; 
}

void BlueflyGpsData::serialwritegps(const char* pcomm) 
{
    serial->print("$"); 
    if (pcomm[0] == '$') 
        pcomm++; 
    char checksum = 0; 
    while ((*pcomm != 0) && (*pcomm != '*')) {
        checksum ^= *(pcomm);
        serial->print(*pcomm); 
        pcomm++; 
    } 
    serial->print("*"); 
    serial->print(checksum, HEX); 
    serial->print("\r\n"); 
}


//int32_t Dprsreadingprev = 0; 

// returns 0: nothing
//         1: GPS record   11: GPS record bad checksum
//         2: prsreading 
//         3: BST settings
uint8_t BlueflyGpsData::ParseFetchAvailableLine() 
{
    // we use the \r\n end of lines to our advantage, the first character to process, the second to clear
    int nreads = 0; 
    int navailable = serial->available(); 
    
    //while (serial->available()) {
    for (int a = 0; a < navailable; a++) {
        nreads++; 
        if (nreads == 200) {
            P("too many gps byte reads in one fetch\n"); 
            break; 
        }
        
        char c = serial->read(); 
        //Serial.write(c);  // spew out to debug
        
        // start of GPS record case
        if ((c == '$') || (nrec == 0)) {
            mstampdollar = millis(); // (could put millis() call before avaliable() called, but keep it minimal)
            recline[0] = c; 
            recchecksum = 0; 
            nrec = 1; 
            recblock[0] = 0; 
            nrecblock = 1; 
            linenumber++; 
            npoststarblocks = 0; 
            continue; 
        }
        // initiating start of record (PRS records don't begin with a $)
        if (c == '\n') {
            nrec = 0; 
            nrecblock = 0; 
            recline[0] = '\0'; 
            npoststarblocks = 0; 
            continue; 
        }
        
        // end of word in record
        if ((recline[0] == '$') ? ((c == ',') || (c == '*') || (c == '\r')) : ((c == ' ') || (c == '\r'))) {
            recline[nrec] = '\0'; 
            recblock[nrecblock] = nrec + 1; 
            if (nrecblock < RECBLOCKLENGTH-2)
                nrecblock++; 
            else {
                P("overflow gps recblock\n"); 
                nrec = 0; 
                nrecblock = 0; 
                return 0; 
            }
        } else {
            recline[nrec] = c; 
        }
        nrec++; 
        
        if (c == '*')
            npoststarblocks++; 
        else if (npoststarblocks == 0)
            recchecksum ^= c;   // have "continue"d on the $ symbol
        
        // end of record case
        if (c == '\r') {
            nrecblock--; 
            if ((nrecblock == 2) && (strcmp(recline + recblock[0], "PRS") == 0)) {  // PRS XXXXX
                prsreading = atohex(recline + recblock[1]); 
                //if (abs(prsreading - Dprsreadingprev) > 300) { P("bad prs parse: "); P(recline); P("  "); P(recline + recblock[1]); P("\n"); }            
                //Dprsreadingprev = prsreading; 
    
                return 2; 
            }
            if ((nrecblock == 30) && (strcmp(recline + recblock[0], "SET") == 0)) {
                //     BST BFK BFL BFP BAC BAD BTH BFQ  BFI BSQ BSI BFS BOL BOS BRM BVL  BOM BOF BQH   BRB BPT BUR BLD BR2 BHV BHT BBZ BZT BSM
                // SET 0   100 20  1   0   1   180 1000 100 400 100 40  5   30  100 1000 0   1   21325 207 1   0   1   34  20  600 0   40  100
                //     0   1   2   3   4   5   6   7    8   9   10  11  12  13  14  15   16  17  18    19  20  21  22  23  24  25  26  27  28
                BOF = atoint(recline + recblock[18]); 
                BRB = atoint(recline + recblock[20]); 
                P("BRB "); 
                P(BRB); 
                P("  *****\n"); 
                return 3; 
            }
            if ((recline[0] == '$') && (npoststarblocks == 1)) {
                nrecblock--; // remove checksum block
                uint8_t hchecksum = atohex(recline + recblock[nrecblock]); 
                if (recchecksum == hchecksum)
                    return 1;  // good checksum
                else {
                    /*P("chkk<"); 
                    PH(hchecksum); 
                    P(","); 
                    PH(recchecksum);
                    P(","); 
                    P(recline + recblock[nrecblock]); 
                    P(">"); */
                    return 11; // bad checksum
                }
            }
            return 0; 
        }
    }
    return 0; 
}


// initialization states and running states [this is a state machine]
// currentstate
//   0      : startup
//   1      : baregps startup
//   10-12  : cycle round to establish baudrate to GPS 
//   20     : baudrate to GPS is now 57600
//   21-23  : set the GPS rate and record types
//   30     : GPS good, now set baro
//   40     : all is go unless a record missing for 4 seconds
// returns  0: nothing  1:Gprmc  3:Gpgga  30:Gpgga withtransform  2:Gpvtg  4:Gpgsa  10:Pressure  13:Gpgga no lock -- and +100 for bad checksum
uint8_t BlueflyGpsData::FetchBlueflyData(long mstamp, EastingNorthingLocalLinearTrans* penllt)
{
    if (currentstate == 0)
        serial->begin(baregps ? 9600 : 57600);  // can't do this in the constructor
        
    uint8_t res = ParseFetchAvailableLine(); 
    uint8_t prevstate = currentstate; 
    uint8_t fres = 0; 

    // GPS type record returned
    /*if ((res == 1) || (res == 11)) {
        if (res != 1) {
            P("BADGPSCHECKSUM "); 
            P(res); 
            P("chk<"); 
            P(recline + recblock[nrecblock]); 
            P(","); 
            PH(recchecksum);
            P(">"); 
        } else {
            P("goodGPS "); 
        }
        for (int i = 0; i < nrecblock; i++) {
            P(recline + recblock[i]); 
            P(";"); 
        }
        P("\n"); 
        
    // pressure type reading
    } else if (res == 2) {
        
    // bluefly settings reading
    } else if (res == 3) {  
        P("GPS baud receiver set at "); 
        P(BRB); 
        P("\n"); 
    }*/

    
    // initial
    if ((res == 0) && ((currentstate == 0) || (currentstate == 10)) && (mstamp > mstampchangestate)) {
        serial->begin(baregps ? 9600 : 57600);  // can't do this in the constructor
        if (!baregps) {
            BRB = 0; 
            serial->write("$BST*\r\n"); // causes a reading of BST settings which sets the value of BRB
        } else if (BRB == 0) {
            BRB = 207;  // fake the bluefly signal that we are reading gps at 9600
        }
        mstampchangestate = mstamp + 4000; 
        if (currentstate == 10) {
            P("bluefly no response\n"); 
        }
        currentstate = 10; 
    } else if (((res == 3) || baregps) && (currentstate == 10)) {  // drop straight through for baregps case
        serialwritegps("$PMTK220,1000*"); // to check if GPS functioning
        mstampchangestate = mstamp + 3000; 
        currentstate = 11;     
    } else if ((res == 1) && (currentstate == 11)) {
        if (strcmp(recline + recblock[0], "$PMTK001") == 0) {
            if (BRB == 34) {
                currentstate = 20; // communication made with gps and at the right baud rate
                P("*GPS working at baud rate 57600\n"); 
                mstampchangestate = mstamp + 4000; 
            } else {
                serialwritegps("$PMTK251,57600*"); 
                currentstate = 12; 
                mstampchangestate = mstamp + 2000; 
            }
        } 
    
    // swap baud rate we read the GPS at
    } else if ((currentstate == 11) && (mstamp > mstampchangestate)) {
        if (!baregps) {
            if (BRB == 34) 
                serial->write("$BRB 207*\r\n"); 
            else 
                serial->write("$BRB 34*\r\n"); 
        } else {
            BRB = (BRB == 207 ? 34 : 207); // fake the change in the switches in fake bluefly data
            serial->begin(BRB == 207 ? 9600 : 57600); 
        }
        mstampchangestate = mstamp + 3000; 
        currentstate = 10; 

    } else if ((currentstate == 12) && (mstamp > mstampchangestate)) {
        if (!baregps) {
            serial->write("$BRB 34*\r\n"); 
        } else {
            serial->begin(57600); 
            BRB = 34; 
        }
        currentstate = 10; 
        mstampchangestate = mstamp + 1000; 
        
    // GPS reading at right baud rate
    } else if ((currentstate == 20) && (mstamp > mstampchangestate)) {
        P("Setting GPS records "); 
        P(BRB); 
        P("\n"); 
        serialwritegps("$PMTK314,0,5,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0*");  // GPGLL=0,GPRMC=5,GPVTG=1,GPGGA=1,GPGSA=0,GPGSV=0
        mstampchangestate = mstamp + 5000; 
        currentstate = 21; 
    } else if ((res == 1) && (currentstate == 21) && (strcmp(recline + recblock[0], "$PMTK001") == 0) && (strcmp(recline + recblock[1], "314") == 0) && (recline[recblock[2]] == '3')) {
        currentstate = 22; 
        mstampchangestate = mstamp + 1000; 
    } else if ((currentstate == 21) && (mstamp > mstampchangestate)) {
        P("PMTK314 failed falling back\n"); 
        currentstate = 10; 
    } else if ((currentstate == 22) && (mstamp > mstampchangestate)) {
        if (gpsfixinterval == 2000) {
            serialwritegps("$PMTK220,2000*"); 
        } else if (gpsfixinterval == 1000) {
            serialwritegps("$PMTK220,1000*"); 
        } else if (gpsfixinterval == 500) {
            serialwritegps("$PMTK220,500*"); 
        } else if (gpsfixinterval == 250) {
            serialwritegps("$PMTK220,250*"); 
        } else if (gpsfixinterval == 200) {
            serialwritegps("$PMTK220,200*"); 
        } else {
            serialwritegps("$PMTK220,100*");   // default fastest rate of 10Hz
        }
        mstampchangestate = mstamp + 5000; 
        currentstate = 23; 
        
    // GPS now providing right data
    } else if ((res == 1) && (currentstate == 23) && (strcmp(recline + recblock[0], "$PMTK001") == 0) && (strcmp(recline + recblock[1], "220") == 0) && (recline[recblock[2]] == '3')) {
        currentstate = 30; 
        mstampchangestate = mstamp + 1000; 
    } else if ((currentstate == 23) && (mstamp > mstampchangestate)) {
        P("PMTK220 failed falling back\n"); 
        currentstate = 10; 
    } else if ((currentstate == 30) && (mstamp > mstampchangestate)) {
        mstampchangestate = mstamp + 500; 
        if (!baregps) {
            serial->write("$BOF 1*\r\n"); 
            currentstate = 31; 
        } else {
            currentstate = 40; // skip past all the bluefly setup commands
        }
    } else if ((currentstate == 31) && (mstamp > mstampchangestate)) {
        serial->write("$BHV 0*\r\n");  // height timeout (default 20)
        currentstate = 32; 
        mstampchangestate = mstamp + 500; 
    } else if ((currentstate == 32) && (mstamp > mstampchangestate)) {
        serial->write("$BHT 10000*\r\n");  // seconds timeout (default 600)
        currentstate = 33; 
        mstampchangestate = mstamp + 500; 
    } else if ((currentstate == 33) && (mstamp > mstampchangestate)) {
        if (bloud)
            serial->write("$BVL 1000*\r\n");  // volume; 1000 is max
        else
            serial->write("$BVL 10*\r\n");    // volume; 1000 is max
        P("Volume=");
        P(bloud ? "loud\n" : "quiet\n"); 
        currentstate = 35; 
        mstampchangestate = mstamp + 500; 
    } else if ((currentstate == 35) && (mstamp > mstampchangestate)) {
        serial->write("$BST*\r\n"); 
        mstampchangestate = mstamp + 4000; 
        currentstate = 36; 
    } else if ((res == 3) && (currentstate == 36)) {
        P("Set the BOF speed "); 
        P(BOF); 
        P("\n"); 
        currentstate = 40; // all is go
        mstampchangestate = mstamp + 1000; 
    } else if ((currentstate == 35) && (mstamp > mstampchangestate)) {
        P("$BOF failed falling back\n"); 
        currentstate = 20; 
        mstampchangestate = mstamp + 1000; 
        
    // now streaming the records
    } else if ((currentstate == 40) && ((res == 1) || (res == 11))) {
        mstampchangestate = mstamp + 4000; 
        if (IsGprmc()) {
            if (ParseGprmc()) {
                fres = 1; 
            }
        } else if (IsGpgga()) {
            Ngpsreading++; 
            if (ParseGpgga()) {
                fres = 3; 
                if (penllt != 0) {
                    if (!penllt->IsValid() && (isotimestamp[2] != '9')) {
                        penllt->SetLinearTransOrigin(latminutes10000, lngminutes10000); 
                        fres = 30; 
                    }
                    ApplyLocalEuclideanTransform(*penllt);  
                }
            } else {
                fres = 13; 
            }
        } else if (IsGpvtg()) {
            if (ParseGpvtg()) 
                fres = 2; 
        } else if (IsGpgsa()) {
            if (ParseGpgsa()) 
                fres = 4; 
        }
        if (res == 11)
            fres += 100; 
        mstampchangestate = mstamp + 4000; 
        
    } else if ((res == 2) && (currentstate == 40)) {  // barometric pressure reading
        Nprsreading++; 
        fres = 10; 
        mstampchangestate = mstamp + 4000; 
    } else if ((currentstate == 40) && (mstamp > mstampchangestate)) {
        P("Missing data, falling back\n");
        currentstate = 20;  
    } else if (mstamp > mstampchangestate) {
        P("BAD unknown state dropoff "); 
        P(currentstate); 
        P("\n"); 
        mstampchangestate = mstamp + 5000; 
    }
    
    if (prevstate != currentstate) 
    {
        P("   bfgpsstate: "); 
        P(currentstate); 
        P("\n"); 
    }
    return fres; 
} 



int32_t minutes10000parse(char* c, bool b3digit) 
{
    int32_t degrees = (b3digit ? (*(c++) - '0') * 100 : 0); 
    degrees += (c[0] - '0')*10 + (c[1] - '0'); 
    int32_t minutes = (c[2] - '0')*10 + (c[3] - '0'); 
    int32_t minutesfraction =((c[5] - '0')*1000 + (c[6] - '0')*100 + (c[7] - '0')*10 + (c[8] - '0')); 
    return minutesfraction + 10000*(minutes + 60*degrees); 
}

int32_t float100parse(char* c)
{
    int32_t res = 0; 
    while (('0' <= *c) && (*c <= '9'))
        res = res*10 + (*(c++) - '0'); 
    if ((c[0] == '.') && (('0' <= c[1]) && (c[1] <= '9')) && (('0' <= c[2]) && (c[2] <= '9')))
        return res*100 + (c[1] - '0')*10 + (c[2] - '0'); 
    return -1; 
}

bool BlueflyGpsData::SetIsoTimestampFromGps(char* c1, char* c9)
{
    if (c9 != 0) {
        isotimestamp[8] = c9[0]; 
        isotimestamp[9] = c9[1]; 
        isotimestamp[5] = c9[2]; 
        isotimestamp[6] = c9[3]; 
        isotimestamp[2] = c9[4]; 
        isotimestamp[3] = c9[5]; 
    } else if ((c1[0] == '0') && (isotimestamp[11] != '0')) {
        return false;  // cross midnight but no date field set case
    }

    isotimestamp[11] = c1[0]; 
    isotimestamp[12] = c1[1]; 
    isotimestamp[14] = c1[2]; 
    isotimestamp[15] = c1[3]; 
    isotimestamp[17] = c1[4]; 
    isotimestamp[18] = c1[5]; 
    isotimestamp[20] = c1[7]; 
    isotimestamp[21] = c1[8]; 
    isotimestamp[22] = c1[9]; 
    isotimestamp[23] = '\0'; 
    
    mstampmidnight = (c1[0]-'0')*(10*60*60*1000) + (c1[1]-'0')*(60*60*1000) + (c1[2]-'0')*(10*60*1000) + (c1[3]-'0')*(60*1000) + (c1[4]-'0')*(10*1000) + (c1[5]-'0')*(1000) + (c1[7]-'0')*100+(c1[8]-'0')*10+(c1[9]-'0'); 
    
    return true; 
}

int32_t intparse(char* c)
{
    int32_t res = 0; 
    while (('0' <= *c) && (*c <= '9'))
        res = res*10 + (*(c++) - '0'); 
    return res; 
}

int32_t float10parse(char* c, int fac10)
{
    int32_t res = 0; 
    while (('0' <= *c) && (*c <= '9'))
        res = res*10 + (*(c++) - '0'); 
    if (*(c++) != '.')
        return -1; 
    res = res*fac10; // either 10 or 100
    if ((fac10 == 100) && ('0' <= *c) && (*c <= '9')) 
        res += (*(c++) - '0') * 10; 
    if (('0' <= *c) && (*c <= '9')) 
        res += (*c - '0'); 
    return res; 
}


// GPRMC = Recommended Minimum Navigation Information
bool BlueflyGpsData::IsGprmc() { return ((strcmp(recline, "$GPRMC") == 0) && (nrecblock == 13)); }
bool BlueflyGpsData::ParseGprmc()
{
    // $GPRMC,164742.682,A,5324.1915,N,00259.0953,W,0.36,36.82,190115,,,A*48
    if (recline[recblock[2]] != 'A') 
        return false; 
    SetIsoTimestampFromGps(recline + recblock[1], recline + recblock[9]);
    latminutes10000 = minutes10000parse(recline + recblock[3], false)*(recline[recblock[4]] == 'N' ? 1 : -1); 
    lngminutes10000 = minutes10000parse(recline + recblock[5], true)*(recline[recblock[6]] == 'E' ? 1 : -1); 
    //velknots100 = float100parse(recline + recblock[7]); 
    veldegrees100 = float100parse(recline + recblock[8]); 
    //navmode = recline[recblock[11]]; 
    return true; 
}

bool BlueflyGpsData::IsGpgga() { return ((strcmp(recline, "$GPGGA") == 0) && (nrecblock == 15)); }
bool BlueflyGpsData::ParseGpgga()
{
    // $GPGGA,064306.590,5323.7234,N,00258.3325,W,  2, 10, 1.00,38.3,M,49.4,M,0000,0000*4A
    positionfixindicator = recline[recblock[6]] - '0'; // is this valid case?
    if (positionfixindicator == 0)
        return false; 

    SetIsoTimestampFromGps(recline + recblock[1], 0);   // no date included
    latminutes10000 = minutes10000parse(recline + recblock[2], false)*(recline[recblock[3]] == 'N' ? 1 : -1); 
    lngminutes10000 = minutes10000parse(recline + recblock[4], true)*(recline[recblock[5]] == 'E' ? 1 : -1); 
    //nsatellitesused = intparse(recline + recblock[7]); 
    horizontaldilutionofposition100 = float10parse(recline + recblock[8], 100); 
    altitude10 = float10parse(recline + recblock[9], 10); 
    //assert strcmp(recline + recblock[10], "M"); 
    //geoidalseparation10 = float10parse(recline + recblock[11], 10); 
    //assert strcmp(recline + recblock[12], "M"); 
    // age of diff. corr. recblock[13], recblock[14]  // is this time in seconds? 
    return true; 
}

bool BlueflyGpsData::IsGpvtg() { return ((strcmp(recline, "$GPVTG") == 0) && (nrecblock == 10)); }
bool BlueflyGpsData::ParseGpvtg()
{
    // $GPVTG,62.47,T,,M,0.29,N,0.53,K,D*02
    if (recline[recblock[9]] == 'N')  // undocumented but observed
        return false; 
    veldegrees100 = float100parse(recline + recblock[1]); 
    //assert strcmp(recline + recblock[2], "T"); 
    //assert strcmp(recline + recblock[4], "M"); 
    //velknots100 = float100parse(recline + recblock[5]); 
    //assert strcmp(recline + recblock[6], "N"); 
    velkph100 = float100parse(recline + recblock[7]); 
    //assert strcmp(recline + recblock[8], "K"); 
    //recline[recblock[9]] = 'A' autonomous, 'D' differential, 'E' estimated
    return true; 
}


bool BlueflyGpsData::IsGpgsa() { return ((strcmp(recline, "$GPGSA") == 0) && (nrecblock == 18)); }
bool BlueflyGpsData::ParseGpgsa()
{
    if (recline[recblock[2]] == '1')
        return false; // no fix
    // $GPGSA,A,3,29,31,16,25,,,,,,,,,2.82,2.66,0.92*0A
    //mode2dimension = recline[recblock[2]] - '0'; 
    positiondilutionofposition100 = float100parse(recline + recblock[15]); 
    horizontaldilutionofposition100 = float100parse(recline + recblock[16]); 
    verticaldilutionofposition100 = float100parse(recline + recblock[17]); 
    return true; 
}


// http://www.ordnancesurvey.co.uk/business-and-government/help-and-support/navigation-technology/os-net/coordinate-spreadsheet.html
EastingNorthing::EastingNorthing(double deglat, double deglng, int countrycode)
{
  if (countrycode == 0) {
    // inputs
    double PHI = deglat * 0.017453292519943295;
    double LAM = deglng * 0.017453292519943295;

    // Constants (sheet 2)
    double a = 6377563.396; 
    double b = 6356256.909; 

// note that these are for WGS84?
a = 6378137.0000; 
b = 6356752.3142; 

    double F0 = 0.9996012717; // NatGrid scale factor on central meridian
    double E0 = 400000; // northing & easting of true origin, metres
    double N0 = -100000; 
    double aFo = a*F0; 
    double bFo = b*F0; 
    double PHI0 = 49 * 0.017453292519943295;
    double LAM0 = -2 * 0.017453292519943295; 

    // sheet 7 calculations
    double aFo_squared = aFo*aFo; 
    double e_squared = (aFo_squared - bFo*bFo) / aFo_squared; 
    double n = (aFo - bFo) / (aFo + bFo); 
    double sinPHI = sin(PHI); 
    double rho_denomenator = 1 - (e_squared * sinPHI * sinPHI); 
    double nu = aFo / sqrt(rho_denomenator); 
    double rho = (nu * (1 - e_squared)) / rho_denomenator; 
    double eta2 = (nu / rho) - 1; 
    double xP = LAM - LAM0; 

    // Marc(bFo,n,PHI0,PHI), function in spreadsheet macro.  (Note floats for 5/4 int ratios)
    double n2 = n*n; 
    double n3 = n2*n; 
    double Ma = (1 + n + (5.0 / 4) * n2 + (5.0 / 4) * n3) * (PHI - PHI0);
    double Mb = (3 * n + 3 * n * n + (21.0 / 8) * n3) * sin(PHI - PHI0) * cos(PHI + PHI0);
    double Mc = ((15.0 / 8) * n2 + (15.0 / 8) * n3) * sin(2 * (PHI - PHI0)) * cos(2 * (PHI + PHI0));
    double Md = (35.0 / 24) * n3 * sin(3 * (PHI - PHI0)) * cos(3 * (PHI + PHI0));
    double M = bFo * (Ma - Mb + Mc - Md); // meridional arc

    double I = M + N0; 
    double cosPHI = cos(PHI); 
    double II = (nu/2) * sinPHI * cosPHI; 
    double tanPHI = tan(PHI); 
    double cosPHI_squared = cosPHI*cosPHI; 
    double cosPHI_cubed = cosPHI_squared*cosPHI; 
    double tanPHI_squared = tanPHI*tanPHI; 
    double III = ((nu/24)*sinPHI*cosPHI_cubed) * (5 - (tanPHI_squared) + (9*eta2)); 
    double cosPHI_quint = cosPHI_cubed * cosPHI_squared;
    double tanPHI_quart = tanPHI_squared*tanPHI_squared; 
    double IIIA = ((nu/720)*sinPHI)*cosPHI_quint * (61 - (58 * tanPHI_squared) + tanPHI_quart); 
    double IV = nu * cosPHI; 
    double V = (nu/6) * cosPHI_cubed*((nu/rho) - tanPHI_squared); 
    double VI = (nu/120) * cosPHI_quint * (5 - (18 * tanPHI_squared) + tanPHI_quart + (14*eta2) - (58 * tanPHI_squared * eta2)); 

    /*P("I "); P(I); P("\n"); 
    P("II "); P(II); P("\n"); 
    P("III "); P(III); P("\n"); 
    P("IIIA "); P(IIIA); P("\n"); 
    P("IV "); P(IV); P("\n"); 
    P("V "); P(V); P("\n"); 
    P("VI "); P(VI); P("\n"); 
    P("\n"); */
    
    double P_squared = xP*xP; 
    double P_cubed = P_squared*xP; 
    oseasting = E0 + xP*IV + P_cubed*V + P_cubed*P_squared*VI; 
    osnorthing = I + P_squared*II + P_squared*P_squared*III + P_cubed*P_cubed*IIIA; 
    
  } else {
    oseasting = deglng*36000; 
    osnorthing = deglat*36000; 
  }
  
    P("Easting northing conversion: lat: "); 
    P6(deglat); P(", lng: "); P6(deglng); 
    P(" to EN "); 
    P(oseasting); P(", "); P(osnorthing); 
    P("\n"); 
}

EastingNorthingLocalLinearTrans::EastingNorthingLocalLinearTrans()
{
    latdivisorE100 = 0; 
}

bool EastingNorthingLocalLinearTrans::IsValid() 
{
    return (latdivisorE100 != 0); 
}

void EastingNorthingLocalLinearTrans::SetLinearTransOrigin(int32_t lOffsetlatminutes10000, int32_t lOffsetlngminutes10000) 
{
    P("latminutes: "); 
    P(lOffsetlatminutes10000); 
    P("\n"); 
    P("lngminutes: "); 
    P(lOffsetlngminutes10000); 
    P("\n"); 
    
    Offsetlatminutes10000 = lOffsetlatminutes10000; 
    Offsetlngminutes10000 = lOffsetlngminutes10000; 
    
    double d600000 = 600000.0; 
    double deglat = Offsetlatminutes10000/d600000; 
    double deglng = Offsetlngminutes10000/d600000; 
    int countrycode = 1; 
    if ((deglat > 40) && (deglat < 70) && (deglng > -9) && (deglng < 9))
        countrycode = 0; 
    EastingNorthing en00(deglat, deglng, countrycode); 
    EastingNorthing en10(deglat + 0.001, deglng, countrycode); 
    EastingNorthing en01(deglat, deglng + 0.001, countrycode); 

    int64_t divisorCM = 100; 
    latdivisorE100 = (en10.oseasting != en00.oseasting ? divisorCM*600/(en10.oseasting - en00.oseasting) : 999999); // 600 is the factor of 0.001*600000
    latdivisorN100 = (en10.osnorthing != en00.osnorthing ? divisorCM*600/(en10.osnorthing - en00.osnorthing) : 999999); 
    lngdivisorE100 = (en01.oseasting != en00.oseasting ? divisorCM*600/(en01.oseasting - en00.oseasting) : 999999); 
    lngdivisorN100 = (en01.osnorthing != en00.osnorthing ? divisorCM*600/(en01.osnorthing - en00.osnorthing) : 999999); 
    if (latdivisorE100 == 0)  latdivisorE100 = 1; 
    if (latdivisorN100 == 0)  latdivisorN100 = 1; 
    if (lngdivisorE100 == 0)  lngdivisorE100 = 1; 
    if (lngdivisorN100 == 0)  lngdivisorN100 = 1; 
    
    double radfac = 45/atan(1); 
    double angLat = (radfac * atan2(en10.oseasting - en00.oseasting, en10.osnorthing - en00.osnorthing)); 
    //double angLng = (radfac * atan2(en01.oseasting - en00.oseasting, en01.osnorthing - en00.osnorthing)); 
    //P("radfac "); P(radfac); P("\n"); 
    P("angLat100 "); P(angLat100); P("\n"); 
    //P("angLng100 "); P(angLng100); P("\n"); 
    angLat100 = (int)(angLat * 100); 
    
    P("latdivisorE "); P(latdivisorE100);  P("  latdivisorN "); P(latdivisorN100); P("\n"); 
    P("lngdivisorE "); P(lngdivisorE100);  P("  lngdivisorN "); P(lngdivisorN100); P("\n"); 
}


void BlueflyGpsData::ApplyLocalEuclideanTransform(EastingNorthingLocalLinearTrans& enllt) 
{
    int32_t rlatminutes10000 = latminutes10000 - enllt.Offsetlatminutes10000; 
    int32_t rlngminutes10000 = lngminutes10000 - enllt.Offsetlngminutes10000; 
    relE10 = (1000*rlatminutes10000) / enllt.latdivisorE100 + (1000*rlngminutes10000) / enllt.lngdivisorE100; 
    relN10 = (1000*rlatminutes10000) / enllt.latdivisorN100 + (1000*rlngminutes10000) / enllt.lngdivisorN100; 
}


