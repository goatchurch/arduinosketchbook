#include "Arduino.h"
#include "blueflygps.h"

#define P(X)  Serial.print(X) 

// get modes programmed in with failures to parse

// This same library is used to run the GPS unit as to run the bluefly
// with the embedded GPS in it


BlueflyGpsData gpsdata(&Serial3, 200, false, true);  // bluefly lower level
//BlueflyGpsData gpsdata(&Serial2, 500, true); // non-bluefly direct 

EastingNorthingLocalLinearTrans enllt; 

void setup() 
{
    Serial.begin(9600); 
}


int i = 0; 
void loop() 
{
    if (Serial.available()) {
        Serial.print("ZZZ:"); 
        bool bctrlRfound = false; 
        while (Serial.available()) {
            char ch = Serial.read(); 
            Serial.write(ch);
            if (ch == '\r')
                bctrlRfound = true; 
            if ((ch == '\n') && !bctrlRfound)
                gpsdata.serial->write('\r');
            gpsdata.serial->write(ch);
        }
    }

    // while (Serial1.available())  P((char)Serial1.read());  return; 
    uint8_t igps = gpsdata.FetchBlueflyData(millis(), &enllt); 
    if (igps == 10) {
        if ((gpsdata.Nprsreading % 150) == 0) {
            P("Prs "); 
            P(gpsdata.Nprsreading); 
            P("  "); 
            P(gpsdata.prsreading); 
            P("\n"); 
        }
    } else if ((igps == 3) || (igps == 30)) {
        if ((gpsdata.Ngpsreading % 20) == 0) {
            P("gps "); 
            P(gpsdata.Ngpsreading); 
            P(": "); 
            if (enllt.IsValid()) {
                P("gps "); 
                P(gpsdata.isotimestamp); 
                P(" E:"); 
                P(gpsdata.relE10/10); 
                P(" N:"); 
                P(gpsdata.relN10/10); 
                P(" alt:"); 
                P(gpsdata.altitude10/10); 
                P("\n"); 
            } else {
                P("still bad"); 
            }
        }
    } else if (igps == 13) {
        if ((gpsdata.Ngpsreading % 20) == 0) {
            P("gps not locked\n"); 
        }
    }
}


// $BRB 34*  baudrate of GPS to 57600; 207 for 9600
// $PMTK251,9600*17
// $PMTK251,57600*2C
// $PMTK220,1000*1F
// $PMTK220,500*2B
// $PMTK220,100*2F

// TMP 187
// PRS 190F9
// BFV 10 208
//     BST BFK BFL BFP BAC BAD BTH BFQ  BFI BSQ BSI BFS BOL BOS BRM BVL  BOM BOF BQH   BRB BPT BUR BLD BR2 BHV BHT BBZ BZT BSM
// SET 0   100 20  1   0   1   180 1000 100 400 100 40  5   30  100 1000 0   1   21325 207 1   0   1   34  20  600 0   40  100
//     0   1   2   3   4   5   6   7    8   9   10  11  12  13  14  15   16  17  18    19  20  21  22  23  24  25  26  27  28

// MS5611 2136 56367 59487 34796 32667 32088 27301 49135 
// $GPGGA,001046.799,,,,,0,0,,,M,,M,,*4C
// $GPGSA,A,1,,,,,,,,,,,,,,,*1E
// $GPRMC,001046.799,V,,,,,0.00,0.00,060180,,,N*46
// $GPVTG,0.00,T,,M,0.00,N,0.00,K,N*32

// $BOF 50*  once a second; or 1 back to default

