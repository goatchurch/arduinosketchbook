// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef BLUEFLYGPS_h
#define BLUEFLYGPS_h

// currently hardcoded to Serial1 (should be in the constructor)

// for Julians home at cx = -1783521, cy = 32037238
// lat: 53.3953966, lng: -2.9725350 
// easting northing: EN 335331.45, 389242.07
// with:  latdivisorE 39577  latdivisorN 539
//        lngdivisorE 902  lngdivisorN -66249
// where relative position in metres is given by:
//  eastmetres = ry/latdivisorE100 + rx/lngdivisorE100
//  northmetres = ry/latdivisorN100 + rx/lngdivisorN100
// NB these are divisors, so small numbers => big influence.  
// don't be confused by big changes in latdivisorE while lngdivisorE remains constant

struct EastingNorthingLocalLinearTrans
{
    // current linear transform to metres setting
    int32_t Offsetlatminutes10000; 
    int32_t Offsetlngminutes10000; 
    int32_t latdivisorE100; 
    int32_t latdivisorN100; // should do an atan2 here to work out the velocity angular deviation
    int32_t lngdivisorE100; 
    int32_t lngdivisorN100; 
    int32_t angLat100; 
    
    EastingNorthingLocalLinearTrans(); 
    bool IsValid(); 
    void SetLinearTransOrigin(int32_t lOffsetlatminutes10000, int32_t lOffsetlngminutes10000); 
};

struct EastingNorthing
{
    double oseasting; 
    double osnorthing; 
    EastingNorthing(double deglat, double deglng, int countrycode); 
};


#define RECLINELENGTH 180
#define RECBLOCKLENGTH 40

class BlueflyGpsData
{
public:
    HardwareSerial* serial; 
    long mstampchangestate; 
    uint8_t currentstate; 
    int gpsfixinterval; 

    uint8_t devno; // device number
    bool bloud; 

    bool baregps;  // true when not actually bluefly but connected to the gps directly
    BlueflyGpsData(HardwareSerial* serial, int lgpsfixinterval=100, bool lbaregps=false, bool lbloud=false); 
    
    long Nprsreading; 
    long Ngpsreading; 
    int32_t prsreading; 
    uint8_t ParseFetchAvailableLine(); 
    uint8_t FetchBlueflyData(long mstamp, EastingNorthingLocalLinearTrans* penllt); 
    void serialwritegps(const char* pcomm); 
    int BRB;  // uart1 (GPS) baud code 34=57600, 207=9600
    int BOF;  // outputFrequency, between 0 and 50
    
    char recline[RECLINELENGTH]; 
    int nrec; 
    int recblock[RECBLOCKLENGTH];  // indexes into recline
    int nrecblock; 
    int linenumber; 
    long mstampdollar;        // millis at time when '$' start of line character was seen
    uint8_t recchecksum; 
    int npoststarblocks;      // separates out the checksum (should be 1)
    
    // current GGA reading
    char* isotimestamp;       // date part got from RMC reading for UTC
    long mstampmidnight;      // time part turned into ms since midnight UTC
    int32_t latminutes10000;  // minutes * 10000, so no floating point value needed!
    int32_t lngminutes10000;  // minutes * 10000
    int positionfixindicator; // 0 no fix, 1 GPS fix, 2 Differential GPS fix
    //int nsatellitesused;      // not interesting
    int32_t horizontaldilutionofposition100;  // accuracy? 
    int32_t verticaldilutionofposition100;    // accuracy?
    int32_t positiondilutionofposition100;    // accuracy? [slightly: this is geometry of satellites in view]
    int32_t altitude10;
    //int32_t geoidalseparation10; // what's this?

    // current VTG reading
    uint16_t velkph100;      // *100 to get full precision from device
    uint16_t veldegrees100;  // *100 (relative to lines of latitude)
    
    // relative metres offset from start in east-north grid
    int32_t relE10; 
    int32_t relN10; 
    void ApplyLocalEuclideanTransform(EastingNorthingLocalLinearTrans& enllt); 

    bool SetIsoTimestampFromGps(char* c1, char* c9); 
    bool IsGprmc(); 
    bool ParseGprmc();  // this one has the date but no altitude (read infrequently)
    bool IsGpgga(); 
    bool ParseGpgga();  // this has position and time but no date
    bool IsGpvtg(); 
    bool ParseGpvtg();  // this has velocity vector
    bool IsGpgsa(); 
    bool ParseGpgsa();  // this has the precision errors
};



#endif
