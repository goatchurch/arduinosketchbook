// Copyright 2016 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"
#include "hotwireanemometer.h"

//#define P(X) Serial.print(X)
#define P(X)


HotWireAnemometer::HotWireAnemometer(int lpinoutWind, int lpintmp)
{
    pinoutWind = lpinoutWind; 
    pintmp = lpintmp; 
    pinMode(pinoutWind, INPUT);
    pinMode(pintmp, INPUT);
}
    
uint16_t HotWireAnemometer::Readrawwind()
{
    return analogRead(pinoutWind); 
}

uint16_t HotWireAnemometer::Readrawtmp()
{
    return analogRead(pintmp); 
}

float HotWireAnemometer::Readwindmps()
{
    uint16_t ad = Readrawwind(); 
    float windMPH = pow(((ad*3.3/5.0 - 264.0) / 85.6814), 3.36814); 
    return windMPH*(5280*12*2.54/100/3600); 
}

float HotWireAnemometer::ReadtmpC()
{
    uint16_t adtmp = Readrawtmp(); 
    return ((((float)adtmp*3.3) / 1024.0) - 0.400) / .0195; 
}

