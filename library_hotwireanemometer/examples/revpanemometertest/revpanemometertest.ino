
#include "Arduino.h"
#include "hotwireanemometer.h"

#define P(X) Serial.print(X)

// pins on the dust monitor are 
// 1 V-LED
// 2 LED-GND
// 3 LED          pin 15 LED on
// 4 S-GND 
// 5 Vout         pin 17 read value
// 6 Vcc

HotWireAnemometer hwa(15, 17); 

void setup() 
{
    Serial.begin(9600); 
pinMode(15, INPUT); 
pinMode(17, INPUT); 
}

void loop() 
{
    P(millis()); 
    P(","); 
    P(hwa.Readwindmps()); 
//    P(hwa.Readrawwind()); 
    P(","); 
    P(hwa.ReadtmpC()); 
//    P(hwa.Readrawtmp()); 
    P("\n"); 
    delay(100); 
}


