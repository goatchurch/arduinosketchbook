// Copyright 2017 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef HOTWIREANEMOMETER_h
#define HOTWIREANEMOMETER_h

// This is the modern devices revP version
// needs an external 9V battery
class HotWireAnemometer
{
public:
    int pinoutWind; 
    int pintmp; 
    
    HotWireAnemometer(int lpinoutWind, int lpintmp); 
    uint16_t Readrawwind();   
    uint16_t Readrawtmp();    

    float Readwindmps();
    float ReadtmpC(); 
};


#endif

