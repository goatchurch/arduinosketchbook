"""
// standard pressure at altitude (abbout 10bar/m)
// 101325      0m
// 100129    100m
//  98945    200m
//  95461    500m
//  89875   1000m
//  84556   1500m
//  79495   2000m
//  74682   2500m
//  70109   3000m
//  65764   3500m
//  61640   4000m
// approximation coefficients using:
// b = {0: 101325, 4000: 61640, 100: 100129, 200: 98945, 3500: 65764, 2000: 79495, 1000: 89875, 500: 95461, 3000: 70109, 2500: 74682, 1500: 84556}
// def fun(x):  return sum((p*p*x[2] + p*x[1] + x[0] - a)**2  for p, a in b.items())
// scipy.optimize.minimize(fun=fun, x0=[10000,-0.1,0], method='Powell)
"""


