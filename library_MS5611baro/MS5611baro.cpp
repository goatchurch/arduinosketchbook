// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"
#include <Wire.h>
#include "MS5611baro.h"

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X&0xff < 16 ? "0" : ""); Serial.print(X&0xff, HEX) 
#define PN Serial.println("")


enum { 
    BS_STILL=0, 
    BS_STARTTEMP, BS_READTEMP0, BS_READTEMP, 
    BS_STARTPRESSURE, BS_READPRESSURE0, BS_READPRESSURE, 
    BS_CONVERSION, BS_CONVERSIONDONE
} BAROSTATE; 

uint16_t MS5611::readRegister16(uint8_t reg)
{
#if SPI_VERSION 
    digitalWrite(chipselect, LOW);
    SPI.transfer(reg); //Send register location
    uint8_t r1 = SPI.transfer(0); 
    uint8_t r2 = SPI.transfer(0); 
    digitalWrite(chipselect, HIGH);
#else
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.requestFrom(MS5611_ADDRESS, 2);
    while(!Wire.available())
        ;
    uint8_t r1 = Wire.read(); 
    uint8_t r2 = Wire.read(); 
    Wire.endTransmission();
#endif    
    uint16_t value = (r1 << 8) | r2;
    PH(reg); P("   "); PH(r1); P(" "); PH(r2); P("  "); P(value); PN; 
    return value;
}

int32_t MS5611::readRegister24()
{
#if SPI_VERSION
    digitalWrite(chipselect, LOW);
    SPI.transfer(0x00); 
    uint8_t r1 = SPI.transfer(0); 
    uint8_t r2 = SPI.transfer(0); 
    uint8_t r3 = SPI.transfer(0); 
    digitalWrite(chipselect, HIGH);
#else
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x00); 
    Wire.endTransmission();
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.requestFrom(MS5611_ADDRESS, 3);
    while(!Wire.available())
        ;
    uint8_t r1 = Wire.read(); 
    uint8_t r2 = Wire.read(); 
    uint8_t r3 = Wire.read(); 
    Wire.endTransmission();
#endif    
    int32_t Dx = ((int32_t)r1 << 16) | ((int32_t)r2 << 8) | (int32_t)r3;
    return Dx; 
}


#if SPI_VERSION
void MS5611::initiate(int lchipselect)
{
    chipselect = lchipselect; 
    pinMode(chipselect, OUTPUT);
    SPI.begin();
    digitalWrite(chipselect, LOW);
    SPI.transfer(0x1E); // reset
    digitalWrite(chipselect, HIGH);
#else
void MS5611::initiate()
{
    Wire.begin();
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x1E);   // reset
    Wire.endTransmission();
#endif
    bstate = 0; 
    mstamp = -1; 

    delay(100);
    uint16_t C1 = readRegister16(0xA2);  // 47490
    SENST1 = C1 * (uint32_t)0x8000; 
    uint16_t C2 = readRegister16(0xA4);  // 48233
    OFFT1 = C2 * (uint32_t)0x10000; 
    
    C3 = readRegister16(0xA6);           // 29313
    C4 = readRegister16(0xA8);           // 26259
    uint16_t C5 = readRegister16(0xAA);  // 31497
    Tref = (int32_t)C5 * 0x100; 
    C6 = readRegister16(0xAC);           // 28282
}


// should be turned into a state machine; and use SPI
void MS5611::UpdateDvals()
{
    // raw temperature
#if SPI_VERSION
    digitalWrite(chipselect, LOW);
    SPI.transfer(0x48);
    digitalWrite(chipselect, HIGH);
#else
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x48);
    Wire.endTransmission();
#endif
    delay(10); 
    D1 = readRegister24(); 

    // raw pressure (done second so reading is late as possible)
#if SPI_VERSION
    digitalWrite(chipselect, LOW);
    SPI.transfer(0x58);
    digitalWrite(chipselect, HIGH);
#else
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x58);
    Wire.endTransmission();
#endif   
    mstamp = millis(); 
    delay(10);
    D2 = readRegister24(); 
}
  
void MS5611::UpdateTP() 
{
    int64_t dT = D2 - Tref; // difference to reference temperature
    TEMP = 2000 + dT*C6/0x00800000; 
    int64_t OFF = (int64_t)OFFT1 + dT*C4/0x80; 
    int64_t SENS = SENST1 + dT * C3 / 0x100;
    
    if (TEMP < 2000) {
        int64_t T2 = dT*dT/0x80000000; 
        int32_t ra = TEMP - 2000; 
        ra = ra*ra; 
        OFF -= 5*ra/2; 
        SENS -= 5*ra / 4; 
        if (TEMP < -1500) {
            int32_t rb = TEMP - (-1500); 
            rb = rb*rb; 
            OFF -= 7*rb; 
            SENS -= 11*rb / 2; 
        }
        TEMP -= T2; 
    }
    
    Pr = (SENS * D1 / 0x200000 - OFF) / 0x8000; 
    approxAlt = 1.33344341e+04 + Pr*(-1.82688011e-01 + Pr*5.04743950e-07);  // quadratic approximation formula
}


void MS5611::readloop()
{
    // raw temperature
    if (bstate == BS_STARTTEMP) {
    #if SPI_VERSION
        digitalWrite(chipselect, LOW);
        SPI.transfer(0x48);
        digitalWrite(chipselect, HIGH);
    #else
        Wire.beginTransmission(MS5611_ADDRESS);
        Wire.write(0x48);
        Wire.endTransmission();
    #endif
        mstamp = millis(); 
        bstate = BS_READTEMP0; 
        return; 
    }
    
    if (bstate == BS_READTEMP0) {
        bstate = BS_READTEMP; 
        return; 
    }
    
    if (bstate == BS_READTEMP) {
        if (millis() < mstamp + 10)
            return; 
        D1 = readRegister24(); 
        bstate = BS_STARTPRESSURE; 
        return;  // not drop through for closer response in time for pressure
    }
    
    // raw pressure (done second so reading is late as possible)
    if (bstate == BS_STARTPRESSURE) {
    #if SPI_VERSION
        digitalWrite(chipselect, LOW);
        SPI.transfer(0x58);
        digitalWrite(chipselect, HIGH);
    #else
        Wire.beginTransmission(MS5611_ADDRESS);
        Wire.write(0x58);
        Wire.endTransmission();
    #endif   
        mstamp = millis(); 
        bstate = BS_READPRESSURE0; 
        return; 
    }

    if (bstate == BS_READPRESSURE0) {
        bstate = BS_READPRESSURE; 
        return; 
    }
    
    if (bstate == BS_READPRESSURE) {
        if (millis() < mstamp + 10)
            return; 
        D2 = readRegister24(); 
        bstate = BS_CONVERSION; 
        // drop through so we get pressure at its most up to date
    }
    
    if (bstate == BS_CONVERSION) {
        UpdateTP(); 
        bstate = BS_CONVERSIONDONE; 
        return; 
    }

    if (bstate == BS_CONVERSIONDONE) 
        bstate = BS_STILL; 
}

