// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef MS5611_h
#define MS5611_h



// this library can support the I2C or the SPI interfaces to this device
#define SPI_VERSION 0

#if SPI_VERSION
#include <SPI.h>
#else
//#include <Wire.h>
#define MS5611_ADDRESS                (0x77)
#endif


class MS5611
{
public:
#if SPI_VERSION
    void initiate(int lchipselect);
#else
    void initiate();
#endif
    uint8_t bstate;  // 0 static, 1 start temp at next loop, 2 reading temp (waiting) [opportunity to sdflush], 3 reading temp (waiting), 
                     // 4 start pressure next loop, 5 reading pressure (waiting), 6 reading pressure (waiting), 
                     // 7 do conversion
    uint32_t mstamp; 
    
    int32_t D1;   // raw temperature
    int32_t D2;   // raw pressure
    int32_t TEMP; // *100degC
    int32_t Pr;   // in Pascals (1Atmosphere = 101kPa)
    float approxAlt; 

    void readloop(); 
    void UpdateDvals(); 
    void UpdateTP(); 
        
private:
    uint32_t SENST1; // Pressure sensitivity
    uint32_t OFFT1;  // Pressure offset
    uint16_t C3;     // Temperature coefficient of pressure sensitivity
    uint16_t C4;     // Temperature coefficient of pressure offset
    int32_t Tref;    // Reference temperature
    int32_t C6;      // Temperature coefficient of the temperature
    
#if SPI_VERSION
    int chipselect;
#endif

    uint16_t readRegister16(uint8_t reg); 
    int32_t readRegister24(); 
};


#endif
