#include <Wire.h>

#define MS5611_ADDRESS                (0x77)

uint32_t SENST1; // Pressure sensitivity
uint32_t OFFT1;  // Pressure offset
uint16_t C3;     // Temperature coefficient of pressure sensitivity
uint16_t C4;     // Temperature coefficient of pressure offset
int32_t Tref;    // Reference temperature
int32_t C6;      // Temperature coefficient of the temperature

int32_t D1;   // raw temperature
int32_t D2;   // raw pressure
int32_t TEMP; // *100degC
int32_t Pr;   // in Pascals (1Atmosphere = 101kPa)
float approxAlt; 

uint16_t readRegister16(uint8_t reg)
{
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission();
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.requestFrom(MS5611_ADDRESS, 2);
    while(!Wire.available())
        ;
    uint8_t r1 = Wire.read(); 
    uint8_t r2 = Wire.read(); 
    Wire.endTransmission();
    uint16_t value = (r1 << 8) | r2;
    return value;
}

int32_t readRegister24()
{
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x00); 
    Wire.endTransmission();
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.requestFrom(MS5611_ADDRESS, 3);
    while(!Wire.available())
        ;
    uint8_t r1 = Wire.read(); 
    uint8_t r2 = Wire.read(); 
    uint8_t r3 = Wire.read(); 
    Wire.endTransmission();
    int32_t Dx = ((int32_t)r1 << 16) | ((int32_t)r2 << 8) | (int32_t)r3;
    return Dx; 
}


// should be turned into a state machine; and use SPI
void UpdateDvals()
{
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x48);
    Wire.endTransmission();
    
    delay(10); 
    D1 = readRegister24(); 

    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x58);
    Wire.endTransmission();
    delay(10);
    D2 = readRegister24(); 
}
  
void UpdateTP() 
{
    int64_t dT = D2 - Tref; // difference to reference temperature
    TEMP = 2000 + dT*C6/0x00800000; 
    int64_t OFF = (int64_t)OFFT1 + dT*C4/0x80; 
    int64_t SENS = SENST1 + dT * C3 / 0x100;
    
    if (TEMP < 2000) {
        int64_t T2 = dT*dT/0x80000000; 
        int32_t ra = TEMP - 2000; 
        ra = ra*ra; 
        OFF -= 5*ra/2; 
        SENS -= 5*ra / 4; 
        if (TEMP < -1500) {
            int32_t rb = TEMP - (-1500); 
            rb = rb*rb; 
            OFF -= 7*rb; 
            SENS -= 11*rb / 2; 
        }
        TEMP -= T2; 
    }
    Pr = (SENS * D1 / 0x200000 - OFF) / 0x8000; 
    approxAlt = 1.33344341e+04 + Pr*(-1.82688011e-01 + Pr*5.04743950e-07);  // quadratic approximation formula
}



void setup()
{
    Serial.begin(9600); 
    Wire.begin();
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x1E);   // reset
    Wire.endTransmission();
    delay(100);
    
    uint16_t C1 = readRegister16(0xA2);  // 47490
    SENST1 = C1 * (uint32_t)0x8000; 
    uint16_t C2 = readRegister16(0xA4);  // 48233
    OFFT1 = C2 * (uint32_t)0x10000; 
    
    C3 = readRegister16(0xA6);           // 29313
    C4 = readRegister16(0xA8);           // 26259
    uint16_t C5 = readRegister16(0xAA);  // 31497
    Tref = (int32_t)C5 * 0x100; 
    C6 = readRegister16(0xAC);           // 28282
    
    
    //set timer2 interrupt at 2kHz
    TCNT2  = 0;              //initialize counter value to 0
    OCR2A = 249;             // = (16*1000) / (64) - 1 (must be <256)
    TCCR2A = (1 << WGM21);   // turn on CTC mode
    TCCR2B = (1 << CS22);    // 64 bit prescaler
    TIMSK2 |= (1 << OCIE2A); // enable timer compare interrupt
}

void loop()
{
    static long Sm = 0; 
    long Dm = millis(); 
    if (Dm/50 == Sm/50)
       return; 
    Sm = Dm; 
  
    UpdateDvals(); 
    UpdateTP(); 
    Serial.print(millis()); 
    Serial.print(" "); 
    //Serial.println(approxAlt); 
    Serial.println(TEMP); 
return; 

    static long B = 0; 
    long m = millis();         
    if (m/100 != B/100)
       delay(5);
    else 
       delay(15);  
    B = m; 
}


// for plotting
/*
fin = open("/home/goatchurch/datalogging/templogs/sp.txt")
cont = [ ]
for line in fin:
    a = line.split()
    if len(a) == 2:
        cont.append((int(a[0])*0.0001, float(a[1])*0.01))
        
#sendactivity("clearallcontours")
sendactivity("contours", contours=[cont], materialnumber=0)

sendactivity("contours", contours=[[(0,c),(60,c)]  for c in range(40)], materialnumber=1)
sendactivity("contours", contours=[[(t,0),(t,40)]  for t in range(60)], materialnumber=1)
*/

