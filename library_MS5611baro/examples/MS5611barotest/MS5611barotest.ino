// to get SPI version, change setting in MS5611.h and include the relevant library here and change initiate so it builds

//#include <SPI.h>
#include "Arduino.h"

#include <Wire.h>
#include "MS5611baro.h"

#define P(X)  Serial.print(X)


MS5611 ms5611;

//void setup() {}
//void loop() {}

void setup() 
{
    Serial.begin(9600);
    P("Initialize MS5611 Sensor\n"); 
delay(2000); 
    P("Initialize MS5611 Sensor\n"); 

    ms5611.initiate(); 
}

int BS_STILL = 0;
int BS_CONVERSIONDONE=8;
int BS_STARTTEMP = 1; 

void loop()
{
    if (ms5611.bstate != BS_STILL) {
        ms5611.readloop(); 
        if (ms5611.bstate == BS_CONVERSIONDONE) {
            //    P(ms5611.D1); P(" "); P(ms5611.D2); P("  "); P(ms5611.TEMP); P("  "); P(ms5611.Pr); P("  "); P(ms5611.approxAlt); PN; 
            P(ms5611.mstamp); P(" "); 
            P(ms5611.Pr), P("Mb "); 
            P(ms5611.TEMP); P("C\n"); 
        }
    } else {
       ms5611.bstate = BS_STARTTEMP; 
    }
    return; 
}

