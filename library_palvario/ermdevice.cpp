// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <JeeLib.h>
#include <Wire.h>

// PWM pins 3, 5, 6, 9, 10, 11

#define DISABLESOFTI2C 0  // to try and find the bug which hangs rf12_sendNow()

// digital pin 4 for SCL green, digital pin 14 (A0) for SDA yellow
#define I2C_NOINTERRUPT 0
#define SCL_PIN 4
#define SCL_PORT PORTD
#define SDA_PIN 0
#define SDA_PORT PORTC 

#if DISABLESOFTI2C
#define I2C_READ   1
#define I2C_WRITE   0
bool i2c_start(uint8_t addr) { return true;}
bool i2c_write(uint8_t value) { return true; }
uint8_t i2c_read(bool last) { return 0; }
bool i2c_rep_start(uint8_t addr) { return true; }
void i2c_stop(void) {;}
boolean i2c_init(void) { Serial.print("softi2c disabled\n"); return false; }
#else
#include <SoftI2CMaster.h>
#endif

#include "palvario.h"

#define P(X) Serial.print(X)
#define PH(X) { Serial.print(X < 16 ? "0" : ""); Serial.print(X, HEX); }



ERMdevice::ERMdevice(uint8_t lpinnumber) 
{
    pinnumber = lpinnumber; 
    bi2cdevice = ((pinnumber == 0) || (pinnumber == 1)); 
    bsoft = (pinnumber == 1); // 0 and 1 are the i2c devices, the others are the actual PWM pin controls
    bworking = false; 
}

void ERMdevice::StartBuzz(uint8_t leff, long lmstamp) 
{
    eff = leff; 
    if (bi2cdevice) {
        writeRegister8(0x04+0, eff);    // DRV2605_REG_WAVESEQ1 
        writeRegister8(0x04+1, 0);      // end waveform
        writeRegister8(0x0C, 1);        // DRV2605_REG_GO      play effect
    } else {
        analogWrite(pinnumber, 255); 
    }
    mstamp = lmstamp; 
}

void ERMdevice::CompleteBuzz(long lmstamp) 
{
    if (!bi2cdevice && (lmstamp > mstamp + 450)) {
        analogWrite(pinnumber, 0); 
        mstamp = lmstamp; 
    }
}


uint8_t ERMdevice::readRegister8(uint8_t reg) 
{
    //assert(bi2cdevice); 
    uint8_t x;  // 0x5A == DRV2605_ADDR
    if (bsoft) {  
        if (!bworking)  
            return 0;   // avoid crashes
        uint8_t bres = i2c_start((0x5A<<1)+I2C_WRITE); 
        i2c_write((byte)reg);
        i2c_rep_start((0x5A<<1)+I2C_READ);
        x = i2c_read(true);
        i2c_stop();
    } else {
        Wire.beginTransmission(0x5A);
        Wire.write((byte)reg);
        Wire.endTransmission();
        Wire.requestFrom((byte)0x5A, (byte)1);
        x = Wire.read();
    }
    return x;
}

void ERMdevice::writeRegister8(uint8_t reg, uint8_t val) 
{
    //assert(bi2cdevice); 
    if (bsoft) {
        if (!bworking)  
            return;  // avoid crashes
        i2c_start((0x5A<<1)+I2C_WRITE);
        i2c_write((byte)reg);
        i2c_write((byte)val);
        i2c_stop();
    } else {
        Wire.beginTransmission(0x5A);
        Wire.write((byte)reg);
        Wire.write((byte)val);
        Wire.endTransmission();
    }
}


void ERMdevice::BeginERM() 
{
    if (!bi2cdevice) {
        pinMode(pinnumber, OUTPUT); 
        return; 
    }

    // the SCL pin being high is good indicator that the I2C board is attached
    // without this, the i2c_start() code hangs in a loop waiting for this to be so
    // (maybe it needs a pullup resistor) to save this hassle.
    if (bsoft) {
        for (int i = 0; i < 10; i++) {
            P("digitalread(4)="); 
            P(digitalRead(SCL_PIN)); 
            P("\n"); 
            if (digitalRead(SCL_PIN)) {
                bworking = true; 
                break; 
            }
        }
        if (!bworking)  
            return; 
        i2c_init();
    } else {
        Wire.begin();
        bworking = true; 
    }

    // differs from docs on 0 - E0, 16 - 3E, 17 - 89, 18 - 0C, 19 - 6C, 
    P(bsoft ? "Soft" : "Hard"); 
    P(" startup reg values: "); 
    for (int i = 0; i != 0x23; i++) {
        //PH(i); P(":"); 
        P(" "); 
        PH(readRegister8(i)); 
    }
    P("\n"); 
  
    writeRegister8(0x01, 0x00); // out of standby  DRV2605_REG_MODE
  
    writeRegister8(0x02, 0x00); // no real-time-playback  DRV2605_REG_RTPIN
  
    writeRegister8(0x04, 1); // strong click  DRV2605_REG_WAVESEQ1
    writeRegister8(0x05, 0); // DRV2605_REG_WAVESEQ2
  
    writeRegister8(0x0D, 0); // no overdrive  DRV2605_REG_OVERDRIVE
  
    writeRegister8(0x0E, 0);  // DRV2605_REG_SUSTAINPOS
    writeRegister8(0x0F, 0);  // DRV2605_REG_SUSTAINNEG
    writeRegister8(0x10, 0);  // DRV2605_REG_BREAK
    writeRegister8(0x13, 0x64);  // DRV2605_REG_AUDIOMAX
  
    //writeRegister8(0x17, 0x64);  // OD_CLAMP DRV2605_REG_AUDIOMAX
    float overdrive_voltage = 5.0; 
    writeRegister8(0x17, min(255, (int)(overdrive_voltage/0.02196))); // OD clamp  default 0x89
    //P("Set overdrive voltage: 0x");  PH(readRegister8(0x17));  P("\n"); 

    // V (ERM-OL_AV) = 0.02196*OD_CLAMP[7:0]

    // ERM open loop
  
    // 7.5.4.1 Programming for ERM Open-Loop Operation
    // To configure the DRV2605 device in ERM open-loop operation, the ERM must be selected by writing the
    // N_ERM_LRA bit to 0 (in register 0x1A), and the ERM_OPEN_LOOP bit to 1 in register 0x1D.


    // turn off N_ERM_LRA
    writeRegister8(0x1A, readRegister8(0x1A) & 0x7F);  // DRV2605_REG_FEEDBACK
    // turn on ERM_OPEN_LOOP
    writeRegister8(0x1D, readRegister8(0x1D) | 0x20);  // DRV2605_REG_CONTROL3

    writeRegister8(0x03, 1);  // DRV2605_REG_LIBRARY
    writeRegister8(0x01, 0x00);   // DRV2605_REG_MODE, DRV2605_MODE_INTTRIG
}

bool ERMdevice::CalibrateERM(uint16_t ch) 
{
    //assert(bi2cdevice); 
    if (ch == 0) {
        P("calibration values before:\n");
        P("  BEMF_GAIN "); PH(readRegister8(0x1A) & 0x03); P("\n"); 
        P("  A_CAL_COMP "); PH(readRegister8(0x18)); P("\n"); 
        P("  A_CAL_BEMF "); PH(readRegister8(0x19)); P("\n"); 
        P("  DIAG_RESULT "); PH(readRegister8(0x00) & 0x80); P("\n"); 
    }
    
    writeRegister8(0x01, 0x07);  // auto calibration

    // (a) ERM_LRA — selection will depend on desired actuator.
    // (b) FB_BRAKE_FACTOR[2:0] — A value of 2 is valid for most actuators.
    // (c) LOOP_GAIN[1:0] — A value of 2 is valid for most actuators.
    uint8_t breakfactor = 3 & (2 + ch & 3); 
    uint8_t loopgain = 2 & (2 + (ch >> 2) & 3); 
    writeRegister8(0x1A, breakfactor<<4 /*FB_BRAKE_FACTOR*/ | loopgain<<2 /*LOOP_GAIN*/ | 2/*default BEMF_GAIN*/); 
    
    // (d) RATED_VOLTAGE[7:0] — See the Rated Voltage Programming section for calculating the correct
    float rated_voltage = 2.3; 
    writeRegister8(0x16, min(255, (int)(rated_voltage/0.02133))); // rated voltage default 0x3F

    // (e) OD_CLAMP[7:0] — See the Overdrive Voltage-Clamp Programming section for calculating the correct
    float overdrive_voltage = 3.3; 
    float tfac = 1; // (tdrivetime + tidsstime + tblankingtime)/(tdrivetime - 0.0003); 
    writeRegister8(0x17, min(255, (int)(overdrive_voltage/0.02133*tfac))); // OD clamp  default 0x89
    
    // (f) AUTO_CAL_TIME[1:0] — A value of 3 is valid for most actuators.
    writeRegister8(0x1E, 3<<4/*AUTO_CAL_TIME*/); 

    // (g) DRIVE_TIME[4:0] — See the Drive-Time Programming for calculating the correct register value.
    uint8_t drivetime = (ch == 0 ? 0x13 : ((ch >> 8) & 3) << 3); 
    writeRegister8(0x1B, 0x80/*STARTUP_BOOST*/ | drivetime/*DRIVE_TIME*/); // default 0x93
    
    // (h) SAMPLE_TIME[1:0] — A value of 3 is valid for most actuators.
    // (i) BLANKING_TIME[1:0] — A value of 1 is valid for most actuators.
    // (j) IDISS_TIME[1:0] — A value of 1 is valid for most actuators.
    uint8_t sampletime = 3 & (3 + (ch >> 4) & 3); 
    uint8_t blankingtime = 2 & (2 + (ch >> 6) & 3); 
    writeRegister8(0x1C, sampletime<<4/*SAMPLE_TIME*/ | blankingtime<<2/*BLANKING_TIME*/ | 1/*IDISS_TIME*/); // default 0xF5

    if (ch != 0) {
        P("ch: ");  PH(ch);  
        P(" 1ABC "); PH(readRegister8(0x1A)); PH(readRegister8(0x1B)); PH(readRegister8(0x1C)); 
        P(" 167 "); PH(readRegister8(0x16)); PH(readRegister8(0x17)); 
    }
    
    writeRegister8(0x0C, 0x01);        // DRV2605_REG_GO
    while (readRegister8(0x0C) & 0x01) {
        if (ch == 0)
            P(" still calibrating\n"); 
        delay(100); 
    }
    bool bres = ((readRegister8(0x00) & 0x80) == 0); 
    
    if (ch == 0) {
        P("calibration values after:\n");
        P("  BEMF_GAIN "); PH(readRegister8(0x1A) & 0x03); P("\n"); 
        P("  A_CAL_COMP "); PH(readRegister8(0x18)); P("\n"); 
        P("  A_CAL_BEMF "); PH(readRegister8(0x19)); P("\n"); 
        P("  DIAG_RESULT "); PH(readRegister8(0x00) & 0x80); P("\n");  // set means failed
    }
    
    writeRegister8(0x01, 0x00);   // back to standard mode DRV2605_REG_MODE, DRV2605_MODE_INTTRIG
    if (ch != 0)
        P(bres ? "YAY!!!\n\n\n" : " fail\n"); 
    return bres; 
}

