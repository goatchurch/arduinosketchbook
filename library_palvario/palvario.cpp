// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <JeeLib.h>
#include <Wire.h>

#include "palvario.h"

void rf12_sendNowTimeout(uint8_t hdr, const void* ptr, uint8_t len) {
    int x = 0; 
    while (!rf12_canSend()) {
        rf12_recvDone(); // keep the driver state machine going, ignore incoming
        x++; 
        if (x == 10000) {
            Serial.print("rf12_sendNowTimeout timed out\n"); 
            return; 
        }
     }
    rf12_sendStart(hdr, ptr, len);
}



#define P(X) Serial.print(X)
#define PH(X) { Serial.print(X < 16 ? "0" : ""); Serial.print(X, HEX); }

//On most Arduino boards (those with the ATmega168 or ATmega328), this function works on pins 3, 5, 6, 9, 10, and 11. On t

HarnessUnit::HarnessUnit() :
    palvariosoft(1), palvariohard(0), 
    palvarioA(5), palvarioB(6)
{
    // PWM pins 3, 5, 6, 9, 10, 11
    
    virtuallagaltitude = 0.0; 
    receivedaltitude = 0.0; 
    simulatedvelocitymps = 0.0; 
    mstimestampreceivedaltitude = millis(); 
}

void HarnessUnit::ApplyVelocity()
{
    long mstamp = millis(); 
    
    palvariosoft.CompleteBuzz(mstamp); 
    palvariohard.CompleteBuzz(mstamp); 
    palvarioA.CompleteBuzz(mstamp); 
    palvarioB.CompleteBuzz(mstamp); 

    if (simulatedvelocitymps != 0.0) {
        if (mstamp >= mstimestampreceivedaltitude + 100) {
            receivedaltitude += simulatedvelocitymps*0.1; 
            mstimestampreceivedaltitude = mstamp; 
        }
    }
    
    if (mstamp < mstampaltcycle)
        return; 

    // add some smoothing
    virtuallagaltitude = virtuallagaltitude * 0.999 + receivedaltitude * 0.001; 
    float altdiff = receivedaltitude - virtuallagaltitude; 
    if (false || receivedaltitude != 0.0) {
        P(mstamp);
        P("  alt "); 
        P(receivedaltitude); 
        P(" "); 
        P(virtuallagaltitude); 
        P("  "); 
        P(altdiff); 
        P("\n"); 
    }
    
    // work out alternative steps in the up vario
    uint8_t eff = 92; 
    float gapstep = 0.5 + fabs(altdiff*0.3); 
    if ((altdiff > gapstep) && (mstamp + 350 > mstampchestbuzzstart)) {
        virtuallagaltitude += gapstep; 
        mstampchestbuzzstart = mstamp; 
        bchestleft = !bchestleft; 
        P(bchestleft ? "*\n" : "   *\n"); 
        ERMdevice* ermdevice = (bchestleft ? &palvariosoft : &palvariohard); 
        ermdevice->StartBuzz(eff, mstamp); 
    } else if ((altdiff < -gapstep) && (mstamp + 350 > mstamplegbuzzstart)) {
        virtuallagaltitude -= gapstep; 
        mstamplegbuzzstart = mstamp; 
        blegleft = !blegleft; 
        P(blegleft ? "$\n" : "   $\n"); 
        ERMdevice* ermdevice = (blegleft ? &palvarioA : &palvarioB); 
        ermdevice->StartBuzz(eff, mstamp); 
    }
    
    mstampaltcycle = mstamp + 100; 
}


void HarnessUnit::StartUp()
{
    palvariosoft.BeginERM(); 
    palvariohard.BeginERM(); 
    palvarioA.BeginERM(); 
    palvarioB.BeginERM(); 
}

uint8_t effect = 1;
uint8_t xeffect = 0;
int nalt = 0; 
void HarnessUnit::OrgEffectLoop()
{
    nalt++; 
    bool busesoft = ((nalt % 2) == 0); 
    ERMdevice& palvario = (busesoft ? palvariosoft : palvariohard); 
    if (Serial.available()) {
        char buf[10]; 
        String s = Serial.readString(); 
        Serial.println(s); 
        s.toCharArray(buf, 10); 
        if (buf[0] == 'c') {
            palvario.CalibrateERM(0); 
            return; 
        } else if (buf[0] == 'C') {
            for (int i = 1; i < 256; i++) {
                if (palvario.CalibrateERM(i))
                    while(1); 
            }
            return; 
        } else if (buf[0] == 'l') {
            for (int i = 0; i != 0x23; i++) {
                //PH(i); P(":"); 
                P(" "); 
                PH(palvario.readRegister8(i)); 
            }
            P("\n"); 
            return; 
        }
        
        xeffect = atoi(buf); 
        Serial.println(xeffect); 
        effect = xeffect; 
    }        

  Serial.print(busesoft ? "S:" : "H:"); 
  Serial.print("Effect #"); 
  Serial.println(effect);
  analogWrite(5, (effect % 2 == 0 ? 255 : 0)); 
  analogWrite(6, (effect % 2 == 1 ? 255 : 0)); 

  uint8_t msg[2]; 
  msg[0] = (busesoft ? 1 : 0); 
  msg[1] = effect; 
  rf12_sendNowTimeout(0, msg, 2);

  // set the effect to play
  palvario.writeRegister8(0x04+0, effect); // DRV2605_REG_WAVESEQ1 
  palvario.writeRegister8(0x04+1, 0);      // end waveform
  palvario.writeRegister8(0x0C, 1);        // DRV2605_REG_GO      play effect

  // wait a bit
  delay(500);

  if (xeffect == 0) {
    effect++;
    if (effect > 117) effect = 1;
  }
}

void HarnessUnit::BlinkEffect(uint8_t eff)
{
    P("Blink "); P(eff); P("\n"); 
    palvariosoft.writeRegister8(0x04+0, eff);    // DRV2605_REG_WAVESEQ1 
    palvariosoft.writeRegister8(0x04+1, 0);      // end waveform
    palvariosoft.writeRegister8(0x0C, 1);        // DRV2605_REG_GO      play effect

    palvariohard.writeRegister8(0x04+0, eff);    // DRV2605_REG_WAVESEQ1 
    palvariohard.writeRegister8(0x04+1, 0);      // end waveform
    palvariohard.writeRegister8(0x0C, 1);        // DRV2605_REG_GO      play effect

    long msstamp = millis(); 
    while ((millis() < msstamp + 3000) && ((palvariosoft.readRegister8(0x0C) & 0x01) || (palvariohard.readRegister8(0x0C) & 0x01)))
        delay(100); 
    
    // following section causes the rf12_sendNow to crash later! (and then it started crashing even without this)
    analogWrite(palvarioA.pinnumber, 128); 
    analogWrite(palvarioB.pinnumber, 255); 
    delay(350); 
    analogWrite(palvarioA.pinnumber, 255); 
    analogWrite(palvarioB.pinnumber, 128); 
    delay(350); 
    analogWrite(palvarioA.pinnumber, 0); 
    analogWrite(palvarioB.pinnumber, 0); 
}


bool HarnessUnit::SetChevState(uint8_t i, char nextchevstate, char chchange, long msstamp, long newmsnevtdisp) 
{
    uint8_t eff = 92; 
    chcevtstates[i] = chevtstates[i]; 
    mssevts[i] = msstamp; 
    P("Chev "); P(i); P(" gostate '"); P(nextchevstate); P("' change "); P(chchange); P("\n"); 
    chevtstates[i] = nextchevstate; 
    msnevts[i] = msstamp + newmsnevtdisp; 
    if (chchange == '.')
        return false; 
    ERMdevice* ermdevice = 0; 
    uint8_t ermpin = 0; 
    if ((i == 0) || (i == 1)) 
        ermdevice = (i == 0 ? &palvariosoft : &palvariohard); 
    else 
        ermpin = (i == 2 ? 5 : 6); 
    
    if (chchange == '0') {
        if (ermdevice) {
            if (ermdevice->readRegister8(0x0C) & 0x01) 
                ermdevice->writeRegister8(0x0C, 0x00); // stop early
        } else {
            analogWrite(ermpin, 0); 
        }
    } else if (chchange == '1') {
        if (ermdevice) {
            ermdevice->writeRegister8(0x04+0, eff); // DRV2605_REG_WAVESEQ1 
            ermdevice->writeRegister8(0x04+1, 0);      // end waveform
            ermdevice->writeRegister8(0x0C, 1);        // DRV2605_REG_GO      play effect
        } else {
            analogWrite(ermpin, 255); 
        }
    }
    return true; 
}

bool HarnessUnit::NextChevState(uint8_t i, long mstamp, char chevtstateoverride) 
{
    if (chevtstateoverride != '.') 
        chevtstates[i] = chevtstateoverride; 
    else if (mstamp < msnevts[i])
        return '.';
    
    switch (chevtstates[i]) {
    case 'e':   return SetChevState(i, 'E', '1', mstamp, 400);  
    case 'E':   return SetChevState(i, 'd', '0', mstamp, 300);  
    case 'd':   return SetChevState(i, 'C', '1', mstamp, 400);  
    case 'C':   return SetChevState(i, 'c', '0', mstamp, 300);  
    case 'c':   return SetChevState(i, 'B', '1', mstamp, 400);  
    case 'B':   return SetChevState(i, 'b', '0', mstamp, 600);  
    case 'b':   return SetChevState(i, 'A', '1', mstamp, 500);  
    case 'A':   return SetChevState(i, 'a', '0', mstamp, 10000);  
    case 'a':   return SetChevState(i, 'a', '.', mstamp, 20000);  
    case '\0':  return SetChevState(i, 'a', '.', mstamp, 20000);  
    }
    return false; 
}
