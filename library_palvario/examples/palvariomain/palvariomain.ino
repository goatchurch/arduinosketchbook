#include <JeeLib.h>
#include <Wire.h>
#include "palvario.h"
#include "serialinputbuffer.h"

#if 0   // to get the build system to work
#define SCL_PIN 4
#define SCL_PORT PORTD
#define SDA_PIN 0
#define SDA_PORT PORTC 
#include <SoftI2CMaster.h>
#endif

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)


// Serial receives codes of [ABCD][a-f] for AB smart, CD dumb
// and the letter for the state machine

// try codes 92, 89, 58, 15, 76, 70, 93, 1, 7, 27, 118 (continuous)

// soft type digital pin 4 for SCL green, digital pin 14 (A0) for SDA yellow
HarnessUnit harnessunit;
SerialInputBuffer serialP(&Serial);  // generally a connection to the programmer as real instructions come through the rf12

long pingfrequencyms = 2000; 
long mstamplastrfping; 

void setup()
{
    rf12_initialize(2, RF12_868MHZ, 100);
    serialP.SetupSerial(9600); 
    harnessunit.StartUp(); 

P("startup\n");
    harnessunit.BlinkEffect(92); 
    harnessunit.BlinkEffect(76); 
    harnessunit.BlinkEffect(93); 

    //interruptSetup();                 // sets up to read Pulse Sensor signal every 2mS 

    mstamplastrfping = millis() + pingfrequencyms / 2; 
}


void hreact(char* buff, int8_t bufflen, char src)
{
    if (((buff[0] == 's') || (buff[0] == 'h')) && ((buff[1] == 'c') || (buff[1] == 'C')) && (buff[2] == 'a') && (buff[3] == 'l')) {
        ERMdevice& palvario = (buff[0] == 's' ? harnessunit.palvariosoft : harnessunit.palvariohard); 
        if (buff[1] == 'c') {
            palvario.CalibrateERM(0); 
        } else {
            for (int i = 1; i < 256; i++) {
                if (palvario.CalibrateERM(i)) {
                    P("Calibration found\n  Yay!!!\n(deliberate crash)\n"); 
                    while(1); 
                }
            }
        }
        return; 
    }

    P("Received "); P(src); P(": ");  
    for (int i = 0; i < bufflen; i++)
        P(buff[i]); 
    P("\n"); 
    
    if ((bufflen > 2) && (buff[0] == 'a')) {
        harnessunit.receivedaltitude = atof(buff+1); 
        harnessunit.mstimestampreceivedaltitude = millis(); 
        return; 
    }
    
    if ((bufflen > 2) && (buff[0] == 'v')) {
        harnessunit.simulatedvelocitymps = atof(buff+1);   
        harnessunit.mstimestampreceivedaltitude = millis() - 100; 
    }
    
    int ib = 0; 
    while ((bufflen - ib >= 2) && ((buff[ib] >= 'A') && (buff[ib] <= 'D'))) {
        harnessunit.NextChevState(buff[ib] - 'A', millis(), buff[ib + 1]); 
        ib += 2; 
    }
}


char* msg = "hping:12345678";  // h for ping from harness
char* HexMstamponMSG(long mstamp)
{
    for (int i = 0; i < 8; i++) {
        char nb = ((mstamp >> (4*(7-i))) & 0x0F); 
        msg[6+i] = nb + (nb < 10 ? '0' : 'A'-10); 
    }
    return msg; 
}


long counter; 
void loop()
{
  #if 0
 rf12_recvDone(); 
     ++counter; P(counter); 
     P("about to sendnow\n"); 
     if ((counter % 4) == 0)
         rf12_sendNowTimeout(0, &counter, sizeof(counter)); 
     delay(50);  rf12_recvDone(); 
     delay(50);  rf12_recvDone(); 
     delay(50);  rf12_recvDone(); 
     delay(50);  rf12_recvDone(); 
 return; 
  #endif
     
    harnessunit.ApplyVelocity(); 

    // send a ping message out on the radio waves
    long mstamp = millis(); 
    if (mstamp > mstamplastrfping + pingfrequencyms) {
        HexMstamponMSG(mstamp); 
        P("Sending Rf: "); P(msg); P("\n"); 
        rf12_sendNowTimeout(0, msg, 14); 
        mstamplastrfping = mstamp; 
    }

    if (serialP.RecCheck()) {
        if (serialP.buff[0] == '$')  {
             P("Sending the text on rf: "); 
             P(serialP.buff+1); 
             P("\n"); 
             rf12_sendNowTimeout(0, serialP.buff+1, serialP.bufflen-1); 
        }
        else
            hreact(serialP.buff, serialP.bufflen, 'c'); 
    }
    if (rf12_recvDone() && (rf12_crc == 0)) 
        hreact((char*)rf12_data, rf12_len, 'r'); 
}



