// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef PALVARIO_h
#define PALVARIO_h

void rf12_sendNowTimeout(uint8_t hdr, const void* ptr, uint8_t len); 

// change this name to ERMdevice and allow for it being the two non-smart ones
class ERMdevice
{
public:
    bool bsoft; 
    uint8_t pinnumber; 
    bool bi2cdevice; 
    bool bworking;  // as in not knackered when we first looked on the line
    uint8_t eff; 
    long mstamp; 
    
    ERMdevice(uint8_t lpinnumber); 
    uint8_t readRegister8(uint8_t reg); 
    void writeRegister8(uint8_t reg, uint8_t val); 
    
    void StartBuzz(uint8_t leff, long lmstamp); 
    void CompleteBuzz(long lmstamp);
     
    void BeginERM(); 
    bool CalibrateERM(uint16_t ch); 
};

class HarnessUnit
{
public:
    ERMdevice palvariosoft; 
    ERMdevice palvariohard; 
    ERMdevice palvarioA; 
    ERMdevice palvarioB; 
    
    long mssevts[4]; // start time of this event
    long msnevts[4]; // next event change time
    char chevtstates[4];  // next event state
    char chcevtstates[4]; // current state
    bool SetChevState(uint8_t i, char nextchevstate, char chchange, long msstamp, long newmsnevtdisp); 
    bool NextChevState(uint8_t i, long mstamp, char chevtstateoverride='.'); 

    float virtuallagaltitude; 
    float receivedaltitude; 
    float simulatedvelocitymps; // factored in every 100ms, disabled by zero
    long mstimestampreceivedaltitude; 
    long mstampaltcycle;        // apply change every 100ms
    
    long mstampchestbuzzstart; 
    uint8_t bchestleft; 
    long mstamplegbuzzstart; 
    uint8_t blegleft; 
    
    void ApplyVelocity(); 

    HarnessUnit(); 
    void StartUp(); 
    void OrgEffectLoop(); 
    void BlinkEffect(uint8_t eff); 
};


void interruptSetup(); 


#endif

