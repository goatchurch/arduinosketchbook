// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include "SDlogging.h"

// have added line into SdVolume::cacheRawBlock() in sdVolume.cpp to stop crash in mkdir
// if (!sdCard_) return false; // added in to stop crashing in mkdir

#define P(X) Serial.print(X)

SDlogger::SDlogger(uint8_t lchipselect, const char* dirname) :
    SDloggerBase(lchipselect, dirname)
{;}

void SDlogger::close()  {  sdfile.close();  sdstate = 1;  }
void SDlogger::flush()  
{
    if (pinLEDforflush != -1)
        analogWrite(pinLEDforflush, 100); 
    sdfile.flush();  
    mstamplastflush = mstamplaststartline; 
    Nlinessincelastflush = 0; 
    Ncharssincelastflush = 0; 
    if (pinLEDforflush != -1)
        analogWrite(pinLEDforflush, 0); 
}
bool SDlogger::writestring(const char* str) 
{
    return sdfile.write((uint8_t*)str, strlen(str)); 
}

size_t SDlogger::write(uint8_t v) 
{
    return sdfile.write(v); 
}


void SDlogger::SDprintStats()
{
    logfilename[nlogfileslash] = '\0';  // terminate the string at the slash

    sdfile = SD.open(logfilename, O_READ); 
    while(true) {
        File entry =  sdfile.openNextFile();
        if (!entry) 
            break; 
        P(entry.name());
        if (entry.isDirectory()) {
            P("/");
        } else {
            P("\t\t");
            P(entry.size()); 
        }
        P("\n"); 
    }
    sdfile.close(); 

    logfilename[nlogfileslash] = '/';  
}


void SDlogger::SetupSD()  
{  
    pinMode(chipselect, OUTPUT);
    bool bsucc = SD.begin(chipselect); 
    sdstate = (bsucc ? 1 : 0); // 0 for dead 
    P("sdstate="); 
    P(sdstate); 
    P(bsucc ? "good":"bad"); 
    P("\n"); 
}

bool SDlogger::opennextlogfileforwriting(const char* pisodatetime, uint32_t mstamp)
{
    if (sdstate > 1) {
        close(); 
        sdstate = 1; 
    }
    if (sdstate == 0) {
        P("sd already dead\n"); // crashes if you do from failed state
        return false; 
    }
    logfilename[nlogfileslash] = '\0';  // terminate the string at the slash
    if (!SD.exists(logfilename)) {
        P("qmaking directory: "); P(logfilename); P("\n"); 
        if (!SD.mkdir(logfilename)) {
            P("mkdir failed\n"); 
            return false; 
        }
    }
    logfilename[nlogfileslash] = '/'; 
    setlogfilename(logfilenumber);  
    while (SD.exists(logfilename)) {
        if ((logfilenumber % 10) == 0) {  P(logfilename);  P(" exists \n"); }
        logfilenumber++; 
        setlogfilename(logfilenumber);  
    }

    // reuse last file if size zero
    if (logfilenumber != 0) {
        setlogfilename(logfilenumber - 1);  
        sdfile = SD.open(logfilename, O_READ); 
        P(logfilename); P(" size: "); P(sdfile.size()); P("\n"); 
        if (sdfile.size() == 0) 
            logfilenumber--; 
        else 
            setlogfilename(logfilenumber);  
        sdfile.close(); 
    }

    Nloglines = 0; 
    Nloglinesfromserialbuff = 0; 
    Nloglinesfailed = 0; 
    Nlinessincelastflush = 0; 
    Ncharssincelastflush = 0; 
    mstamplastsuccess = mstamp; 
    mstamplaststartline = mstamp; 
    mstamplastflush = mstamp; 
    
    P("Openinglogfile: "); P(logfilename); P("\n"); 
    sdfile = SD.open(logfilename, O_RDWR | O_CREAT | O_APPEND); 

    sdstate = 4; 

    return true; 
}


void SDlogger::writeline(int offs)
{
    sddata[offs++] = '\n'; 
long m0 = millis();  
    if (sdfile.write(sddata, offs)) {
long mD = millis() - m0; 
if (mD > 10) {
    P("Long sdfile.write call "); 
    P(mD); 
    P(" cmd:"); 
    P(sddata[0]); 
    P(" logl "); 
    P(Nloglines); 
    P(" chrs "); 
    P(Ncharssincelastflush); 
    P("\n"); 
} 
        Nloglines++; 
        Nlinessincelastflush++; 
        Ncharssincelastflush += offs; 
        mstamplastsuccess = mstamplaststartline; 
    } else {
        Nloglinesfailed++; 
        if (Nloglinesfailed == 10) 
            P("At least 10 log lines failed\n"); 
        if ((sdstate != 0) && (Nloglinesfailed > 100) && (mstamplaststartline - mstamplastsuccess > 10000) && (mstamplaststartline - mstamplastflush > 10000)) {
            P("Attempting to reconnect to SD card file:"); 
            P(logfilename); 
            P("\n"); 
            SetupSD(); 
            P("SetupSD done\n"); 
            if ((sdstate != 0) && SD.exists(logfilename)) {
                File lsdfile = SD.open(logfilename, O_READ); 
                P(logfilename); P(" size: "); P(lsdfile.size()); P("\n"); 
                lsdfile.close(); 
                P("Reopeninglogfile: "); P(logfilename); P("\n"); 
                sdfile = SD.open(logfilename, O_RDWR | O_CREAT | O_APPEND); 
                Nloglinesfailed = 0; 
            }
            mstamplastflush = mstamplaststartline; 
        }
    }
}
