import re, time, math
fname = "/home/goatchurch/datalogging/templogs/038.TXT"
secondsfac = 60

digpower = [1 << 4*i  for i in range(16) ]

def parseline(line):
    res = { "C":line[0] }
    for k, v, v1 in re.findall('([a-z]+)(?:([0-9A-F]+)|"([^"]*)")', line[1:]):
        if not v1:
            bneg = k not in "w" and re.match("[89A-F]", v[0])
            res[k] = int(v, 16) - (bneg and digpower[len(v)] or 0)
        else:
            res[k] = v1  # quoted string
    return res

fin = open(fname)
while not re.match("\s*$", fin.readline()):  pass
plines = [parseline(line)  for line in fin]
print(len(plines))
plines[50001:50050]
sendactivity("clearalltriangles")
sendactivity("clearallcontours")
for i in range(3):
    cont = [(pl["t"]*0.001/secondsfac, pl["c"]/16.0)  for pl in plines  \
             if pl["C"] == 'D' and pl['i'] == i]
    sendactivity("contours", contours=[cont], materialnumber=3)
    print(len(cont))


sendactivity("contours", contours=[[(0,c), (1000,c)]  for c in range(21)], materialnumber=0)
sendactivity("contours", contours=[[(0,c), (1000,c)]  for c in range(0,21,5)], materialnumber=3)
sendactivity("contours", contours=[[(c,0), (c,20)]  for c in range(0,40)], materialnumber=0)


cont = [(pl["t"]*0.001/secondsfac, pl["c"]*0.01)  for pl in plines  \
         if pl["C"] == 'B']
print(len(cont))
sendactivity("contours", contours=[cont], materialnumber=3)

cont = [(pl["t"]*0.001/secondsfac, pl["p"]*0.001-99)  for pl in plines  \
         if pl["C"] == 'B']
sendactivity("contours", contours=[cont], materialnumber=2)

cont = [(pl["t"]*0.001/secondsfac, pl["c"]*0.01-15)  for pl in plines  \
         if pl["C"] == 'B']
sendactivity("contours", contours=[cont], materialnumber=3)


Cpl = {}; [Cpl.setdefault(pl["C"], pl)  for pl in plines  if pl["C"] not in Cpl]
Rpl = Cpl["R"]
latdivisorE100, latdivisorN100 = Rpl["e"], Rpl["n"] 
lngdivisorE100, lngdivisorN100 = Rpl["f"], Rpl["o"]
print(latdivisorE100, latdivisorN100)
print(lngdivisorE100, lngdivisorN100)
#latdivisorE100, latdivisorN100 = 39076, 539
#lngdivisorE100, lngdivisorN100 = 902, -65421

# the GPS location
Qpl = Cpl["Q"]

conts = [ ]
vconts = [ ]
for pl in plines:
  if pl["C"] == 'Q':
    rx = 100*(pl["x"]-Qpl["x"])
    ry = 100*(pl["y"]-Qpl["y"])
    me, mn = ry/latdivisorE100+rx/lngdivisorE100, ry/latdivisorN100+rx/lngdivisorN100
    conts.append((me, mn))
  if pl["C"] == 'V':
    d = pl["d"]
    if d < 0:  d += 65536
    rt = math.radians(d/100)
    vt = pl["v"]/1000
    vconts.append([(me, mn), (me+math.sin(rt)*vt, mn+math.cos(rt)*vt)])
sendactivity("contours", contours=[conts], materialnumber=3)
#sendactivity("contours", contours=vconts[::5], materialnumber=0)
len(conts)

# wind
sendactivity("clearallcontours")
conts = [(pl["t"]*0.001/secondsfac, (80000./pl["w"]))  \
         for pl in plines  if pl["C"] == 'W']
sendactivity("contours", contours=[conts], materialnumber=1)
sendactivity("contours", contours=[[(0,0), (1000,0)]], materialnumber=0)

# temperature
cont = [((pl["t"]*0.001), (pl["c"]/16*10))  \
         for pl in plines  if pl["C"] == 'D' and pl["i"] == 0 ]
sendactivity("contours", contours=[cont], materialnumber=3)
sendactivity("contours", contours=[[(0,0), (1000,0)]], materialnumber=0)



