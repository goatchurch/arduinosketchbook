// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef SDLOGGING_h
#define SDLOGGING_h

// must have this in the ino file
#include <SD.h>
#include "SDloggingBase.h"

class SDlogger : public SDloggerBase
{
protected:
    File sdfile;
public:
    SDlogger(uint8_t lchipselect, const char* dirname); 
    virtual void close(); 
    virtual void flush(); 
    virtual bool writestring(const char* str);
    virtual void writeline(int offs); 
    virtual size_t write(uint8_t v); 
    
    virtual void SetupSD(); 
    virtual void SDprintStats();  // if you call this after log file is open, it drops the handle
    virtual bool opennextlogfileforwriting(const char* pisodatetime, uint32_t mstamp); 
};

#endif
