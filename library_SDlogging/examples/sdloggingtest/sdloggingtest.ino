#include <SPI.h>
#include <SD.h>
//#include <SdFat.h>
#include "SDloggingBase.h"
#include "SDlogging.h"
#include "serialinputbuffer.h"

SDlogger sdlogger(2, "OOR"); 
#define P(X) Serial.print(X)

const char* pisotimestamp = "2010-01-01T13:09:09"; 

void printDirectory(File dir, int numTabs) {
   while(true) {
     
     File entry =  dir.openNextFile();
     if (! entry) {
       // no more files
       break;
     }
     for (uint8_t i=0; i<numTabs; i++) {
       Serial.print('\t');
     }
     Serial.print(entry.name());
     if (entry.isDirectory()) {
       Serial.println("/");
       printDirectory(entry, numTabs+1);
     } else {
       // files have sizes, directories do not
       Serial.print("\t\t");
       Serial.println(entry.size(), DEC);
     }
     entry.close();
   }
}


SensorSerialReceiver ssr(&Serial1); 


void setup() 
{
  delay(2000); 
    Serial.begin(9600);
    delay(2000);
    Serial.println("hi there"); 
    Serial.println("hi there2"); 
  pinMode(2, OUTPUT);
  SD.begin(2); 
  File root = SD.open("/");
  printDirectory(root, 0);
//while(1); 

    delay(2000);
    Serial.println("hi there"); 
    Serial.println("hi there2"); 
    sdlogger.SetupSD(); 
    Serial.println("setup"); 
    sdlogger.SDprintStats(); 
    Serial.println("stats done"); 
    
    const char* pisotimestamp = "2010-01-01T13:09:09"; 
    sdlogger.opennextlogfileforwriting(pisotimestamp, millis()); 
    sdlogger.writelogfileheader(millis(), pisotimestamp, 0); 

    ssr.SetupSerial(750000); 

}


void loop() 
{
    sdlogger.loggpscoeffs(millis(), pisotimestamp, 1, 2, 3, 4); 
    delay(20);  
    if (ssr.RRecCheck()) {
            sdlogger.logserialbuff(ssr.buff, ssr.bufflen); 
    }
       

    P("Nlog "); 
    P(sdlogger.Nloglines); 
    P(" -"); 
    P(sdlogger.Nloglinesfailed); 
    P("\n"); 
    
}
