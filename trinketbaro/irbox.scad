
wt = 2;   // wall thickness
bv = 1;   // hull bevel
eps = 0.02;
ovl = 3; // lid overlap
baseheight = 16.5; 
baselength = 30.0; 
basewidth = 19.5; 


module positive()
{
	union() {
		cube([basewidth, baselength, baseheight]);
		translate([2.5, -15, 0])
			cube([12, 25, baseheight]); 
		translate([6.0, -15, -3.3])
			cube([6, 18, baseheight]); 
		translate([5.0, -17.5, -3.3])
			cube([8, 18, baseheight]); 
		translate([11.4, baselength+wt*2+1, 4.3+5.1])
			rotate([90, 0, 0])
				cylinder(h=10, r=4.3, center=false, $fn=50);
	};
}

module outerbase()
{
	union() {
		hull() {
			translate([-wt,-wt,-wt])
				cube([basewidth+2*wt, baselength+2*wt, baseheight+wt-eps]);
			translate([0,0,-wt-bv])
				cube([basewidth, baselength, baseheight+wt-eps]);
		}
		intersection() {
			translate([11.4, baselength+wt*2, 4.3+5.1])
				rotate([90, 0, 0])
					cylinder(h=10, r=4.3+wt, center=false, $fn=50);
			translate([-wt,-wt,-wt])
				cube([basewidth+2*wt, baselength+2*wt+wt, baseheight+wt-ovl-wt-eps-0.5]);
		}
	}
}

module base()
{
	union() {
		difference() {
			outerbase(); 
			positive(); 
		}
	}
}

module lid() 
{
  union() {
    difference() {
		union() {
			hull() {
				translate([-wt-wt,-wt-wt,-wt+baseheight-ovl])
					cube([basewidth+4*wt, baselength+4*wt, ovl+wt+wt-eps]);
				translate([0,0,-wt+baseheight-ovl+bv])
					cube([basewidth, baselength, ovl+wt+wt-eps]);
			};
		};
		union() {
			outerbase(); 
			positive(); 
		}
    }
  }
}

union() {
translate([0,0,wt+bv])
	base(); 

translate([-20,0,baseheight+bv+wt]) rotate([0,180,0])
		lid();

};


