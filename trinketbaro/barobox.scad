
wt = 2;
bv = 1; 
eps = 0.02;
ovl = 3; 

module positive()
{
	union() {
		cube([40, 55, 16]);
		translate([2.5,35,0])
			cube([12, 25, 16]); 
	};
}

module outerbase()
{
	hull() {
		translate([-wt,-wt,-wt])
			cube([40+2*wt, 55+2*wt, 16+wt-eps]);
		translate([0,0,-wt-bv])
			cube([40, 55, 16+wt-eps]);
	}
}

module base()
{
	difference() {
		outerbase(); 
		positive(); 
	}
}

module lid() 
{
	difference() {
		union() {
			hull() {
				translate([-wt-wt,-wt-wt,-wt+16-ovl])
					cube([40+4*wt, 55+4*wt, ovl+wt+wt-eps]);
				translate([0,0,-wt+16-ovl+bv])
					cube([40, 55, ovl+wt+wt-eps]);
			};
			difference() {
				translate([-wt-wt-ovl+eps,16,12])
					cube([3,20,5]);
				translate([-wt-wt-ovl+eps+1.3,18,11])
					cube([1.7+eps,16,7+eps]);
			};
			difference() {
				translate([ovl+eps+41,16,12])
					cube([3,20,5]);
				translate([ovl-eps+0+41,18,11])
					cube([1.7+eps,16,7+eps]);
			};

		};
		union() {
			outerbase(); 
			positive(); 
		}
	}
}

union() {
translate([0,0,wt+bv])
base(); 

translate([-20,0,17+wt])
rotate([0,180,0])
lid();
};
