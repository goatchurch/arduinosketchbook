
wt = 2;   // wall thickness
bv = 1;   // hull bevel
eps = 0.02;
ovl = 5; // lid overlap
basewidth = 27; 
baselength = 36.0; 
baseheight = 17.8; 

module positive()
{
	union() {
		cube([basewidth, baselength, baseheight]);
		translate([-5, 8, 0])
			cube([12, 14, baseheight]); 
		translate([-6, 10, -10])
			cube([9, 10, baseheight]); 
	};
}


module outerbase()
{
   union() {
		translate([-wt,-wt,-wt])
			cube([basewidth+2*wt, baselength+2*wt, baseheight+wt-eps]);
	}
}

module base()
{
	union() {
		difference() {
			outerbase(); 
			positive(); 
		}
	}
}

module lid() 
{
  union() {
    difference() {
		union() {
			translate([-wt-wt,-wt-wt,-wt+baseheight-ovl])
				cube([basewidth+4*wt, baselength+4*wt, ovl+wt+wt-eps]);
		};
		union() {
			//outerbase(); 
			translate([-wt-0.05,-wt-0.05,-wt])
				cube([basewidth+2*wt+0.05, baselength+2*wt, baseheight+wt-eps]);
			//positive(); 
			union() {
				cube([basewidth, baselength, baseheight]);
				translate([-5, 8, -3])
					cube([12, 14, baseheight]); 

				translate([4.7, 10.5, wt*2+eps])
					cube([16.3, 16.0, baseheight]);
				translate([2.0, 21.5, wt*2+eps])
					cube([2.0, 3.0, baseheight]);
			}
		}
    }
  }
}

union() {
translate([0,0,wt])
	base(); 
translate([-20,0,baseheight+wt])  rotate([0,180,0])
		lid();

};

