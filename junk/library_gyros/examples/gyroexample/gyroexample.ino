#include <Wire.h>
#include "gyros.h"

#define P(X) Serial.print(X)

GyroData gyrodata; 

void setup() 
{
    Serial.begin(9600);
    Wire.begin(); 
    gyrodata.SetupGyro(); 
}

int n = 0; 
void loop() 
{
    delay(20);
    
    long t0 = millis(); 
    gyrodata.ReadGyro(); 
    long tread = millis() - t0; // mstamp is offset back by filter delay
    P(tread); 
    P("ms "); 
    P(" AcX = ");   P(gyrodata.AcX);
    P(" | AcY = "); P(gyrodata.AcY);
    P(" | AcZ = "); P(gyrodata.AcZ);
    P(" | Tmp = "); P(gyrodata.Tmp/340.00+36.53);  //equation for temperature in degrees C from datasheet
    P(" | GyX = "); P(gyrodata.GyX);
    P(" | GyY = "); P(gyrodata.GyY);
    P(" | GyZ = "); P(gyrodata.GyZ);
    P("\n"); 
    
    if (n++ == 50)
        gyrodata.PrintSettings(); 
}


