// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef BARORECEIVER_H
#define BARORECEIVER_H

#define USE_BAROPINCHANGEINTERRUPT_FILTER_BOGUS_SIGNALS 1

// this is compatible with the gemmacode file installed on the Trinket

// it is also reused for the adctmp36 board with the same protocol and slightly different temp format
class BaroReceiver
{
public:
    volatile int16_t temp;  
    volatile uint8_t seq;     // real sequence from first block reading
    volatile uint8_t tempseq; // incremented sequence
    volatile int32_t Pr; 
    volatile int32_t Prseq;   // incremented sequence
    volatile int16_t mtimeslipworst; 
    volatile int16_t Dbytecount;  

    volatile uint32_t m0; 
    volatile uint32_t m05; 
    volatile uint32_t m1; 
    volatile int m2; 
    volatile int m3; 
    volatile int m4; 

    uint16_t gemmambaud;  // programmed in as 180, but correction to teensy clock at receiving end and with other delays gets this set by observation
    int16_t sliptimelimit; 
    
    volatile int lmtimeslipworst; 
#if USE_BAROPINCHANGEINTERRUPT_FILTER_BOGUS_SIGNALS
    bool bbaropinstate;   // attempt to filter out bad interrupts
#endif
    volatile int n5; 

    volatile int16_t recb;  
    volatile uint8_t bitcount; 
    volatile uint8_t bit6lo; 
    volatile uint8_t bit6mid; 

    volatile int32_t m80; 
    volatile int32_t mC0; 

    uint8_t interruptnumber;  // on the uno interruptnumber 1 is pin 3, but on the teensy the numbers are the same
    uint8_t pininterruptnumber; 
    uint8_t itmp36; 

    void barointerrupt(); 
    void barointerruptmeasure();
    BaroReceiver(uint8_t lpininterruptnumber, uint8_t linterruptnumber, uint16_t lgemmambaud, int16_t lsliptimelimit, uint8_t itmp36); 

    bool BAROmeasuremicros(); 
    void BAROsetinterrupts(); 
};

#endif
