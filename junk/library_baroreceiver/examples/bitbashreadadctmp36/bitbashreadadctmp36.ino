// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include <Arduino.h>
#include "baroreceiver.h"

#define P(X) Serial.print(X)

BaroReceiver baroreceiver(4, 4, 190, 2);    // teensy version
//BaroReceiver baroreceiver(16, 16);   // teensy version using baro slot
//BaroReceiver baroreceiver(3, 1);   // Uno version

void setup()
{
    Serial.begin(9600);   
    Serial.print("Check trinket\n"); 
    // pinMode(4, INPUT); while(1) { P(digitalRead(4)); P("\n"); delay(20); }
    //while(1)  baroreceiver.BAROmeasuremicros(); 
    baroreceiver.BAROmeasuremicros();
    baroreceiver.BAROsetinterrupts(); 
}

int prevtempseq = 0; 
long microstamp = 0; 
long mstamp = 0; 
long nb = 0; 
int lastprplustemp; 
void loop()                     // run over and over again
{
    long mstamp = millis(); 
    while (prevtempseq == baroreceiver.tempseq) {
        if (millis() > mstamp + 1000) {
            P(baroreceiver.Dbytecount); 
            P("no signal\n"); 
            mstamp = millis(); 
        }
    }
    prevtempseq = baroreceiver.tempseq; 

    P("  temp: "); 
    int16_t ftemp = baroreceiver.temp | ((baroreceiver.seq & 0x0F) << 12); 
    int16_t fseq = (baroreceiver.seq >> 4) & 0x03; 
    float mvolts = 1000*1.024 * ftemp / 32767.0; 
    float tmpforoled = (mvolts - 500)/10; 
    P(ftemp);  
    P(" #"); 
    P(fseq); 
    P("  "); 
    P(tmpforoled); 
    P("C"); 
    P("  sliptimes:"); 
    P(baroreceiver.mtimeslipworst); 
    P("\n"); 
}
