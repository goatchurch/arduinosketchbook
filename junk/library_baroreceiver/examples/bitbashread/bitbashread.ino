// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include <Arduino.h>
#include "baroreceiver.h"

#define P(X) Serial.print(X)

uint16_t gemmabaud = 190; // 188 on device0, 191 on device1
int16_t sliptimelimit = 6;  // value quite high (was 2).  
                            // maybe should invest in error correcting code

BaroReceiver baroreceiver(21, 21, gemmabaud, sliptimelimit, 1);  
//BaroReceiver baroreceiver(3, 1);  // Uno kind

void setup()
{
    Serial.begin(9600);   
    Serial.print("Check trinket\n"); 

    //while(1)  baroreceiver.BAROmeasuremicros(); 
    baroreceiver.BAROmeasuremicros();
    baroreceiver.BAROsetinterrupts(); 
}

int32_t prevPrseq = 0; 
long microstamp = 0; 
long mstamp = 0; 
long nb = 0; 
int lastprplustemp; 
int nsamples = 0; 
int nfailures = 0; 
void loop()                     // run over and over again
{
    long mstamp = millis(); 
    while (prevPrseq == baroreceiver.Prseq) {
        if (millis() > mstamp + 1000) {
            Serial.println("no signal"); 
            mstamp = millis(); 
        }
    }
    
    if (nsamples > 1200) {
        nsamples = 0; 
        nfailures = 0; 
        P("reset failure count\n"); 
    }
    prevPrseq = baroreceiver.Prseq; 
    nsamples++; 

    int prplustemp = baroreceiver.Pr + baroreceiver.temp; 
    bool bprchange = (abs(lastprplustemp - prplustemp) > 5); 
    if ((baroreceiver.sliptimelimit != -1) && (baroreceiver.mtimeslipworst > baroreceiver.sliptimelimit))
        nfailures++; 

    if (true || (baroreceiver.seq == 0) || bprchange) {
        P("  baro: "); 
        P(baroreceiver.Pr);  
        P(" btemp: "); 
        P(baroreceiver.temp); 
        P(" #"); 
        P(baroreceiver.seq); 
        P("  sliptime "); 
        P(baroreceiver.mtimeslipworst); 
        P("  ps "); 
        P(baroreceiver.Prseq); 
        P("  failurerate:"); 
        int tpercent = nfailures*1000/nsamples; 
        P(tpercent/10); 
        P("."); 
        P(tpercent%10); 
        P('%'); 
        P("\n"); 
        lastprplustemp = prplustemp; 
    }
    return; 


    long newmicrostamp = micros(); 
    P("   "); 
    P(newmicrostamp - microstamp); 
    P("\n"); 
    microstamp = newmicrostamp; 

    P(baroreceiver.seq < 10 ? "0" : ""); 
    P(baroreceiver.seq); 
    P(" T="); 
    P(baroreceiver.temp*0.01); 
    P("  Pr="); 
    P(baroreceiver.Pr); 
}



