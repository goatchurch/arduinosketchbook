// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include "baroreceiver.h"

// TMP36 style:
// if (prevtempseq != baroreceiver.tempseq) 
//   int16_t ftemp = baroreceiver.temp | ((baroreceiver.seq & 0x0F) << 12); 
//   int16_t fseq = (baroreceiver.seq >> 4) & 0x03; 
//   float mvolts = 1000*1.024 * ftemp / 32767.0; 
//   float tmpforoled = (mvolts - 500)/10; 
// Or with the original baro, the record advance is on baroreceiver->Prseq

#define P(X) Serial.print(X)

// http://en.wikipedia.org/wiki/Barometric_formula
//float L0 = -0.0065  // lapse rate K/m
//float T0 = 15 + 273.15   // standard temperature (at sea level)
//float P0 = 101325.00 // standard pressure at sea level
// P = P0 * ( T0 / (T0 + L0 * alt))**(9.80665 * 0.0289644 / (8.31432 * L0))

// given an altitude, temp and pressure at start, work out a rough conversion factor before take-off
// print(p/((T0 / (T0 + L0 * alt))**(9.80665 * 0.0289644 / (8.31432 * L0))))
// P = P0 * ( T0 / (T0 + L0 * alt))**(9.80665 * 0.0289644 / (8.31432 * L0))

// http://www.hills-database.co.uk/altim.html

// interrupts can only be attached to global functions, so this dereferences into an object
BaroReceiver* pbaroreceiver1 = 0; 
void barointerrupt1()  {  pbaroreceiver1->barointerrupt();  }
void barointerrupt1measure()  {  pbaroreceiver1->barointerruptmeasure();  }

// tmp36 style interrupts
BaroReceiver* pbaroreceiver2 = 0; 
void barointerrupt2()  {  pbaroreceiver2->barointerrupt();  }
void barointerrupt2measure()  {  pbaroreceiver2->barointerruptmeasure();  }


// this doesn't work very well.  should better mimic the real loading of values to work out the baud rate experimentally
void BaroReceiver::barointerruptmeasure()
{
#if USE_BAROPINCHANGEINTERRUPT_FILTER_BOGUS_SIGNALS
    bool lbstate = digitalRead(pininterruptnumber); 
    if (lbstate == bbaropinstate)
        return; 
    bbaropinstate = lbstate; 
#endif

    uint32_t m = micros(); 
    int16_t md = m - m1; 
#if USE_BAROPINCHANGEINTERRUPT_FILTER_BOGUS_SIGNALS
    if (md < 10)
        return; 
#endif
    
    
    n5++; 
    int16_t dmd = abs(md - gemmambaud); 
    if (dmd < gemmambaud/2) {
        m2++; 
        if (m2 > 0) {
            m0 += md; 
            if (dmd < gemmambaud/5) {
                m4++;   // closer in count
                m05 += md; 
            }
        }
    } else {
        m3++; 
    }    
    m1 = m; 
}


BaroReceiver::BaroReceiver(uint8_t lpininterruptnumber, uint8_t linterruptnumber, uint16_t lgemmambaud, int16_t lsliptimelimit, uint8_t litmp36)
{
    pininterruptnumber = lpininterruptnumber; 
    interruptnumber = linterruptnumber; 
    itmp36 = litmp36; // 0 or 1 to say if this is a tmp36 type and to set the interrupts (redundant)
    
    temp = 0x0000;  
    seq = 0x00; 
    tempseq = 0; 
    Pr = 0x00000000; 
    Prseq = 0; 

    gemmambaud = lgemmambaud; 
    sliptimelimit = lsliptimelimit; 
    
    n5 = 0; 

    recb = 0x0000;  
    bitcount = 0x00; 
    bit6lo = 0x00; 
    bit6mid = 0x00; 
    Dbytecount = 0; 
}


bool BaroReceiver::BAROmeasuremicros()
{
    m0 = 0;    // sum
    m1 = 0;    // previous micros()
    m2 = -10;  // count
    m3 = 0; 
    m4 = 0; 
    m05 = 0; 

    if (itmp36 == 1) 
        pbaroreceiver1 = this; 
    else
        pbaroreceiver2 = this; 
    
    pinMode(pininterruptnumber, INPUT);   
    attachInterrupt(interruptnumber, (itmp36 == 1 ? barointerrupt1measure : barointerrupt2measure), CHANGE);
    P("measuring interruptpinnumber ");
    P(interruptnumber); 
    P(" on pin ");
    P(pininterruptnumber); 
    P("\n"); 
    delay(2000); 
    detachInterrupt(interruptnumber);
    if (m2 == -10) {
        P("No short changes; total changes: "); 
        P(m3); 
        P("\n"); 
        return false; 
    }
    if (m2 < 5) {
        P("Too few short changes\n"); 
        return false; 
    }
    //gemmambaud = (m0 + (m2/2))/m2; 
    P("gemmambaud = "); P((m0 + (m2/2))/m2); 
    P(" closer: "); P((m05 + (m4/2))/m4); 
    P("\n"); 
    P(" n="); P(m2); P("(closer: "); P(m4); P(")"); P("  "); P(m0); P("  avg "); P(m0*1.0/m2); P("  discards: "); P(m3); P("\n"); 
    return true; 
}


void BaroReceiver::barointerrupt()
{
#if USE_BAROPINCHANGEINTERRUPT_FILTER_BOGUS_SIGNALS
    bool lbstate = digitalRead(pininterruptnumber); 
    if (lbstate == bbaropinstate)
        return; 
    bbaropinstate = lbstate; 
#endif
    uint32_t m = micros(); 
    uint32_t md = m - m0; 
    
    // big distance from start of changes, restart the buffer
    if (md > gemmambaud * 15) {
        recb = 0x8000; 
        bitcount = 0x00; 
        m0 = m; 
        return; 
    }
    
    // start shifting the bits up
    uint8_t gbits = ((md - bitcount * gemmambaud + gemmambaud / 2) / gemmambaud); 
    
    int32_t mtimeslip = abs(md - (bitcount + gbits) * gemmambaud); 
    if (mtimeslip > lmtimeslipworst)
        lmtimeslipworst = mtimeslip; 
    
    recb = (recb >> 1) ^ 0x8000; 
    recb = recb >> (gbits - 1); 
    bitcount += gbits; 
    if ((recb & 0x007F) == 0)
       return;  
        
    // we have a value
    Dbytecount++; 
    uint16_t lrecb = recb; 
    while (lrecb & 0x3F)
        lrecb = lrecb << 1; 
    uint8_t rec6 = lrecb >> 8; 

    // interpret the top bits into which slot they belong    
    uint8_t trec6 = rec6 & 0xC0; 
    //Serial.print(rec6, HEX); 
    
    // data sequence given in trinketbaro.ino is:
    //    sendbitbangbyte(0x00 | (uint8_t)(TEMP & 0x3F)); 
    //    sendbitbangbyte(0x40 | (uint8_t)((TEMP >> 6) & 0x3F)); 
    //    sendbitbangbyte(0x80 | (uint8_t)(m20count & 0x3F)); 

    //    sendbitbangbyte(0x00 | (uint8_t)(Pr & 0x3F)); 
    //    sendbitbangbyte(0x40 | (uint8_t)((Pr >> 6) & 0x3F)); 
    //    sendbitbangbyte(0xC0 | (uint8_t)((Pr >> 12) & 0x3F)); 
    
    // data sequence given in trinketadctmp36.ino is:
    //    sendbitbangbyte(0x00 | (uint8_t)(TEMP & 0x3F)); 
    //    sendbitbangbyte(0x40 | (uint8_t)((TEMP >> 6) & 0x3F)); 
    //    sendbitbangbyte(0x80 | (uint8_t)((TEMP >> 12) & 0x0F) | (uint8_t)((m20count << 4) & 0x30)); 


    // low 6bits
    if (trec6 == 0x00) {
        bit6lo = rec6 & 0x3F; 
        bit6mid = 0x80; 
    }
    
    // middle 6bits
    else if (trec6 == 0x40) {
        bit6mid = (bit6mid & 0x80) | (0x40) | (rec6 & 0x3F); 
    }
    
    // top 6bits when temperature/sequence number
    else if (trec6 == 0x80) {
        m80 = m; 
        if ((bit6mid & 0xC0) == 0xC0) {
            temp = ((int16_t)(bit6mid & 0x3F) << 6) | bit6lo; 
            bit6mid = 0x00; 
            mtimeslipworst = lmtimeslipworst; 
            lmtimeslipworst = 0; 
            seq = rec6 & 0x3F; 
            tempseq++; 
        }
    }
    
    // top 6bits when Pressure
    else {  // (trec6 == 0xC0)
        mC0 = m - m80; 
        if ((bit6mid & 0xC0) == 0xC0) {
            Pr = ((int32_t)(rec6 & 0x3F) << 12) | ((int32_t)(bit6mid & 0x3F) << 6) | bit6lo; 
            bit6mid = 0x00; 
            mtimeslipworst = lmtimeslipworst; 
            lmtimeslipworst = 0; 
            Prseq++; 
        }
    }
}

void BaroReceiver::BAROsetinterrupts()
{
    if (itmp36 == 1)
        pbaroreceiver1 = this; 
    else
        pbaroreceiver2 = this; 
    pinMode(pininterruptnumber, INPUT);   
    attachInterrupt(interruptnumber, (itmp36 == 1 ? barointerrupt1 : barointerrupt2), CHANGE);
}

