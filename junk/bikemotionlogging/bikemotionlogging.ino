#include <SPI.h>
#include <Wire.h>

// change when USE_SDFAT 1
//#include <SdFat.h>  
#include <SD.h>

#include <SFE_MicroOLED.h>
#include "SDlogging.h"
#include "simplegps.h"
#include "simpledatetime.h"
#include "oledoutput.h"
#include "gyros.h"
#include "windmeter.h"
#include "trinketbarobitbashing.h"
#include <OneWire.h>
#include "DX18x20temperature.h"


SDlogger sdlogger(2); 
GpsData gpsdata; 
EastingNorthingLocalLinearTrans enllt; 
GyroData gyrodata; 
OledOutput oled(10, 8, 9);
DallasTemperature dst(21); 
WindReceiver wr; 

#define P(X) Serial.print(X)

void setup() 
{
    Serial.begin(9600);
    pinMode(20, OUTPUT);
    digitalWrite(20, HIGH);
    oled.SetupOLED(); 
    
    sdlogger.SetupSD(); 
    delay(1000);

    pinMode(16, INPUT); 
    BAROmeasuremicros(16);
    BAROsetinterrupts(16); 


    dst.FindAllSensors(); 
    if (dst.ns > 0)
        dst.beginreading(0); 

    sdlogger.SDprintStats(); 
    digitalWrite(20, LOW);


    char* pisotimestamp = isotimestampfromEpoch(Teensy3Clock.get()); 
    sdlogger.opennextlogfileforwriting(pisotimestamp); 
    oled.write_oled(sdlogger.logfilename + 4); 
    delay(2000);
    sdlogger.writelogfileheader(millis(), pisotimestamp); 
    
    gpsdata.GpsSetup(true); 

    Wire.begin(); 
    gyrodata.CheckHardware(); 
    oled.write_xy(-999, -999, -999);  
    
    wr.SetWindpin(17); 
}



uint8_t BAROprevseq = 0; 
void FetchBaro() 
{
    uint8_t BAROseq = BAROGetseq(); 
    if (BAROseq == BAROprevseq)
        return; 
            
    int32_t Pr = BAROGetPr(); 
    int16_t temp100 = BAROGettemp(); 
    long mstamp = millis(); 
    if (BAROseq == 9) {
        P("BARO: t=");
        P(temp100*0.01); 
        P(" Pr="); 
        P(Pr); 
        P("\n"); 
    }
    BAROprevseq = BAROseq; 
    sdlogger.logbaro(mstamp, BAROseq, Pr, temp100); 
}


void FetchGPS()
{
    int igps = gpsdata.GpsLoop(&enllt); 
    if (igps == -1)
        return; 
    int mstamp = millis(); 
    if (igps == 30) 
        sdlogger.loggpscoeffs(mstamp, gpsdata.isotimestamp, enllt.latdivisorE100, enllt.latdivisorN100, enllt.lngdivisorE100, enllt.lngdivisorN100); 
    if ((igps == 30) || (igps == 3))
        sdlogger.loggpsabs(mstamp, gpsdata.latminutes10000, gpsdata.lngminutes10000, gpsdata.altitude10); 
    else if (igps == 2)
        sdlogger.loggpsvel(mstamp, gpsdata.velkph100, gpsdata.veldegrees100); 
}


long millisnextgyroreading = 0; 
void FetchGyro()
{
    if (millisnextgyroreading < millis()) {
        gyrodata.MakeReading(); 
        sdlogger.logcompass(millis(), gyrodata.cpx, gyrodata.cpy, gyrodata.cpz); 
        sdlogger.loggyro(millis(), gyrodata.AcX, gyrodata.AcY, gyrodata.AcZ, gyrodata.GyX, gyrodata.GyY, gyrodata.GyZ); 

        Serial.print("  x: ");
        Serial.print(gyrodata.cpx);
        Serial.print("  y: ");
        Serial.print(gyrodata.cpy);
        Serial.print("  z: ");
        Serial.print(gyrodata.cpz);
        Serial.print("\n");
        millisnextgyroreading = millis() + 100; 
        //write_xy(gyrodata.cpx, gyrodata.cpy, gyrodata.cpz);  
    }
}


long lastwindcount = 0; 
void FetchWind()
{
    long windcount = wr.revcount; 
    if (lastwindcount != windcount) {
        long mstamp = millis(); 
        sdlogger.logwind(mstamp, windcount, wr.revtime); 
        if ((windcount % 10) == 0) {
            P(sdlogger.Nloglines); P("  wind: "); P(wr.revtime);  P("\n"); 
        }
        lastwindcount = windcount; 
    }
}

void FetchDallas()
{
    while (dst.continuereading()) {
        P("T-Sensor "); 
        P(dst.i); 
        P(" "); 
        P(dst.t16values[dst.i]/16.0); 
        P("\n"); 
        
        sdlogger.logdallas(millis(), dst.i, dst.t16values[dst.i]); 
        
        // cycle through the sensors due to difficulty of seeing them at once
        //dst.beginreading(dst.i); 
        dst.beginreading((dst.i + 1) % dst.ns); 
    }
}


long mstampoled = 0; 
void loop() 
{
    long mstamp = millis(); 
    FetchBaro(); 
    FetchGPS(); 
    //FetchGyro(); 
    FetchWind(); 
    FetchDallas(); 
    if (mstamp > mstampoled + 200) {
        oled.write_xy(gpsdata.relE10/10, gpsdata.relN10/10, (int)(gpsdata.velkph100*36/1000)); 
        mstampoled = mstamp; 
    }
}


