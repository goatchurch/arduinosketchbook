#include <OneWire.h>

// annoying hack needed in the OneWire library to make it compile:
//    https://forum.pjrc.com/threads/252-OneWire-library-for-Teensy-3-%28DS18B20%29
    

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X < 16 ? "0" : ""); Serial.print(X, HEX) 
#define PN Serial.println("")

#include <SPI.h>
#include "Adafruit_BLE_UART.h"

boolean bbleworking = true; 

#define ADAFRUITBLE_REQ 5
#define ADAFRUITBLE_RDY 3 // This should be an interrupt pin, on Uno thats #2 or #3
#define ADAFRUITBLE_RST 15

OneWire  ds(4);  

int led_bleworking = 13; 
int led_bledata = 12; 
byte addr[8];
byte nbits = 12; 
int msdelay; 
long millitoread; 
void setup_dallas(void) 
{
    ds.reset_search();
    if (!ds.search(addr)) 
        P("Failed to get address of sensor.");
    P("Sensor ROM = ");
    for(int i = 0; i < 8; i++) 
        Serial.print(addr[i], HEX);
    if (OneWire::crc8(addr, 7) != addr[7]) 
        P(" ** CRC is not valid!");
    if (addr[0] != 0x28)
        P(" ** unexpected chip"); // should be for DS18B20
    PN; 

    ds.write(0x4E); // WRITE SCRATCHPAD
    ds.write(0x11); 
    ds.write(0x22); 
    ds.write((nbits - 9)<<5 | 0x1F); 

    ds.reset();
    ds.select(addr); 
    ds.write(0x48); // COPY SCRATCHPAD to eeprom
    delay(30); 
    
    msdelay = 750; 
    millitoread = millis() + 1000; 
}

int n = 0; 
byte data[12];
float currenttemp; 
bool readtemp()
{
    if (millis() < millitoread)
         return false;    
    byte present = ds.reset();
    P("  Data = ");
    PH(present);
    P(" ");
    ds.select(addr);    
    ds.write(0xBE);         // READ SCRATCHPAD 
    for (int i = 0; i < 9; i++) {           // we need 9 bytes
        data[i] = ds.read();
        PH(data[i]);
    }
    if (OneWire::crc8(data, 8) != data[8]) 
        P(" ** CRC error"); 
    
    
    byte cfg = ((data[4] >> 5) & 3); 
    int16_t raw = (data[1] << 8) | (data[0] & (0xFF << (3 - cfg)));
    
    currenttemp = (float)raw / 16.0;
    P("  Temp="); P(currenttemp);  P(" bits="); P(cfg+9); 
    PN; 
    
    ds.reset();
    ds.select(addr);
    ds.write(0x44, 1); // start conversion, with parasite power on at the end
    millitoread = millis() + 750; 
    return true; 
}



Adafruit_BLE_UART BTLEserial = Adafruit_BLE_UART(ADAFRUITBLE_REQ, ADAFRUITBLE_RDY, ADAFRUITBLE_RST);
void setup_ble() 
{
    BTLEserial.setDeviceName("B_Expt"); /* 7 characters max! */
    BTLEserial.begin();
    bbleworking = true; 
}

void setup(void)
{
  Serial.begin(9600);
  Serial.println(F("Adafruit Bluefruit Low Energy nRF8001 Print echo demo"));
  setup_ble(); 
  pinMode(led_bleworking, OUTPUT); 
  pinMode(led_bledata, OUTPUT);  
  setup_dallas(); 
}

aci_evt_opcode_t laststatus = ACI_EVT_INVALID;
void read_ble()
{
    BTLEserial.pollACI();
    aci_evt_opcode_t status = BTLEserial.getState();
    if ((status == ACI_EVT_DEVICE_STARTED) && (status != laststatus)) {
      digitalWrite(led_bleworking, LOW);
      Serial.println("** Advertising started");
    }
    if ((status == ACI_EVT_CONNECTED) && (status != laststatus)) {
       digitalWrite(led_bleworking, HIGH);
       Serial.println(F("* Connected"));
    }
    if ((status == ACI_EVT_DISCONNECTED) && (status != laststatus)) {
      Serial.println(F("* Disconnected or advertising timed out"));
      digitalWrite(led_bleworking, LOW);
    }
    laststatus = status;
    if (status != ACI_EVT_CONNECTED)
        return; 

    // Lets see if there's any data for us!
    if (BTLEserial.available() == 0)
       return; 
       
    digitalWrite(led_bledata, HIGH);
    String blebuffer = ""; 
    while ((blebuffer.length() < 20) && BTLEserial.available())
        blebuffer += (char)BTLEserial.read();
    Serial.print("Received from ble: \""); 
    Serial.print(blebuffer); 
    Serial.println("\""); 
    digitalWrite(led_bledata, LOW);
}

void write_ble(String s) 
{
  digitalWrite(led_bledata, HIGH);
  // We need to convert the line to bytes, no more than 20 at this time
  uint8_t sendbuffer[20];
  s.getBytes(sendbuffer, 20);
  char sendbuffersize = min(20, s.length());
  Serial.print(F("\n* Sending -> \"")); 
  Serial.print((char *)sendbuffer); 
  Serial.println("\"");
  // write the data
  BTLEserial.write(sendbuffer, sendbuffersize);
  digitalWrite(led_bledata, LOW);

  if (s == "s") {  
    for (int n = 10000; n < 10050; n++) {
        BTLEserial.print(String("BS-")+String(n));  
        BTLEserial.pollACI();
    }
  }
}

void loop()
{
  if (bbleworking) {
   read_ble(); 
  }
  if (Serial.available()) {
    // Read a line from Serial
    Serial.setTimeout(100); // 100 millisecond timeout
    String s = Serial.readString();
    if (bbleworking) {
      write_ble(s);  
    }
  }
  
  write_ble(String((int)(analogRead(A0)/1023.0*5*100))+String("cV"));  
//  if (readtemp()) 
//      write_ble(String((int)(currenttemp*100))+String("hDegs"));  
}

