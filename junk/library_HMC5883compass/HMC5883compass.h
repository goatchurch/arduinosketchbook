// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef HMC5883COMPASS_h
#define HMC5883COMPASS_h

// http://diyhacking.com/arduino-mpu-6050-imu-sensor-tutorial/
class CompassData
{
public:
    bool bcompassfound; 
    int8_t gainconfig; 
    int8_t dataoutputrateconfig; 
    int8_t measurementaverageconfig; 
    
    int16_t cpx, cpy, cpz;   // readings are always between 0xF800 and 0x07FF
    float xdisp, xfac, ydisp, yfac, zdisp, zfac; // calibration factors
    float cvx, cvy, cvz;     // scaled and fitted to the unit sphere
    
    CompassData(); 
    uint32_t Readregisterbytes(int addresslocation, int nbytes);  // 10 for identity, 0 for config/mode
    void Writeregisterbyte(int addresslocation, uint8_t val);  
    bool SetupCompass(); 
    bool ReadCompass(); 
    float CompassSelfTest(bool bnegativebias); 
};


#endif
