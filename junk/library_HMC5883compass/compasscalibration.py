import re, time, math, sys, imp, scipy.optimize
sys.path.append("/home/goatchurch/datalogging/arduinosketchbook/pyscripts")
import parselog

fname = "/home/goatchurch/datalogging/templogs/208.TXT"
Dcompasscalibrateoutput = """

    # paste output of library_HMC5883compass/examples/compasscalibrate into here

"""
if len(compasscalibrateoutput) < 100:
    rl = parselog.RL("/home/goatchurch/datalogging/templogs/208.TXT", sendactivity)
    rawcompassvals = [(pl["x"], pl["y"], pl["z"])  for pl in rl.plines  if pl["C"] == "C"]
else:
    rawcompassvals = [list(map(int, ln.split()[2:]))  for ln in compasscalibrateoutput.splitlines()]

# thin the points to avoid over-sampling in one spot    
seenfac = 10;   # 5 unit increments
pmrange = 500;  # the range limits of the values
pmexp = pmrange*2//seenfac + 2;

cvals = [ ]
seenvals = set()
nvalscanned = 0
for c in rawcompassvals:
    ix = (c[0] + pmrange)//seenfac
    iy = (c[1] + pmrange)//seenfac
    iz = (c[2] + pmrange)//seenfac
    iv = ix + pmexp*(iy + pmexp*iz)
    nvalscanned += 1
    if iv not in seenvals:
        cvals.append(c)
        seenvals.add(iv)
print("total points found", nvalscanned, "selected points", len(cvals))

# plot the raw points
sendactivity("clearalltriangles")
sendactivity("clearallcontours")
sendactivity("clearallpoints")

seenfac = 10;   # 5 unit increments
pmrange = 500;  # the range limits of the values
pmexp = pmrange*2//seenfac + 2;

cvals = [ ]
seenvals = set()
nvalscanned = 0
for c in rawcompassvals:
    ix = (c[0] + pmrange)//seenfac
    iy = (c[1] + pmrange)//seenfac
    iz = (c[2] + pmrange)//seenfac
    iv = ix + pmexp*(iy + pmexp*iz)
    nvalscanned += 1
    if iv not in seenvals:
        cvals.append(c)
        seenvals.add(iv)
print("total points found", nvalscanned, "selected points", len(cvals))
sendactivity("points", points=[(c[0]*0.01, c[1]*0.01, c[2]*0.01)  for c in cvals], materialnumber=1)

# initial guess
xmin, xmax = min(c[0]  for c in cvals), max(c[0]  for c in cvals)
ymin, ymax = min(c[1]  for c in cvals), max(c[1]  for c in cvals)
zmin, zmax = min(c[2]  for c in cvals), max(c[2]  for c in cvals)
x0 = ((xmax + xmin)/2, 2/(xmax - xmin), (ymax + ymin)/2, 2/(ymax - ymin), (zmax + zmin)/2, 2/(zmax - zmin))

# error function
def fun(x):
    xdisp, xfac, ydisp, yfac, zdisp, zfac = x
    return sum((math.sqrt(((c[0] - xdisp)*xfac)**2 + ((c[1] - ydisp)*yfac)**2 + ((c[2] - zdisp)*zfac)**2) - 1.0)**2  for c in cvals)

print(fun((0,1,0,1,0,1)), fun(x0))
bnds = ((-500,500), (0.001,0.05), (-500,500), (0.001,0.05), (-500,500), (0.001,0.05))
g = scipy.optimize.minimize(fun, x0, method=None, bounds=bnds, options={"maxiter":500}) 
print(g)
print(x0)
print(list(g.x))

sendactivity("clearallpoints")
# plot the new points factored
xdisp, xfac, ydisp, yfac, zdisp, zfac = g.x
print("x0 and x err", fun(x0), fun((xdisp, xfac, ydisp, yfac, zdisp, zfac)))
sendactivity("points", points=[((c[0] - xdisp)*xfac, (c[1] - ydisp)*yfac, (c[2] - zdisp)*zfac)  for c in cvals], materialnumber=2)

print("xdisp = %.6f; xfac = %.6f; ydisp = %.6f; yfac = %.6f; zdisp = %.6f; zfac = %.6f;" % tuple(g.x))


# globe shape
r = 1
sendactivity("contours", contours=[[(math.cos(math.radians(th))*math.cos(math.radians(phi))*r, math.sin(math.radians(th))*math.cos(math.radians(phi))*r, math.sin(math.radians(phi))*r)  for th in range(0, 370, 10)]  for phi in range(-90, 100, 10)])
sendactivity("contours", contours=[[(math.cos(math.radians(th))*math.cos(math.radians(phi))*r, math.sin(math.radians(th))*math.cos(math.radians(phi))*r, math.sin(math.radians(phi))*r)  for phi in range(-90, 100, 10)]  for th in range(0, 370, 10)])

