// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <Wire.h>
#include "HMC5883compass.h"

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)

CompassData::CompassData()
{
    bcompassfound = false; 
    dataoutputrateconfig = 0x10*1 + 0x08*0 + 0x04*1;  // 15 times a second
    measurementaverageconfig = 0x40*1 + 0x20*1;       // 8 samples per reading averaged
    gainconfig = 0x80*1 + 0x40*0 + 0x20*0; // +- 4.7 Ga

    // experimental values (could be set at start during shaking or into EEprom)
    //xdisp = -313.365888; xfac = 0.001726; ydisp = 102.240447; yfac = 0.001635; zdisp = -55.093229; zfac = 0.002050;
    xdisp = -76.730667; xfac = 0.004349; ydisp = 11.009579; yfac = 0.004278; zdisp = -51.942233; zfac = 0.005065;
}


float CompassData::CompassSelfTest(bool bnegativebias) 
{
    bcompassfound = (Readregisterbytes(10, 3) == 0x483433); // H43
    if (!bcompassfound)
        return false; 

    uint8_t measurementbiasconfig = (bnegativebias ? 0x02*1 + 0x01*0 : 0x02*0 + 0x01*1); 
    Writeregisterbyte(0x00, measurementaverageconfig | dataoutputrateconfig | measurementbiasconfig);  
    Writeregisterbyte(0x01, gainconfig);  
    Writeregisterbyte(0x02, 0x02*0 + 0x01*0);  // continuous measurement mode

    delay(10); 
    ReadCompass();  // get rid of first reading
    
    for (int i = 0; i < 20; i++) {
        delay(170); 
        ReadCompass();  
        P("self-test "); P(measurementbiasconfig); 
        P(bnegativebias ? "neg" : "pos"); 
        P("  x: "); P(cpx);
        P("  y: "); P(cpy);
        P("  z: "); P(cpz);
        P("\n"); 
    }
    return (fabs(cpx) + fabs(cpy) + fabs(cpz))/3; 
}
 
uint32_t CompassData::Readregisterbytes(int addresslocation, int nbytes) 
{
    Wire.beginTransmission(0x1E); 
    //Wire.write(0x3C); // this is required by the datasheet, but isn't used in practice
    Wire.write(addresslocation);   // advance to identification register
    Wire.endTransmission(); 
    Wire.requestFrom(0x1E, nbytes);
    uint32_t res = 0; 
    for (int i = 0; i < nbytes; i++) 
        res = (res << 8) | (Wire.read() & 0xFF); 
    return res; 
}

void CompassData::Writeregisterbyte(int addresslocation, uint8_t val) 
{
    Wire.beginTransmission(0x1E); //open communication with HMC5883
    Wire.write(addresslocation); //select mode register
    Wire.write(val); //continuous measurement mode
    Wire.endTransmission();
}


bool CompassData::SetupCompass()
{
    // check compass identification
    bcompassfound = (Readregisterbytes(10, 3) == 0x483433); // H43
    if (!bcompassfound)
        return false; 
    
    // 0x02 = mode register, 0x00 = continuous measurement mode
    Writeregisterbyte(0x00, measurementaverageconfig | dataoutputrateconfig);  
    Writeregisterbyte(0x01, gainconfig);  
    Writeregisterbyte(0x02, 0x02*0 + 0x01*0);  // continuous measurement mode
    delay(10); 
    ReadCompass(); 
}

bool CompassData::ReadCompass() 
{
    if (!bcompassfound) {
        if (!SetupCompass())
            return false; 
    }
    
    Wire.beginTransmission(0x1E);
    Wire.write(0x03); //select register 3, X MSB register
    Wire.endTransmission(); 

    //Read data from each axis, 2 registers per axis.  Range is at most +- 0x0800 or 2048
    Wire.requestFrom(0x1E, 6);
    cpx = Wire.read()<<8; //X msb
    cpx |= Wire.read(); //X lsb
    cpz = Wire.read()<<8; //Z msb
    cpz |= Wire.read(); //Z lsb
    cpy = Wire.read()<<8; //Y msb
    cpy |= Wire.read(); //Y lsb
    
    bool bbadreading = ((cpx == -1) && (cpy == -1) && (cpz == -1)); 
    if (bbadreading)
        bcompassfound = false;   // compass possibly lost, so prepare to reboot next time

    cvx = (cpx - xdisp)*xfac; 
    cvy = (cpy - ydisp)*yfac; 
    cvz = (cpz - zdisp)*zfac; 

    return !bbadreading;  
}


