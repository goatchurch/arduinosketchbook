#include <Wire.h>
#include "HMC5883compass.h"

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)

CompassData compassdata; 

// used to ensure a distribution of measurements
int Nseenvals = 500; 
int seenfac = 5;   // 5 unit increments
int pmrange = 500; // the range limits of the values
int pmexp = pmrange*2/seenfac + 2;
int *seenvals; 

int seenvalI = 0; 
bool bseenvalwrapping = false; 

void setup() 
{
    Serial.begin(9600);
    P("hi there");
    seenvals = (int*)malloc(Nseenvals*sizeof(int)); 
    
    Wire.begin(); 
    uint32_t compassiddent = compassdata.Readregisterbytes(10, 3);  
    uint32_t compassconfig = compassdata.Readregisterbytes(0, 3);
    compassdata.SetupCompass(); 
}

void loop() 
{
    delay(67); 
    if (!compassdata.ReadCompass()) 
        P("Bad\n"); 
        
    int ix = (compassdata.cpx + pmrange)/seenfac;
    int iy = (compassdata.cpy + pmrange)/seenfac;
    int iz = (compassdata.cpz + pmrange)/seenfac;
    int iv = ix + pmexp*(iy + pmexp*iz); 
    
    for (int i = seenvalI - 1; i >= 0; i--) {
        if (seenvals[i] == iv)
            return; 
    }
    if (bseenvalwrapping) {
        for (int i = Nseenvals - 1; i >= seenvalI; i--) {
          if (seenvals[i] == iv)
              return; 
        }
    }
    if (seenvalI == Nseenvals) {
        seenvalI = 0; 
        bseenvalwrapping = true;
    }
    seenvals[seenvalI] = iv; 
    P(seenvalI); 
    P(" "); 
    P(iv); 
    P("  "); 
    seenvalI += 1; 
    P(compassdata.cpx);
    P(" ");
    P(compassdata.cpy);
    P(" ");
    P(compassdata.cpz);
    P("\n"); 
}

