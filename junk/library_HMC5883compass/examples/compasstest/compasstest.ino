#include <Wire.h>
#include "HMC5883compass.h"

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)

CompassData compassdata; 

void setup() 
{
    Serial.begin(9600);
    P("hi there"); 
    delay(2000); 
    Wire.begin(); 
    
    P("Indenfication bytes: "); 
    uint32_t compassiddent = compassdata.Readregisterbytes(10, 3);  
    PH(compassiddent); 
    P("\n"); 
    P("Configuration registers: ");
    uint32_t compassconfig = compassdata.Readregisterbytes(0, 3);
    PH(compassconfig);  //    default: 0x102003   for 0x10 == 15Hz reading, 0x20 == 1.3Gauss gain, 0x03 == in idle mode
    P("\n"); 
  
    compassdata.CompassSelfTest(true); 
    P("\n"); 
    compassdata.CompassSelfTest(false); 
    P("\n"); 
    compassdata.CompassSelfTest(true); 
    P("\n"); 
    compassdata.SetupCompass(); 
}

void loop() 
{
 
    if (compassdata.ReadCompass()) {
        P("x: ");
        P(compassdata.cpx);
        P("  y: ");
        P(compassdata.cpy);
        P("  z: ");
        P(compassdata.cpz);
        P("  L: "); 
        P(sqrt(compassdata.cpx*compassdata.cpx + compassdata.cpy*compassdata.cpy + compassdata.cpz*compassdata.cpz)); 
        P("\n"); 
        delay(66);   // 15Hz
    } else {
        P("bad reading"); 
        P("\n"); 
        delay(1000); 
    }
}

