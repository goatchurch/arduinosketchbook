
#define P(X) Serial.print(X)

unsigned short wB, w0, w1, w2, w3, w4, w5, w6, w7, w8, w9; 
unsigned short ws[16]; 



void setup() 
{
    Serial.begin(9600);
    pinMode(8, OUTPUT);
    pinMode(9, OUTPUT);

    pinMode(2, INPUT); // 8 cycle 40kHz signal
    pinMode(4, INPUT);

    pinMode(6, INPUT);
    pinMode(7, INPUT); // 8 cycle 40kHz signal
    //port8 = portInputRegister(digitalPinToPort(8));
    //P(clockCyclesToMicroseconds(1000)); 
}

void MakeReadingA()
{
    PORTB &= 0b11111100; 
    delayMicroseconds(2);
    PORTB |= 0b00000011; 
    delayMicroseconds(10);
    PORTB &= 0b11111100; 

    noInterrupts();
    unsigned short c = 0; 
    unsigned short* pws = ws; 

    do {
        do { if (++c == 0)  return; } while ((PIND & 0x04) == 0);    // pin 2
        do { if (++c == 0)  return; } while ((PIND & 0x04) != 0); 
        *pws = c; 
        ++pws; 
    } while (pws != (ws+8)); 

    do {
        do { if (++c == 0)  return; } while ((PIND & 0x40) == 0);    // pin 6
        do { if (++c == 0)  return; } while ((PIND & 0x40) != 0); 
        *pws = c; 
        ++pws; 
    } while (pws != (ws+16)); 

    interrupts();
}

void MakeReadingB()
{
    PORTB &= 0b11111100; 
    delayMicroseconds(2);
    PORTB |= 0b00000011; 
    delayMicroseconds(10);
    PORTB &= 0b11111100; 

    noInterrupts();
    unsigned short c = 0; 
    unsigned short* pws = ws; 

    do {
        do { if (++c == 0)  return; } while ((PIND & 0x80) == 0);    // pin 7
        do { if (++c == 0)  return; } while ((PIND & 0x80) != 0); 
        *pws = c; 
        ++pws; 
    } while (pws != (ws+8)); 

    do {
        do { if (++c == 0)  return; } while ((PIND & 0x10) == 0);    // pin 4
        do { if (++c == 0)  return; } while ((PIND & 0x10) != 0); 
        *pws = c; 
        ++pws; 
    } while (pws != (ws+16)); 
}

void PrintWS(char ch)
{
    P(ch); 
    P(" "); 
    P(ws[0]); P("  "); 
    for (int i = 1; i < 16; i++) {
        if (ws[i] != 0) {
            P(ws[i] - ws[i-1]); 
            P(" "); 
        }
    }
    P("\n"); 
}

void ClearWS() 
{
    unsigned short* pws = ws; 
    do {
        *pws = 0; 
        ++pws; 
    } while (pws != (ws+16)); 
}

long sumc = 0; 
void loop() 
{
/*    ClearWS(); MakeReadingA();  interrupts();  PrintWS('A'); 
    delay(500); 
    ClearWS(); MakeReadingB();  interrupts();  PrintWS('B'); 
    delay(50); 
    return; 
*/

    for (int i = 0; i < 10; i++) {
        MakeReadingA();  interrupts(); 
        P(ws[8] - ws[7]); 
        P(" "); 
        delay(50); 
        MakeReadingA();  interrupts(); 
        P(ws[8] - ws[7]); 
        P(" "); 
        delay(50); 
    }
    P("\n"); 
//    analogWrite(5, 150-(nc % 150)); 
}



