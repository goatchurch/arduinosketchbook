
module rect()
{
	union()
	{
		cylinder(h = 90, r = 2.5, $fn=20);
		translate([150, 0, 0])
			cylinder(h = 90, r = 2.5, $fn=20);
		translate([0, -2.5, 0])
			cube([152.5, 5, 4]);
		translate([0, -2.5, 86])
			cube([152.5, 5, 4]);
	}
};

// the result
rotate([0,0,45])
union() {
	// strengthening on corners
	hull() intersection() { cube([12,12,18], center=true); rect(); };
	hull() intersection() { translate([150,0,0])  cube([12,12,18], center=true); rect(); };
	hull() intersection() { translate([150,0,90])  cube([12,12,18], center=true); rect(); };
	hull() intersection() { translate([0,0,90])  cube([12,12,18], center=true); rect(); };

	rect();

// X part
translate([75-22.5-5-12,-2.5,0])
multmatrix(m = [ [1, 0, 0.5, 0],
                 [0, 1, 0, 0],
                 [0, 0, 1, 0],
                 [0, 0, 0,  1]
               ]) cube([10,5,90]);

translate([75+22.5-5+12,-2.5,0])
multmatrix(m = [ [1, 0, -0.5, 0],
                 [0, 1, 0, 0],
                 [0, 0, 1, 0],
                 [0, 0, 0,  1]
               ]) cube([10,5,90]);


// bridge with nodule
translate([150/2-15,-2-40,90-4+15]) {
	union() {
		cube([30,4,4]);
		hull() {
			translate([13.5,2,0])
				cube([3,2,4]);
			translate([10,0, -4.5])
				cube([10,2,2]);
		}
	}
}

// arms across
hull() {
	translate([150/2-15,-2-40,90-4+15])
		cube([2,4,4]);
	translate([0,0,80])
		cylinder(h = 5, r = 2.5, $fn=20);
}

hull() {
	translate([150/2-15+30-2,-2-40,90-4+15])
		cube([2,4,4]);
	translate([150,0,80])
		cylinder(h = 5, r = 2.5, $fn=20);
}
}; // end union

