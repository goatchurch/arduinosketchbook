
magrad = 6.35;
magheight = 4.99;

sr = 30; 
sbh = 8; 
eps = 0.01;
bv = 0.5; 
module blob()
{
  union() {
	scale([0.9,0.6,1])
	  intersection() {
		translate([-sr+eps,-sr,0])
			cube([sr, sr*2, sbh-0.5]); 
		translate([0,0,-(sr-sbh)])
			sphere(r=sr, $fa=5); 
	}

	scale([2,0.6,1])
	  intersection() {
		translate([0,-sr,0])
			cube([sr, sr*2, sbh-0.5]); 
		translate([0,0,-(sr-sbh)])
			sphere(r=sr, $fa=5); 
	}
  }
};


module hblob() {
  hull() {
	 blob(); 
	 translate([0,0,-bv])
		  cylinder(h=magheight*0.4+bv, r=magrad+3.13, $fn=50); 
	}
}

difference() {
	union() {
		hblob(); 

		// handles on the front
		translate([-magrad*1.9, magrad*0.8, magheight*0.6])
			rotate([20, 20, 0])
				difference() {
					translate([0, 0, 0])
						cylinder(h=1, r=3, $fn=50); 
					translate([0, 0, -1])
						cylinder(h=3, r=2, $fn=50); 
				}
		translate([-magrad*1.9, -magrad*0.8, magheight*0.6])
			rotate([-20, 20, 0])
				difference() {
					translate([0, 0, 0])
						cylinder(h=1, r=3, $fn=50); 
					translate([0, 0, -1])
						cylinder(h=3, r=2, $fn=50); 
				}	
	}

	// slightly down tapered cylinder
	translate([0,0,-eps-bv])
		cylinder(h=magheight*0.4+eps*2+bv, r1=magrad-0.13, r2=magrad, $fn=50); 
	translate([0,0,magheight*0.4])
		cylinder(h=magheight+10, r=magrad+0.05, $fn=50); 

	// cross slot on bottom (for spring effect on tight hold
	translate([-0.4, -magrad*1.5, -eps-bv])
		cube([0.8, magrad*3, magheight*0.5 + bv]); 


	// top slot on nose (for wire come out)
	union() {
		translate([-magrad*1.4, -1.0, magheight])
			cube([magrad*0.7, 2.0, magheight*0.5]); 
		translate([-magrad*2.6, -1.0, magheight-3])
			cube([magrad*0.7, 2.0, magheight*0.5]); 
		hull() {
			translate([-magrad*1.4, -1.0, magheight])
				cube([magrad*0.7, 2.0, magheight*0.1]); 
			translate([-magrad*1.92, -1.0, magheight-3])
				cube([magrad*0.1, 2.0, magheight*0.35]); 
		}
	}

	// top slot on tail (for light sensor)
	translate([0, -1.0, magheight])
		cube([magrad*4.5, 2.0, magheight*2.5]); 
	translate([magrad*3.0, -1.0, magheight-3])
		cube([magrad*1, 2.0, magheight*2.5]); 
	translate([magrad*4.5, 0, magheight*0.5])
	  scale([1.5,1,1])
		cylinder(h=magheight+10, r=2.5, $fn=50); 
}

