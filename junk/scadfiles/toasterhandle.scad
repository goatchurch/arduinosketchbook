

cr = 10.5;
th = 2.5;


module oval(cr, h) 
{
	hull() {
		cylinder(r=cr,h=h);
		translate([43-21,0,0]) cylinder(r=cr,h=h);
	}
};


module innerwedge(cr, h) 
{	
	union() {
		multmatrix(m = [ [1, 0, 0, 0],
                 [0, 1, 0, 0],
                 [0, 0.2, 1, 0.2*cr],
                 [0, 0, 0, 1]
               ]) oval(cr,h);
		oval(cr,h); 
	}
};

translate([-7,-(cr+th)+0.05, 0]) 
  union() {
	difference() {
		innerwedge(cr+th, 10); 
		translate([0,0,-0.05])
			innerwedge(cr, 8); 
	}
	translate([-cr-0.1,-1.5/2, 4])
	cube([43+th/2,1.5,9]);
}

hull() {
  cube([3,28,9]);
  translate([0.5,0,1])
	  cube([2,31,7]);
}
hull() {
	cube([3,1,15]);
	cube([3,5,9]);
}



