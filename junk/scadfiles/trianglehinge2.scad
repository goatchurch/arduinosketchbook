

eps = 0.01;

rr=2.2; 
cr=2.6;

module striang() 
{
union() {
  translate([0,0,-4])
    cylinder(h=50+4*2, r=rr, $fn=20);

  hull() {
    translate([0,0,54])
      sphere(r=rr, $fn=20);
    translate([6,0,54])
      sphere(r=rr, $fn=20);
  }

  hull() {
    translate([0,0,-4])
      sphere(r=rr, $fn=20);
    translate([60,0,-4])
      sphere(r=rr, $fn=20);
  }

  hull() {
    translate([6,0,54])
      sphere(r=rr, $fn=20);
    translate([60,0,-4])
      sphere(r=rr, $fn=20);
  }
}
}


module btriang() 
{
difference() {
union() {
  cylinder(h=50, r=cr+2, $fn=20); 
  hull() {
    translate([-2,0,1])  sphere(r=rr, $fn=20);
    translate([-50,0,25])  sphere(r=rr, $fn=20);
  }

  hull() {
    translate([-2,0,49])  sphere(r=rr, $fn=20);
    translate([-50,0,25])  sphere(r=rr, $fn=20);
  }

  hull() {
    translate([-2,0,49])  sphere(r=rr, $fn=20);
    translate([-2,40,25])  sphere(r=rr, $fn=20);
  }

  hull() {
    translate([-50,0,25])  sphere(r=rr, $fn=20);
    translate([-2,40,25])  sphere(r=rr, $fn=20);
  }

  hull() {
    translate([-2,0,1])  sphere(r=rr, $fn=20);
    translate([-2,40,25])  sphere(r=rr, $fn=20);
  }
}
  translate([0,0,-10])
    cylinder(h=70, r=cr, $fn=20); 
}
}

union() {
  striang(); 
  btriang(); 
}
