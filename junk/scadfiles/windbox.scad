
wt = 2;   // wall thickness
bv = 1;   // hull bevel
eps = 0.02;
ovl = 3; // lid overlap
baseheight = 16.5; 
baselength = 25.5; 

module positive()
{
	union() {
		cube([40, baselength, baseheight]);
		translate([2.5, -15, 0])
			cube([12, 25, baseheight]); 
		translate([6.5, -15, -3.3])
			cube([6, 18, baseheight]); 
		translate([5.5, -17.5, -3.3])
			cube([8, 18, baseheight]); 
		translate([40-4-1.5, baselength+wt*2, 15.5-1.5-4])
			rotate([90, 0, 0])
				cylinder(h=10, r=4.3, center=false, $fn=50);
	};
}

module outerbase()
{
	hull() {
		translate([-wt,-wt,-wt])
			cube([40+2*wt, baselength+2*wt, baseheight+wt-eps]);
		translate([0,0,-wt-bv])
			cube([40, baselength, baseheight+wt-eps]);
	}
}

module base()
{
	union() {
		difference() {
			outerbase(); 
			positive(); 
			translate([40-4-1.5-1.2, baselength+wt*2, 15.5-1.5-4+4])
				rotate([90, 0, 0])
					//cylinder(h=10, r=2.4, center=false, $fn=50);
					cube([2.4, 4.4, 10]); 
		}
		translate([27, -2-wt*0.667-wt-0.2, 0.2-wt])
			cube([12, wt*0.667, 6]);
		translate([27, -2-wt*0.667-wt, 1.2-wt])
			cube([2, wt, 4]);
		translate([39, -2-wt*0.667-wt-0.2, 0.2-wt])
			cube([3, wt+2, 6]);

	}
}

module lid() 
{
  union() {
    difference() {
		union() {
			hull() {
				translate([-wt-wt,-wt-wt,-wt+baseheight-ovl])
					cube([40+4*wt, baselength+4*wt, ovl+wt+wt-eps]);
				translate([0,0,-wt+baseheight-ovl+bv])
					cube([40, baselength, ovl+wt+wt-eps]);
			};
		};
		union() {
			outerbase(); 
			positive(); 
		}
    }
    translate([29, baselength-20+2.7, baseheight-6])
        cube([10, wt, 6+eps]);
  }
}

union() {
translate([0,0,wt+bv])
	base(); 

translate([-20,0,baseheight+bv+wt])
	rotate([0,180,0])
		lid();

};

