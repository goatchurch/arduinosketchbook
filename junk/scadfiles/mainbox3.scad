

toppart(); botpart(); 


// DONT FORGET THE REFLECTION IN X
//rotate([180,0,0])
//rotate([0,0,45]) scale([-1,1,1])
//  toppart();
//scale([-1,1,1])
//rotate([180,0,0]) translate([30,-8,0]) rotate([0,0,10])
//botpart(); 


linexheight = 94; 
eps = 0.04; 

boxthickness = 2; 
oboxwidth = 96+boxthickness*2;
iboxdepth = 23; 
boxfrontjut = 35; 
boxleftjut = 15;
boxjutheight = 50;

module toppart() {
  difference() {
    union() {
      linear_extrude(height=linexheight+boxthickness*2, slices=5)  
        topprofile(); 
      // extra lower profile to add under the jut
      linear_extrude(height=linexheight+boxthickness*2, slices=5)  
        polygon([[-15+eps,-4],[-15+eps,iboxdepth+boxthickness*3],
                 [-boxfrontjut,iboxdepth+boxthickness*3],[-boxfrontjut,0],
                 [-25,0]]);  // lucky to get such simple join position!
      // jutting parts
      translate([-boxfrontjut-eps,boxthickness,-boxleftjut-boxthickness])  
        cube([boxthickness*2+boxfrontjut+eps, boxjutheight, boxleftjut+linexheight+boxthickness*3]);
    }

    // main hollow
    translate([-15+boxthickness-eps,boxthickness*2,boxthickness])  cube([96,iboxdepth,linexheight+boxthickness*2+eps*2]); 

    // jutting hollow
    translate([-boxfrontjut-eps+boxthickness,boxthickness*2,-boxleftjut])  
      cube([boxfrontjut+eps, boxjutheight, boxleftjut+linexheight+boxthickness*3]);

    // oled view window
    translate([oboxwidth-15-35,iboxdepth-2,30])
      cube([25,10,22]);
    
  }
}


module botpart() {
  union() {
    linear_extrude(height=linexheight+boxthickness*2, slices=5)  botprofile(); 
    // notches on front bottom
    translate([-15,-23,0]) cube([10,10,10]); 
    translate([-15,-23,30]) cube([10,10,10]); 
    translate([-15,-23,60]) cube([10,10,10]); 
    translate([-15,-23,90]) cube([10,10,linexheight+boxthickness*2-90]); 
  }
}

module outprofile() 
{
  difference() {
    translate([-15,-38])  
      polygon([[3,14],[0,19],[0,40+iboxdepth+boxthickness*2],
               [oboxwidth,40+iboxdepth+boxthickness*2],
               [oboxwidth,40-boxthickness],[oboxwidth-9,20],
               [oboxwidth-25,15],
               [oboxwidth-40,0],[38,0]]);

    // the 4AA cell holder
    translate([oboxwidth-48,-7.5])  hull() {
      circle(r=17.5/2,$fn=60);
      translate([31.0-17.5,0])  circle(r=17.5/2,$fn=60);
    }
  }
}


module topprofile()
{
    intersection() {
      outprofile(); 
      translate([-4,-8])  rotate(-20)  topbarmask();
    }
}

module botprofile()
{
  intersection() {
    outprofile(); 
    translate([-4,-8])  rotate(-20)  bottombarmask();
  }
}




barbreadth = 48;
bardepth = 25.0; 
barbackrad = 9; 
barbackcirclebreadth = 6.3; 
bardepthmaxheightpos = 19.0; 
barfrontclamcirclerad = 14; 
barfrontrad = 7; 
barfrontcirclebreadth = 2.3; 
barfrontclamrad = bardepthmaxheightpos+2; 
barshaverad = 80; 
barshavethickness = 0.8; 
barshavedepthpos = bardepthmaxheightpos+4;

module barform() 
{
 translate([0,-bardepth/2]) 
  intersection() {
    hull() {
        intersection() {	
            translate([barbreadth-barbackrad, bardepth/2])
                circle(r=barbackrad, $fn=50);	
            translate([barbreadth-barbackcirclebreadth, 0])
                square([barbackcirclebreadth, bardepth]); 
        }

        translate([bardepthmaxheightpos-0.5,0])
            square([1, bardepth]); 

        intersection() {
            translate([bardepthmaxheightpos, bardepth-barfrontclamrad])
                circle(r=barfrontclamrad, $fn=70); 
            translate([bardepthmaxheightpos, barfrontclamrad])
                circle(r=barfrontclamrad, $fn=70); 
        }

        translate([barfrontrad, bardepth/2])
            circle(r=barfrontrad, $fn=50);	
    }

    translate([barshavedepthpos, -barshaverad+bardepth-barshavethickness])
        circle(r=barshaverad, $fn=200); 
    translate([barshavedepthpos, barshaverad+barshavethickness])
        circle(r=barshaverad, $fn=200); 
  }
};


bardepthoffset = 6; 
tongueex = 0.3; 
ltonguerad = 3.0; 
ltongueleft = 3.9; 
ltongueneck = 2.1; 
module lefttongue(ex)
{
	translate([-ltongueleft, 0])
	 	circle(r=ltonguerad+ex,$fn=20); 
	translate([-ltongueleft, -ltongueneck/2-ex])
		square([ltongueleft+eps+1,ltongueneck+ex*2]);
}


module topbarmask() 
{
    difference() {
        translate([-40,0])  square([150,80]);
		 
        barform();
        translate([-5,0])  rotate(-90)  lefttongue(tongueex); 
        translate([53,0])  rotate(-90)  lefttongue(tongueex); 
    }
}

module bottombarmask() 
{
  union() {
    difference() {
        translate([-40,-50-0.2])  square([120,50]);
        barform();
    }
    translate([-5,0])  rotate(-90)  lefttongue(0); 
    translate([53,0])  rotate(-90)  lefttongue(0); 
  }
}


