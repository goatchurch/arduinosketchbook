

pw = 110; 
ph = 6; 
th = 2.5; 
eps = 0.1; 
fh = 65; 
bevelh = 20; 
bevelw = 12; 
bevelwx = 8; 

// main channel
module channel() {
translate([0,0,-fh])
difference() {
  translate([-th,-th,0])
    cube([pw+th*2,ph+th*2,fh]); 

  translate([0,0,-eps])
    cube([pw,ph,fh+2]); 

};

}

// funnel part
module funnel() {
difference() {
hull() {
translate([-th,-th,-eps/2])
  cube([pw+th*2,ph+th*2,eps]); 
translate([-th-bevelwx,-th,bevelh])
  cube([pw+th*2+bevelwx*2,ph+th*2+bevelw,eps]); 
}

hull() {
translate([0,eps/2,-eps])
  cube([pw-eps,ph-eps,eps]); 
translate([-bevelwx,eps/2,bevelh])
  cube([pw+bevelwx*2-eps,ph+bevelw-eps,eps+1]); 
}
}
}


rotate([0,180,45])
union() {
	channel(); 
	funnel(); 
	translate([10,-9,10])
		sphere(r=9); 

	// barb
	hull() {
	translate([-3-1-eps,ph/2-eps,-fh+8])
		cube([3+eps,3.5,3]);
	translate([-6-1-eps,ph/2-eps,-fh+13])
		cube([1+eps,3.5,3+2]);
	}
}

