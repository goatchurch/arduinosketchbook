// knob to screw onto Andrew's kitchen freezer on single screw. 
// should make full handle with two screws at some point
sr = 12;
difference() {
  intersection() {
	translate([0,0,sr*0.9])
		multmatrix(m = [ 
					[2.5, 0.3, 0, 0],
                 [0, 1.3, 0, 0],
                 [0, 0, 1, 0],
                 [0, 0, 0, 1] ])
			sphere(r=sr, $fn=80);
	translate([-30,-30,0])
		cube([60,60,60]);
  };
  translate([0,0,4])
	  cylinder(r=7,h=50, $fn=50); 
  translate([0,0,-0.1])
	  cylinder(r=3,h=10, $fn=30);
}

