
uprightlength = 35; boardlength=100.0; grillslots=5;
//uprightlength = 15; boardlength=30.0; grillslots=2; // short trial before big one

//fulluprightprofile(0);

//translate([-30,0,0])  
//bottombox(uprightlength); 
//rotate([180,0,0])
topboxboard(uprightlength); 

//boardshape(0); 

module topboxboard(uprightlength) 
{
  difference() {
    union() {
      translate([uprightgirth,uprightgirthrad-uprightshell+1.2,0])   rotate([0,0,-8.9])
      hull() {
        boardshell(3.0,3.0);
        boardshell(1.0,3.6);
      }
      translate([0,0,(boardlength-uprightlength)/2])
        topbox(uprightlength); 
    }
  translate([uprightgirth,uprightgirthrad-uprightshell+1.2,0])   rotate([0,0,-8.9])
    boardshape(10);
  }
}


boardwidth=41.5; 
boardbasethickness=4.5; 
boardcardshelf=2.8; 
boardclearance=29;
boardwindowgrill=6; 
boardwindowgrillh=3; 
module boardshape(boardlengthex)
{
        union() {
          translate([0,0,-boardlengthex])
            cube([boardwidth,boardbasethickness,boardlength+boardlengthex]);
          translate([boardcardshelf,0,-boardlengthex])
            cube([boardwidth-boardcardshelf*2,boardbasethickness+boardclearance,boardlength+boardlengthex]);

          translate([9,-4,boardlength-eps])
            cube([14,17+4,6]);

          translate([9,boardclearance,boardlength-70-18])
            cube([16,17+4,16]);

          translate([boardcardshelf-15,boardbasethickness+boardwindowgrillh,boardwindowgrill])
 intersection() {
    cube([boardwidth-boardcardshelf*2+30,boardclearance-boardwindowgrillh*2,boardlength-boardwindowgrill*2]);
    union() {
       for (i=[-1,0,1,2,3,4])
       multmatrix(m = [[1, 0, 0, 0],[0, 1, 0, 0],[0, 1, 1, i*boardlength/grillslots],[0, 0, 0, 1]])
         cube([boardwidth-boardcardshelf*2+30,boardclearance-boardwindowgrillh*2,boardlength/grillslots-5]);
    }
  }
         }
}

module boardshell(ex,vex)
{
  hull() {
    translate([0,0,-vex])
      cylinder(r=ex,h=boardlength+vex*2, $fn=40);
    translate([0,boardbasethickness,-vex])
      cylinder(r=ex,h=boardlength+vex*2, $fn=40);
    translate([boardwidth,0,-vex])
      cylinder(r=ex,h=boardlength+vex*2, $fn=40);
    translate([boardwidth,boardbasethickness,-vex])
      cylinder(r=ex,h=boardlength+vex*2, $fn=40);
    translate([boardcardshelf,boardclearance+boardbasethickness,-vex])
      cylinder(r=ex,h=boardlength+vex*2, $fn=40);
    translate([boardwidth-boardcardshelf,boardclearance+boardbasethickness,-vex])
      cylinder(r=ex,h=boardlength+vex*2, $fn=40);
  }
}



eps = 0.03;
uprightbreadth = 63.90; 
uprightwidth = 25.00; 
uprightbackflat = 10.3; 
uprightbackflatrad = 0.8; 
uprightnosediam = 15; 
uprightgirth = 20; 
uprightgirthrad = 20; 
uprightgirth2 = 40; 
uprightgirthrad2 = 46; 
uprightgirth2lower = 2.8; 
uprightgirthF = 13.5; 
uprightgirthradF = 14.2; 
uprightgirth2Flower = 1.7; 
uprightshell=4;

module halfuprightprofile(ex) {
 hull() {
  translate([uprightnosediam/2,0]) 
    circle(r=uprightnosediam/2+ex, $fn=90);
  translate([uprightgirth,uprightwidth/2-uprightgirthrad]) 
    circle(r=uprightgirthrad+ex, $fn=90);
  translate([uprightgirth2,uprightwidth/2-uprightgirthrad2-uprightgirth2lower]) 
    circle(r=uprightgirthrad2+ex, $fn=90);
  translate([uprightgirthF,uprightwidth/2-uprightgirthradF-uprightgirth2Flower]) 
    circle(r=uprightgirthradF+ex, $fn=90);
  translate([uprightbreadth-uprightbackflatrad, uprightbackflat/2-uprightbackflatrad])
    circle(r=uprightbackflatrad+ex, $fn=30);
 }
}

module uprightshellshape(bevelshrink)
{
  hull() {
      fulluprightprofile(uprightshell-bevelshrink);  
      translate([-bottomboxdepththickness/2,0])  rotate(-90)  lefttongue(uprightshell-bevelshrink); 
      translate([uprightbreadth+bottomboxdepththickness/2,0]) rotate(-90)  lefttongue(uprightshell-bevelshrink); 
  }
}

module fulluprightprofile(ex) 
{
  hull() {
    scale([1,-1])
      intersection() {
        translate([-ex,0])
          square([uprightbreadth+ex*2, uprightwidth/2+ex]); 
        halfuprightprofile(ex); 
      }
   }
   intersection() {
     translate([-ex,0])
       square([uprightbreadth+ex*2, uprightwidth/2+ex]); 
     halfuprightprofile(ex); 
   }
}

tongueex = 0.3; 
ltonguerad = 3.0; 
ltongueleft = 3.9; 
ltongueneck = 2.1; 
module lefttongue(ex)
{
	translate([-ltongueleft, 0])
	 	circle(r=ltonguerad+ex,$fn=20); 
	translate([-ltongueleft, -ltongueneck/2-ex])
		square([ltongueleft+eps+1,ltongueneck+ex*2]);
}


bottomboxthickness = uprightshell; 
bottomboxdepththickness = 12; 
bottomboxfronttonguedepth = bottomboxdepththickness/2-0.5;
bevelshrink=0.4;
module bottombox(uprightlength)
{
  union() {
    difference() {
      intersection() {
        linear_extrude(height=uprightlength)  
          translate([-bottomboxdepththickness,-(uprightwidth/2+bottomboxthickness)])
            square([uprightbreadth+bottomboxdepththickness*2,uprightwidth/2+bottomboxthickness]);
        hull() {
          translate([0,0,eps])
            linear_extrude(height=uprightlength-eps*2)  
              uprightshellshape(bevelshrink*2); 
          translate([0,0,bevelshrink])
            linear_extrude(height=uprightlength-bevelshrink*2)  
              uprightshellshape(0); 
        }
      }
      linear_extrude(height=uprightlength)  
        fulluprightprofile(0); 
    }

    linear_extrude(height=uprightlength)  
      translate([-bottomboxfronttonguedepth,0])  rotate(-90)  lefttongue(0); 
    linear_extrude(height=uprightlength)  
      translate([uprightbreadth+bottomboxdepththickness/2,0]) rotate(-90)  lefttongue(0); 
  }
}


module topbox(uprightlength)
{
linear_extrude(height=uprightlength)  
  difference() {
      intersection() {
        translate([-bottomboxdepththickness,0])
          square([uprightbreadth+bottomboxdepththickness*2,uprightwidth/2+bottomboxthickness]);
        uprightshellshape(0); 
      }
      fulluprightprofile(0); 

    translate([-bottomboxfronttonguedepth,0])  rotate(-90)  lefttongue(tongueex); 
    translate([uprightbreadth+bottomboxdepththickness/2,0]) rotate(-90)  lefttongue(tongueex); 
  }
}

