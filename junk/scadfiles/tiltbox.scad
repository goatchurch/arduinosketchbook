
wt = 2;   // wall thickness
bv = 1;   // hull bevel
eps = 0.02;
ovl = 3; // lid overlap
basewidth = 39; 
baselength = 37.2; 
baseheight = 20.1; 
hullridge = 9.0;
tophullridge = 3.0;

module positive()
{
	union() {
		cube([basewidth, baselength, baseheight]);
		translate([-5, 19, 0])
			cube([12, 13, baseheight]); 
		translate([-6, 23, -10])
			cube([7, 6, baseheight]); 
	};
}


module outerbase()
{
   union() {
		translate([-wt,-wt,-wt])
			cube([basewidth+2*wt, baselength+2*wt, baseheight+wt-eps]);
		hull() {
			translate([-wt,-wt,-wt])
				cube([basewidth*0.48+wt, baselength*0.48+wt, wt+wt-eps]);
			translate([0,2,-wt-hullridge])
				cube([basewidth*0.1, baselength*0.1, hullridge+wt-eps]);
		};
		hull() {
			translate([basewidth*0.52,-wt,-wt])
				cube([basewidth*0.48+wt, baselength*0.48+wt, wt+wt-eps]);
			translate([basewidth*0.9,2,-wt-hullridge])
				cube([basewidth*0.1, baselength*0.1, hullridge+wt-eps]);
		};

		hull() {
			translate([-wt,baselength*0.52,-wt])
				cube([basewidth*0.48+wt, baselength*0.48+wt, wt+wt-eps]);
			translate([0,baselength*0.9-2,-wt-hullridge])
				cube([basewidth*0.1, baselength*0.1, hullridge+wt-eps]);
		};
		hull() {
			translate([basewidth*0.52,baselength*0.52,-wt])
				cube([basewidth*0.48+wt, baselength*0.48+wt, wt+wt-eps]);
			translate([basewidth*0.9,baselength*0.9-2,-wt-hullridge])
				cube([basewidth*0.1, baselength*0.1, hullridge+wt-eps]);
		}
	}
}

module base()
{
	union() {
		difference() {
			outerbase(); 
			positive(); 
		}
	}
}

module lid() 
{
  union() {
    difference() {
		union() {
			translate([-wt-wt,-wt-wt,-wt+baseheight-ovl])
				cube([basewidth+4*wt, baselength+4*wt, ovl+wt+wt-eps]);
			hull() {
				translate([-wt-wt,-wt-wt,-wt+baseheight-ovl])
					cube([basewidth*0.4+2*wt, baselength+4*wt, ovl+wt+wt-eps]);
				translate([0,0,-wt+baseheight-ovl+tophullridge])
					cube([basewidth*0.2, baselength, ovl+wt+wt-eps]);
			};
			hull() {
				translate([basewidth*0.6,-wt-wt,-wt+baseheight-ovl])
					cube([basewidth*0.4+2*wt, baselength+4*wt, ovl+wt+wt-eps]);
				translate([basewidth*0.8,0,-wt+baseheight-ovl+tophullridge])
					cube([basewidth*0.2, baselength, ovl+wt+wt-eps]);
			};
		};
		union() {
			outerbase(); 
			//positive(); 
			union() {
				cube([basewidth, baselength, baseheight]);
				translate([-5, 19, 0])
					cube([12, 13, baseheight-3]); 
			}
		}
    }
  }
}

union() {
translate([0,0,wt+hullridge])
	base(); 
translate([-20,0,baseheight+tophullridge+wt])
	rotate([0,180,0])
		lid();

};

