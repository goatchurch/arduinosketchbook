
wt = 2;   // wall thickness
bv = 1;   // hull bevel
eps = 0.02;
ovl = 5; // lid overlap
basewidth = 40; 
baselength = 30.0; 
baseheight = 18.5; 
wtt=1.5;
module positive()
{
	union() {
		cube([basewidth, baselength, baseheight]);
		translate([-5, 13.0, 0])
			cube([10, 13, baseheight]); 
		translate([-6, 13+1.5, -10])
			cube([9, 10, baseheight]); 
		translate([25,baselength-10,5])
			cylinder(r=4,h=20);
	};
}


module outerbase()
{
   union() {
		translate([-wt,-wt,-wt])
			cube([basewidth+2*wt, baselength+2*wt, baseheight+wt-eps]);
	}
}

module base()
{
	union() {
		difference() {
			outerbase(); 
			positive(); 
		}
	}
}

cowlrad = 9; 
module cowl() 
{
translate([25, baselength-10, 0])
union()
{
translate([-cowlrad-wtt,-cowlrad-wtt,60])
  cube([cowlrad*2+wtt*2,cowlrad*2+wtt*2,1]);

difference() {
  translate([-cowlrad-wtt,-cowlrad-wtt,30-eps/2])
    cube([cowlrad*2+wtt*2,cowlrad*2+wtt*2,30+1]);
  translate([0,0,30-eps])
    cylinder(r=cowlrad*sqrt(2), $fn=40,h=30+1);
}

// lower cowl
difference() {
  hull() {
    translate([-cowlrad-wtt,-cowlrad-wtt,30])
      cube([cowlrad*2+wtt*2,cowlrad*2+wtt*2,1]);
    translate([0,0,baseheight+wt/2])
      cylinder(r=6+wtt,h=5);
  }
  hull() {
    translate([-cowlrad,-cowlrad,30])
      cube([cowlrad*2,cowlrad*2,1+eps]);
    translate([0,0,baseheight+wt/2-eps])
      cylinder(r=6,h=5);
  }
}
}
}


module lid() 
{
  union() {
    difference() {
		union() {
			translate([-wt-wt,-wt-wt,-wt+baseheight-ovl])
				cube([basewidth+4*wt, baselength+4*wt, ovl+wt+wt-eps]);
		};
		positive(); 
		outerbase(); 
	 }
	 cowl(); 
  }
}

union() {
translate([0,0,wt])
	base(); 
translate([25,0,60-20+1])
translate([-20,0,baseheight+wt])  rotate([0,180,0])
		lid();
};


//		translate([25,baselength-10,5])
//			cylinder(r=4,h=20);
