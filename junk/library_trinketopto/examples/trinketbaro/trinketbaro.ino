// 
#include "Arduino.h"
#include "trinketopto.h"

#define GEMMADEPLOY 1  // true it deploying on the Attiny85 based trinket/gemma board

// for I2C on the MS5611 need to wire PS to VDD

#if GEMMADEPLOY
// Tools -> Programmer -> USBtinyISP;  Tools -> Board -> Gemma 8Mz
#include <TinyWireM.h>   // needs commenting to subvert build system
#define P(X) 
#define PInit
#define Wire TinyWireM

#define PinOut 3
#define PinLED 1

#else

//#include <Wire.h>      // needs commenting to subvert build system
#define P(X)  Serial.print(X) 
#define PInit Serial.begin(9600)
#define PinOut 2  // otherwise interferes with the serial
#define PinLED 13

#endif

#define BITTIME 180
#define MS5611_ADDRESS 0x77

uint32_t SENST1; // Pressure sensitivity
uint32_t OFFT1;  // Pressure offset
uint16_t C3;     // Temperature coefficient of pressure sensitivity
uint16_t C4;     // Temperature coefficient of pressure offset
int32_t Tref;    // Reference temperature
int32_t C6;      // Temperature coefficient of the temperature

int32_t D1;   // raw temperature
int32_t D2;   // raw pressure
int64_t dT;   // difference to reference temperature
int32_t TEMP; // *100degC
int32_t Pr;   // in Pascals (1Atmosphere = 101kPa)

int32_t Pr4;  // filtered Pressure
uint8_t blighton; // will be on every other 10millibars (like a metre)
uint16_t seqlightchange;  // enables the little flashes to show is alive


int32_t readRegister24()  
{
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x00); 
    Wire.endTransmission();
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.requestFrom(MS5611_ADDRESS, 3);
    while(!Wire.available())
        ;
    uint8_t r1 = Wire.read(); 
    uint8_t r2 = Wire.read(); 
    uint8_t r3 = Wire.read(); 
    Wire.endTransmission();
    int32_t Dx = ((int32_t)r1 << 16) | ((int32_t)r2 << 8) | (int32_t)r3;
    return Dx; 
}

  
void UpdatePr() 
{
    int64_t OFF = (int64_t)OFFT1 + dT*C4/0x0080; 
    int64_t SENS = SENST1 + dT * C3 / 0x0100;
    if (TEMP < 2000) {
        int64_t T2 = dT*dT/0x80000000; 
        int32_t ra = TEMP - 2000; 
        ra = ra*ra; 
        OFF -= 5*ra/2; 
        SENS -= 5*ra / 4; 
        if (TEMP < -1500) {
            int32_t rb = TEMP - (-1500); 
            rb = rb*rb; 
            OFF -= 7*rb; 
            SENS -= 11*rb / 2; 
        }
        TEMP -= T2; 
    }
    Pr = (SENS * D1 / 0x200000 - OFF) / 0x00008000; 
}



void setup()
{
    PInit; 
    P("hi there\n"); 
    pinMode(PinOut, OUTPUT); 
    pinMode(PinLED, OUTPUT);

    // send okay signal 
    okaysignal(2); 

    Wire.begin();
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x1E);   // reset
    Wire.endTransmission();
    P("hi there2\n"); 
    delay(100); 
    
    uint16_t C1 = readRegister16(MS5611_ADDRESS, 0xA2);  // 47490
    SENST1 = C1 * (uint32_t)0x8000; 
    uint16_t C2 = readRegister16(MS5611_ADDRESS, 0xA4);  // 48233
    OFFT1 = C2 * (uint32_t)0x10000; 
    
    C3 = readRegister16(MS5611_ADDRESS, 0xA6);           // 29313
    C4 = readRegister16(MS5611_ADDRESS, 0xA8);           // 26259
    uint16_t C5 = readRegister16(MS5611_ADDRESS, 0xAA);  // 31497
    Tref = (int32_t)C5 * 0x100; 
    C6 = readRegister16(MS5611_ADDRESS, 0xAC);           // 28282
    
    // set the interrupt timing
    cli(); 
    
    #if GEMMADEPLOY
        // http://electronics.arunsworld.com/tag/attiny85/
        TCCR0B = 0; 
        TCNT0  = 0;               //initialize counter value to 0
        OCR0A = 125;              // 1*1000*8/64 (this isn't working)
        TCCR0A |= (1 << WGM01);  // Clear timer on compare match CTC mode
        TCCR0B |= (1 << CS01) | (1 << CS00);   // prescaler 64
        TIMSK = _BV(OCIE0A); // enable timer compare interrupt
    #else
        TCCR2A = 0;// set entire TCCR2A register to 0
        TCCR2B = 0;// same for TCCR2B
        TCNT2  = 0;//initialize counter value to 0
        OCR2A = 249;
        // turn on CTC mode
        TCCR2A |= (1 << WGM21);
        // Set CS21 bit for 64 prescaler
        TCCR2B |= (1 << CS21) | (1 << CS20);   
        // enable timer compare interrupt
        TIMSK2 |= (1 << OCIE2A);
    #endif
    
    sei(); 
    okaysignal(2); 
}

volatile uint8_t m20count = 0; 
volatile uint8_t m20 = 0; 

#if GEMMADEPLOY 
ISR(TIMER0_COMPA_vect)
#else
volatile uint8_t m20s = 0; 
ISR(TIMER2_COMPA_vect)
#endif
{
    #if GEMMADEPLOY == 0
        m20s++; 
        if (m20s < 4)
            return; 
        m20s = 0; 
    #endif
    m20++; 
    if (m20 == 11) {  // actually up to 10 to get the 20ms loop as can't get clear on compare to work
        m20 = 0; 
        m20count++; 
    }
}

void loop()
{
    while (m20)  ;

    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x58);        // request raw pressure
    Wire.endTransmission();
    //uint32_t ust = micros(); 
     
    // work out the pin light behavoir (double flashes when left standing)
    uint8_t m20countC = ((seqlightchange == 0) ? m20count : 0) % 0x7F; 
    digitalWrite(PinLED, (blighton != ((m20countC == 10) == (m20countC == 14))) ? HIGH : LOW); 
    
    dT = D2 - Tref; 
    TEMP = 2000 + dT*C6/0x00800000; 
    
    sendbitbangbyte(BITTIME, 0x00 | (uint8_t)(TEMP & 0x3F)); 
    while (m20 < 2)  ;
    delayMicroseconds(400);  // these readings are very sensitive to doing early, and the temp wrong of 1 deg throws the pressure out complet
    sendbitbangbyte(BITTIME, 0x40 | (uint8_t)((TEMP >> 6) & 0x3F)); 
    while (m20 < 4)  ;
    sendbitbangbyte(BITTIME, 0x80 | (uint8_t)(m20count & 0x3F)); 

    while (m20 < 5)  ;
    D2 = readRegister24();   // read raw pressure
    
    Wire.beginTransmission(MS5611_ADDRESS);
    Wire.write(0x48);        // request raw temp
    Wire.endTransmission();

    UpdatePr(); 
    
    sendbitbangbyte(BITTIME, 0x00 | (uint8_t)(Pr & 0x3F)); 
    while (m20 < 7)  ;
    
    // set up the light behaviour
    Pr4 += Pr - Pr4/4; 
    uint8_t PrMod = (Pr4/4 % 20);
    uint8_t newblighton; 
    if (blighton)
        newblighton = (5 <= PrMod) && (PrMod <= 15); 
    else
        newblighton = (7 <= PrMod) && (PrMod <= 13); 
    if (blighton != newblighton) {
        seqlightchange = 900; // about 10 seconds
    } else if (seqlightchange != 0)
        seqlightchange--; 
    blighton = newblighton; 
    
    delayMicroseconds(1150);
    sendbitbangbyte(BITTIME, 0x40 | (uint8_t)((Pr >> 6) & 0x3F)); 
    while (m20 < 9)  ;
    delayMicroseconds(800);
    sendbitbangbyte(BITTIME, 0xC0 | (uint8_t)((Pr >> 12) & 0x3F)); 

    while (m20 < 10)  ;
    delayMicroseconds(900);  // these readings are very sensitive to doing early, and the temp wrong of 1 deg throws the pressure out complet
    D1 = readRegister24();   // read raw temp

    P(TEMP); 
    //P(millis()); 
    P(" "); 
    P(Pr); 
    //Serial.print(Pr, HEX); 
    P("\n"); 
}




