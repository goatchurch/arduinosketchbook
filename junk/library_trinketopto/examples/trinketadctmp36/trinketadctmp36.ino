// this is a refactoring of the trinketbaro/trinketbaro.ino code using the same basic library
// so it can be shared with the ADCtmp36 board communications
// (not been tested if it does the job, but save till later)
#include "Arduino.h"
#include "trinketopto.h"

// for I2C on the MS5611 need to wire PS to VDD

#if GEMMADEPLOY
#include <TinyWireM.h>   // needs commenting to subvert build system
#else
//#include <Wire.h>      // needs commenting to subvert build system
#endif

#define BITTIME 180
#define M20LOOPSIZE 32    // approx 1/16 or 51 
#define ONESHOTREADING 0  // 0 for continuous
#define MAXMULTIPLEREADINGS 0

// +/-1.024V range = Gain 4 (default), 128 samples per second (can read up to 52degC), continuous reading mode
// bit 15      Operational Status          = 0
// bit 12-14   volts between AIN0 and GND  = 100
// bit 9-11    programmable gain amp 1.024 = 011
// bit 8       device op mode continuous   = 0    +1
// bit 5-7     data rate 128SPS            = 100  or 010 for 16 samples per second
// bit 4       comparator trad with hyst   = 0
// bit 3       polarity alert pin          = 0
// bit 2       non latching operator       = 0
// bit 0-1     comparator queue disabled   = 11
//   ==>  0100 0110  1000 0011
//   ==>  0100 0110  0000 0011
//uint16_t config = 0x4683 | (ONESHOTREADING ? 0x8100 : 0);  
uint16_t config = 0x4603 | (ONESHOTREADING ? 0x8100 : 0);  

void setup()
{
    PInit; 
    P("hi there\n"); 
    pinMode(PinOut, OUTPUT); 
    pinMode(PinLED, OUTPUT);

    // send okay signal 
    okaysignal(2); 

    Wire.begin(); 
    
    // this attempt to reset the device doesn't seem to succeed
    /*Wire.beginTransmission(0x48);
    Wire.write((uint8_t)0x00); // config register
    Wire.write((uint8_t)0x06); // reset
    Wire.endTransmission();
    Wire.requestFrom((uint8_t)0x48, (uint8_t)2);
    Wire.read(); 
    Wire.read(); 
    delay(10); */

    P("original config "); 
    P(readRegister16(0x48, 0x01)); // should be 0x8583
    P(" ? "); 
    P(0x8583); 
    P(" to set to "); 
    P(0x4683); 
    P("\n"); 

    Wire.beginTransmission(0x48);
    Wire.write((uint8_t)0x01); // config register
    Wire.write((uint8_t)(config >> 8));
    Wire.write((uint8_t)config);
    Wire.endTransmission();
    
    Wire.beginTransmission(0x48);
    Wire.write(0x00);
    Wire.endTransmission();
    delay(10); 
    Wire.requestFrom((uint8_t)0x48, (uint8_t)2);
    Wire.read(); 
    Wire.read(); 
    delay(10); 
    Wire.requestFrom((uint8_t)0x48, (uint8_t)2);
    Wire.read(); 
    Wire.read(); 

    // set the interrupt timing
    cli(); 
    
    #if GEMMADEPLOY
        // http://electronics.arunsworld.com/tag/attiny85/
        TCCR0B = 0; 
        TCNT0  = 0;               //initialize counter value to 0
        OCR0A = 125;              // 1*1000*8/64 (this isn't working)
        TCCR0A |= (1 << WGM01);  // Clear timer on compare match CTC mode
        TCCR0B |= (1 << CS01) | (1 << CS00);   // prescaler 64
        TIMSK = _BV(OCIE0A); // enable timer compare interrupt
    #else
        TCCR2A = 0;// set entire TCCR2A register to 0
        TCCR2B = 0;// same for TCCR2B
        TCNT2  = 0;//initialize counter value to 0
        OCR2A = 249;
        // turn on CTC mode
        TCCR2A |= (1 << WGM21);
        // Set CS21 bit for 64 prescaler
        TCCR2B |= (1 << CS21) | (1 << CS20);   
        // enable timer compare interrupt
        TIMSK2 |= (1 << OCIE2A);
    #endif
    
    sei(); 
}

volatile uint8_t m20count = 0; 
volatile uint8_t m20 = 0; 

#if GEMMADEPLOY 
ISR(TIMER0_COMPA_vect)
#else
volatile uint8_t m20s = 0; 
ISR(TIMER2_COMPA_vect)
#endif
{
    #if GEMMADEPLOY == 0
        m20s++; 
        if (m20s < 4)
            return; 
        m20s = 0; 
    #endif
    m20++; 
    if (m20 == M20LOOPSIZE) {  // 100ms loop, though could step it down to take the average of a set of readings
        m20 = 0; 
        m20count++; 
        // digitalWrite(PinOut, (m20count & 2 ? LOW : HIGH)); // causes a flashing in pin out to verify it's controllable
    }
}


uint8_t ledactivecountdown = 2; 
uint16_t prevvalue3; 
uint16_t prevvalue2; 
uint16_t prevvalue1; 
uint16_t prevvalue; 
void loop()
{
    while (m20)  ;
    
    if (ledactivecountdown != 0) 
        digitalWrite(PinLED, HIGH); 
    int mV = (prevvalue > prevvalue3 ? M20LOOPSIZE-9 : 3);  
    
#ifdef ONESHOTREADING
    // kick off a reading if in oneshot mode
    Wire.beginTransmission(0x48);
    Wire.write((uint8_t)0x01); // config register
    Wire.write((uint8_t)(config >> 8));
    Wire.write((uint8_t)config);
    Wire.endTransmission();

    while (m20 < mV)  ;
    if (ledactivecountdown != 0) 
        digitalWrite(PinLED, LOW); 
    while (m20 < M20LOOPSIZE-7)  ;

    Wire.beginTransmission(0x48);
    Wire.write(0x00);
    Wire.endTransmission();
    Wire.requestFrom(0x48, 2);
    uint8_t r1 = Wire.read(); 
    uint8_t r2 = Wire.read(); 
    Wire.endTransmission();
    uint16_t vvalue = (r1 << 8) | r2;
#else

// take about 40 readings and return the max value, which should filter out the voltage drop noise cases, we hop
#if MAXMULTIPLEREADINGS
    uint16_t vvalue = 0; 
    uint32_t vvaluesum = 0; 
    for (int T = 1; T <= M20LOOPSIZE-7; T++) {
        while (m20 < T)  ;
        Wire.requestFrom(0x48, 2);
        uint8_t r1 = Wire.read(); 
        uint8_t r2 = Wire.read(); 
        Wire.endTransmission();
        uint16_t lvvalue = (r1 << 8) | r2;
        if (lvvalue > vvalue)
            vvalue = lvvalue; 
        //vvaluesum += vvalue; 
        if ((ledactivecountdown != 0) && (T == mV))
            digitalWrite(PinLED, LOW); 
    }
    //vvalue = vvaluesum/(M20LOOPSIZE-7); 

#else
    // continuous reading delay same amount for consistency.  transmission of 0x00 not necessary
    while (m20 < mV)  ;
    if (ledactivecountdown != 0) 
        digitalWrite(PinLED, LOW); 
    while (m20 < M20LOOPSIZE-7)  ;
    Wire.requestFrom(0x48, 2);
    uint8_t r1 = Wire.read(); 
    uint8_t r2 = Wire.read(); 
    Wire.endTransmission();
    uint16_t vvalue = (r1 << 8) | r2;
#endif
#endif

    if (ledactivecountdown != 0) {
        int mV = (vvalue > prevvalue3 ? M20LOOPSIZE-4 : 3);  // mostly on or off during the duty cycle
    #if GEMMADEPLOY == 0
        float mvolts = 1000*1.024 * vvalue / 32767.0; 
        float tmpforoled = (mvolts - 500)/10; 
        P(vvalue); 
        P(" "); 
        P(tmpforoled); 
        P(" "); 
        P((int16_t)(prevvalue - vvalue)); 
        P(" "); 
        P(mV); 
        P("\n"); 
    #endif
        prevvalue3 = prevvalue2; 
        prevvalue2 = prevvalue1; 
        prevvalue1 = prevvalue; 
        prevvalue = vvalue; 
        if (m20count == 255)
            ledactivecountdown--; 
    } 

    // output at end of window 
    while (m20 < M20LOOPSIZE-6)  ;
    sendbitbangbyte(BITTIME, 0x00 | (uint8_t)(vvalue & 0x3F)); 
    while (m20 < M20LOOPSIZE-4)  ;
    sendbitbangbyte(BITTIME, 0x40 | (uint8_t)((vvalue >> 6) & 0x3F)); 
    while (m20 < M20LOOPSIZE-2)  ;
    sendbitbangbyte(BITTIME, 0x80 | (uint8_t)((m20count << 4) & 0x30) | (uint8_t)((vvalue >> 12) & 0x0F)); 
}






