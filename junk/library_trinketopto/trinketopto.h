// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef TRINKETOPTO_h
#define TRINKETOPTO_h

#define GEMMADEPLOY 1

#if GEMMADEPLOY

// Tools -> Programmer -> USBtinyISP;  Tools -> Board -> Gemma 8Mz
#include <TinyWireM.h>   // needs commenting to subvert build system
#define P(X) 
#define PInit
#define Wire TinyWireM
#define PinOut 3
#define PinLED 1

#else

//#include <Wire.h>      // needs commenting to subvert build system
#define P(X)  Serial.print(X) 
#define PInit Serial.begin(9600)
#define PinOut 12  // otherwise interferes with the serial
#define PinLED 13

#endif

void okaysignal(char n); 
uint16_t readRegister16(uint8_t addr, uint8_t reg); 
void sendbitbangbyte(uint8_t bittime, uint8_t v); 

#endif

