// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include "trinketopto.h"
#if GEMMADEPLOY
#include <TinyWireM.h>
//#define Wire TinyWireM
#else
//#include <Wire.h>  // comment out accordingly to help the linker
#endif

uint16_t readRegister16(uint8_t addr, uint8_t reg)
{
    Wire.beginTransmission(addr);
    Wire.write(reg);
    Wire.endTransmission();
    Wire.beginTransmission(addr);
    Wire.requestFrom(addr, (uint8_t)2);
    for (uint16_t i = 500; !Wire.available(); i--) {
        if (i == 0) {
            P("readRegister16 failed\n"); 
            return 0; 
        }
        delayMicroseconds(50); 
    }
    uint8_t r1 = Wire.read(); 
    uint8_t r2 = Wire.read(); 
    Wire.endTransmission();
    uint16_t value = (r1 << 8) | r2;
    return value;
}

void okaysignal(char n) 
{
    for (char i = 0; i < n; i++) { 
        digitalWrite(PinLED, HIGH); // PORTD |= PinOutBit;
        delay(200); 
        digitalWrite(PinLED, LOW); // PORTD &= PinOutMask;
        delay(400); 
    }
    P("ok\n"); 
}

void sendbitbangbyte(uint8_t bittime, uint8_t v)
{
    //delayMicroseconds(BITTIME*2);
    digitalWrite(PinOut, LOW); 
    delayMicroseconds(bittime); 
    
    digitalWrite(PinOut, (v & 0x01 ? HIGH : LOW)); 
    delayMicroseconds(bittime); 
    digitalWrite(PinOut, (v & 0x02 ? HIGH : LOW)); 
    delayMicroseconds(bittime); 
    digitalWrite(PinOut, (v & 0x04 ? HIGH : LOW)); 
    delayMicroseconds(bittime); 
    digitalWrite(PinOut, (v & 0x08 ? HIGH : LOW)); 
    delayMicroseconds(bittime); 
    digitalWrite(PinOut, (v & 0x10 ? HIGH : LOW)); 
    delayMicroseconds(bittime); 
    digitalWrite(PinOut, (v & 0x20 ? HIGH : LOW)); 
    delayMicroseconds(bittime); 
    digitalWrite(PinOut, (v & 0x40 ? HIGH : LOW)); 
    delayMicroseconds(bittime); 
    digitalWrite(PinOut, (v & 0x80 ? HIGH : LOW)); 
    delayMicroseconds(bittime); 
    if (v & 0x80) {
        digitalWrite(PinOut, LOW);
        delayMicroseconds(bittime); 
    }
    digitalWrite(PinOut, HIGH);
}

