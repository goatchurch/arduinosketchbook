// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include "serialinputbuffer.h"

#define SERIALINPUTBUFFER_BUFFSIZE 128  // the Z-record is 65 bytes long (this is the line buffer)

// number used in teensy/avr/cores/teensy3/serial1.c
#define SIB_RX_BUFFER_SIZE     12500

#define P(X) Serial.print(X)

SerialInputBuffer::SerialInputBuffer(HardwareSerial* lserial) :
    serial(lserial)
{
    mstamprecdur = 0; 
    buff = (char*)malloc(SERIALINPUTBUFFER_BUFFSIZE*sizeof(char)); 
    bufflen = 0; 
    buff[0] = '\0';
    Dmaxnsavailable = 0;  
    bclearfirstbuffer = (serial != 0);  // get ready to clear out backlog of bytes
}


void SerialInputBuffer::SetupSerial(long baudrate)
{
    if (serial)
        serial->begin(baudrate);
    else
        Serial.begin(baudrate); // usb_serial_class problem
}

bool SerialInputBuffer::shouldselectivelydiscardrecordfromfillingbuffer()
{
    int nsavailable = serial->available(); 
    if (nsavailable < SIB_RX_BUFFER_SIZE/2)
        return false; 
P("getting long "); 
P(buff[0]); 
P(" "); 
P(nsavailable); 
P("\n");         
    if ((buff[0] == 'Z') || (buff[0] == 'F'))
        return true; 
    if (nsavailable > SIB_RX_BUFFER_SIZE - 100)
        return true; 
    return false; 
}

bool SerialInputBuffer::RecCheck()
{
    // starting off, throw away till buffer is clear and at the end of a record
    if (bclearfirstbuffer) {
        while (serial->available() > SIB_RX_BUFFER_SIZE/8)
            serial->read(); 
        while (serial->available()) {
            if (serial->read() == '\n') {
                bclearfirstbuffer = false; 
                break; 
            }
        }
        Dmaxnsavailable = 0; 
    }

    int nsavailable = (serial ? serial->available() : Serial.available()); 
    if (nsavailable == 0)
        return false; 
        
    if (nsavailable > Dmaxnsavailable) 
        Dmaxnsavailable = nsavailable; 
    while (true) {
        if (buff[bufflen] == '\0') {  // restarting new line
            mstamprecdur = millis(); 
            bufflen = -1; 
        }
        buff[++bufflen] = (serial ? serial->read() : Serial.read()); 
        if (bufflen == SERIALINPUTBUFFER_BUFFSIZE-3) {
            buff[++bufflen] = '\\';  // force continuation when overrun
            buff[++bufflen] = '\n';  
        }
        if (buff[bufflen] == '\n') {
            buff[bufflen] = '\0'; 
            mstamprecdur = millis() - mstamprecdur; 
            return true; 
        }
        nsavailable = (serial ? serial->available() : Serial.available()); 
        if (nsavailable == 0)
            break; 
    }
    return false; 
}

void SerialInputBuffer::SendSerial(char* chs)
{
    if (serial) {
        serial->print(chs); 
        serial->write('\n'); 
    } else {
        Serial.println(chs); 
    }
}



