// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef SERIALINPUTBUFFER_h
#define SERIALINPUTBUFFER_h

class SerialInputBuffer
{
public:
    HardwareSerial* serial; 
    int Dmaxnsavailable; 
    long mstamprecdur; // records record duration
    bool bclearfirstbuffer; 
    char* buff;        
    int8_t bufflen; 
    
    SerialInputBuffer(HardwareSerial* lserial); 
    void SetupSerial(long baudrate=9600); 
    bool RecCheck(); 
    void SendSerial(char* chs); 
    bool shouldselectivelydiscardrecordfromfillingbuffer(); 
};



#endif
