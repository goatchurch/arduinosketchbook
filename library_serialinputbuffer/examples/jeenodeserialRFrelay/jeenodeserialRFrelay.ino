#include <JeeLib.h>

// this sits on the Jeenode on the basebar attached to the Teensy and acts as an RF relay 
// to the harness Jeenode
#define BUFFSIZE 64

long mstamprecdur = 0; 
char buff[BUFFSIZE]; 
int8_t bufflen = 0; 

long pingfrequencyms = 10000; 
long mstamplastserialping; 
long mstamplastrfping; 

char* msg = "rping:12345678";  // r for ping from relay (both to teensy and to harness jeenode)
char* HexMstamponMSG(char ch, long mstamp)
{
    msg[0] = ch; 
    for (int i = 0; i < 8; i++) {
        char nb = ((mstamp >> (4*(7-i))) & 0x0F); 
        msg[6+i] = nb + (nb < 10 ? '0' : 'A'-10); 
    }
    return msg; 
}

bool RecCheck()
{
    while (Serial.available()) {
        if (buff[bufflen] == '\n') {
            mstamprecdur = millis(); 
            bufflen = -1; 
        }
        buff[++bufflen] = Serial.read(); 
        if (bufflen == BUFFSIZE-2) 
            buff[++bufflen] = '\n'; 
        if (buff[bufflen] == '\n') {
            mstamprecdur = millis() - mstamprecdur; 
            return true; 
        }
    }
}

void setup()
{
    Serial.begin(9600);
    buff[0] = '\n'; 
    rf12_initialize(1, RF12_868MHZ, 100);
    Serial.write("hi there\n"); 
    pinMode(4, OUTPUT); 

    long mstamp = millis(); 
    mstamplastserialping = mstamp + pingfrequencyms / 4; 
    mstamplastrfping = mstamp + pingfrequencyms * 3 / 4; 
}

void loop()
{
    // incoming from the Serial port tied to Serial3 on the Teensy
    if (RecCheck()) {
        // "echo" request
        if ((bufflen >= 5) && (buff[0] == 'e') && (buff[1] == 'c') && (buff[2] == 'h') && (buff[3] == 'o')) {
            Serial.write("eeko:"); 
            for (int i = 4; i < bufflen; i++)
                Serial.write(buff[i]); 
            Serial.write('\n'); 
            
        // any other message is automatically relayed to the rf unit
        } else {
            digitalWrite(4, HIGH); 
            rf12_sendNow(0, buff, bufflen);
            //Serial.write("Sending: "); 
            //for (int i = 0; i < bufflen; i++) {
            //    Serial.write(buff[i]); 
            //    Serial.write(" "); 
            //}
            //Serial.write("\n"); 
            // do we want an ack signal too?
            //delay(100); 
            digitalWrite(4, LOW); 
            //mstamplastrfping = millis(); // this suppresses the rping, but is too confusing
        }
    }
    
    // incoming signal from the RF unit relayed back to the serial port
    if (rf12_recvDone() && (rf12_crc == 0)) {
        Serial.write('H');  // code to say the source is from the harness
        for (int i = 0; i < rf12_len; i++)
            Serial.write((char)rf12_data[i]); 
        Serial.write('\n'); 
        //mstamplastserialping = millis(); // this suppresses the rping, but is too confusing
    }
    
    // send a message to the teensy to show we are alive
    long mstamp = millis(); 
    if (mstamp > mstamplastserialping + pingfrequencyms) {
        Serial.write('R');  // code to say the message is sourced from the relay jeenode
        Serial.write(HexMstamponMSG('r', mstamp)); 
        Serial.write('\n'); 
        mstamplastserialping = mstamp; 
    }
    
    // send a ping message out on the radio waves
    if (mstamp > mstamplastrfping + pingfrequencyms) {
        HexMstamponMSG('r', mstamp); 
        rf12_sendNow(0, msg, 14); 
        mstamplastrfping = mstamp; 
    }
}


