#include "Arduino.h"
#include "serialinputbuffer.h"

// one if these run with loop(), other with loop1()
// the Serial1 wires are connected together
// d100 - transmit delay(100) into loop in sending device
// e100 - use delay(100) in receiving device
// f100 - do an immediate delay(100) to test capabilities of buffers
// b9600 - set the baud rate either ends.  Should be able to reach to 6000000 according 
// a23s0 - set slot zero to analogRead pin 23 for streaming of analog values
//  to https://forum.pjrc.com/threads/23687-Teensy-3-0-UART-datarate

// lessons: 
//    seems safe to use baud rate at 750000
//    buffer is 64 chars
//    use availableForWrite() to find characters left in buffer
//    use write(uint8_t c) for byte writes
//    write() will hang and wait if the buffer is full
//    use available to see number of characters
//    use peek to look at first character
//    sensible use of these and first char disclosing the length of the record could eliminate need for a buffer

SerialInputBuffer* sb; 
SerialInputBuffer* sbS; 
char buff[100]; 
int T0 = 0; 
long baud1rate = 9600; 

#define P(X)  Serial.print(X)

char hexchar1(uint8_t c)  { return "0123456789ABCDEF"[0x0F & c]; }
int buff32(int offs, int32_t k)
{
    buff[offs++] = hexchar1(k >> 28); 
    buff[offs++] = hexchar1(k >> 24); 
    buff[offs++] = hexchar1(k >> 20); 
    buff[offs++] = hexchar1(k >> 16); 
    buff[offs++] = hexchar1(k >> 12); 
    buff[offs++] = hexchar1(k >> 8); 
    buff[offs++] = hexchar1(k >> 4); 
    buff[offs++] = hexchar1(k); 
    return offs; 
}

int buff12(int offs, int k)
{
    buff[offs++] = hexchar1(k >> 8); 
    buff[offs++] = hexchar1(k >> 4); 
    buff[offs++] = hexchar1(k); 
    return offs; 
}

#define NSLOTS 4
int slots[NSLOTS]; 

void setup()
{
    sb = new SerialInputBuffer(&Serial1); 
    sb->SetupSerial(baud1rate); 
    T0 = millis(); 
    
    // this seems like a fake serial buffer that doesn't respect baud rate
    sbS = new SerialInputBuffer(0); 
    sbS->SetupSerial(9600); 
    pinMode(13, OUTPUT);
    Serial.begin(9600);  

    //sb = sbS; // uncomment to make output to terminal monitoring for testing
              // remember to set line endings to Newline or it won't pick up your typed commands
              
    for (int i = 0; i < NSLOTS; i++)
        slots[i] = -1; 
    pinMode(23, INPUT); 
}


long n = 0; 
long delaymillis = 100; 
long tserialwritetime = 0; 
long nwaitloop = 0; 

void loop()
{
    int offs = 0; 
    for (int i = 0; i < NSLOTS; i++) {
        if (slots[i] != -1) {
            buff[offs++] = 'a'; 
            offs = buff12(offs, analogRead(slots[i])); 
        }
    }   

    // conventional case when no slots are filled
    if (offs == 0) {
        buff[offs++] = 't'; 
        offs = buff32(offs, millis()); 
        
        buff[offs++] = ' '; 
        buff[offs++] = 'n'; 
        offs = buff32(offs, n++); 
        
        buff[offs++] = ' '; 
        buff[offs++] = 'w'; 
        offs = buff32(offs, tserialwritetime); 
        //offs = buff32(offs, nwaitloop); 
    } else {
        buff[offs++] = ' '; 
        buff[offs++] = 'n'; 
        offs = buff32(offs, n++);  // append a count onto analog read cases
    }
        
    buff[offs++] = '\0'; 

    // prove that we can do the waiting on the outside of SendSerial()
    if (sb != sbS)  {  // (hangs if we 
        nwaitloop = 0; 
        while (sb->serial->availableForWrite() < 30)
            nwaitloop++; 
    }
        
    long swT0 = micros(); 
    digitalWrite(13, HIGH); 
    sb->SendSerial(buff); 
    tserialwritetime = micros() - swT0; 
    digitalWrite(13, LOW); 
    
    if (sb->RecCheck()) {
        if (sb->buff[0] == 'd') {
            delaymillis = atoi(sb->buff+1); 
            digitalWrite(13, HIGH); delay(200); digitalWrite(13, LOW); delay(200); digitalWrite(13, HIGH); delay(200); digitalWrite(13, LOW); 
        } else if (sb->buff[0] == 'b') {
            baud1rate = atoi(sb->buff+1);
            digitalWrite(13, HIGH); 
            delay(500); 
            sb->serial->begin(baud1rate); 
            digitalWrite(13, LOW); 
            delay(500); 
            digitalWrite(13, HIGH); 
            delay(500); 
            digitalWrite(13, LOW); 
        } else if (sb->buff[0] == 'a') {
            int sn = sb->buff[4]-'0'; 
            int an = (sb->buff[1]-'0')*10 + (sb->buff[2]-'0'); 
            if ((sn >= 0) && (sn < NSLOTS) && (an >= 14) && (an <= 23) && (sb->buff[3] == 's')) {
                slots[sn] = an; 
                pinMode(an, INPUT); 
                for (int i = 0; i < an; i++) {
                    digitalWrite(13, HIGH); delay(50); digitalWrite(13, LOW); delay(50);
                }
            }
        }
    }
    delay(delaymillis); 
}

long hexval(char c)  { return (c <= '9' ? c - '0' : c - 'A' + 10); }
long hex32read(char* buff)
{
    return (hexval(buff[0]) << 28) | (hexval(buff[1]) << 24) | (hexval(buff[2]) << 20) | (hexval(buff[3]) << 16) | (hexval(buff[4]) << 12) | (hexval(buff[5]) << 8) | (hexval(buff[6]) << 4) | (hexval(buff[7]) << 0); 
}

long prevrmillis = 0; 
long prevrn = 0; 
int delaymillisRead = 5; 
int Nrecs = 0; 
int Nbadtimesteps = 0; 
int Nbadns = 0; 
void loop1()
{
     if (sbS->RecCheck()) {
         P("III "); 
         Serial.println(sbS->buff); 
         if (sbS->buff[0] == 'd') {   // delay code in other loop
             Serial.println(atoi(sbS->buff+1)); 
             sb->SendSerial(sbS->buff); 
             delaymillis = atoi(sbS->buff+1); 
         } else if (sbS->buff[0] == 'e') {
             delaymillisRead = atoi(sbS->buff+1); 
         } else if (sbS->buff[0] == 'f') {
             delay(atoi(sbS->buff+1)); 
         } else if (sbS->buff[0] == 'b') {
             sb->SendSerial(sbS->buff); 
             baud1rate = atoi(sbS->buff+1);
             sb->serial->flush(); 
             delay(100);  
             sb->serial->begin(baud1rate); 
         }
     }     
     while (sb->RecCheck()) {
         if ((sb->buff[0] == 't') && (strlen(sb->buff) == 29)) {
             Nrecs++; 
             long rmillis = hex32read(sb->buff+1); 
             long rn = hex32read(sb->buff+11); 
             if (abs(rmillis - prevrmillis - delaymillis > 3))
                 Nbadtimesteps++; 
             if (rn != prevrn + 1)
                 Nbadns += rn - (prevrn + 1); 
             prevrmillis = rmillis; 
             prevrn = rn; 
         }
    }
     
    long T1 = millis(); 
    if (T1 - T0 > 200) {
        P("n="); 
        P(Nrecs); 
        P(" badm="); 
        P(Nbadtimesteps); 
        P(" badns="); 
        P(Nbadns); 
        P(" : "); 
        P(sb->buff); 
        P("\n"); 

        Nrecs = 0; 
        Nbadtimesteps = 0; 
        Nbadns = 0; 
        T0 = T1; 
    } 
    delay(delaymillisRead); 
}


