// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef SSLOGGING_h
#define SSLOGGING_h

// used to over-ride the SDloggerBase and reuse all the functions SDlogging functions with minimal coding
class SerialStreamlogger : public SDloggerBase
{
public:
    long nbufferfullskipped; 
    long Nmcount; 

    HardwareSerial* serial; 
    long baudrate; 
    SerialStreamlogger(HardwareSerial* lserial, long lbaudrate); 
    virtual void close(); 
    virtual void flush(); 
    virtual bool writestring(const char* str);
    virtual void writeline(int offs); 
    virtual size_t write(uint8_t v); 
    
    virtual void SetupSD(); 
    virtual void SDprintStats();  // if you call this after log file is open, it drops the handle
    virtual bool opennextlogfileforwriting(const char* pisodatetime, uint32_t mstamp); 
    virtual bool checkfornextflush(); 
};

#endif
