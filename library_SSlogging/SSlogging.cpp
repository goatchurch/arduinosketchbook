// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include "SDloggingBase.h"
#include "SSlogging.h"

#define P(X) Serial.print(X)

SerialStreamlogger::SerialStreamlogger(HardwareSerial* lserial, long lbaudrate) :
    SDloggerBase(0, 0), serial(lserial), baudrate(lbaudrate)
{
    Nmcount = 0; 
    nbufferfullskipped = 0;
    if (serial)
        serial->begin(baudrate); 
    else
        Serial.begin(baudrate); 
}

void SerialStreamlogger::close()  {;}
void SerialStreamlogger::flush()  
{
    logmrecord(millis(), Nlinessincelastflush, Nmcount); 
    Nmcount++; 
    
    // these values are also reset in checkfornextflush(), to avoid confusion
    mstamplastflush = mstamplaststartline; 
    Nlinessincelastflush = 0; // means we don't include this line in the count
    Ncharssincelastflush = 0; 
    
    // no point in actually flushing the serial line (unless the buffer is full) as all it does is hang here till it's clear
    serial->flush(); 
}

bool SerialStreamlogger::checkfornextflush()
{
    long mstamp = millis(); 
    if ((Ncharssincelastflush > 128000) || (Nlinessincelastflush == 5000) || (mstamp - mstamplastflush > 10000)) {
        flush();  // resets the values 
        mstamplastflush = mstamp; 
        Nlinessincelastflush = 0; // means we don't include this line in the count
        Ncharssincelastflush = 0; 
        return true; 
    }
    return false; 
}


bool SerialStreamlogger::writestring(const char* str) 
{
    if (serial) {
        serial->write(str); // includes the \n
    } else {
        Serial.print(str); 
    }
    return true; 
}

// used by Print interface
size_t SerialStreamlogger::write(uint8_t v) 
{
    if (serial)
        return serial->write(v);   
    return Serial.write(v);   
}


void SerialStreamlogger::SDprintStats()  {
    Serial.println("SDprintStats");
}
void SerialStreamlogger::SetupSD() {
    Serial.println("SetupSD");
}
bool SerialStreamlogger::opennextlogfileforwriting(const char* pisodatetime, uint32_t mstamp) { 
    Serial.println("opennextlogfileforwriting"); Serial.flush(); 
    Serial.println(mstamp); Serial.flush(); 
    return false;   // return false prevents header from being put there
}

void SerialStreamlogger::writeline(int offs)
{
    Nloglines++; 
    Nlinessincelastflush++; 
    Ncharssincelastflush += offs; 
    mstamplastsuccess = mstamplaststartline; 
    if (pinLEDforflush != -1)
        digitalWrite(pinLEDforflush, HIGH); 
    if (serial) {
        sddata[offs++] = '\n'; 
        if (offs > serial->availableForWrite()) {
            P("!!! Skipping: "); 
            Serial.write(sddata, offs); 
            P("serial->availableForWrite()="); 
            P(serial->availableForWrite()); 
            P("\n"); 
            // consider changing TX_BUFFER_SIZE in hardware/teensy/avr/cores/teensy3/serial1.c
            nbufferfullskipped++; 
        } else {
            serial->write(sddata, offs); 
        }
    } else {
        sddata[offs++] = '\0'; 
        Serial.println((char*)sddata); 
    }
    if (pinLEDforflush != -1)
        digitalWrite(pinLEDforflush, LOW); 
    return; 
}


