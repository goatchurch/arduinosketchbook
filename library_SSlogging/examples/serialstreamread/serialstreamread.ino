#include "serialinputbuffer.h"

#define P(X) Serial.print(X)

SerialInputBuffer ssi(&Serial1); 

const char* rectypes = "ZYQVPFDWHISGLNMOBX"; 
int rectypecounts[19]; 

int resetrectypecounts() 
{
    int res = 0; 
    for (int i = 0; i < 18; i++) {
        res += rectypecounts[i]; 
        rectypecounts[i] = 0; 
    }
    return res; 
}

void updaterectypecount(char ch)
{
    const char* rj = strchr(rectypes, ch); 
    if (rj != 0) 
        rectypecounts[rj - rectypes]++; 
    else
        P(ch); 
}

void setup() 
{
    Serial.begin(9600); 
    ssi.SetupSerial(750000); 
    resetrectypecounts(); 
}

long prevmtime = 0; 
void loop() 
{
    //while(Serial1.available())  Serial.write(Serial1.read());  return; 
  
    while (ssi.RecCheck()) {
        if (ssi.buff[0] == 'M') {
            P(ssi.buff); 
            long t = strtol(ssi.buff+2, 0, 16); 
            long l = strtol(ssi.buff+11, 0, 16); 

            for (int i = 0; i < 18; i++) {
                if (rectypecounts[i] != 0) {
                    P(" "); 
                    P(rectypes[i]); 
                    P(rectypecounts[i]); 
                }
            }
            
            long lr = resetrectypecounts(); 
            P(" "); 
            P(t - prevmtime); 
            prevmtime = t; 
            P(" l="); 
            P(l); 
            P(" "); 
            P(lr); 
            P("\n"); 
        } else {
            updaterectypecount(ssi.buff[0]); 
        }
    }
}
