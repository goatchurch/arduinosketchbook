// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef SIMPLERF_h
#define SIMPLERF_h

class SimpleRF
{
public:
    int pinread; 
    int pinpower; 
    
    SimpleRF(int lpinread, int lpinpower); 
};


#endif
