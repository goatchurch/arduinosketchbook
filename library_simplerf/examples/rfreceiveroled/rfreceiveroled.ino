#include <SPI.h>
#include <Wire.h>  // needed for compilation of oled library even if we don't use it
#include <SFE_MicroOLED.h>
#include "oledoutput.h"
#include "simplerf.h"

#define P(X) Serial.print(X)

OledOutput oled(10, 8, 9); 
SimpleRF simplerf(4, 3); 

void setup() 
{
    Serial.begin(9600);
    delay(500);
    oled.SetupOLED(); 
}

int n = 0; 
void loop()
{
  oled.clear(PAGE);            // Clear the display
  int px = 0; 
  int py = 0; 
  for (int i = 0; i < 120*16; i++) {
      delayMicroseconds(14); 
      int r = digitalRead(simplerf.pinread); 
      if (r)
        oled.pixel(px, py); 
      px++; 
      if (px == LCDWIDTH) {
          px = 0; 
          py++; 
      }
  }
  oled.display();
  delay(100); 
  if ((n % 50) == 0) {
    if ((n/50 % 8) != 7) {
      digitalWrite(simplerf.pinpower, HIGH); 
      Serial.println("enpin high"); 
    } else {
      digitalWrite(simplerf.pinpower, LOW); 
      Serial.println("enpin low"); 
    }
  }
  n++; 
}
