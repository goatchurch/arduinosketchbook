#include "simplerf.h"

SimpleRF simplerf(4, 3); 

void setup() {
  Serial.begin(9600); 

}

int n = 0; 
void loop() {
  for (int i = 0; i < 100; i++) {
      delayMicroseconds(18); 
      Serial.print(digitalRead(simplerf.pinread)); 
  }
  Serial.println(); 
  delay(100); 
  if ((n % 100) == 0) {
    if ((n/100 % 2) == 0) {
      digitalWrite(simplerf.pinpower, HIGH); 
      Serial.println("enpin high"); 
    } else {
      digitalWrite(simplerf.pinpower, LOW); 
      Serial.println("enpin low"); 
    }
  }
  n++; 
}

