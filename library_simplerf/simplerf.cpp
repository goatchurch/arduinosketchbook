// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"
#include "simplerf.h"

#define P(X) Serial.print(X)
//#define P(X)


SimpleRF::SimpleRF(int lpinread, int lpinpower) 
{
    pinread = lpinread; 
    pinpower = lpinpower; 
    pinMode(pinread, INPUT); 
    pinMode(pinpower, OUTPUT); 
}
    


