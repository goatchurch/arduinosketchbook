// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <Wire.h>
#include "gyros.h"

// DLPF_CFG = Bandwidth 20Hz, delay 8ms (filters)
#define DLPF_CFGval 0x04
#define DLPF_CFGdelay 8

// DLPF_CFG = Bandwidth 5Hz, delay 19ms (filters)
//#define DLPF_CFGval 0x06
//#define DLPF_CFGdelay 19


GyroData::GyroData()
{
}

void GyroData::WriteByte(uint8_t reg, uint8_t val)
{
    Wire.beginTransmission(0x68);
    Wire.write(reg);
    Wire.write(val);
    Wire.endTransmission(true); 
}

// see RM-MPU-9150A.pdf
void GyroData::SetupGyro()
{
    WriteByte(0x1C, 0x00);  // AFS_SEL 2g gain
    WriteByte(0x19, 0x00);  // SMPRT_DIV  (can't see the difference here when made higher)
    WriteByte(0x1A, DLPF_CFGval);  // DLPF_CFG = Bandwidth 20Hz, delay 8ms (filters)
    WriteByte(0x6B, 0x00);  // PWR_MGMT_1 register, set to zero (wakes up the MPU-6050)
}

void GyroData::PrintSettings() 
{
    Wire.beginTransmission(0x68);
    Wire.write(0x19);
    Wire.endTransmission(false);
    Wire.requestFrom(0x68, 7, true);  
    for (int i = 0; i < 7; i++) {
        Serial.print(i+0x19, HEX); 
        Serial.print(" "); 
        Serial.println(Wire.read(), HEX); 
    }
}

void GyroData::ReadGyro() 
{
    mstamp = millis() - DLPF_CFGdelay; // compensate for the delay from the DLPF_CFG filtering
    
    Wire.beginTransmission(0x68);
    Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
    Wire.endTransmission(false);
    
    Wire.requestFrom(0x68, 14, true);  // request a total of 14 registers
    AcX = Wire.read()<<8;
    AcX |= Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)     
    AcY = Wire.read()<<8;
    AcY |= Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
    AcZ = Wire.read()<<8; 
    AcZ |= Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
    Tmp = Wire.read()<<8; 
    Tmp |= Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
    GyX = Wire.read()<<8; 
    GyX |= Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
    GyY = Wire.read()<<8; 
    GyY |= Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
    GyZ = Wire.read()<<8; 
    GyZ |= Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
}


