#include <Wire.h>
#include "gyros.h"

#define P(X) Serial.print(X)

GyroData gyrodata; 

int cubeside = 0; 
char* sidenames[6] = { "top", "left", "back", "right", "front", "upside down" }; 

int measuredcubeside = -1; 
long measuredcubesidetimems = 0; 
int pinmode20 = LOW; 

float flatthreshhold = 0.8; 
int setcubeside(float ax, float ay, float az) 
{
    if (az > flatthreshhold)
        return 0; // top
    if (ay > flatthreshhold)
        return 1; // left
    if (ax > flatthreshhold)
        return 2; // back
    if (ay < -flatthreshhold)
        return 3;
    if (ax < -flatthreshhold)
        return 4; 
    if (az < -flatthreshhold)
        return 5; 
    return -1; // in between
}

void setup() 
{
    Serial.begin(9600);
    Wire.begin(); 
    gyrodata.CheckHardware(); 
    
    pinMode(20, OUTPUT); 
    pinmode20 = LOW; 
    digitalWrite(20, pinmode20); 
}


void loop() 
{
    delay(100);
    
    gyrodata.MakeReading(); 
    long vsq = gyrodata.AcX*gyrodata.AcX + gyrodata.AcY*gyrodata.AcY + gyrodata.AcZ*gyrodata.AcZ; 
    float vlen = sqrt(vsq); 
    float ax = gyrodata.AcX/vlen; 
    float ay = gyrodata.AcY/vlen; 
    float az = gyrodata.AcZ/vlen; 
    /*
    P(vlen); P("   "); 
    P(ax); P(" "); 
    P(ay); P(" "); 
    P(az); P("\n"); 
    */
    int lcubeside = setcubeside(ax, ay, az);  
    
    // measure length of time in one position so we can flip upside down
    if (lcubeside != measuredcubeside) {
        measuredcubeside = lcubeside; 
        measuredcubesidetimems = millis(); 
    }

    // we've been still for 200ms
    if ((measuredcubeside != cubeside) && (millis() > measuredcubesidetimems + 200) && (measuredcubeside != -1)) {
        cubeside = measuredcubeside; 
        P("Change to "); 
        P(sidenames[cubeside]); 
        P("\n"); 
        pinmode20 = (pinmode20 == LOW ? HIGH : LOW); // toggle light
        digitalWrite(20, pinmode20); 
    }
    
    return; 
}

