#include <Wire.h>
#include "gyros.h"

#define P(X) Serial.print(X)

GyroData gyrodata; 

void setup() 
{
    Serial.begin(19200);
    Serial.println("hi there"); 
    
    Wire.begin(); 
    gyrodata.SetupGyro(); 
    pinMode(9, OUTPUT); 

        // 11000 RPM at 5V, 183Hz, 1rev per 5.45ms 
        // (at 100/255 we have a measure of about 60ms, 12 times slower for 40% the voltage)
    analogWrite(9, 50); 
}

int n = 0; 
bool bvibratoron = false; 
long mstamp = 0; 
int readings[100*3]; 

int gyroburst(int* vals, int n, int mtime) 
{
    long mstamp = millis(); 
    for (int i = 0; i < n;  i++) {
        mstamp += mtime; 
        while (millis() < mstamp)
            ;
        gyrodata.ReadGyro(); 
        *vals++ = gyrodata.AcX; 
        *vals++ = gyrodata.AcY; 
        *vals++ = gyrodata.AcZ; 
    }
    return millis() - mstamp; 
}

float dsq(float x, float y, float z) 
{
    return x*x + y*y + z*z; 
}
float Square(float x)  
{
    return x*x; 
}

void getelongationvector(float& vx, float& vy, float& vz, float x0, float y0, float z0, int* vals, int n)
{
    long xg2sum = 0; 
    long yg2sum = 0; 
    long zg2sum = 0; 
    for (int i = 0; i < n; i++) {
        xg2sum += Square(vals[i*3] - x0); 
        yg2sum += Square(vals[i*3+1] - y0); 
        zg2sum += Square(vals[i*3+2] - z0); 
    }
    float xe = sqrt(xg2sum); 
    float ye = sqrt(yg2sum); 
    float ze = sqrt(zg2sum); 
    float velen = sqrt(xe*xe + ye*ye + ze*ze); 
    vx = xe/velen; 
    vy = ye/velen; 
    vz = ze/velen; 

    // need to find the actual resolution direction
    float v00 = 0, v01 = 0, v10 = 0, v11 = 0; 
    for (int i = 0; i < n; i++) {
        float kx = (vals[i*3] - x0)*vx; 
        float ky = (vals[i*3+1] - y0)*vy; 
        float kz = (vals[i*3+2] - z0)*vz; 
        v00 += Square(kx+ky+kz); 
        v01 += Square(kx-ky+kz); 
        v10 += Square(kx+ky-kz); 
        v11 += Square(kx-ky-kz); 
    }
    
    if ((v00 >= v01) && (v00 >= v10) && (v00 >= v11)) 
       ;
    else if ((v01 >= v10) && (v01 >= v11)) 
        vy = -vy; 
    else if (v10 >= v11) 
        vz = -vz; 
    else {
        vy = -vy; 
        vz = -vz; 
    }
}

void measurefacts(int* vals, int n, int mtime) 
{
    long xsum = 0; 
    long ysum = 0; 
    long zsum = 0; 
    for (int i = 0; i < n; i++) {
        xsum += vals[i*3]; 
        ysum += vals[i*3+1]; 
        zsum += vals[i*3+2]; 
    }
    float x0 = (float)xsum/n; 
    float y0 = (float)ysum/n; 
    float z0 = (float)zsum/n; 
    float glen = sqrt(x0*x0 + y0*y0 + z0*z0); 
  
    // presumed attempt to get the longest axis
    float vx, vy, vz; 
    getelongationvector(vx, vy, vz, x0, y0, z0, vals, n); 
    
    float prevt0 = -1; 
    float prevv = -1; 
    float sumgt = 0; 
    float sumgtsq = 0; 
    float sumsq = 0; 
    int ngt = -1; 
    for (int i = 0; i < n; i++) {
        float v = (vals[i*3] - x0)*vx + (vals[i*3+1] - y0)*vy + (vals[i*3+2] - z0)*vz; 
        sumsq += v*v; 
        if ((i != 0) && (prevv < 0) && (v >= 0)) {
            float lam = (0-prevv)/(v - prevv); 
            float t0 = (i-1)*mtime*(1-lam) + i*mtime*lam; 
            ngt++; 
            if (ngt != 0) {
                float gt = t0 - prevt0; 
                sumgt += gt; 
                sumgtsq += gt*gt; 
            }
            prevt0 = t0; 
        }
        prevv = v; 
    }
    Serial.print(ngt); 
    Serial.print(" g"); 
    Serial.print((int)glen); 
    Serial.print(" Avg Wavtime ");  
    if (ngt >= 4) {
        float wav = sumgt / ngt; 
        float freq = 1000/wav; 
        Serial.print(wav); 
        Serial.print(" var "); 
        Serial.print((sumgtsq - sumgt*sumgt/ngt)/(ngt-1)); 
        Serial.print(" freq "); 
        Serial.print(freq); 
        Serial.print(" RMSacc "); 
        float rmsa = sqrt(sumsq/n)/glen*9800; 
        Serial.print(rmsa); 
        Serial.print(" mov "); 
        Serial.print(rmsa/(freq*sqrt(2)*3.14159)); 
        Serial.print(" mm/s."); 
        Serial.print("  elongv "); 
        Serial.print(vx); 
        Serial.print(" "); 
        Serial.print(vy); 
        Serial.print(" "); 
        Serial.print(vz); 
    }
    
    Serial.print('\n'); 
}


void loop() 
{
    int mtime = 3; 
    int n = 100;   // heap overflow if we go beyond this
    int mslip = gyroburst(readings, n, mtime); 

    if (true) {
        Serial.print("\nslip: "); 
        Serial.println(mslip);  
        for (int i = 0; i < n; i++) {
            Serial.print(i*mtime); 
            Serial.print(' '); 
            Serial.print(readings[i*3]); 
            Serial.print(' '); 
            Serial.print(readings[i*3+1]); 
            Serial.print(' '); 
            Serial.print(readings[i*3+2]); 
            Serial.print('\n'); 
        }
        Serial.print('\n'); 
    }
    
    measurefacts(readings, n, mtime); 
}

/*
# theory to recover the energy in the theoretical data
#   position = m*sin(pi*2*freq*t)
#   velocity = m*pi*2*freq*cos(pi*2*freq*t)
#     maxvel = m*pi*2*freq
#   accel    = -m*pi^2*4*freq^2*sin(pi*2*freq*t)
#   rmsaccel = m*pi^2*freq^2*2*sqrt(2)
# maxvel = rmsaccel/(pi*freq*sqrt(2))
freq = 1000/fwav
trmsaccel = math.sqrt(sum(v**2  for t, v in tvs)/len(tvs))
print(trmsaccel, m/math.sqrt(2))  # good match
*/

