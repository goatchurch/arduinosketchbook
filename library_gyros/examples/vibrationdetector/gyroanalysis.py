import re, math
from woody.icovarmat import covarmatC

fname = "/home/goatchurch/datalogging/arduinosketchbook/library_gyros/examples/vibrationdetector/gyrodata_gerald.txt"
ftxt  = open(fname).read()
fblocks = re.findall("slip.*?\n(.*?)\n\n(?s)", ftxt)

fblock = fblocks[-1]
yo = 0.5
sendactivity("clearalltriangles")

def plotk(fblock, yo):
    k0 = [ list(map(int, fline.split()))  for fline in fblock.split("\n")]
    x0 = sum(x  for t, x, y, z in k0)/len(k0)
    y0 = sum(y  for t, x, y, z in k0)/len(k0)
    z0 = sum(z  for t, x, y, z in k0)/len(k0)
    tfac, fac = 0.002, 0.00001
    def Cont(k):
        return [[(t*tfac,0,0), (t*tfac+(x-x0)*fac,(y-y0)*fac,(z-z0)*fac)]  for t, x, y, z in k]
    sendactivity(contours=Cont(k0))
    pts = [(x-x0,y-y0,z-z0)  for t,x,y,z in k0]
    sendactivity(points=[(x*fac,y*fac,z*fac)  for x,y,z in pts])

    comat = covarmatC()
    for t,x,y,z in k0:  comat.addptC(x,y,z)
    comat.flatmat()
    vx,vy,vz = comat.ei[2]
    
    xe = math.sqrt(sum((x-x0)**2  for t, x, y, z in k0)); 
    ye = math.sqrt(sum((y-y0)**2  for t, x, y, z in k0)); 
    ze = math.sqrt(sum((z-z0)**2  for t, x, y, z in k0)); 
    velen = math.sqrt(xe*xe + ye*ye + ze*ze); 
    vx = xe/velen; 
    vy = ye/velen; 
    vz = ze/velen; 
    vpp = sum((((x-x0)*vx+(y-y0)*vy+(z-z0)*vz))**2  for t,x,y,z in k0)
    vpm = sum((((x-x0)*vx-(y-y0)*vy+(z-z0)*vz))**2  for t,x,y,z in k0)
    vmp = sum((((x-x0)*vx+(y-y0)*vy-(z-z0)*vz))**2  for t,x,y,z in k0)
    vmm = sum((((x-x0)*vx-(y-y0)*vy-(z-z0)*vz))**2  for t,x,y,z in k0)
    k = max([(vpp, 0), (vpm,1), (vmp, 2), (vmm, 3)])[1]
    if k in [1,3]:
        vy = -vy
    if k in [2,3]:
        vz = -vz
    
    #print((vx,vy,vz), comat.ei[2])

    
    vs = [(t, ((x-x0)*vx+(y-y0)*vy+(z-z0)*vz))  for t,x,y,z in k0]
    sendactivity(contours=[[(t*tfac,v*fac+yo)  for t,v in vs]], materialnumber=1)
    sendactivity(points=[(t*tfac,v*fac+yo)  for t,v in vs], materialnumber=1)
    t0s = [ ]
    for i in range(1, len(vs)):
        if (vs[i-1][1] < 0) and vs[i][1] >= 0: 
            lam = (0-vs[i-1][1])/(vs[i][1] - vs[i-1][1])
            t0s.append(vs[i-1][0]*(1-lam) + vs[i][0]*lam)

    sendactivity(points=[(t*tfac,(t1-t)/50)  for t, t1 in zip(t0s, t0s[1:])], materialnumber=3)
    td = [t1-t0  for t0, t1 in list(zip(t0s, t0s[1:]))]
    if td:
        fwav = sum(td)/len(td)
        freq = 1000/fwav
        g9p8 = math.sqrt(x0**2+y0**2+z0**2)
        rmsaccel = math.sqrt(sum(v**2  for t, v in vs)/len(vs))/g9p8*9800
        maxaccel = max(abs(v)  for t, v in vs)/g9p8*9800
        print(g9p8, "wav ", fwav, freq, "rms facrt2", rmsaccel, rmsaccel*math.sqrt(2), maxaccel, "mm/s motion", rmsaccel/(math.pi*freq*math.sqrt(2)))

#print(rmsaccel/(math.pi*freq*math.sqrt(2)))  # approx frequency
        
            
            
for i in range(1,len(fblocks)):
    plotk(fblocks[-i], i*0.5)
    
plotk(fblocks[-1], 4*0.5)
    
#plotk(fblocks[-1], 1*0.5)
           
           
