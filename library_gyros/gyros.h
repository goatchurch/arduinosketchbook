// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef GYROS_h
#define GYROS_h

// should separate gyro from compass I think.  Two different devices
// then a third class that might combine the two if necessary.  
// http://diyhacking.com/arduino-mpu-6050-imu-sensor-tutorial/
class GyroData
{
public:
    long mstamp; 
    int16_t AcX, AcY, AcZ; 
    int16_t Tmp; // temperature 
    int16_t GyX, GyY, GyZ;

    GyroData(); 
    void WriteByte(uint8_t reg, uint8_t val); 
    void PrintSettings(); 
    void SetupGyro(); 
    void ReadGyro(); 
};


#endif
