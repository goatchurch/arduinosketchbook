# README #

Julian's complex hang-glider flight loggger

### Assemblige Information ###

Implemented with a multiplicity of sensors across two Teensy3.1 devices connected 
by a 750000000 baud transfer serial line.  

Check this code out as the arduinosketchbook directory and then add symbolic links 
into the libraries directory:

ln -s ../library_SDlogging libraries/SDlogging

The main sensor Teensy3.1 (bottom shelf) runs library_flightlogger/examples/sensortoserial

The display and SD card Teensy3.1 runs library_flightlogger/examples/logoledfromstream

They both are based on the same class (using the same interface to write to the SD card 
as to write to the transfer serial line, and it is possible to rewire a sensor from one to 
another by changing which objects are initialized.  

The original design had a single board, but there were too many delays running the 
SD card, so the two controller method was made and it was convenient to leave the 
code the same on each board.  

There are individual example files in each library_ directory to test the performance 
of each sensor individually.

We generally transfer and record sensor readings in raw numbers, so the conversion is 
done in the individual examples, or in flystatus.cpp which is sets the numbers and graphs 
that appear on the display.
