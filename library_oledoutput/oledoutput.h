// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef OLEDOUTPUT_h
#define OLEDOUTPUT_h

// I think this is 64x48 pixels

// Do not use this one.  It's the small and redundant OLED, but there is a lot of code to delete if 
// you get rid of this library first

class OledOutput : public  MicroOLED
{
public: 
    OledOutput(uint8_t rst, uint8_t dc, uint8_t cs);
    char* oledbuffer; 
    
    void SetupOLED(const char* s=0, int devicenumber=-1); 
    void write_oled(const char* s, const char* s2=0);  
    void write_gps(int x, int y, int a, const char* isotimestamp); 
    void writechar(char c); 
    void writebaro(int32_t Pr, int16_t temp, int32_t tbaro); 
    void writebaroF(int32_t PrF, int32_t PrFsmoothed, uint8_t currentstate); 
    void writetemps(float t0, float t1, float t2); 
    void writewind(int32_t revtime); 
    void writelightlevel(int16_t lightlevel); 
    void writeir(int16_t irt16, int16_t irtamb16); 
    void writehumidity(char cc, int16_t rawHumidity, int16_t rawTemperature); 
    void writeorient(char cc, int16_t qw, int16_t qx, int16_t qy, int16_t qz, uint8_t currentstate); 
    void writevinvolts(char cc, uint32_t vinvolts); 
    void writefilestatus(char* logfilename, uint32_t mstamp, int32_t Nloglines, int32_t Nloglinesfailed); 
}; 


#endif
