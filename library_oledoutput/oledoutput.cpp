// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <SFE_MicroOLED.h>
#include "oledoutput.h"

#define P(X) Serial.print(X)

OledOutput::OledOutput(uint8_t rst, uint8_t dc, uint8_t cs) :
    MicroOLED(rst, dc, cs)
{
    oledbuffer = 0; 
    pinMode(cs, OUTPUT); 
    digitalWrite(cs, HIGH); 
}

void OledOutput::SetupOLED(const char* s, int devicenumber)
{
    MicroOLED& oled = *this; 
    oled.begin();    // Initialize the OLED
    oled.clear(ALL); // Clear the display's internal memory
    oled.display();  // Display what's in the buffer (splashscreen)
    delay(500);     // Delay 1000 ms
    oledbuffer = (char*)malloc(50*sizeof(char)); 

    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    if (s != 0) {
        while (*s != 0) 
            oled.write(*(s++));
    }
    if (devicenumber != -1) {
        oled.write('#');
        oled.print(devicenumber);
    }
    oled.display();
}

void OledOutput::write_oled(const char* s, const char* s2) 
{
    MicroOLED& oled = *this; 
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    while (*s != 0) 
        oled.write(*(s++));
    if (s2 != 0) {
        oled.write('\n');
        while (*s2 != 0) 
            oled.write(*(s2++));
    }
    oled.display();
}

void OledOutput::writechar(char c)
{
    MicroOLED& oled = *this; 
    oled.clear(PAGE);     
    oled.setFontType(1);  
    oled.setCursor(28, 18);
    oled.write(c);
    oled.display();
}

void OledOutput::write_gps(int x, int y, int a, const char* isotimestamp)
{
    OledOutput& oled = *this; 
    int c = 0; 
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    oled.print(abs(x)); 
    oled.setCursor(50, 0);
    oled.write(x < 0 ? 'W' : 'E'); 

    oled.setCursor(0, 16);
    oled.print(abs(y)); 
    oled.setCursor(50, 16);
    oled.write(y < 0 ? 'S' : 'N'); 

    oled.setCursor(0, 32);
    if (isotimestamp) {
        oled.setFontType(0);
        oled.print(isotimestamp); 
    } else {
        oled.print(a); 
        oled.setCursor(50, 32);
        oled.write('m'); 
    }
    oled.display();
}

void OledOutput::writebaro(int32_t Pr, int16_t temp, int32_t tbaro) 
{
    OledOutput& oled = *this; 
    int c = 0; 
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    oled.print("Mbar:"); 
    oled.setCursor(0, 16);
    oled.print(Pr*0.01); 
    oled.setCursor(0, 32);
    if (tbaro != 0) {
        oled.write('='); 
        oled.print(tbaro); 
    } else {
        oled.print(temp); 
        oled.write('C'); 
    }
    oled.display();
}

void OledOutput::writebaroF(int32_t PrF, int32_t PrFsmoothed, uint8_t currentstate) 
{
    OledOutput& oled = *this; 
    int c = 0; 
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    oled.print("Mbar:"); 
    if (currentstate != 40)  // the working and reading case
        oled.print(currentstate); 
    oled.setCursor(0, 16);
    oled.print(PrF*0.01); 

    oled.setCursor(30, 32);
    oled.print(max(-999,min(999,PrFsmoothed))); 
    oled.display();
}

int nwritetemps = 0; 
void OledOutput::writetemps(float t0, float t1, float t2)
{
    OledOutput& oled = *this; 
    int c = 0; 
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    oled.print(t0); 
    oled.write('c'); 
    if (t1 != 0) {
        oled.setCursor(2, 16);
        oled.print(t1); 
        oled.write('c'); 
    } else {
        nwritetemps++; 
        oled.setCursor(2, 16);
        oled.print(nwritetemps); 
    }
    if (t2 != 0) {
        oled.setCursor(4, 32);
        oled.print(t2); 
        oled.write('%'); 
    }
    oled.display();
}


float mpssmoothed = 0.0;
void OledOutput::writewind(int32_t revtime)
{
    OledOutput& oled = *this; 
    float mps = (revtime != 0 ? 80000./revtime : -1.0); 
    mpssmoothed = mpssmoothed*0.98 + mps*0.02; 
    int c = 0; 
    oled.clear(PAGE);
    oled.setFontType(0);
    oled.setCursor(0, 0);
    oled.print("wind:~"); 
    oled.setFontType(2);
    oled.setCursor(0, 16);
    oled.print(mps); 
    //oled.setCursor(4, 16);
    //oled.print(80000./max(1, revtime)); 

    oled.setFontType(1);
    if (fabs(mpssmoothed - mps) < 0.2) {
        oled.setCursor(0, 32);
        oled.write('*'); 
    }
    oled.setCursor(30, 32);
    oled.print("m/s"); 
    oled.display();
}

void OledOutput::writelightlevel(int16_t lightlevel) 
{
    OledOutput& oled = *this; 
    int c = 0; 
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    oled.print("light:"); 
    oled.setCursor(4, 16);
    oled.print((1024-lightlevel)*100/1024); 
    oled.print("%"); 
    oled.display();
}

void OledOutput::writeir(int16_t irt16, int16_t irtamb16) 
{
    OledOutput& oled = *this; 
    int c = 0; 
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    oled.print("I-R:"); 
    oled.setCursor(4, 16);
    oled.print(irt16*0.02 - 273.15); 
    oled.print("C"); 
    oled.setCursor(0, 32);
    oled.print("("); 
    oled.print(irtamb16*0.02 - 273.15); 
    oled.display();
}

void OledOutput::writehumidity(char cc, int16_t rawHumidity, int16_t rawTemperature) 
{
    OledOutput& oled = *this; 
    int c = 0; 
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    oled.print("Humid"); 
    oled.setCursor(4, 16);
    oled.print(-6 + (125 * ((rawHumidity & 0xFFFC) / (float)65536))); 
    oled.print("%"); 
    oled.setCursor(0, 32);
    oled.print(-46.85 + (175.72 * ((rawTemperature & 0xFFFC) / (float)65536))); 
    oled.print("C"); 
    oled.display();
}

void OledOutput::writevinvolts(char cc, uint32_t vinvolts) 
{
    OledOutput& oled = *this; 
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    oled.print("Power"); 
    oled.setCursor(4, 16);
    oled.print(vinvolts / 1000); 
    oled.print('.'); 
    oled.print((vinvolts/100) % 10); 
    oled.print((vinvolts/10) % 10); 
    oled.print((vinvolts) % 10); 
    oled.print('V'); 
    oled.display();
}


void OledOutput::writeorient(char cc, int16_t qw, int16_t qx, int16_t qy, int16_t qz, uint8_t currentstate) 
{
    OledOutput& oled = *this; 
    int c = 0; 
    oled.clear(PAGE);
    oled.setFontType(1);
    if (cc == 'o') {
        oled.setCursor(0, 0);
        oled.print("q:"); 
        int vsq = qw*(int)qw + qx*(int)qx + qy*(int)qy + qz*(int)qz; 
        float vfac = 100/sqrt(vsq); 
        oled.print((int)(vfac*qx)); 
        oled.setCursor(4, 16);
        oled.print((int)(vfac*qy)); 
        oled.setCursor(4, 32);
        oled.print((int)(vfac*qz)); 
    } else if (cc == 'c') {
        oled.setCursor(0, 0);
        oled.print("sys: "); 
        oled.print(qw); 
        oled.setCursor(0, 16);
        oled.print("g a m"); 
        oled.setCursor(0, 32);
        oled.print(qx);
        oled.print(' '); 
        oled.print(qy);
        oled.print(' '); 
        oled.print(qz); 
    } else if (cc == 'e') {
        oled.setCursor(0, 0);
        oled.print("eu:"); 
        oled.print(qx); 
        oled.setCursor(4, 16);
        oled.print(qy); 
        oled.setCursor(8, 32);
        oled.print(qz); 
    } else if (cc == 'b') {
        oled.setCursor(0, 0);
        oled.print("badort"); 
        oled.setCursor(0, 16);
        oled.print(qx); 
        oled.print("min"); 
        oled.setCursor(0, 32);
        oled.print(qw); 
        oled.print("sec"); 
    }
    if (currentstate != 30) {
        oled.setCursor(42, 16);
        oled.print(currentstate);
    }
    
    oled.display();
}

void OledOutput::writefilestatus(char* logfilename, uint32_t mstamp, int32_t Nloglines, int32_t Nloglinesfailed)
{
    OledOutput& oled = *this; 
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    oled.print(logfilename+4); 
    oled.setCursor(0, 16); 
    if (Nloglinesfailed == 0) {
        oled.print(Nloglines); 
    } else {
        oled.print("Bad"); 
        oled.print(Nloglinesfailed); 
    }
    oled.setCursor(0, 32); 
    oled.print(mstamp/60000); 
    oled.print('m'); 
    if (mstamp < 60000*100) {
        oled.print((mstamp/1000) % 60); 
        oled.print('s'); 
    }
    oled.display();
}
