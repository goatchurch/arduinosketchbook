#include <SPI.h>
#include <Wire.h>  // needed for compilation of oled library even if we don't use it
#include <SFE_MicroOLED.h>
#include "oledoutput.h"

OledOutput oled(10, 3, 9); 

#define P(X) Serial.print(X)

void setup() 
{
    Serial.begin(9600);
    delay(500);
    oled.SetupOLED(); 
    oled.writetemps(-999, -999, -999);  
}

int x = 0; 
int y = 0; 
bool bnottouched = true; 
char sbuff[52]; 
void loop()
{
    if (bnottouched && (Serial.available() == 0)) {
        x++; 
        y--; 
        long m = millis(); 
        oled.writewind(6000);  
        long md = millis() - m; 
        P("millis oled write: "); 
        P(md); 
        P("\n"); 
        delay(200); 
        return; 
    }
    
    bnottouched = false; 
    if (Serial.available()) {
        delay(100); 
        int nc = 0; 
        while (Serial.available() && (nc < 50)) { 
            sbuff[nc++] = Serial.read(); 
        }
        sbuff[nc] = '\0'; 
        
        oled.clear(PAGE);            // Clear the display
        oled.setCursor(0, 0);        // Set cursor to top-left
        oled.setFontType(0);         // Smallest font
        oled.print(sbuff); 
        oled.display();
    }
}
