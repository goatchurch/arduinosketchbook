#include <SPI.h>
#include <Wire.h>  // needed for compilation of oled library even if we don't use it
#include <SFE_MicroOLED.h>
#include <SD.h>
#include "oledoutput.h"
#include "SDlogging.h"

OledOutput oled(10, 8, 9); 

#define P(X) Serial.print(X)

int enpin = 3; 
int recpin = 4;

void setup() 
{
    Serial.begin(9600);
    delay(500);
    oled.SetupOLED(); 
    oled.write_xy(-999, -999, -999);  

    pinMode(recpin, INPUT); 
    pinMode(enpin, OUTPUT); 
    digitalWrite(enpin, HIGH); 
}

int n = 0; 
void loop()
{

  oled.clear(PAGE);            // Clear the display
  int px = 0; 
  int py = 0; 
  for (int i = 0; i < 2000; i++) {
      delayMicroseconds(14); 
      int r = digitalRead(recpin); 
      if (r)
        oled.pixel(px, py); 
      px++; 
      if (px == LCDWIDTH) {
          px = 0; 
          py++; 
      }
  }
//        oled.clear(PAGE);            // Clear the display
//        oled.setCursor(0, 0);        // Set cursor to top-left
//        oled.setFontType(0);         // Smallest font
//        oled.print(sbuff); 
        oled.display();
  delay(100); 
  if ((n % 50) == 0) {
    if ((n/50 % 8) != 7) {
      digitalWrite(enpin, HIGH); 
      Serial.println("enpin high"); 
    } else {
      digitalWrite(enpin, LOW); 
      Serial.println("enpin low"); 
    }
  }
  n++; 
}
