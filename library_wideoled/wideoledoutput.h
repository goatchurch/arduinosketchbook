// Copyright 2014 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef WIDEOLEDOUTPUT_h
#define WIDEOLEDOUTPUT_h

class Flystatus; 

#define yTimestmpsstep 50

// this one is 128x64 pixels
class WideOledOutput
{
public: 
    Adafruit_SSD1306& display; 
    bool bdimmed; 
    WideOledOutput(Adafruit_SSD1306& ldisplay); // so it can be declared outside on the global scope (their code prob assumes this)
    void SetupOLED(const char* s=0, int devicenumber=-1); // leaves it scrolling with a sine wave
    void Ddim(bool bdim); 
    
    void writefilestatus(char* logfilename, uint32_t mstamp, int32_t Nloglines, int32_t Nloglinesfailed); 
    void writeflystatus(Flystatus& flystatus, long mstamp);
    void plotflyorientation(Flystatus& flystatus); 

    // scrolling graph technology
    const char* gtitle; 
    int16_t yM[128], yR[128]; 
    int16_t yMin; 
    int16_t yMax; 
    uint16_t yGridLines; 
    float yValFac; 
    int8_t yValPrec; 
    int32_t yValOffset; 
    int16_t nscrollcount; 
    int32_t yValmsstep; 
    int32_t yValmsstamp; 
    int16_t yLo128, yHi128; 
    int16_t yTimestmps[10]; 
    int16_t jGfirst; 
    int joinscroll(int j0, int j1, int jfirst); 
    void setupvaluescrollgraph(const char* lgtitle, float lfyGridLines, float lyValFac, uint8_t lyValPrec, int32_t lyValOffset, int32_t lyValmsstep); 
    bool addscrollgraph(float yval, int32_t mstamp); 
    void plotscrollgraph(bool bFillBelow, int replayprogress); 
};


#endif
