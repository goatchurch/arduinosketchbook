// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "math.h"
#include "quat.h"
#include "blueflygps.h"
#include "flystatus.h"
#include "wideoledoutput.h"

#define P(X) Serial.print(X)

// Note: we have the version of the Adafruit_SSD1306 library from the website, rather than
// distributed with teensy, as the teensy one seems to dither the horizontal lines

WideOledOutput::WideOledOutput(Adafruit_SSD1306& ldisplay) :
    display(ldisplay)
{
}

void WideOledOutput::SetupOLED(const char* s, int devicenumber)
{
    display.begin(SSD1306_SWITCHCAPVCC);
    
    Adafruit_SSD1306& y = display; 
    y.clearDisplay();
    int h0 = 30; 
    for (int i = 1; i <= 128; i++) {
        int h1 = sin(radians(i/64.0*360))*31+32; 
        y.drawLine(i-1, h0, i, h1, WHITE); 
        h0 = h1; 
    }
    y.display(); 
    y.startscrollleft(0x00, 0x0F);  // this is a faster scroll setting
    bdimmed = false; 
    y.dim(bdimmed); 
    
#if 0
    oled.clear(PAGE);
    oled.setFontType(1);
    oled.setCursor(0, 0);
    if (s != 0) {
        while (*s != 0) 
            oled.write(*(s++));
    }
    if (devicenumber != -1) {
        oled.write('#');
        oled.print(devicenumber);
    }
    oled.display();
#endif
}

void WideOledOutput::Ddim(bool bdim)
{
    if (bdimmed != bdim) {
        bdimmed = bdim; 
        display.dim(bdimmed); 
    }
} 


void ppdegc(Adafruit_SSD1306& y, int cx, int cy, float c) 
{
    y.setCursor(cx, cy);
    y.print(c); 
    //y.print((char)248); 
}


void plothorizon(Adafruit_SSD1306& y, float sinattack, float sinroll, int widr)
{
    float midy = (32 + sinattack*32); 
    int yleft = (int)(midy - sinroll*widr/2 + 0.5); 
    int yright = (int)(midy + sinroll*widr/2 + 0.5); 
    
    int yr = max(yleft, yright); 
    if (widr != 64) {
        y.fillRect(64-widr, 0, widr*2, yr, BLACK); 
        y.drawLine(63-widr, 0, 63-widr, 63, BLACK); 
        y.drawLine(64+widr, 0, 64+widr, 63, BLACK); 
    }
    y.fillRect(64-widr, yr, widr*2, 64-yr, WHITE); 
    y.fillTriangle(64-widr, yr, 63+widr, yr, (yleft < yright ? 64-widr : 63+widr), min(yleft, yright), WHITE); 
}

const char* Ecalibstat = "SsGgAaMm"; 
void plotorientation(Adafruit_SSD1306& y, float sinattack, float sinroll, float degnorth, uint16_t calibstat, float slowsinattack, float slowsinroll) 
{
    y.clearDisplay();

    plothorizon(y, slowsinattack, slowsinroll, 64); 
    plothorizon(y, sinattack, sinroll, 38); 
    
    y.setTextSize(2);
    y.setTextColor(INVERSE);
    int dd = (int)(degnorth+1) % 360; 
    y.setCursor(64-15, 64-16);
    if (dd < 100)
        y.print(dd < 10 ? "00" : "0"); 
    y.print(dd);
    
    y.setTextSize(1);
    y.setTextColor(WHITE);
    y.setCursor(0, 0); 
    
    uint16_t bb = 0x0080; 
    for (int e = 0; e < 8; e++) {
        if ((calibstat & bb) == 0) 
            y.print(Ecalibstat[e]); 
        bb >>= 1; 
    }

    y.display(); 
}


void planepanel(Adafruit_SSD1306& y, Flystatus& s) 
{
    y.clearDisplay();
    y.setTextSize(1);
    y.setTextColor(WHITE);
    ppdegc(y, 129-5*7, 0*8, s.degcS);  
    ppdegc(y, 129-5*7, 1*8, s.degcG);  
    ppdegc(y, 129-5*7, 2*8, s.degcIA);  
    ppdegc(y, 129-5*7, 3*8, s.degcX);  
    
    y.setCursor(129-12*7, 0*8); 
    y.print(s.humidS);
    y.print(" %");
    y.setCursor(129-12*7, 1*8); 
    y.print(s.humidG);
    y.print(" %");
    y.setCursor(129-12*7, 2*8); 
    y.print(s.degcI);
    y.print(" C");
    
    y.setCursor(129-7*(s.baroF>=100000?6:5), 4*8+1);
    y.print((int)(s.baroF));
    y.setCursor(129-7*(s.baroB>=100000?6:5), 5*8+0);
    y.print((int)(s.baroB));
    
    y.setCursor(0, 0);
    y.print("a"); 
    y.print((int)(degrees(asin(s.quat.GetSinAttack()))));
    y.setCursor(0, 8);
    y.print("r"); 
    y.print((int)(degrees(asin(s.quat.GetSinRoll()))));
    y.setCursor(0, 16);
    y.print("N"); 
    y.print((int)(s.quat.GetNorthOrient()));

    y.setCursor(4*7-3, 0); 
    uint16_t bb = 0x00C0; 
    for (int e = 0; e < 4; e++) {
        if ((s.calibstat & bb) != bb) 
            y.print(Ecalibstat[e*2 + (((s.calibstat & 0x00AA) != 0) ? 0 : 1)]); 
        bb >>= 2; 
    }
    
    
    y.setCursor(0, 3*8);
    y.print(s.wspeed);
    y.print("m/s");
    
    y.setCursor(0, 4*8);
    y.print((int)(s.wspeedX));
    y.print("DMb");
    
    y.setCursor(64, 4*8);
    y.print("*");
    int li = (int)(s.lightI*100); 
    if (s.replayfilesize != 0) 
        li = (int)(s.replayfilebytes*100/s.replayfilesize);  // put in file progress instead of light here on replay case
    if (li < 10)
        y.print("0");
    y.print(li);

    y.setCursor(64, 5*8);
    y.print("^");
    int ld = (int)(s.wspeedH+0.5); 
    if (ld < 10)
        y.print("0"); 
    y.print(ld);

    if ((long)millis() <= s.mstampNoLock + 1000) {
        y.setCursor(0, 6*8);
        y.print("no GPS lock"); 
    } else {
        y.setCursor(0, 6*8);
        y.print("("); 
        y.print((int)s.relE); 
        y.print(","); 
        y.print((int)s.relN); 
        y.print(","); 
        y.print((int)s.Galt); 
        y.print(")v"); 
        y.print((int)s.Gvmps); 
        y.print("m"); 
        y.print((int)s.Gvdeg); 
        
        y.print(" e"); 
        y.print((int)max(abs(s.relE - s.relE2), abs(s.relN - s.relN2))); 
    }

    y.setCursor(0, 7*8);
    if (s.logfilenameTail != 0)
        y.print(s.logfilenameTail); 
    else
        y.print("badSD");
    y.print(":");
    y.print(s.Nloglines-s.Nloglinesfromserialbuff); 
    y.print("+");
    y.print(s.Nloglinesfromserialbuff); 
    y.print("-"); 
    y.print(s.Nloglinesfailed); 

    y.display(); 
}


void WideOledOutput::setupvaluescrollgraph(const char* lgtitle, float lfyGridLines, float lyValFac, uint8_t lyValPrec, int32_t lyValOffset, int32_t lyValmsstep) 
{
    gtitle = lgtitle; 
    yGridLines = (uint16_t)(lfyGridLines/lyValFac + 0.5); 
    yValFac = lyValFac; 
    yValPrec = lyValPrec; 
    yValOffset = lyValOffset; 
    yValmsstep = lyValmsstep; 
    nscrollcount = 0; 
    for (int i = 0; i < 128; i++)
        yR[i] = -1; 
    jGfirst = 127; 
}

int WideOledOutput::joinscroll(int j0, int j1, int jfirst)
{
    for (int j = j0; j < j1; j++) {
        yM[j-1] = yM[j]; 
        yR[j-1] = yR[j]; 
        if ((jfirst == -1) && (yR[j-1] != -1))
            jfirst = j-1; 
    }
    if (j1 != 128) {
        if (yR[j1] == -1) {
            yM[j1] = yM[j1+1]; 
            yR[j1] = yR[j1+1]; 
        }
        if (yR[j1+1] != -1) {
            int16_t yA = min(yM[j1]-yR[j1], yM[j1+1]-yR[j1+1]);
            int16_t yB = max(yM[j1]+yR[j1], yM[j1+1]+yR[j1+1]);
            yM[j1-1] = (yA + yB)>>1; 
            yR[j1-1] = (yB - yA)>>1; 
            if (jfirst == -1)
                jfirst = j1-1; 
        }
    }
    return jfirst; 
}


bool WideOledOutput::addscrollgraph(float yval, int32_t mstamp)
{
    int32_t m = -(yval/yValFac - yValOffset);   // negate everything to turn the plot the right way up
    if ((m < yLo128) || (nscrollcount == 0))  yLo128 = m; 
    if ((m > yHi128) || (nscrollcount == 0))  yHi128 = m; 
    if (yValmsstamp/yValmsstep == mstamp/yValmsstep) {
        return false; 
    }
    
    int jfirst = -1; 
    if ((nscrollcount % 8) == 0)
        jfirst = joinscroll(1, 9, jfirst); 
    if ((nscrollcount % 4) == 0)
        jfirst = joinscroll(10, 43, jfirst); 
    if ((nscrollcount % 2) == 0)
        jfirst = joinscroll(44, 83, jfirst); 
    jfirst = joinscroll(84, 128, jfirst); 
    if ((jfirst != -1) && (jfirst < jGfirst))
        jGfirst = jfirst; 

    nscrollcount++; 

    if (((nscrollcount % yTimestmpsstep) == 0) && (nscrollcount < 10*yTimestmpsstep)) {
        yTimestmps[nscrollcount/yTimestmpsstep-1] = jGfirst; 
        //P("NNN "); P(yTimestmps[nscrollcount/yTimestmpsstep-1]); P(" "); P(nscrollcount); P("  "); P(nscrollcount); P(" "); P(yTimestmpsstep); P("\n"); 
    }

    yM[127] = (yLo128 + yHi128)>>1; 
    yR[127] = (yHi128 - yLo128)>>1; 
    yValmsstamp = mstamp; 
    yLo128 = m; 
    yHi128 = m; 

    return true; 
}


void WideOledOutput::plotscrollgraph(bool bFillBelow, int replayprogress)
{
    Adafruit_SSD1306& y = display; 

    // calculate the shift and scaling
    int16_t yMid = (yMax + yMin)>>1; 
    int16_t yShiftCount = 0; 
    uint16_t yShifter = (yMax - yMin)>>1; 
    while (yShifter > 32) {
        yShiftCount++; 
        yShifter >>= 1; 
    }

    // draw the graph (while working out the new yRange)
    y.clearDisplay(); 
    int x0 = -1; 
    for (int i = 0; i < 128; i++) {
        if (yR[i] == -1)
            continue; 
        int16_t yLo = yM[i]-yR[i]; 
        int16_t yHi = yM[i]+yR[i]; 
        if ((x0 == -1) || (yLo < yMin))
            yMin = yLo; 
        if ((x0 == -1) || (yHi > yMax))
            yMax = yHi; 
        if (x0 == -1) 
            x0 = i; 
        int yB = ((yLo - yMid)>>yShiftCount) + 32; 
        int yBL = max(1, (yHi - yLo)>>yShiftCount); 
        if (bFillBelow)
            y.drawFastVLine(i, yB+yBL/2, 64-yB-yBL/2, WHITE); 
        else
            y.drawFastVLine(i, yB, yBL, WHITE); 
    }

    char charbuf[16]; 
    if (yGridLines > 0) {
        uint16_t lyGridLines = yGridLines; 
        uint16_t lyValPrec = max(0, yValPrec); // negative precision loses sigfig
        while ((64<<yShiftCount)/lyGridLines >= 6) {
            lyGridLines *= 5; 
            if ((64<<yShiftCount)/lyGridLines < 6) 
                break; 
            lyGridLines *= 2; 
            if ((lyValPrec != 0) && (yValOffset == 0)) 
                lyValPrec--; 
        }
        int16_t k0 = (((int32_t)(-32)<<yShiftCount) + yMid)/lyGridLines; 
        int16_t k1 = min((((int32_t)(32)<<yShiftCount) + yMid)/lyGridLines, k0+10); 
        y.setTextSize(1);
        y.setTextColor(INVERSE);
        P(" grid "); P(yGridLines); P(" lyGridLines "); P(lyGridLines); P("  yValOffset "); P(yValOffset); P("  yValFac "); P(yValFac); P(" lyValPrec "); P(lyValPrec); P(" yminmax "); P(yMin); P("<"); P(yMax);  P("\n");         
        for (int16_t k = k0 - 2; k <= k1 + 2; k++) {
            int16_t yG = (k*lyGridLines); 
            int16_t yP = ((yG - yMid)>>yShiftCount) + 32; 
            if (yP < -8) 
                continue; 
            if (yP > 64)
                break; 
            y.drawFastHLine(x0, yP, 128-x0, WHITE); 
            int8_t cwidth = 15; 
            float gval = (-yG+yValOffset)*yValFac; // negated to recover the right number
            dtostrf(gval, cwidth, lyValPrec, charbuf); 
            int16_t xP = (128 - 1); // write the numbers characters back to front
            if (yValPrec < 0)
                cwidth += yValPrec; 
            while ((cwidth > 0) && (charbuf[cwidth] != ' ')) {
                if (charbuf[cwidth] == '.')
                    xP += 1; 
                y.setCursor(xP, yP+2); 
                y.write(charbuf[cwidth]);
                xP -= (charbuf[cwidth] == '.' ? 4 : 6); 
                cwidth--;  
            }
        }
    }
    
    for (int i = min(9, nscrollcount/yTimestmpsstep)-1; i >= 0; i--) {
        y.drawFastVLine(yTimestmps[i], 61, 3, INVERSE); 
    }
    
    if (gtitle != 0) {
        y.setTextSize(1);
        y.setTextColor(INVERSE);
        y.setCursor(0, 0); 
        y.print(gtitle); 
    }
    
    if (replayprogress != 0) {
        y.setTextSize(1);
        y.setTextColor(INVERSE);
        y.setCursor(0, 32); 
        y.print(replayprogress); 
        y.print('%'); 
    }
    
    y.display(); 
}


// 64 down, 128 across
void WideOledOutput::writeflystatus(Flystatus& flystatus, long mstamp)
{
    planepanel(display, flystatus);  
}

void WideOledOutput::plotflyorientation(Flystatus& flystatus)
{
    plotorientation(display, flystatus.quat.GetSinAttack(), flystatus.quat.GetSinRoll(), flystatus.quat.GetNorthOrient(), flystatus.calibstat, flystatus.slowsinattack, flystatus.slowsinroll); 
}


