#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "wideoledoutput.h"
#include "wideoledoutput.h"

Adafruit_SSD1306 y(10, 9, 8, 7, 6); // OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS
WideOledOutput* wideoled = 0; 

void plotorientation(Adafruit_SSD1306& y, float sinattack, float sinroll, float degnorth, uint16_t calibstat, float slowsinattack, float slowsinroll); 

void setup() 
{
    wideoled = new WideOledOutput(y);
    wideoled->SetupOLED(); 
    y.stopscroll();
}

void loop() 
{
    for (int i = 0; i < 360; i++) {
        double rad = i*(3.14159265358979/180); 
        double radprev = (i-35)*(3.14159265358979/180); 
        plotorientation(y, sin(rad)*0.5, cos(rad)*0.5, i, 0, sin(radprev)*0.5, cos(radprev)*0.5); 
        delay(20); 
    }
}


