// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <Wire.h>
#include "HTU21Dhumidity.h"
#include <math.h>

uint8_t check_crc(uint16_t message_from_sensor, uint8_t check_value_from_sensor); 

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)

HumidityData::HumidityData()
{
    rawHumidity = 0;    // bit1 is status bit set to 1
    rawTemperature = 0; // bit1 is status bit set to 0 to denote temperature reading
    commandstate = 0;   // 0xF5 humidity measurement, 0xF3 temperature measurement -- both No Hold Master cases
    milliscommand = 0; 
}

void HumidityData::beginreading(uint8_t lcommandstate)
{
    commandstate = lcommandstate; 
    Wire.beginTransmission(0x40);
    Wire.write(commandstate);     // Measure humidity or temperature with no bus holding
    Wire.endTransmission();
    milliscommand = millis(); 
}

// could pass in a mtimestamp to check for timeouts
bool HumidityData::valueready()
{
    Wire.requestFrom(0x40, 3);
    if (!Wire.available())
        return false; 
        
    long endmilliscommand = millis(); 
    commandduration = endmilliscommand - milliscommand; 

    byte msb, lsb, checksum;
    msb = Wire.read();
    lsb = Wire.read();
    checksum = Wire.read();

    uint16_t rawvalue = (((uint16_t)msb) << 8) | lsb;
    
    if (check_crc(rawvalue, checksum) != 0) 
        P("Bad HumidityData crc\n"); 
        
    //uint16_t lrawvalue = rawvalue & 0xFFFC; //Zero out the status bits but keep them in place
    if (commandstate == 0xF5) 
        rawHumidity = rawvalue; 
    else if (commandstate == 0xF3)
        rawTemperature = rawvalue; 
    commandstate = 0; 
    
    milliscommand = endmilliscommand; 
    return true; 
}

float HumidityData::GetHumidity() 
{
    float tempRH = (rawHumidity & 0xFFFC) / (float)65536; //2^16 = 65536
    return -6 + (125 * tempRH); //From page 14
}
    
float HumidityData::GetTemperature()    
{
    float tempTemperature = (rawTemperature & 0xFFFC) / (float)65536; //2^16 = 65536
    return -46.85 + (175.72 * tempTemperature); //From page 14
}

float HumidityData::DewpointTemp()
{
    float tempC = GetTemperature(); 
    float humid = GetHumidity(); 
    float A = 8.1332;  //From page 15
    float B = 1762.39; 
    float C = 235.66; 
    float PPamb = pow(10, A - B/(tempC + C)); 
    return -C - B/(log10(humid*PPamb/100) - A); 
}



//Set sensor resolution
/*******************************************************************************************/
//Sets the sensor resolution to one of four levels
//Page 12:
// 0/0 = 12bit RH, 14bit Temp
// 0/1 = 8bit RH, 12bit Temp
// 1/0 = 10bit RH, 13bit Temp
// 1/1 = 11bit RH, 11bit Temp
//Power on default is 0/0

void HumidityData::SetUserRegister(bool bhighresolution, bool benableheater)
{
    Wire.beginTransmission(0x40);
    Wire.write(0xE7); //Read the user register
    Wire.endTransmission();
    Wire.requestFrom(0x40, 1);
    uint8_t userRegister = Wire.read();
    P("User register ");
    Serial.print(userRegister, HEX); 
    
    if (bhighresolution)
        userRegister &= 0b01111110; // 0/0 = 12bit RH, 14bit Temp (default)
    else
        userRegister |= 0b10000001; // 1/1 = 11bit RH, 11bit Temp
        
    if (benableheater)
        userRegister |= 0x04; 
    else
        userRegister &= 0xFB; 

    P("  new value: ");
    Serial.print(userRegister, HEX); 
    P("\n"); 

    Wire.beginTransmission(0x40);
    Wire.write(0xE6); //Write to the user register
    Wire.write(userRegister); //Write the new resolution bits
    Wire.endTransmission();
}



//Give this function the 2 byte message (measurement) and the check_value byte from the HTU21D
//If it returns 0, then the transmission was good
//If it returns something other than 0, then the communication was corrupted
//From: http://www.nongnu.org/avr-libc/user-manual/group__util__crc.html
//POLYNOMIAL = 0x0131 = x^8 + x^5 + x^4 + 1 : http://en.wikipedia.org/wiki/Computation_of_cyclic_redundancy_checks
uint8_t check_crc(uint16_t message_from_sensor, uint8_t check_value_from_sensor)
{
  //Test cases from datasheet:
  //message = 0xDC, checkvalue is 0x79
  //message = 0x683A, checkvalue is 0x7C
  //message = 0x4E85, checkvalue is 0x6B

  uint32_t remainder = (uint32_t)message_from_sensor << 8; //Pad with 8 bits because we have to add in the check value
  remainder |= check_value_from_sensor; //Add on the check value

  uint32_t divsor = (uint32_t)0x988000; //This is the 0x0131 polynomial shifted to farthest left of three bytes

  for (int i = 0 ; i < 16 ; i++) //Operate on only 16 positions of max 24. The remaining 8 are our remainder and should be zero when we're done.
  {
    //Serial.print("remainder: ");
    //Serial.println(remainder, BIN);
    //Serial.print("divsor:    ");
    //Serial.println(divsor, BIN);
    //Serial.println();

    if( remainder & (uint32_t)1<<(23 - i) ) //Check if there is a one in the left position
      remainder ^= divsor;

    divsor >>= 1; //Rotate the divsor max 16 times so that we have 8 bits left of a remainder
  }

  return remainder;
}



