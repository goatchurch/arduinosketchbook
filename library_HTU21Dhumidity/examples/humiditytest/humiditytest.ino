#include <Wire.h>
#include "HTU21Dhumidity.h"

#define P(X) Serial.print(X)

HumidityData humiditydata;

void setup()
{
  Serial.begin(9600);
  delay(1000); 
  Serial.println("Humidity Example1!");
  delay(1000); 
  Serial.println("Humidity Example2!");
  Wire.begin(); 
  Serial.println("Humidity Example3!");
}

int if3, if5; 
int maxif35 = 10000; 
void loop()
{
    humiditydata.beginreading(0xF3); 
    for (if3 = 0; if3 < maxif35; if3++) {
         if (humiditydata.valueready())
             break; 
    }
    
    if (if3 != maxif35) {
        humiditydata.beginreading(0xF5); 
        for (if5 = 0; if5 < maxif35; if5++) {
             if (humiditydata.valueready())
                 break; 
        }
    } else {
        if5 = 0; 
    }
    
    if ((if3 != maxif35) && (if5 != maxif35)) {
        P("Humidity: "); 
        P(-6 + (125 * ((humiditydata.rawHumidity & 0xFFFC) / (float)65536))); 
        P("%   temp: "); 
        P(-46.85 + (175.72 * ((humiditydata.rawTemperature & 0xFFFC) / (float)65536))); 
        P("C\n"); 
    } else {
        P("bad if3="); 
        P(if3); 
        P(" if5="); 
        P(if5); 
        P("\n"); 
    }
    delay(50);
}
