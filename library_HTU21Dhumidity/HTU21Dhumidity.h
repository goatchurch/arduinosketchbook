// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef HTU21DHUMIDITY_h
#define HTU21DHUMIDITY_h

// http://diyhacking.com/arduino-mpu-6050-imu-sensor-tutorial/
class HumidityData
{
public:
    uint16_t rawHumidity; 
    uint16_t rawTemperature; 
    uint8_t commandstate; // 0xF5 humidity measurement, 0xF3 temperature measurement (soft reset would be 0xFE)
    long milliscommand; 
    long commandduration; 

    HumidityData(); 
    
    void beginreading(uint8_t lcommandstate);
    bool valueready(); 
    float GetHumidity(); 
    float GetTemperature(); 
    float DewpointTemp();  
    
    void SetUserRegister(bool bhighresolution, bool benableheater); 
};

#endif

