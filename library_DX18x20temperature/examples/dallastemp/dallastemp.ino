// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.
#include "Arduino.h"
#include <OneWire.h>
#include "DX18x20temperature.h"

// flat face you, left is ground, right is live, middle is signal with a 4.7k pullup resistor onto the live
DallasTemperature dst(16); 

#define P(X) Serial.print(X)

void setup(void)
{
    pinMode(20, OUTPUT); 
    digitalWrite(20, HIGH); 
    delay(2000); 
    digitalWrite(20, LOW); 
    Serial.begin(9600); 
    Serial.println("hi there"); 
    dst.FindAllSensors(); 
    for (int i = 0; i < dst.ns; i++)
        dst.beginreading(i, 760 + (i * 760 / dst.ns)); 
}



void loop()
{
    //Serial.println(dst.FindAllSensors());  delay(2000);  return; 
    
    // use two digits here to swap the entries around, and then when in right order we can write the numbers out into the eeproms 
    // so that the order is consistent when we restart with this string again 
    if (Serial.available()) {
        P("available: "); 
        P(Serial.available()); 
        char s0 = Serial.read(); 
        char s1 = (Serial.available() ? Serial.read() : '0'); 
        while (Serial.available())
            Serial.read(); // clear the buffer
        if (s0 == 'E') {
            P("Set the eeproms\n"); 
            dst.SetEeproms(); 
        } else if ((s0 >= '0') && (s0 < '0'+dst.ns) && (s1 >= '0') && (s1 < '0'+dst.ns) && (s0 != s1)) {
            P("swapping "); 
            uint8_t* c = dst.paddr[s0-'0']; 
            dst.paddr[s0-'0'] = dst.paddr[s1-'0']; 
            dst.paddr[s1-'0'] = c; 
        }
    }

    int i = dst.readingready(); 
    if (i == -1)
        return; 
        
    long m = millis(); 
    int16_t t16 = dst.fetchreading16(i); 
    P(millis() - m); 
    for (int j = 0; j <= i; j++)
        P("   .    "); 
    P(t16/16.0); 
    P("\n"); 
    dst.beginreading(i, 760); 
}
