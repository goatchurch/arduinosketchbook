// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"
#include <OneWire.h>
#include "DX18x20temperature.h"
#include <SFE_MicroOLED.h>  // Include the SFE_MicroOLED library


DallasTemperature dst(21); 

// this was intended to read the values simultaneously, but it seems that talking to one sensor 
// kills the reading on the other.  Could it a a parasitic power thing? 
void setup(void)
{
    delay(2000); 
    Serial.println("hi there"); 
    dst.FindAllSensors(); 
    
    if (dst.ns > 0)
        dst.beginreading(0); 
    
    //for (int i = 0; i < dst.ns; i++)
    //    dst.beginreading(i); 
}

void loop()
{
    while (dst.continuereading()) {
        Serial.print("Sensor "); 
        Serial.print(dst.i); 
        Serial.print(" "); 
        Serial.println(dst.t16values[dst.i]/16.0); 
        
        // cycle through the sensors due to difficulty of seeing them at once
        //dst.beginreading(dst.i); 
        dst.beginreading((dst.i + 1) % dst.ns); 
    }
}
