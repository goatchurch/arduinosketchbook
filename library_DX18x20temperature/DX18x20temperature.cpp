// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"
#include <OneWire.h>
#include "DX18x20temperature.h"

// annoying hack needed in the OneWire library to make it compile:
//    https://forum.pjrc.com/threads/252-OneWire-library-for-Teensy-3-%28DS18B20%29

#undef P
#define P(X) Serial.print(X)

DallasTemperature::DallasTemperature(int pinnumber) : 
    OneWire(pinnumber)
{
    for (int j = 0; j < MAX_DS_SENSORS; j++)
        paddr[j] = 0; 
    ns = 0; 
} 

int DallasTemperature::FindAllSensors(bool bbeginreadings)
{
    // search out the sensors on this string
    this->reset_search(); 
    for (ns = 0; ns < MAX_DS_SENSORS; ns++) {
        uint8_t addr[8]; 
        if (!this->search(addr))
            break; 
        if (paddr[ns] == 0) 
            paddr[ns] = (uint8_t*)malloc(10*sizeof(byte)); 
        P("SSensor ROM = ");
        for (int j = 0; j < 8; j++) {
            paddr[ns][j] = addr[j]; 
            Serial.print(addr[j], HEX);
            Serial.print(j != 0 ? "." : ""); 
        }
        if (OneWire::crc8(addr, 7) != addr[7]) 
            Serial.print(" ** CRC is not valid!");
        if (addr[0] != 0x28)
            Serial.print(" ** unexpected chip"); // should be for DS18B20
        Serial.print("\n"); 
        millisreading[ns] = 0; 
    }
    
    // read the eeprom numbers from each sensor
    for (int k = 0; k < ns; k++) {
        P("SSSensor ROM = ");
        for (int j = 0; j < 8; j++) {
            if (paddr[k][j] < 16)
                P("0"); 
            Serial.print(paddr[k][j], HEX);
        }
        delay(10); 
        this->reset();
        this->select(paddr[k]);
        this->write(0xBE);         // READ SCRATCHPAD 
        this->read(); 
        this->read(); 
        paddr[k][8] = this->read(); 
        paddr[k][9] = this->read(); 
        this->reset();
        P(" eeeprom "); 
        Serial.print((paddr[k][8] << 8) | paddr[k][9], HEX);
        P("\n"); 
    }
    
    // bubble sort the eeprom values
    for (int a = 0; a < ns*ns; a++) {
        int nswaps = 0; 
        for (int b = 1; b < ns; b++) {
            uint16_t ebm1 = (paddr[b-1][8] << 8) | paddr[b-1][9]; 
            uint16_t eb = (paddr[b][8] << 8) | paddr[b][9]; 
            P("  "); 
            Serial.print(ebm1, HEX); 
            P("."); 
            Serial.print(eb, HEX); 
            if (ebm1 > eb) {
                uint8_t* c = paddr[b-1]; 
                paddr[b-1] = paddr[b]; 
                paddr[b] = c; 
                nswaps++; 
            }
        }
        P("\n"); 
        if (nswaps == 0)
            break; 
    }
 
    if (bbeginreadings) {
        for (int i = 0; i < ns; i++)
            beginreading(i, 760 + (i * 760 / ns)); 
    }
    
    return ns; 
}

void DallasTemperature::SetEeproms()
{
    uint8_t nbits = 12; 
    for (i = ns - 1; i >= 0; i--) {
        this->reset();
        this->select(paddr[i]); 
        this->write(0x4E); // WRITE SCRATCHPAD
        this->write(i); 
        this->write(ns); 
        this->write((nbits - 9)<<5 | 0x1F); 
        delay(30); 
    
        this->reset();
        this->select(paddr[i]); 
        this->write(0x48); // COPY SCRATCHPAD to eeprom
        delay(30); 
    } 
}
 
void DallasTemperature::beginreading(int j, int msoffset)
{
    this->reset();
    //this->skip();   // could apply to all if done
    this->select(paddr[j]);
    this->write(0x44, 1); // start conversion, with parasite power on at the end
    millisreading[j] = millis() + msoffset; 
}

int DallasTemperature::readingready() 
{
    long m = millis(); 
    for (int k = ns - 1; k >= 0; k--) {
        if ((millisreading[k] != 0) && (m > millisreading[k])) {
            return k; 
        }
    }
    return -1; 
}

int16_t DallasTemperature::fetchreading16(int k) 
{
    this->reset();
    this->select(paddr[k]);
    this->write(0xBE);         // READ SCRATCHPAD 
    uint8_t d0 = this->read();
    uint8_t d1 = this->read();
    //this->reset();  
    millisreading[k] = 0; 
    return (d1 << 8) | d0; 
    
    // only read first two bytes and then bail out for speed (7ms instead of 11ms)
    //for (int j = 0; j < 9; j++)
    //    data[j] = this->read();
    //if (OneWire::crc8(data, 8) != data[8])  P(" ** CRC error"); 
    // uint8_t rcfg = ((data[4] >> 5) & 3); // represents number of bits
    // t16reading (data[1] << 8) | (data[0] & (0xFF << (3 - rcfg))); 
}
