// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef DX18X20temperature_h
#define DX18X20temperature_h

#define MAX_DS_SENSORS 5

class DallasTemperature : public OneWire
{
public:
    uint8_t* paddr[MAX_DS_SENSORS];  // [][10], first 8 bytes is the address, last two is the eeprom values
    int ns; // number of sensors
    int i;  // index of last sensor to have read

public: 
    long millisreading[MAX_DS_SENSORS]; 
    
    DallasTemperature(int pinnumber); 
    int FindAllSensors(bool bbeginreadings=false); 
    void SetEeproms(); 

    void beginreading(int j, int msoffset=760); 
    int readingready(); 
    int16_t fetchreading16(int k);   // 16 is the factor need to divide by to get temp in degrees C.
}; 
    

#endif
