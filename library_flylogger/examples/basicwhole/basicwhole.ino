// to do
// * make serial read function with big buffer
// * have verification of the number of lines which should be coming through
// * start experimenting with high data stream
// * wire in the blocks into a plastic board that is inset deep

#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <OneWire.h>
#include <SFE_MicroOLED.h>

#include "SDloggingBase.h"
#include "SDlogging.h"
#include "SSlogging.h"
#include "oledoutput.h"
#include "DX18x20temperature.h"
#include "simpledatetime.h" 
#include "blueflygps.h"
#include "windmeter.h"
#include "lightsensor.h"
#include "MLX90614irthermometer.h"
#include "HTU21Dhumidity.h"
#include "T5403barometer.h"
#include "MS5611baro.h"
#include "px4pitot.h"
#include "SHT31Dhumidity.h"
#include "si7021humidity.h"
#include "BNO055.h"
#include "T5403barometer.h"
#include "serialinputbuffer.h"

#include "HTU21Dhumidity.h"

#include "flylogger.h"

int devicenumber = 3; // 1 if the smaller second device

Flylogger wholeSOB(devicenumber); 

void setup(void)  
{
    // numbers here are pin allocations on the Teensy3.1
    // SPI devices using MOSI=11, MISO=12, SCK=13
    wholeSOB.oled =       new OledOutput(10, 3, 9); 

//    wholeSOB.sdlogger =   new SDlogger(2, "OOR"); 
    wholeSOB.sdlogger =   new SerialStreamlogger(0, 115200); // 0 for USB, else &Serial2
    
    wholeSOB.simpledatetime = new SimpleDatetime();   
    wholeSOB.dst =        new DallasTemperature(16); 
    wholeSOB.enllt =      new EastingNorthingLocalLinearTrans(); 
    wholeSOB.blueflygpsdata =    new BlueflyGpsData(&Serial1); 
    wholeSOB.lightsensor =         new LightSensor(14, 15, 22, 23);   // lpinreadlight, lpinpower, lpinLED, lpinButton      (used also for battery power measurment)
    //wholeSOB.lightsensorgestures = new LightSensorGestures(); 
    wholeSOB.sdlogger->pinLEDforflush = wholeSOB.lightsensor->pinLED; 
    wholeSOB.wr =              new WindReceiver(17); 

    // I2C devices using SCL0=19, SDA0=18
    //wholeSOB.irthermometer =   new IRthermometer(); 
    //wholeSOB.tbarometer =      new T5403Barometer(); 
//    wholeSOB.humiditydata =        new HumidityData(); 

    wholeSOB.orientdata =      new BNO055orientation(); 
    wholeSOB.orientdata->SetupSerial(&Serial3); 


    wholeSOB.consoleserial =  new SerialInputBuffer(0);  // debugging input output (stands in for fake usb_serial_class& case)
    wholeSOB.SetupWhole(); 
    Serial.println("SetupWhole done"); 
}


#define P(X) Serial.print(X); 

void loop()
{
    wholeSOB.LoopWhole(); 
    return; 
    if (true && wholeSOB.FetchOrientation(10, 5)) {
        Quat q(wholeSOB.orientdata->qw(), wholeSOB.orientdata->qx(), wholeSOB.orientdata->qy(), wholeSOB.orientdata->qz()); 
        /*P(q.q0); 
        P(" "); 
        P(q.q1); 
        P(" "); 
        P(q.q2); 
        P(" "); 
        P(q.q3); 
        P("   "); 
        */
        Mat3x3 m(q); 
        P(m.a00); 
        P(" "); 
        P(m.a01); 
        P(" "); 
        P(m.a02); 
        P("\n"); 
    }
}

