// to do:

// * receive serial commands that ask for calibration constants (and maybe set them)
// * receive serial command for turning up the volume on the bluefly
//  * consider MS5611 baro in there too instead -- stop it crashing if not there
//  * SSreceiving class to receive the serial input and log to SD card 
//    (could use peek on first character to decide how many constitute full record to avoid having a buffer)
//  * all with single clip wire to it, so that other components can be in the boat including phone and batteries

// Pins on the RJ4 connector, top side with hole facing down
// short  Long  short  Long
//  RX     TX    G      +


#include <Wire.h>
#include <SPI.h>
#include <SdFat.h>
#include <OneWire.h>
#include <SFE_MicroOLED.h>

#include "SDloggingBase.h"
#include "SSlogging.h"
#include "oledoutput.h"
#include "DX18x20temperature.h"
#include "simpledatetime.h" 
#include "blueflygps.h"
#include "windmeter.h"
#include "lightsensor.h"
#include "MLX90614irthermometer.h"
#include "HTU21Dhumidity.h"
#include "T5403barometer.h"
#include "MS5611baro.h"
#include "px4pitot.h"
#include "SHT31Dhumidity.h"
#include "si7021humidity.h"
#include "BNO055.h"
#include "T5403barometer.h"
#include "HTU21Dhumidity.h"
#include "serialinputbuffer.h"
#include "quat.h"
#include "flystatus.h"
#include "flylogger.h"


int devicenumber = 4; 

Flylogger wholeSOB(devicenumber); 

void setup(void)  
{
pinMode(13, OUTPUT); 
for (int i = 0; i < 2; i++) {
    digitalWrite(13, HIGH); 
    delay(500); 
    digitalWrite(13, LOW); 
    delay(500); 
    Serial.begin(9600); 
    Serial.println("888"); 
    digitalWrite(13, HIGH); 
    delay(500); 
    digitalWrite(13, LOW); 
}

    wholeSOB.sdlogger =   new SerialStreamlogger(&Serial1, 750000); 
    //wholeSOB.sdlogger =   new SerialStreamlogger(0, 115200); // 0 for USB, else &Serial2
    wholeSOB.sdlogger->pinLEDforflush = 13; // wholeSOB.lightsensor->pinLED; 

#if 1
    wholeSOB.simpledatetime = new SimpleDatetime();   
    //wholeSOB.dst =           new DallasTemperature(16); 
    wholeSOB.enllt =          new EastingNorthingLocalLinearTrans(); 
    wholeSOB.blueflygpsdata = new BlueflyGpsData(&Serial3, 200, false, true); // set the update interval to 200ms instead of 100ms
                                      // false=realbluefly, true=loud 

    //wholeSOB.lightsensor =  new LightSensor(14, 15, 13, -1);   // lpinreadlight, lpinpower, lpinLED, lpinButton      (used also for battery power measurment)
    //wholeSOB.lightsensorgestures = new LightSensorGestures(); 
    wholeSOB.wr =             new WindReceiver(21); 

    // I2C devices using SCL0=19, SDA0=18
    //wholeSOB.humiditydata =   new HumidityData(); 
    //wholeSOB.tbarometer =     new T5403Barometer();   
    wholeSOB.irthermometer =  new IRthermometer(); 
    wholeSOB.si7021humidity = new SI7021humidity(); 
    wholeSOB.sht31humidity =  new SHT31humidity();
    wholeSOB.ms5611baro =     new MS5611();   
    wholeSOB.px4pitot =       new PX4pitot();

    // might carry this and set an LED on when it's not working
    wholeSOB.orientdata =     new BNO055orientation(); 
    wholeSOB.orientdata->SetupSerial(&Serial2); 
#endif

    wholeSOB.consoleserial =  new SerialInputBuffer(0);  // debugging input output (stands in for fake usb_serial_class& case)
    wholeSOB.SetupWhole(); 
    Serial.println("SetupWhole done for serial writing"); 
}

void loop()
{
    wholeSOB.LoopWhole(); 

    // used to create a lot of M-lines to the data stream to test what was filling up the buffers
    //wholeSOB.sdlogger->flush(); 
    //delayMicroseconds(400); 
}

