#include <Wire.h>
#include <SPI.h>
#include "SdFat.h"
#include <OneWire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include "SDloggingBase.h"
#include "SDloggingFat.h"
#include "serialinputbuffer.h"
#include "lightsensor.h"
#include "hotwireanemometer.h"
#include "quat.h"
#include "blueflygps.h"
#include "flystatus.h"
#include "wideoledoutput.h"
#include "flylogger.h"

// work to do:
// pitot values, gps values, light?
// plot one of the metrics (eg the accelerometer peaks)
// build jupyter plotting technology from the data sets (is this possible in 3D dynamic?)

// scipy curve_fit??
// look up max G-force and how it adds up.
// A series of on-going realtime processing measurements

int devicenumber = 5; 

Flylogger wholeSOB(devicenumber); 
Flystatus flystatus; 

#define P(X) Serial.print(X)

//SensorSerialReceiver ssr(&Serial1); 
SerialInputBuffer ssr(&Serial1); 
SDloggerFat* sdreplayfile = 0;

Adafruit_SSD1306 display(5, 20, 8, 7, 6); // OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS  (was 6, 7, ... till needed those for the serial port on spare GPS)
//display.begin(SSD1306_SWITCHCAPVCC);    // done in WideOledOutput::SetupOLED()

void setup() 
{
    Serial.begin(9600); 

    ssr.SetupSerial(750000); 

    pinMode(13, OUTPUT); 
    digitalWrite(13, HIGH); delay(50); digitalWrite(13, LOW); delay(90); digitalWrite(13, HIGH); delay(50); digitalWrite(13, LOW); 
    
    // numbers here are pin allocations on the Teensy3.1
    // SPI devices using MOSI=11, MISO=12, SCK=13

    // logging to SD card, to serial stream or to usb monitor
    sdreplayfile = new SDloggerFat(4, "OOR"); // keep in derived class till we decide it's not a replay file

//Serial.println("hihihihihi");  
//delay(1000); 
//Serial.println("hihihihihi2");  
   
    //wholeSOB.sdlogger =      new SerialStreamlogger(0, 115200); // 0 for USB, else &Serial2
    
    if (sdreplayfile) {
        sdreplayfile->pinLEDforflush = -1; // wholeSOB.lightsensor->pinLED; 
        sdreplayfile->SetupSD(); 
        if (sdreplayfile->sdstate == 1) {
            sdreplayfile->SDprintStats(); 
            if (sdreplayfile->openreplayfile()) {
                Serial.println("Replay logger engaged"); 
                flystatus.replayfilesize = sdreplayfile->replayfilesize(); 
                flystatus.replayfilebytes = 0; 
            } else {
                wholeSOB.sdlogger = sdreplayfile; // copied over
                sdreplayfile = 0; // normal case!
                Serial.println("normal non-replay case"); 
            }
        } else {
            Serial.println("SD card not working"); 
            sdreplayfile = 0; 
            wholeSOB.sdlogger = 0; 
        }
    }


    //wholeSOB.simpledatetime = new SimpleDatetime(); 
    wholeSOB.enllt =          new EastingNorthingLocalLinearTrans(); 
    wholeSOB.blueflygpsdata = new BlueflyGpsData(&Serial2, 200, true);  // non-bluefly direct 

    wholeSOB.lightsensor =    new LightSensor(23, 22, -1, -1);   // lpinreadlight, lpinpower, lpinLED, lpinButton      (used also for battery power measurment)

// not working
//    wholeSOB.hotwireanemometer = new HotWireAnemometer(15, 17);           // lpinreadOUT, lpinreadtmp
    
    wholeSOB.wideoled =       new WideOledOutput(display);
    wholeSOB.consoleserial =  new SerialInputBuffer(0);  // debugging input output (stands in for fake usb_serial_class& case)
    wholeSOB.flystatus =      &flystatus; 
    wholeSOB.SetupWhole(); 
    Serial.println("SetupWhole done for serial receive"); 
}


long lastdisplaymstamp = 0; 
int prevDmaxnsavailable = 0; 
void loop() 
{
    // replay file case
    if (sdreplayfile) {
        if (sdreplayfile->sdstate == 8) {
            ssr.bufflen = sdreplayfile->readreplayfileintobuff(ssr.buff); 
            if (ssr.bufflen != -1) {
                flystatus.processrec(ssr.buff, ssr.bufflen); 
                if (flystatus.dtval > 0)
                    delay(min(10, flystatus.dtval/5)); 
                flystatus.replayfilebytes += ssr.bufflen + 1;  
                flystatus.replayfilesize = sdreplayfile->replayfilesize(); 

            } else {
                digitalWrite(13, HIGH);  // goes on when we finish (endless loop)
                P("donedone\n"); 
            }
        }
        // LoopWhole code to handle button and display on the replay
        if (wholeSOB.lightsensor)    wholeSOB.FetchLight(); 
        if (wholeSOB.wideoled)       wholeSOB.WriteWideOled(); 
        return; 
    }
    
    //while(Serial1.available())  Serial.write(Serial1.read());  return; 
    wholeSOB.LoopWhole(); 

    while (ssr.RecCheck()) {
        bool bgoodrec = flystatus.processrec(ssr.buff, ssr.bufflen); 
        if (wholeSOB.sdlogger) {
            if (!ssr.shouldselectivelydiscardrecordfromfillingbuffer()) {
                if (!bgoodrec) 
                    wholeSOB.sdlogger->writestring("*"); 
                wholeSOB.sdlogger->logserialbuff(ssr.buff, ssr.bufflen); 
            } else { 
                P("discarding "); P(ssr.buff); P("\n"); 
            }
            wholeSOB.sdlogger->checkfornextflush(); 
        }
    }

    if (ssr.Dmaxnsavailable > prevDmaxnsavailable) {
        if (ssr.Dmaxnsavailable > 1000) {
            P(" Dmaxnsavailable now at "); 
            P(ssr.Dmaxnsavailable); 
            P("\n"); 
        }
        if (ssr.Dmaxnsavailable > 1000)
            ssr.Dmaxnsavailable = 0; 
        prevDmaxnsavailable = ssr.Dmaxnsavailable; 
    }
            
}



