
#include "quat.h"
#include "blueflygps.h"
#include "flystatus.h"

#define P(X) Serial.print(X)

float tButt3low1b100_b[] = { 3.75683802e-06,   1.12705141e-05,   1.12705141e-05, 3.75683802e-06 }; 
float tButt3low1b100_a[] = { 1.        ,       -2.93717073,       2.87629972,   -0.93909894 }; 

float dt = 1.1; 
float decaypersec = 0.09491938347551916; 
ABFilter ed1(ABFilter::expdecayfac(dt, decaypersec), dt); 
//ABFilter ed1(tButt3low1b100_a, tButt3low1b100_b, 4, 0.1); 

void setup() 
{
    Serial.begin(9600); 
    delay(1000); 
    P("hihi\n"); 
    delay(1000); 
    for (int i = 0; i < 10; i++) {
        ed1.addvalue(i, 4); 
        P(i); P(" "); P(ed1.filtval()); P("  "); P(ed1.difffiltval()); P("\n"); 
    }

    for (int i = 10; i < 30; i++) {
        // optional line to strip out a batch:
//            if ((i >= 15) && (i <= 24))  continue; 
        float x = 10+(i-10)*0.1; 
        ed1.addvalue(i, 10+(i-10)*0.1); 
        // optional line to add in linear samples:
//            ed1.addvalue(i+0.3, 10+(i+0.3-10)*0.1); 
        P(i); P(" "); P(x); P(" "); P(ed1.filtval()); P("  "); P(ed1.difffiltval()); P("\n"); 
    }
}

void loop() 
{
  
}
