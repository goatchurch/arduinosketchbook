// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"
#include "math.h"
#include "quat.h"
#include "blueflygps.h"
#include "flystatus.h"

#define P(X) Serial.print(X)
#define PD(X) Serial.println(X)


// kill this class
const char* Flystatus::rectypes = "ZYQVPFDWHISGNOBXMUL"; 

bool Dbhexerror = false; 
uint16_t hexval(uint8_t c)  
{
    if ((c < '0') || (c > 'F'))  
    {
        P((int)c); P("HexError\n"); 
        Dbhexerror = true; 
    }
    return c - (c <= '9' ? '0' : 'A'-10); 
}

uint16_t hexread16(const char* buff)
{
    return (hexval(buff[0])<<12) | (hexval(buff[1])<<8) | (hexval(buff[2])<<4) | (hexval(buff[3])); 
}
uint32_t hexread24(const char* buff)
{
    return (((uint32_t)hexread16(buff))<<8) | (hexval(buff[4])<<4) | (hexval(buff[5])); 
}
uint32_t hexread32(const char* buff)
{
    return (((uint32_t)hexread16(buff))<<16) | hexread16(buff+4); 
}


//b, a = scipy.signal.butter(3, 0.01, 'low')
float Butt3low1b100_b[] = { 3.75683802e-06,   1.12705141e-05,   1.12705141e-05, 3.75683802e-06 }; 
float Butt3low1b100_a[] = { 1.        ,       -2.93717073,       2.87629972,   -0.93909894 }; 

float Butt3low1b40_b[] = { 5.60701149e-05, 1.68210345e-04, 1.68210345e-04, 5.60701149e-05 }; 
float Butt3low1b40_a[] = { 1.        ,       -2.84296052,  2.69801053,    -0.85460144 }; 

Flystatus::Flystatus() : 
    Faz(Butt3low1b100_a, Butt3low1b100_b, 4, 0.01), 
    baroEntFiltTemp(1.0, 0.5), // this is no filter, just a 0.5second aggregation
    baroEntFiltBaro(ABFilter::expdecayfac(0.08664339756999316, 0.5), 0.5), // temp_rE=0.08664339756999316 // -log(0.5)/8 for 8 second half life
    baroFiltDiff(Butt3low1b40_a, Butt3low1b40_b, 4, 0.02)
{
    degcI = 0; 
    degcIA = 0; 
    degcS = 0; 
    degcG = 0; 
    humidS = 0; 
    humidG = 0; 
    baroB = 0; 
    baroF = 0; 
    wspeed = 0; 
    dbaroX = 0; 
    wspeedX = 0; 
    degcX = 0; 
    lightI = 0; 
    wspeedH = 0; 
    replayfilesize = 0; 
    tval = 0;
    dtval = 0;
    
    ax = 0; ay = 0; az = 0; 
    gx = 0; gy = 0; gz = 0; 
    calibstat = 0; 
    quat.Set(1,0,0,0); 
    slowsinattack = 0; 
    slowsinroll = 0; 
    
    Gms = 0; Gx = 0; Gy = 0; Galt = 0; Gvmps = 0; Gvdeg = 0; Gisodate[23] = 0; Gisodate[0] = 0; 
    relE = 0; relN = 0; relE2 = 0; relN2 = 0; 
    //EastingNorthingLocalLinearTrans enllt; 

    for (int i = 0; i < NRECTYPES; i++) 
        rectypecounts[i] = 0; 
    prevmtime = 0; 
    prevnMcount = -1; 

    minserialtimestampoffset = -999; 
    serialtimestampoffset = 0; 
    serialtimestampoffsetcount = 0; 
    Nloglines = 0; Nloglinesfromserialbuff = 0; Nloglinesfailed = 0; 
    logfilenameTail = 0; 
}

long Dminprsecf = 0, Dmaxprsecf = 0; 
long Nprsreading = 0; 
long DbaroFtprev = 0; 
long Dnreccount = 0; 
bool Flystatus::processrec(const char* buff, int8_t bufflen)
{
    Dnreccount++; 
    Dbhexerror = false; 
    long prevtval = tval; 
    tval = (buff[1] == 't' ? hexread32(buff+2) : 0); 
    dtval = tval - prevtval; 
    
    float mstamp = tval*0.001; 
    
    const char* rj = strchr(rectypes, buff[0]); 
    if (rj != 0) 
        rectypecounts[rj - rectypes]++; 
        
    else { P("unknown rec char "); P(buff[0]); P("\n"); }
    
    if (buff[0] == 'I') {
        uint16_t rawI = hexread16(buff+11); 
        if (rawI != 0xFFFF)
            degcI = rawI*0.02 - 273.15; 
        uint16_t rawIA = hexread16(buff+16); 
        if (rawIA != 0xFFFF) {
            degcIA = rawIA*0.02 - 273.15; 
        }
        //P(buff); P(" I "); P(rawI); P(degcI); P("\n");  
    } else if (buff[0] == 'S') {
        degcS = (hexread16(buff+16)*175.0)/0xFFFF - 45.0;  
        humidS = (hexread16(buff+11)*100.0)/0xFFFF; 
        //P(buff); P(" S "); P(degcS); P("C "); P(humidS); P("%\n");  
    } else if (buff[0] == 'G') {
        degcG = (hexread16(buff+16)*175.25/65536) - 46.85;  
        humidG = (hexread16(buff+11)*125.25)/65536 - 6; 
        // P(buff); P(" G "); P(degcG); P("C "); P(humidG); P("%\n");  
        baroEntFiltTemp.addvalue(mstamp, degcG); // might be better with the tS sensor
    } else if (buff[0] == 'B') {
        baroB = hexread24(buff+11); 
        //P(buff); P(" B "); P(baroB); P(" Mb "); P("\n");  
        
        // moved from baroF as this is a lot more stable
        baroFiltDiff.addvalue(mstamp, baroB);  // not used?
        baroEntFiltBaro.addvalue(mstamp, baroB); 
    } else if (buff[0] == 'F') {
        baroF = hexread24(buff+11); 
        //if (abs(baroF - 104200) > 300) { P(buff); P(" F "); P(baroF); P(" Mb "); P("\n"); }
        //long DbaroFt = hexread32(buff+2); 
        //if (DbaroFt == DbaroFtprev) { P("arg "); P(buff); P(" "); P(bufflen); P("  "); P(Dnreccount); P("\n"); }
        //DbaroFtprev = DbaroFt; 
        //P(buff); P(" F "); P(baroF); P(" Mb "); P("\n");  
        
        
    } else if (buff[0] == 'Z') {  // Zt00D0F0BBxFFEDy0011z0015a02C4bFDC4cFE96 w22CAxEA13yCFCBz08FEsF2F2
        if (bufflen != 65)
            return false; 
        ax = (int16_t)hexread16(buff+11); 
        ay = (int16_t)hexread16(buff+16); 
        az = (int16_t)hexread16(buff+21); 
        gx = (int16_t)hexread16(buff+26); 
        gy = (int16_t)hexread16(buff+31); 
        gz = (int16_t)hexread16(buff+36); 
        quat.Set((int16_t)hexread16(buff+41), (int16_t)hexread16(buff+46), (int16_t)hexread16(buff+51), (int16_t)hexread16(buff+56)); 
        calibstat = hexread16(buff+61) & 0xFF; 
        float slowfac = 0.1; 
        slowsinattack = slowsinattack*(1-slowfac) + quat.GetSinAttack()*slowfac; 
        slowsinroll = slowsinroll*(1-slowfac) + quat.GetSinRoll()*slowfac; 
        
        Faz.addvalue(mstamp, az); 

        //float qw, qx, qy, qz; 
        //P(buff); P(" Z ("); P(ax); P(","); P(ay); P(","); P(az); P(")\n");  
    } else if (buff[0] == 'W') {  // no longer have these
        float revtime = hexread16(buff+11); 
        if ((revtime != 0) && (revtime != 0xFFFF))
            wspeed = 80000.0/revtime; 
        else
            wspeed = 0.0; 
        //P(buff); P(" W "); P(wspeed); P(" m/s "); P("\n");  
    } else if (buff[0] == 'X') {  // Xt00003714d0000ABA3r02DEn0004w0002FEBF   ; was: Xt000DFB74d203Br02C3 X 62.60 Mb 19.08C
        //float psi = ((float)hexread16(buff+11))*(1.0/(0x3FFF*0.4)) - 1.25;  
        float psi = ((float)hexread32(buff+11)*3/2/8)*(1.0/(0x3FFF*0.4)) - 1.25;  
        dbaroX = psi*6894.75728;     // into pascals
        degcX = ((float)hexread16(buff+20))*(200.0/0x7FF) - 50; 
        wspeedX = (dbaroX - 50)*0.4; 
        if (degcX > 1000)  degcX = 0; 
        if (dbaroX > 1000) dbaroX = 0; 
        uint16_t windcountsum = hexread16(buff+25); 
        uint32_t revtimesum = hexread32(buff+30); 
        wspeed = (windcountsum != 0 ? 80000.0*windcountsum/revtimesum : 0); 
        //P(buff); P(" X "); P(dbaroX); P(" Mb "); P(degcX); P("C  "); P(" wspeed "); P(wspeed); P("m/s\n");  
    } else if (buff[0] == 'L') {
        lightI = hexread16(buff+11)/1024.0; 
        //P(buff); P(" L "); P(lightI); P("\n");  
    } else if (buff[0] == 'U') {
        float windMPH = pow(((hexread16(buff+11)*3.3/5.0 - 264.0) / 85.6814), 3.36814); 
        wspeedH = windMPH*(5280*12*2.54/100/3600); 
        //P(buff); P(" D "); P(wspeedH); P("\n");  
    } else if (buff[0] == 'Q') { // Qt00019B7Au02719984y0180F234x0006D545a3CED
        uint8_t devno = buff[42]-'A'; // this is the only record type that is processrecced from the top floor teensy, but could be expanded when we decide top floor should be primary
        int32_t latminutes10000 = hexread32(buff+20); 
        int32_t lngminutes10000 = hexread32(buff+29); 
        //P("X"); P(Gx); P(" Y"); P(Gy); P(" Alt:"); P(Galt); P("\n");            
        if (!enllt.IsValid()) 
            enllt.SetLinearTransOrigin(latminutes10000, lngminutes10000); 
        int32_t rlatminutes10000 = latminutes10000 - enllt.Offsetlatminutes10000; 
        int32_t rlngminutes10000 = lngminutes10000 - enllt.Offsetlngminutes10000; 
        int32_t relE10 = (1000*rlatminutes10000) / enllt.latdivisorE100 + (1000*rlngminutes10000) / enllt.lngdivisorE100; 
        int32_t relN10 = (1000*rlatminutes10000) / enllt.latdivisorN100 + (1000*rlngminutes10000) / enllt.lngdivisorN100; 

        if (devno == 1) {  // primary bluefly device
            Gms = hexread32(buff+11); 
            Gy = latminutes10000/600000.0; 
            Gx = lngminutes10000/600000.0; 
            Galt = ((int16_t)hexread16(buff+38))*0.1; 
            relE = relE10*0.1; 
            relN = relN10*0.1; 
        } else if (devno == 0) { // secondary onboard device
            relE2 = relE10*0.1; 
            relN2 = relN10*0.1; 
        }
    } else if (buff[0] == 'V') { // Vt00019B86v002Ad005B2F
        Gvmps = hexread16(buff+11)*(1000.0/3600.0/100.0); 
        Gvdeg = hexread16(buff+16)*0.01; 
    } else if (buff[0] == 'R') { // Rt00019C41d"2016-08-31T11:23:19.500"eFFFFBE4En0000021Cf000002D4o00005820
        memcpy(Gisodate, buff+12, 23); 
        P("GPS datetime: "); 
        P(Gisodate); 
    } else if (buff[0] == 'P') { // Pt0014D9E2h0000v000000
        long Gdh = hexread16(buff+11); 
        long Gdv = hexread24(buff+16); 
        if ((Gdh == 0) && (Gdv == 0))
            mstampNoLock = millis(); 
    } else if (buff[0] == 'M') {
        // this is a clean record we can use for the timestamp offset
        serialtimestampoffset = tval - millis(); 
        if ((serialtimestampoffsetcount == 0) || (serialtimestampoffset < minserialtimestampoffset))
            minserialtimestampoffset = serialtimestampoffset; 
        serialtimestampoffsetcount++; 
        if ((serialtimestampoffsetcount % 100) == 0) {
            P("serialtimemsdiff "); 
            P(serialtimestampoffset); 
            P("  minserialtimestampoffset "); 
            P(minserialtimestampoffset); 
            P("\n"); 
        }

        
        long lr = 0; 
        for (int i = 0; i < NRECTYPES-3; i++) // don't count the ULM records
            lr += rectypecounts[i]; 
        long nMcount = hexread32(buff+11); 
        long lbetweenreccount = hexread32(buff+20); 
        if ((lbetweenreccount != lr) || (prevnMcount+1 != nMcount) || ((nMcount % 2000) == 0)) {
            for (int i = 0; i < NRECTYPES; i++) {
                if (rectypecounts[i] != 0) {  
                    P(rectypes[i]); 
                    P(rectypecounts[i]);  
                    P(" "); 
                }
            }
            P(buff); 
            P(" "); 
            P(tval - prevmtime); 
            P("ms l="); 
            P(lbetweenreccount); 
            P(" "); 
            P(lr); 
            P("  mcount="); 
            P(nMcount);
            P(","); 
            P(prevnMcount+1); 
            P("\n"); 
        }
        prevnMcount = nMcount; 
        prevmtime = tval; 
        for (int i = 0; i < NRECTYPES; i++) 
            rectypecounts[i] = 0; 
    } else {
        P("unknown: "); 
        P(buff); 
        P("\n"); 
    }
    if (Dbhexerror) {
        P("hexerror in: '"); 
        P(buff); 
        P("'\n"); 
        Dbhexerror = false; 
        return false; 
    }
    return true; 
}

float Flystatus::getairentropy()
{
    // baroEntFilt(1.0, 0.5), tempEntFilt(ABFilter::expdecayfac(0.07, 0.5), 0.5)
    // baroEntFilt.addvalue(mstamp, baroF); 
    // tempEntFilt.addvalue(mstamp, baroF); 
    // should take account of nt0bytimestep to compare the same sample windows

    float fb = baroEntFiltBaro.filtval(); // grouped into half second intervals and then exp filtered
    //float ft = degcG; // unfiltered
    float ft = baroEntFiltTemp.filtval(); // unfiltered, but 0.5sec aggregated and averaged
    float gamma = 1.4; 
    float res = pow(fb, 1-gamma) * pow(ft+273.16, gamma); 
    P("ent calc "); P(fb); P("  "); P(ft); P(" "); P(res); P("\n"); 
    return res; 
//    return ft; 
}

float Flystatus::getaltodiff()
{
    P("getaltodiff "); P(baroFiltDiff.filtval()); P(" diff "); P(baroFiltDiff.difffiltval()); P("\n"); 
    return baroFiltDiff.difffiltval()*0.1; // factor down by air density
}


ABFilter::ABFilter() :
    a(0), b(0), n(0)
{;}

ABFilter::ABFilter(const float* la, const float* lb, int ln, float ltimestep) :
    a(la), b(lb), n(ln), timestep(ltimestep)
{
    xybuff = (float*)malloc(2*n*sizeof(float)); 
    for (int i = 0; i < 2*n; i++)
        xybuff[i] = 0; 
    xybuffpos = 0; 

    t0 = -1.0; 
    prevt = -1.0; 
    prevx = -1.0; 
    sumX = 0.0; 
}

ABFilter::ABFilter(float b0, float ltimestep) :
    a(0), b(0), n(0), timestep(ltimestep)
{
    xybuff = (float*)malloc(3*sizeof(float)); // xybuff[0] is the y, xybuff[1] is the decayfactor, xybuff[2] is prev-y
    xybuff[0] = 0; 
    xybuff[1] = b0; 
    xybuff[2] = 0; 
    xybuffpos = -1; 

    t0 = -1.0; 
    prevt = -1.0; 
    prevx = -1.0; 
    sumX = 0.0; 
}

float ABFilter::expdecayfac(float dt, float decaypersec)
{
    return 1 - exp(-dt*decaypersec); 
} 


bool ABFilter::addvalueT(float t, float x)
{
    //P("addvalueT "); P(t); P(" "); P(x);     
    // assert t0 <= prevt <= t0 + timestep; 
    if (t <= t0) {
        prevx = x; 
    //P(" t<t0\n");         
        return false; 
    }
    if (t < t0 + timestep) {
        sumX += (t - prevt)*(x + prevx)*0.5; 
        prevt = t; 
        prevx = x; 
        //P(" t<t0+timestep\n");         
        return false; 
    }
    float lam = (t0 + timestep - prevt)/(t - prevt); 
    //assert 0 <= lam <= 1; 
    float xlam = prevx*(1 - lam) + x*lam; 
    //P(" lam "); P(lam); P(" xlam "); P(xlam); 
    // float tlam = prevt*(1 - lam) + t*lam == prevt + lam*(t - prevt) == prevt + (t0 + timestep - prevt) == t0 + timestep 
    sumX += (t0 + timestep - prevt)*(xlam + prevx)*0.5; 
    t0 = t0 + timestep; 
    nt0bytimestep++; 
    // TOL_ZERO(t0 - nt0bytimestep*timestep); 
    //P(" t0 "); P(t0); P("\n");     
    prevt = t0; 
    prevx = xlam; 
    return true; 
}

void ABFilter::addvalueF(float x)
{
//P("addvalueF "); P(x); P("\n");     
    if (n == 0) {
        float y = xybuff[0]; 
        xybuff[2] = y; 
        xybuff[0] = x*xybuff[1] + y*(1-xybuff[1]); 
        return; 
    }
    
    xybuff[xybuffpos] = x; 
    int j = xybuffpos; 
    float y = 0; 
    for (int i = 0; i < n; i++) {
        y += xybuff[j]*b[i]; 
        if (i != 0)
            y -= xybuff[j+n]*a[i]; 
        if (j == 0)
            j = n; 
        j -= 1; 
    }
    if (a[0] != 1)
        y /= a[0]; 
    xybuff[xybuffpos+n] = y; 
    xybuffpos += 1;
    if (xybuffpos == n)
        xybuffpos = 0; 
}

void ABFilter::firstvalue(float t, float x)
{
    nt0bytimestep = (int)floor(t/timestep); 
    t0 = nt0bytimestep*timestep; // get the timesteps syncronized!
    t0 = t; 
    prevt = t0; 
    prevx = x; 
    sumX = 0; 

    if (n == 0) {
        xybuff[0] = x; 
        P("Setting exp filter "); P(xybuff[0]); P("  dt "); P(timestep); P("\n"); 
        xybuff[2] = x; 
    } else {
        for (int i = 0; i < 2*n; i++)
            xybuff[i] = x; 
    }
}


void ABFilter::addvalue(float t, float x)
{
    if (t0 == -1) {
        firstvalue(t, x); 
        return; 
    }
    if (abs(t - t0) > timestep*64) {  
        firstvalue(t, x); 
        P(" big timestep, restarting filter\n"); 
        return; 
    }
    
    int i = 64; // protect against infinite loops
    while (addvalueT(t, x) && (i-- > 0)) {
        addvalueF(sumX/timestep); 
        sumX = 0; 
    }
}

float ABFilter::filtval()
{
    if (n == 0) 
        return xybuff[0]; 
    int j = (xybuffpos == 0 ? n-1 : xybuffpos-1); 
    return xybuff[j+n]; 
}

float ABFilter::difffiltval() 
{
    if (n == 0) 
        return (xybuff[0] - xybuff[2])/timestep; 
    int j = (xybuffpos == 0 ? n-1 : xybuffpos-1); 
    int j1 = (j == 0 ? n-1 : j-1); 
    float res = (xybuff[j+n] - xybuff[j1+n])/timestep; 
    return res; 
}

 
