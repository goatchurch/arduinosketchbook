// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#include "Arduino.h"

#include <Wire.h>
#include <SPI.h>
#include "SdFat.h"
#include <OneWire.h>
#include <SFE_MicroOLED.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "quat.h"
#include "blueflygps.h"
#include "flystatus.h"
#include "wideoledoutput.h"

#include "SDloggingBase.h"
#include "SDloggingFat.h"
#include "SSlogging.h"
#include "oledoutput.h"
#include "DX18x20temperature.h"
#include "simpledatetime.h"
#include "windmeter.h"
#include "lightsensor.h"
#include "hotwireanemometer.h"
#include "MLX90614irthermometer.h"
#include "BNO055.h"
#include "HTU21Dhumidity.h"
#include "T5403barometer.h"
#include "MS5611baro.h"
#include "px4pitot.h"
#include "SHT31Dhumidity.h"
#include "si7021humidity.h"
#include "serialinputbuffer.h"
#include "flylogger.h"

#define P(X) Serial.print(X)
#define PD(X) Serial.println(X)

Flylogger::Flylogger(int ldevicenumber) 
{
    devicenumber = ldevicenumber; 
    
    oled = 0; 
    wideoled = 0; 
    sdlogger = 0; 
    sdreplaylogger = 0; 
    dst = 0; 
    enllt = 0; 
    blueflygpsdata = 0; 
    wr = 0; 
    simpledatetime = 0; 
    lightsensor = 0; 
    humiditydata = 0; 
    irthermometer = 0;
    humiditydata = 0;
    si7021humidity = 0;
    sht31humidity = 0; 
    tbarometer = 0; 
    ms5611baro = 0; 
    px4pitot = 0;
    flystatus = 0; 
    
    oleddisplaymodePrior = 0; 
    oleddisplaymode = 0; 
}

// old oled display case
long mstamplastoleddisplaymodesetting = 0; 
char oledmodesettingsrcPrior = 0;
char oledmodesettingsrc = 0;  // src = [i]nit, [k]eybutton, [c]onsole, [b]luetooth, c[a]librate-orientation, '[v]olts, '[o]rient calib
void Flylogger::SetOleddisplayMode(char loleddisplaymode, char src) 
{
    if (oled == 0)
        return; 
    if (((oleddisplaymode == 'v') || (oledmodesettingsrc == 'k')) && (src == 'o'))
        return; 
    
    if (src == 'v') {
        if (loleddisplaymode == 'v') {
            oleddisplaymodePrior = oleddisplaymode; 
            oledmodesettingsrcPrior = oledmodesettingsrc; 
        } else if (oledmodesettingsrc == 'v') {  // set back
            oleddisplaymode = oleddisplaymodePrior; 
            oledmodesettingsrc = oledmodesettingsrcPrior; 
        }
    }
    
    oledmodesettingsrc = src; 
    if (loleddisplaymode == 'r')  ; 
    else if (loleddisplaymode != 'n')  oleddisplaymode = loleddisplaymode; 
    
    // 'n'ext cases
    else if (oleddisplaymode == 'o')  oleddisplaymode = 'b'; 
    else if (oleddisplaymode == 'b')  oleddisplaymode = 'f'; 
    else if (oleddisplaymode == 'f')  oleddisplaymode = 'h'; 
    else if (oleddisplaymode == 'h')  oleddisplaymode = 'g'; 
    else if (oleddisplaymode == 'g')  oleddisplaymode = 'l'; 
    else if (oleddisplaymode == 'l')  oleddisplaymode = 'w'; 
    else if (oleddisplaymode == 'w')  oleddisplaymode = 't'; 
    else if (oleddisplaymode == 't')  oleddisplaymode = 'o'; 
    
    else  oleddisplaymode = 't'; 

    P("Setting OledDisplayMode "); 
    P(oleddisplaymode); 
    P(" "); 
    P(src); 
    P("\n"); 
    mstamplastoleddisplaymodesetting = millis(); 
}

void Flylogger::SetupWhole() 
{
    if (wideoled) {
        wideoled->SetupOLED("device:", devicenumber); 
    }    

    if (consoleserial)
        consoleserial->SetupSerial(); 
    PD("consoleserial done"); 

    if (lightsensor && (lightsensor->pinLED != -1))
        analogWrite(lightsensor->pinLED, 50); 
    
    if (oled) {
        oled->SetupOLED("device:", devicenumber); 
        SetOleddisplayMode('t', 'i'); 
    }
    
    if (sdlogger->sdstate == 0) {
        sdlogger->SetupSD(); 
        sdlogger->SDprintStats(); 
    }
    PD("sdlogger done"); 
    delay(300); // to get a chance to open the serial console
    if (lightsensor && (lightsensor->pinLED != -1))
        analogWrite(lightsensor->pinLED, 0); 
    
    if (dst) {
        int ns = dst->FindAllSensors(true); 
        if (oled) {
            oled->oledbuffer[0] = '0'+ns; 
            oled->oledbuffer[1] = 0;
            oled->write_oled("dallas", oled->oledbuffer); 
        }
    }
    
    if (simpledatetime) {
        simpledatetime->SetfromEpoch(Teensy3Clock.get()); 
        delay(350); 
    }
    
    if (sdlogger) {
        uint32_t mstamp = millis(); 
        if (sdlogger->opennextlogfileforwriting((simpledatetime ? simpledatetime->pisotimestamp : 0), mstamp)) {
            if (oled)
                oled->write_oled(sdlogger->logfilename + 4); 
            sdlogger->writelogfileheader(mstamp, (simpledatetime ? simpledatetime->pisotimestamp : 0), devicenumber); 
        } else {
            if (oled)
                oled->write_oled("No log file"); 
        }
    }
    PD("sdloggerhead done"); 

    // list all I2C devices we could be having
    if (irthermometer || humiditydata || si7021humidity || tbarometer || sht31humidity || ms5611baro || px4pitot)
        Wire.begin(); 
    PD("wirebegin done"); 
    delay(350);
        
    if (wr) {
        wr->SetWindInterrupts(); 
        PD("windinterrupts done"); 
    }
    
    if (tbarometer)
        tbarometer->T5403SetUp(); 
    if (ms5611baro) {
        PD("about to do ms5611baro"); 
        ms5611baro->initiate(); 
        PD("ms5611baro done"); 
    }
    
    if (si7021humidity) {
        si7021humidity->resetchip(); 
        delay(20); 
        si7021humidity->checkchip();
        si7021humidity->checkstatus();
    }
    
    if (sht31humidity) {
        sht31humidity->writeCommand(0x30A2);   // soft reset
        delay(1000); 
        sht31humidity->writeCommand(0x2737);   // read 10Hz?
    }
    
    if (wideoled) {
        wideoled->display.stopscroll();
    }
}    




float irtmpforoled = -999.0; 
int irmstampdivided = 0; 
void Flylogger::FetchIRtemp(int mstampdivider, int mstampoffset)
{
    long mstamp = millis(); 
    int lirmstampdivided = (mstamp+mstampoffset)/mstampdivider; 
    if (lirmstampdivided == irmstampdivided)
        return; 
    irmstampdivided = lirmstampdivided; 
    uint16_t irt16 = irthermometer->ReadIRtemp16(0x07); // takes 0.517ms to read
    uint16_t irtamb16 = irthermometer->ReadIRtemp16(0x06); // ambient temp of device
    if (irt16 != 0xFFFF) {
        if (sdlogger)
            sdlogger->logirlevel(mstamp, irt16, irtamb16); 
        if (oled && (oleddisplaymode == 'i'))
            oled->writeir(irt16, irtamb16); 
    } 
    
    if (oled && (oleddisplaymode == 't'))
        irtmpforoled = (irt16 != 0xFFFF ? irt16*0.02 - 273.15 : -999.0); 
}


float tempreadings[3]; 
void Flylogger::FetchDallas() 
{
    int i = dst->readingready(); 
    if (i == -1)
        return; 
    int16_t t16 = dst->fetchreading16(i); 
    if (sdlogger)
        sdlogger->logdallas(millis(), i, t16);
        
    // this oled section function needs comlete redoing
    if (oled && (oleddisplaymode == 't') && (abs(tempreadings[i] - t16) > 2)) {
        tempreadings[i] = t16*0.0625; 
        if (dst->ns <= 2) {
            if (humiditydata) 
                tempreadings[2] = -6 + (125 * ((humiditydata->rawHumidity & 0xFFFC) / (float)65536)); 
            else if (si7021humidity)
                tempreadings[2] = si7021humidity->gethumidity(); 
        }
        if (irtmpforoled != -999.0)
            tempreadings[0] = irtmpforoled; 
        oled->writetemps(tempreadings[0], tempreadings[1], tempreadings[2]); 
    }
    dst->beginreading(i); 
}

long lastlightmstamp = 0; 
long prevlightlevel = 0; 
long lastbuttonmstamp = 0; 
int Flylogger::FetchLight()
{
    long lightlevel = lightsensor->ReadLight(); 
    long mstamp = millis(); 
    if ((mstamp >= lastlightmstamp + 100) && ((abs(lightlevel - prevlightlevel) >= 10) || (mstamp >= lastlightmstamp + 500))) {
        if (sdlogger) {
            if ((flystatus == 0) || (flystatus->minserialtimestampoffset != -999)) {
                uint8_t* sddata = sdlogger->loglightlevel(mstamp+(flystatus ? flystatus->minserialtimestampoffset : 0), lightlevel); 
                if (flystatus)
                    flystatus->processrec((const char*)sddata, -1);   // hack in processing of the buffer as the light cell is on the main board
            }
        }
        prevlightlevel = lightlevel; 
        lastlightmstamp = mstamp; 
    }
    
    bool bNextAdvance = false; 
    int rb = lightsensor->ReadButton(); 
    if (rb == 1) {
        long mstamp = millis(); 
        lastbuttonmstamp = mstamp; 
        SetOleddisplayMode('v', 'v'); 
    } else if (rb == -1) {
        SetOleddisplayMode('r', 'v'); 
        long mstamp = millis(); 
        if (mstamp < lastbuttonmstamp + 1000)
            bNextAdvance = true; 
        lastbuttonmstamp = mstamp; 
    }
    if (bNextAdvance) {
        SetOleddisplayMode('n', 'k'); 
        if (oled)
            oled->writechar(oleddisplaymode); 
        digitalWrite(lightsensor->pinLED, LOW); 
    }
    return 0; 
}


long lasthotwiremstamp = 0; 
void Flylogger::FetchHotWireWind()
{
    long mstamp = millis(); 
    if (mstamp >= lasthotwiremstamp + 250) {
        uint16_t hwwind = hotwireanemometer->Readrawwind(); 
        uint16_t hwtmp = hotwireanemometer->Readrawtmp(); 
        if (sdlogger) {
            if ((flystatus == 0) || (flystatus->minserialtimestampoffset != -999)) {
                uint8_t* sddata = sdlogger->loghotwirewind(mstamp+(flystatus ? flystatus->minserialtimestampoffset : 0), hwwind, hwtmp); 
                if (flystatus)
                    flystatus->processrec((const char*)sddata, -1);   // hack in processing of the buffer as the light cell is on the main board
            }
        }
        lasthotwiremstamp = mstamp; 
    }
}


long last7021humiditytimestamp = 0; 
bool b7021fetchstate = false; 
void Flylogger::Fetch7021Humidity()
{
    long mstamp = millis(); 
    if (b7021fetchstate == false) {
        if (mstamp < last7021humiditytimestamp + 200)
            return; 
        si7021humidity->setmeasurementHT(false);
        b7021fetchstate = true; 
        last7021humiditytimestamp = mstamp; 
        return; 
    }
    if (mstamp < last7021humiditytimestamp + 20)
        return; 
    b7021fetchstate = false; 
    if (!si7021humidity->fetchmeasurementHT()) 
        return; 
    if (sdlogger)
        sdlogger->log7021humidity(mstamp, si7021humidity->rawhumid, si7021humidity->rawtemp); 
}


long last31humiditytimestamp = 0; 
void Flylogger::Fetch31Humidity()
{
    long mstamp = millis(); 
    if (mstamp < last31humiditytimestamp + 200)
        return;  
    if (sht31humidity->readstream()) {
        if (sdlogger)
            sdlogger->log31humidity(mstamp, sht31humidity->rawHumidity, sht31humidity->rawTemperature); 
    }
    last31humiditytimestamp = mstamp; 
}


long lasthumiditymstamp = 0; 
bool breadhumiditytmpnext = false; 
void Flylogger::FetchHumidity()
{
    long mstamp = millis(); 
    if (humiditydata->commandstate == 0) {
        if (mstamp < lasthumiditymstamp + 200)
            return; 
        humiditydata->beginreading(breadhumiditytmpnext ? 0xF3 : 0xF5); 
        breadhumiditytmpnext = !breadhumiditytmpnext; 
    }
    
    if (!humiditydata->valueready()) 
        return; 
    lasthumiditymstamp = mstamp; 
    if (!breadhumiditytmpnext)
        return; 
        
    if (sdlogger)
        sdlogger->loghumidity(mstamp, humiditydata->rawHumidity, humiditydata->rawTemperature); 
}



bool btbarocoeffsoutput = false;
void Flylogger::FetchTBarometer()
{
    long mstamp = millis(); 
    if (tbarometer->bstate == 0) {
        if (mstamp > tbarometer->mstamp + 100)
            tbarometer->RequestPressure(); 
        return; 
    }
    if (!tbarometer->valuesready()) 
        return; 
    if (sdlogger) {
        if (!btbarocoeffsoutput) { 
            sdlogger->logtbarocoeffs(tbarometer->mstamp, tbarometer->c1, tbarometer->c2, tbarometer->c3, tbarometer->c4, tbarometer->c5, tbarometer->c6, tbarometer->c7, tbarometer->c8); 
            btbarocoeffsoutput = true; 
        }
        sdlogger->logtbarometer(tbarometer->mstamp, tbarometer->temperature_raw, tbarometer->pressure_raw); 
    }
}


void Flylogger::FetchMS5611baro()
{
    const int BS_STILL = 0, BS_CONVERSIONDONE=8, BS_STARTTEMP = 1; 
    if (ms5611baro->bstate != BS_STILL) {
        ms5611baro->readloop(); 
        if (ms5611baro->bstate == BS_CONVERSIONDONE) {
            //P(ms5611.D1); P(" "); P(ms5611.D2); P("  "); P(ms5611.TEMP); P("  "); P(ms5611.Pr); P("  "); P(ms5611.approxAlt); PN; 
            //P(ms5611baro->mstamp); P(" "); P(ms5611baro->Pr), P("Mb "); P(ms5611baro->TEMP); P("C\n"); 
            if (sdlogger)
                sdlogger->logbbarometer(ms5611baro->mstamp, ms5611baro->Pr, ms5611baro->TEMP); 
        }
    } else {
       ms5611baro->bstate = BS_STARTTEMP; // keep going on a tight tight loop (of approx 20ms)
    }
}



// wind blade code now aggregated into PXpitot timewindows
uint32_t lastwindcount = 0; 
uint16_t windcountsum = 0; 
uint32_t revtimesum = 0; 
void Flylogger::FetchWind()
{
    uint32_t windcount = wr->revcount; 
    if (lastwindcount != windcount) {
        revtimesum += wr->revtime;  // get it out before next revolution passes and hits the interrupt
        windcountsum++; 
        
        // old logging per blade, now put into the PX4 record
        //if (sdlogger) 
        //    sdlogger->logwind(millis(), windcount, wr->revtime); 
        if ((windcount % 20) == 0) {
            if (sdlogger)
                P(sdlogger->Nloglines); 
            P("  wind: "); P(wr->revtime);  P("\n"); 
        }
        lastwindcount = windcount; 
    }
}

// 25ms cycle with readings that are scaled by 3/2 and averaged across 8 samples
// Using this to aggregate and read the wind meter revolutions at same time
long px4nextmstamp = 0; // this is 25ms
long px4setreadmstamp = 0; 
uint32_t sumrawppressca = 0; 
int nrawppressca = 0; 
int nrawppresbad = 0; 
void Flylogger::FetchPX4pitot()
{
    long mstamp = millis(); 
    if (mstamp < px4nextmstamp)
        return; 
    if (px4setreadmstamp == 0) {
        if (px4pitot)
            px4pitot->setreadpitot(); 
        px4setreadmstamp = mstamp; 
        px4nextmstamp = mstamp + 2; // if 1 it could be just short of clocking
    } else {
        if (px4pitot) {
            px4pitot->freadpitot(); 
            if (px4pitot->status != 0)
                nrawppresbad++; 
            //P("PX4 "); P(micros()); P(" "); P(px4pitot->rawpress); P("\n"); 
            sumrawppressca += px4pitot->getrawscaled(); 
        } else {
            nrawppresbad++; 
        }
        nrawppressca++; 
        if (nrawppressca == 8) {   // aggregated by 8 to get to 200ms
            if (sdlogger) {
                sdlogger->logpitot(px4setreadmstamp, sumrawppressca, px4pitot->rawtemp, windcountsum, revtimesum); 
            }
            windcountsum = 0; 
            revtimesum = 0; 

            sumrawppressca = 0; 
            nrawppressca = 0; 
            nrawppresbad = 0; 
        }
        
        // it would be nice if we could misalign 16 readings against 250ms
        // this could be done by making the steps at i*250//16, although it 
        // find i to satisfy i*16 <= j*250 < i*16+16; dividing out works on low side, 
        // but need to check one out on the upper side
        px4nextmstamp = (mstamp/25+1)*25; 
        px4setreadmstamp = 0; 
    }
}



// orientation wors on serial line and polls it for readings directly to get it up to 100Hz
int adctmpmstampdivided = 0; 
int adctmpmstampdividedSec = 0; 
uint8_t prevcalibstat = 0; 
int ncalibsoutput = 0; 
int ncalibrationuploadedcountdown = 0;  // disable
int nbadquats = 0; 
bool orientdatagoodreading; 
long lastorientgoodreading; 
long timesinceorienggoodreading; 
long goodreadingcountup = 0; 
long goodreadingcountupSec = 0; 
int logorientcount = 0; // now goes into top byte of calibstat instead of prevcalibstat
bool Flylogger::FetchOrientation(int mstampdivider, int mstampoffset)
{
    long mstamp = millis(); 
    int ladctmpmstampdivided = (mstamp+mstampoffset)/mstampdivider; 
// The timeout cycle is actually managed inside BNO055orientation::ReadQLGTC() 
// which calls readdatastackBB(0x20, 22, serbuff, reqQtimeout=7, semdmstimeout=11);  
// meaning that the timeout cycle is on a 7ms operation with return slack of up to 11ms
// This is a serial connection rather than I2C so there are buffers which we can monitor
// in order to get every reading at the frequency of 10ms
    /*if (ladctmpmstampdivided == adctmpmstampdivided)
        return false; 
    adctmpmstampdivided = ladctmpmstampdivided; 
    */
    
    int ladctmpmstampdividedSec = mstamp/1000; 
    if (ladctmpmstampdividedSec != adctmpmstampdividedSec) {
        P("orient count per sec "); 
        P(goodreadingcountup - goodreadingcountupSec); 
        P("\n"); 
        adctmpmstampdividedSec = ladctmpmstampdividedSec; 
        goodreadingcountupSec = goodreadingcountup; 
    }
    
    if (orientdata->FetchOrientation()) {
        orientdatagoodreading = true; 
        lastorientgoodreading = mstamp; 
        timesinceorienggoodreading = 0; 
        if (sdlogger) {
            // these two records should be merged into one and interpreted in the orientdata class as such to simplify and shorten the code
            //sdlogger->logacceleration(mstamp, orientdata->linaccx(), orientdata->linaccy(), orientdata->linaccz(), orientdata->gravvecx(), orientdata->gravvecy(), orientdata->gravvecz()); 
            //sdlogger->logorient(mstamp, orientdata->qw(), orientdata->qx(), orientdata->qy(), orientdata->qz(), ((orientdata->prevcalibstat<<8) | orientdata->calibstat)); 
            uint16_t llcalibstat = ((logorientcount & 0xFF)<<8) | orientdata->calibstat; 
            //uint16_t llcalibstat = ((orientdata->prevcalibstat<<8) | orientdata->calibstat)); 
            sdlogger->logorientA(mstamp, orientdata->linaccx(), orientdata->linaccy(), orientdata->linaccz(), orientdata->gravvecx(), orientdata->gravvecy(), orientdata->gravvecz(), orientdata->qw(), orientdata->qx(), orientdata->qy(), orientdata->qz(), llcalibstat); 
            logorientcount++; 
            if ((goodreadingcountup % 5000) == 0) {
                P(" q"); 
                P(orientdata->qw()); 
                P(" "); 
                P(orientdata->qx()); 
                P(" "); 
                P(orientdata->qy()); 
                P(" "); 
                P(orientdata->qz()); 
                P("\n"); 
            }
            
            //Quat q(orientdata->qw(), orientdata->qx(), orientdata->qy(), orientdata->qz()); 
            //Mat3x3 m(q); 
            //P("Q "); P(q.q0); P(" "); P(q.q1); P(" "); P(q.q2); P(" "); P(q.q3); P("\n"); 
            //P("M "); P(m.a00); P(" "); P(m.a01); P(" "); P(m.a02); P("\n"); 
            //P("M "); P(m.a20); P(" "); P(m.a21); P(" "); P(m.a22); P("\n"); 
        }
        
        goodreadingcountup++; 
        if (goodreadingcountup == 500) {
//            orientdata->currentstate = 60; // trigger a calibration upload
// not happening as we don't have a consistent calibration number and haven't yet shown that the 
// calibration values improve the self-calibration sequence
        }
    } else {
        orientdatagoodreading = false; 
        timesinceorienggoodreading = mstamp - lastorientgoodreading; 
        if (timesinceorienggoodreading > 20000) {
            P("Restarting the orientation after "); 
            P(timesinceorienggoodreading/1000); 
            P(" seconds\n"); 
            lastorientgoodreading = mstamp; 
            timesinceorienggoodreading = 0; 
            orientdata->currentstate = 51; // make it go into reset mode
            goodreadingcountup = 0; 
        }
    }
    
    bool bboredwithcalibration = ((ncalibsoutput >= 8)); // || ((ncalibsoutput != 0) && (mstamp > 5*60*1000))); 
    if (!bboredwithcalibration && orientdatagoodreading && ((ladctmpmstampdivided % 10) == 0)) {
        if ((prevcalibstat != orientdata->calibstat) && (orientdata->calibstat == 0xFF)) {
            P("Requesting Orient Calibration "); 
            P(ncalibsoutput); 
            P("\n"); 
            orientdata->currentstate = 40; // triggers a calibration reading
        }
        prevcalibstat = orientdata->calibstat; 
    }

    if ((orientdata->currentstate == 43) && (orientdata->remainingcalibrationreadingattempts != 0)) {
        sdlogger->logorientcalibrations(mstamp, orientdata->calibstat, orientdata->calibvalues, 22); 
        ncalibsoutput++; 
    }
    return orientdatagoodreading; 
}



long Dprevprsreading = 0; 
long Dprevprst = 0; 
long Dminprsec = 0, Dmaxprsec = 0; 
int Digps = 0; 
long mstampbarogoodreadingF = 0; 
long prsreadingsmoothed = 0; 
long Rcountreadings = 0; 
bool Flylogger::FetchGPS()
{
    int igps = blueflygpsdata->FetchBlueflyData(millis(), enllt); 
    if (igps == 0)
        return false; 
    Digps = igps; 
    
    bool bbadgpschecksum = (igps >= 100); 
    if (bbadgpschecksum)
        igps -= 100;
    
    long mstamp = blueflygpsdata->mstampdollar;   
    
    // the bluefly barometer reading
    if (igps == 10) { // bluefly PRS instance
        // this tries to find the spikes that are occurring 
        //if (abs(blueflygpsdata->prsreading - Dprevprsreading) > 80) { P("bad F "); P(mstamp);  P(" ");  P(blueflygpsdata->prsreading); P("\n"); }
        //Dprevprsreading = blueflygpsdata->prsreading; 

        // this establishes that there are cases where the pressure values are buffered up and all get the same millis() stamp
        // 
        //if (mstamp == Dprevprst) { P("argg "); P(mstamp); P(" "); P(blueflygpsdata->prsreading); P(" n"); P(blueflygpsdata->Nprsreading); P("\n"); }
        //Dprevprst = mstamp; 

        /*
        if ((blueflygpsdata->Nprsreading % 50) == 0) {
            P("prmid "); 
            P((Dminprsec+Dmaxprsec)/2); 
            P("  prwid "); 
            P(Dmaxprsec - Dminprsec); 
            P("\n"); 
            Dminprsec = blueflygpsdata->prsreading; 
            Dmaxprsec = blueflygpsdata->prsreading; 
        } else {
            Dminprsec = min(Dminprsec, blueflygpsdata->prsreading); 
            Dmaxprsec = max(Dmaxprsec, blueflygpsdata->prsreading); 
        }*/
        
        sdlogger->logbfprs(mstamp, blueflygpsdata->prsreading); 
        mstampbarogoodreadingF = mstamp; 
        prsreadingsmoothed = (prsreadingsmoothed*95 + blueflygpsdata->prsreading*5+50)/100; 
    }
    if (enllt == 0)  // gps disabled
        return true; 
    
    //long mstamp = millis();   // original, which accounts for 20ms delays in readings due to baudrates!
    if (igps == 30) // first Gpgga instance (where euclidean orthogonal transform is computed) [won't return this if enllt==0]
        sdlogger->loggpscoeffs(mstamp, blueflygpsdata->isotimestamp, enllt->latdivisorE100, enllt->latdivisorN100, enllt->lngdivisorE100, enllt->lngdivisorN100, blueflygpsdata->devno); 
    if ((igps == 3) || (igps == 30))  { // Gpgga instance
        uint8_t* sddata = sdlogger->loggpsabs(mstamp, blueflygpsdata->mstampmidnight, blueflygpsdata->latminutes10000, blueflygpsdata->lngminutes10000, blueflygpsdata->altitude10, bbadgpschecksum, blueflygpsdata->devno); 
        if (flystatus)
            flystatus->processrec((const char*)sddata, -1);   // hack in processing of gps running from main board (to check it)
        Rcountreadings++; 
        if ((Rcountreadings % 1200) == 0) {  // repeat statement of date every 2 minutes
            sdlogger->loggpscoeffs(mstamp, blueflygpsdata->isotimestamp, enllt->latdivisorE100, enllt->latdivisorN100, enllt->lngdivisorE100, enllt->lngdivisorN100, blueflygpsdata->devno); 
        }
    }
    else if (igps == 2)  // Gpvtg instance
        sdlogger->loggpsvel(mstamp, blueflygpsdata->velkph100, blueflygpsdata->veldegrees100, blueflygpsdata->devno); 
    else if (igps == 4)  // Gpgsa instance
        sdlogger->loggpsprec(mstamp, blueflygpsdata->horizontaldilutionofposition100, blueflygpsdata->verticaldilutionofposition100, blueflygpsdata->devno); 
    else if (igps == 13) {
        if ((blueflygpsdata->Ngpsreading % 50) == 0) {
            P("gps not locked\n"); 
            sdlogger->loggpsprec(mstamp, 0, 0, blueflygpsdata->devno); 
        }
    }
        
    return true; 
}

long lastoledmstamp = 0; 
char prevcalibstatforoled = 0; 
void Flylogger::WriteOledMessage()
{
    if (orientdata && (orientdata->calibstat != prevcalibstatforoled)) {
        prevcalibstatforoled = orientdata->calibstat; 
        SetOleddisplayMode('o', 'o'); 
        return; 
    }
    int secstochange = 8; 
    if (oledmodesettingsrc == 'k')  {
        if (oleddisplaymode == 'f') 
            secstochange = 20;
        else
            secstochange = 60*60;  // 60 minutes
    }
    else if ((oledmodesettingsrc == 'b') || (oledmodesettingsrc == 'c'))
        secstochange = 60; 
    else if (oleddisplaymode == 'h')
        secstochange = 3; 

    long mstamp = millis(); 
    if (mstamp - mstamplastoleddisplaymodesetting > secstochange*1000) {
        SetOleddisplayMode('n', 'a'); 
        if (oled)
            oled->writechar(oleddisplaymode); 
    }
    
    // limit the refresh
    if (mstamp < lastoledmstamp + 200)
        return; 
    
    if (sdlogger && (oleddisplaymode == 'f')) {
        oled->writefilestatus(sdlogger->logfilename, mstamp, sdlogger->Nloglines, sdlogger->Nloglinesfailed); 
    }

    if (blueflygpsdata && (oleddisplaymode == 'g')) {
        if (false) {  // output gps velocity
            int wspeed = (wr == 0 || (wr->revtime == 0) ? 0 : 80000/max(1, wr->revtime)); 
            //oled->write_xy(gpsdata->velkph100*36/10, (gpsdata->veldegrees100)/100, wspeed); 
        } else if (enllt->IsValid()) {
            blueflygpsdata->ApplyLocalEuclideanTransform(*enllt);  
            oled->write_gps(blueflygpsdata->relE10/10, blueflygpsdata->relN10/10, blueflygpsdata->altitude10/10, (((mstamp/2000) % 2) == 0 ? blueflygpsdata->isotimestamp : 0)); 
        } else {
            P(blueflygpsdata->currentstate); 
            P(" "); 
            P(Digps); 
            P(" "); 
            P("gps trans still bad\n"); 
        }
    }
    
    if (wr && (oleddisplaymode == 'w'))
        oled->writewind(wr->revtime); 

    if (orientdata && (oleddisplaymode == 'o')) {
        if (orientdatagoodreading) {
            if (orientdata->calibstat != 0xFF) {
                oled->writeorient('c', (orientdata->calibstat>>6) & 3, (orientdata->calibstat>>4) & 3, (orientdata->calibstat>>2) & 3, orientdata->calibstat & 3, orientdata->currentstate); 
            } else {
                oled->writeorient('o', orientdata->qw(), orientdata->qx(), orientdata->qy(), orientdata->qz(), orientdata->currentstate); 
            }
           //orientdata->ReadEuler(); 
           //oled->writeorient('e', 0, orientdata->eulerx/16, orientdata->eulery/16, orientdata->eulerz/16); 
        } else if (timesinceorienggoodreading > 10000) {
            oled->writeorient('b', ((timesinceorienggoodreading/1000) % 60), timesinceorienggoodreading/60000, 0, 0, orientdata->currentstate); 
        }
    }

    if (lightsensor && (oleddisplaymode == 'v')) {
        uint16_t vinvolts = lightsensor->ReadButtonMillivolts(); 
        if (oled && (oleddisplaymode == 'v'))
            oled->writevinvolts('v', vinvolts); 
    }

    if (humiditydata && (oleddisplaymode == 'h'))
        oled->writehumidity('h', humiditydata->rawHumidity, humiditydata->rawTemperature); 
        
    if (oleddisplaymode == 'b') {
        if (blueflygpsdata) 
            oled->writebaroF((mstampbarogoodreadingF > lastoledmstamp ? blueflygpsdata->prsreading : -1), blueflygpsdata->prsreading - prsreadingsmoothed, blueflygpsdata->currentstate); 
    }
    
    if (oleddisplaymode == 'l')
        oled->writelightlevel(prevlightlevel); 
    lastoledmstamp = mstamp; 
}


// The multiple different panels
struct ScrollGraphParams
{ 
    const char* gtitle; 
    float fyGridLines;   // 10^-yValPrec
    float yValFac;      // factor to convert floats to ints in storage
    int8_t yValPrec; 
    
    int32_t yValOffset; // offset to prevent overflow in short int conversion
    int32_t yValmsstep; 
};
ScrollGraphParams scrollgraphparams[] = { 
  { "entropy",  0.01, 0.001, 2, 25000,  500 },
  { "baroB",  100.0,  1,    -2, 100000, 200 },  
  { "irtemp",   0.1,  0.01,  1, 0,      200 },
  { "htemp",    0.1,  0.01,  1, 0,      200 },
  { "hwind",    0.1,  0.01,  1, 0,      200 },
  { "humid",    1.0,  0.01,  0, 0,      200 },
  { "wspeed",   0.1,  0.01,  1, 0,      200 },
  { "accz",     0.1,  0.01,  1, 0,      200 },
  { "barodiff", 1.0,  0.01,   1, 0,     200 }
};   

float getFlystatusval(Flystatus& flystatus, int v)
{
    if (v-- == 0)  return flystatus.getairentropy(); 
    if (v-- == 0)  return flystatus.baroB; 
    if (v-- == 0)  return flystatus.degcI; 
    if (v-- == 0)  return flystatus.degcG; 
    if (v-- == 0)  return flystatus.wspeedH; 
    if (v-- == 0)  return flystatus.humidS; 
    if (v-- == 0)  return flystatus.wspeed; 
    if (v-- == 0)  return flystatus.Faz.filtval(); 
    if (v-- == 0)  return flystatus.dbaroX; 
    return 0; 
}    
#define LENScrollGraphParams 9

    /* float degcI;  // It[ms]r[raw-ir]a[tempamb] IR-sensor
    float degcIA; // Ambient temp from IR sensor
    float degcS;  // St[ms]r[rawhumid]a[rawtemp] Humidity31 meter
    float degcG;  // Gt[ms]r[rawhumid]a[rawtemp] si7021Humidity meter
    float humidS; 
    float humidG; 
    float baroB; 
    float baroF; 
    float wspeed; 
    float dbaroX; 
    float degcX; 
    float lightI; 
    float dustI; 
    */
    
long lastwideoledmstamp = 0; 
int prevwopanel = 0; 
void Flylogger::WriteWideOled()
{
    long mstamp = millis(); 
    if (mstamp < lastwideoledmstamp + 100)
        return; 
    lastwideoledmstamp = mstamp; 
    if (!flystatus)
        return; 
        
    if (sdlogger) {
        flystatus->Nloglines = sdlogger->Nloglines; 
        flystatus->Nloglinesfailed = sdlogger->Nloglinesfailed; 
        flystatus->Nloglinesfromserialbuff = sdlogger->Nloglinesfromserialbuff; 
        flystatus->logfilenameTail = (sdlogger->logfilename ? sdlogger->logfilename+sdlogger->nlogfileslash+1 : 0); 
    }

    // determin which panel we want by the modulus of button press count
    wideoled->Ddim(flystatus->lightI < 0.3); 
    int wopanel = (lightsensor ? lightsensor->nbuttonpresses : 0) % (LENScrollGraphParams+2); 
    if (wopanel == 0)
        wideoled->writeflystatus(*flystatus, mstamp); 
    else if (wopanel == 1)
        wideoled->plotflyorientation(*flystatus); 
    else if (wopanel >= 2)
    {   
        if (wopanel != prevwopanel) {
            ScrollGraphParams& sgp = scrollgraphparams[wopanel-2]; 
            wideoled->setupvaluescrollgraph(sgp.gtitle, sgp.fyGridLines, sgp.yValFac, sgp.yValPrec, sgp.yValOffset, sgp.yValmsstep); 
        }
        if (wideoled->addscrollgraph(getFlystatusval(*flystatus, wopanel-2), mstamp)) {
            wideoled->plotscrollgraph((wopanel-2) == 0, (flystatus->replayfilesize != 0 ? (flystatus->replayfilebytes*100/flystatus->replayfilesize) : 0));
        }
    }
    prevwopanel = wopanel; 
}


void Flylogger::ReceivedSerialInputString(char* rec, char src)
{
    P("Received from src="); P(src); P(": "); 
    P(rec); P("."); P(strlen(rec)); 
    P(".\n"); // sometimes this 

    // filter out BLE connection and disconnection messages (applied to the logger only)
    if ((src == 'b') && (rec[0] == '*')) 
        return; 

    // incoming from the jeenode
    if (src == 'j') {
        if ((rec[0] == '$') || (oleddisplaymode == 'j')) {
            if (oled)
                oled->write_oled(rec);  
        }
        return; 
    }

 
    // write the received 
    if (oled)
        oled->write_oled(rec);  
    SetOleddisplayMode(rec[0], src); 
    
    char* blebuffer = rec; // (save code changes for now)
    if (blebuffer[0] == 't') {
        int i = blebuffer[1] - '0'; 
        if ((i < 0) || (i >= 3)) 
            i = 0; 
        tempreadings[0] = 0; 
    } else if (irthermometer && (blebuffer[0] == 'i')) {
        Serial.println("reading IR"); 
        uint16_t irt16 = irthermometer->ReadIRtemp16(0x07); 
        if (irt16 != 0xFFFF) {
            long irt = (long)irt16*2 - 27315;
            Serial.println(irt); 
        }
    } else if (blebuffer[0] == 'd') {
        simpledatetime->SetfromEpoch(Teensy3Clock.get()); 
        Serial.println(simpledatetime->pisotimestamp); 
        Serial.println(Teensy3Clock.get()); 
    }   
}


void Flylogger::FetchConsoleSerial()
{
    if (consoleserial->RecCheck()) 
        ReceivedSerialInputString(consoleserial->buff, 'c'); 
}


int previcyc = 0; 
void Flylogger::LoopWhole()
{
    if (consoleserial)  FetchConsoleSerial(); 
    if (blueflygpsdata) FetchGPS(); 
    if (lightsensor)    FetchLight(); 
    if (hotwireanemometer) FetchHotWireWind(); 
    if (dst)            FetchDallas(); 
    if (irthermometer)  FetchIRtemp(200, 30);
    if (humiditydata)   FetchHumidity(); 
    if (si7021humidity) Fetch7021Humidity(); 
    if (sht31humidity)  Fetch31Humidity(); 
    if (tbarometer)     FetchTBarometer(); 
    if (ms5611baro)     FetchMS5611baro(); 
    if (orientdata)     FetchOrientation(10, 5); 
    if (wr)             FetchWind(); 
    if (px4pitot || wr) FetchPX4pitot(); 
    if (oled)           WriteOledMessage(); 
    if (wideoled)       WriteWideOled(); 
    if (sdlogger)       sdlogger->checkfornextflush(); 
}


