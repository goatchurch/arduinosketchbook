// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef FLYLOGGER_h
#define FLYLOGGER_h

class DallasTemperature; 
class EastingNorthingLocalLinearTrans; 
class BlueflyGpsData; 
class WindReceiver; 
class SimpleDatetime; 
class LightSensor; 
class LightSensorGestures; 

class BNO055orientation; 
class IRthermometer; 
class HumidityData; 
class SI7021humidity; 
class SHT31humidity; 
class T5403Barometer; 
class MS5611; 
class PX4pitot; 
class HotWireAnemometer; 
class OledOutput; 
class WideOledOutput;
class FlyStatus; 

class Flylogger
{
public:
    int devicenumber; // 0 is first device, 1 is stripped down second device

    char oleddisplaymode; 
    char oleddisplaymodePrior; 
    long mstampprevoledout; 
    
    OledOutput* oled;   // this device is no more, but it's too much hassle to remove it from the codebase
    WideOledOutput* wideoled; 

    // SDlogger is normal, but can optionally be a SerialStreamLogger to reuse the same functions
    SDloggerBase* sdlogger;  
    SDloggerBase* sdreplaylogger;   // this allows for replay of an old log file to test the display interface (graphs etc)
    
    DallasTemperature*  dst; 
    EastingNorthingLocalLinearTrans* enllt; 
    BlueflyGpsData*     blueflygpsdata;  
    WindReceiver*       wr; 
    SimpleDatetime*     simpledatetime; 
    LightSensor*        lightsensor; 
    LightSensorGestures* lightsensorgestures; 
    HotWireAnemometer*  hotwireanemometer; 
    
    BNO055orientation*  orientdata;    // serial1

    // I2C devices
    IRthermometer*      irthermometer;   // address 0x5A         * (to install)
    HumidityData*       humiditydata;    // address 0x40 HTU21  
    SI7021humidity*     si7021humidity;  // address 0x40         *
    SHT31humidity*      sht31humidity;   // address 0x44         *
    T5403Barometer*     tbarometer;      // address 0x77
    MS5611*             ms5611baro;      // address 0x77         *
    PX4pitot*           px4pitot;        // address 0x28         *

    Flystatus*          flystatus; 
    SerialInputBuffer* consoleserial; 

    Flylogger(int ldevicenumber); 
    void SetupWhole(); 
    
    void ReceivedSerialInputString(char* rec, char src);  // src is [b]luetooth, [c]onsole, [j]eenode
    void FetchConsoleSerial(); 

    bool FetchGPS(); 
    void FetchWind(); 
    int  FetchLight(); 
    void FetchHotWireWind(); 
    void FetchDallas(); 
    void FetchIRtemp(int mstampdivider, int mstampoffset); 
    void FetchHumidity(); 
    void Fetch7021Humidity(); 
    void Fetch31Humidity(); 
    void FetchTBarometer(); 
    void FetchMS5611baro(); 
    void FetchPX4pitot(); 
    bool FetchOrientation(int mstampdivider, int mstampoffset); 
    void WriteOledMessage(); 
    void WriteWideOled(); 
    
    void SetOleddisplayMode(char loleddisplaymode, char src); 

    void LoopWhole(); 
};

#endif

