// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This work is free, see COPYING.WTFPL for more details.

#ifndef FLYSTATUS_h
#define FLYSTATUS_h


class ABFilter
{
    const float* a; 
    const float* b; 
    const int n; 
    float* xybuff; 
    int xybuffpos; 
    
    float timestep; 
    float t0; 
    int nt0bytimestep; 
    float prevt; 
    float prevx; 
    float sumX; 
    void firstvalue(float t, float x); 
    bool addvalueT(float t, float x); 
    void addvalueF(float x); 
    
public:
    ABFilter(const float* la, const float* lb, int ln, float ltimestep); 
    ABFilter(float b0, float ltimestep); 
    ABFilter();  
    void addvalue(float t, float x); 
    float filtval(); 
    float difffiltval(); 
    
    static float expdecayfac(float dt, float decaypersec); 
};


#define NRECTYPES 19
uint16_t hexval(uint8_t c); 
uint16_t hexread16(const char* buff); 
uint32_t hexread24(const char* buff); 
uint32_t hexread32(const char* buff); 

// find the delay curves on each of the temps and apply filters
// get max window accelerations and bank angles
// (having built the back of base bar orientation mount)

class Flystatus
{
public:
    Flystatus(); 
    float degcI;  // It[ms]r[raw-ir]a[tempamb] IR-sensor
    float degcIA; // Ambient temp from IR sensor
    float degcS;  // St[ms]r[rawhumid]a[rawtemp] Humidity31 meter
    float degcG;  // Gt[ms]r[rawhumid]a[rawtemp] si7021Humidity meter
    float humidS; 
    float humidG; 
    float baroB; 
    float baroF; 
    float wspeed; 
    float dbaroX; 
    float wspeedX; // approx derived from dbaroX
    float degcX; 
    float lightI; 
    float wspeedH; // hot wire anemometer windspeed (possible)
    long tval;  // mstamp in millis
    long dtval; // difference from previous mstamp

    long replayfilesize; 
    long replayfilebytes; 
    
    // used for calculating entropy
    ABFilter baroEntFiltTemp; 
    ABFilter baroEntFiltBaro; 
    float getairentropy(); 
    
    // accelerometer data
    float ax, ay, az; 
    float gx, gy, gz; 
    Quat quat; 
    uint16_t calibstat; // [Sys,Gyro,Acc,Mag]
    ABFilter Faz; 

    ABFilter baroFiltDiff; 
    float getaltodiff(); 

    // Quat quatslow;  // would like to do a slerp interpolation/filter following of the quat above, but too complicated
    float slowsinattack; 
    float slowsinroll; 

    // GPS readings
    int32_t Gms; 
    char Gisodate[23]; 
    float Gx, Gy, Galt; 
    float Gvmps, Gvdeg; 
    long mstampNoLock; 
    float relE, relN; 
    EastingNorthingLocalLinearTrans enllt; 

    float relE2, relN2; // second GPS position reading

    // code pulled over from SensorSerialReceiver (M-record lists the numbers of records of each time since previous M-record)
    static const char* rectypes; // "ZYQVPFDWHISGNOBXMUL"
    int rectypecounts[NRECTYPES+1]; 
    long prevmtime; 
    long prevnMcount; 

    long minserialtimestampoffset; 
    long serialtimestampoffset; 
    long serialtimestampoffsetcount; 

    // these are copied over from SDlogger repeatedly
    int Nloglines, Nloglinesfromserialbuff, Nloglinesfailed; 
    char* logfilenameTail; 
    
    bool processrec(const char* buff, int8_t bufflen); 
};


#endif
