// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#ifndef BNO055_h
#define BNO055_h

// current device has rj4 connector
// socket facing down, arrangement is
// Short:RX Long:TX Short:Ground Long:+ 

// Pins on the RJ4 connector, top side with hole facing down
// short  Long  short  Long
//  RX     TX    G      +


class BNO055orientation
{
public:
    HardwareSerial* serial; 

    uint8_t serringbuff[64]; 
    uint8_t serringbuffPtr; 
    int serringbuffcount; 
    void checkclearserbuff();   
    void readserringbuff();   
    uint8_t checkEErecord(); 
    bool checkBBrecord(uint8_t length, uint8_t* chdata);  

    uint8_t serbuff[60]; 
    uint8_t nserbuff; 
    
    uint8_t calibvalues[22]; 
    long mstampRgap; 
    long mstampRsendgap; 
    int DDmaxloop; // delay loop count while waiting for a valid response
    
    BNO055orientation(); 
    void SetupSerial(HardwareSerial* lserial); 
    uint8_t currentstate; // see FetchOrientation
    int remainingcalibrationreadingattempts; 
    
    bool readdatastack(uint8_t reg, uint8_t length, uint8_t* chdata);  // error code goes into chdata[0]
    bool writedatastack(uint8_t reg, uint8_t length, uint8_t* chdata); // errror code goes into chdata[0]

    bool CheckWorkingChipID(bool bprintrevision); 
    bool writedata8(uint8_t reg, uint8_t value); 
    uint8_t writedata8EE(uint8_t reg, uint8_t value, long timeoutms); 
    uint8_t readdatastackBB(uint8_t reg, uint8_t length, uint8_t* chdata, int reqQtimeout, int semdmstimeout);  
     
    // status so we know if it's changed
    uint8_t calibstat, prevcalibstat;  
    
    long sendmstamp;   // non-zero gives the time we sent the data, so can use as a timeout
    long recQmstamp;   // time we received this quat data
    uint8_t ReadQLGTC();  // takes 50microseconds if reading is ready, otherwise it can pause before sending a response.  if the delay is too short you can get a duplicate reading
    bool FetchOrientation(); // keeps doing all the inits stuff when necessary

    void SetCalibrations(); 

    int16_t qw()  { return ((int16_t)serbuff[0]) | (((int16_t)serbuff[1]) << 8); };
    int16_t qx()  { return ((int16_t)serbuff[2]) | (((int16_t)serbuff[3]) << 8); };
    int16_t qy()  { return ((int16_t)serbuff[4]) | (((int16_t)serbuff[5]) << 8); };
    int16_t qz()  { return ((int16_t)serbuff[6]) | (((int16_t)serbuff[7]) << 8); };

    int16_t linaccx()  { return ((int16_t)serbuff[8]) | (((int16_t)serbuff[9]) << 8); };
    int16_t linaccy()  { return ((int16_t)serbuff[10]) | (((int16_t)serbuff[11]) << 8); };
    int16_t linaccz()  { return ((int16_t)serbuff[12]) | (((int16_t)serbuff[13]) << 8); };

    int16_t gravvecx()  { return ((int16_t)serbuff[14]) | (((int16_t)serbuff[15]) << 8); };
    int16_t gravvecy()  { return ((int16_t)serbuff[16]) | (((int16_t)serbuff[17]) << 8); };
    int16_t gravvecz()  { return ((int16_t)serbuff[18]) | (((int16_t)serbuff[19]) << 8); };
};

/*
void BNO055orientation::SetCalibrations()
{
    accoffsetx = 494; accoffsety = 2030; accoffsetz = -10354;
    magoffsetx = -11237; magoffsety = 4300; magoffsetz = 9;
    gyroffsetx = -22; gyroffsety = 6; gyroffsetz = 7;
    accradius = -660; magradius = -725;
}

    // calibration values
    int16_t accoffsetx, accoffsety, accoffsetz; 
    int16_t magoffsetx, magoffsety, magoffsetz; 
    int16_t gyroffsetx, gyroffsety, gyroffsetz; 
    int16_t accradius, magradius; 

    // the calibration decoding (if necessary)
    accoffsetx = ((int16_t)calibvalues[0]) | (((int16_t)calibvalues[1]) << 8);
    accoffsety = ((int16_t)calibvalues[2]) | (((int16_t)calibvalues[3]) << 8);
    accoffsetz = ((int16_t)calibvalues[4]) | (((int16_t)calibvalues[5]) << 8);

    magoffsetx = ((int16_t)calibvalues[6]) | (((int16_t)calibvalues[7]) << 8);
    magoffsety = ((int16_t)calibvalues[8]) | (((int16_t)calibvalues[9]) << 8);
    magoffsetz = ((int16_t)calibvalues[10]) | (((int16_t)calibvalues[11]) << 8);

    gyroffsetx = ((int16_t)calibvalues[12]) | (((int16_t)calibvalues[13]) << 8);
    gyroffsety = ((int16_t)calibvalues[14]) | (((int16_t)calibvalues[15]) << 8);
    gyroffsetz = ((int16_t)calibvalues[16]) | (((int16_t)calibvalues[17]) << 8);

    accradius = ((int16_t)calibvalues[18]) | (((int16_t)calibvalues[19]) << 8);
    magradius = ((int16_t)calibvalues[20]) | (((int16_t)calibvalues[21]) << 8);
*/


#endif
