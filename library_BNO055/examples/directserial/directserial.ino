#include <Wire.h>
#include "BNO055.h"

// https://learn.adafruit.com/bno055-absolute-orientation-sensor-with-raspberry-pi-and-beaglebone-black/hardware

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)

BNO055orientation orientation; 
bool bSerialWire = true; 

void dwrite8(uint8_t reg, uint8_t value)
{
   Serial3.write(0xAA); 
   Serial3.write(0x00); 
   Serial3.write(reg); 
   Serial3.write(0x01); 
   Serial3.write(value); 

    while (Serial3.available()) {
        PH(Serial3.read()); 
        P(' '); 
    }
    P('\n'); 
}


void setup()
{
   Serial.begin(115200); 
   delay(1500); 
   P("setupserial\n"); 
   orientation.SetupSerial(&Serial3); 
   P("writing\n"); 

    dwrite8(0X3E, 0X00);  // normal power mode
    delay(10);

    dwrite8(0X3B, 0x00);  // units UDegrees and m/s^2

    dwrite8(0X3F, 0x80); 
    delay(10);

    dwrite8(0X3D, 0X0C);  // back to NDOF mode
    delay(50);

    dwrite8(0X3D, 0X0C);  // back to NDOF mode
    delay(50);
}


void loop()
{
   delay(100); 
   uint32_t mstamp = micros(); 
   Serial3.write(0xAA); 
   Serial3.write(0x01); 
   Serial3.write(0x1A); 
   Serial3.write(0x06); 

   while (!Serial3.available())
     ; 
   while (Serial3.available()) {
        PH(Serial3.read()); 
        P(' '); 
    }
    P(" .\n"); 
}


