#include <Wire.h>
#include "BNO055.h"

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)

BNO055orientation orientation; 


void setup() 
{
    Serial.begin(9600);
    P("begin\n"); 
    delay(1500); 
    P("setupserial\n"); 
    orientation.SetupSerial(&Serial2); 
    delay(1500); 
}



int Nloop = 0; 
int Nnonorientcount = 0; 
int Ncalibcount = 0; 
bool bstream = false; 
int prevcurrentstate = -1; 
int adctmpmstampdivided = 0; 
int mstampdivider = 10; // should be 10

void loop() 
{

    // we synchronize the timings of the readings.  Sometimes with the 9ms cycle 
    // delay it gets held back by up to 0.6ms when a reading is immiment, but if not you get 
    // a duplicate reading.
/*    long mstamp = millis(); 
    int ladctmpmstampdivided = (mstamp+5)/mstampdivider; 
    if (ladctmpmstampdivided == adctmpmstampdivided)
        return; 
    adctmpmstampdivided = ladctmpmstampdivided; 
*/  
    if (orientation.FetchOrientation()) {
        //P(mstamp); 
        if ((Nloop % 50) == 0) {
          P(Nloop); 
          P(" "); 
          P((orientation.calibstat>>6)&3); 
          P((orientation.calibstat>>4)&3); 
          P((orientation.calibstat>>2)&3); 
          P((orientation.calibstat>>6)&3); 
          P(" Q "); 
          P(orientation.qw()); 
          P(" "); 
          P(orientation.qx()); 
          P(" "); 
          P(orientation.qy()); 
          P(" "); 
          P(orientation.qz()); 
          P("  d "); 
          P(orientation.mstampRgap); 
          P("  r "); 
          P(orientation.mstampRsendgap); 
          P("\n"); 
        }        
        if ((orientation.calibstat != orientation.prevcalibstat) && (Ncalibcount < 20)) {
            orientation.currentstate = 40; 
            Ncalibcount++; 
            P("call read calib "); 
            P(Ncalibcount); 
            P("\n"); 
        }
        
        Nloop++; 
        if (Nloop == 500) 
            orientation.currentstate = 40; // read calibration
        if (Nloop == 505) 
            orientation.currentstate = 60; // write calibration
        if (Nloop == 510) 
            orientation.currentstate = 40; // read calibration
        if (Nloop == 600) 
            orientation.currentstate = 51; // reset
    } else {
        Nnonorientcount++; 
        if (orientation.currentstate != prevcurrentstate) {
            P(Nloop); 
            P(" currentstate="); 
            P(orientation.currentstate); 
            P("\n"); 
            prevcurrentstate = orientation.currentstate; 
        }
    }

/*
    if ((Nloop < 200) || (orientation.currentstate != 30))
        delay(10); 
    else
        delay(2); 
*/
}



