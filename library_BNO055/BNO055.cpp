// Copyright 2015 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <Wire.h>
#include "BNO055.h"

#define P(X) Serial.print(X)
#define PH(X) Serial.print(X, HEX)

BNO055orientation::BNO055orientation() 
{
    serial = 0; 
    
    for (int i = 9; i < 64; i++)
        serringbuff[i] = 0; 
    serringbuffPtr = 0; 
    serringbuffcount = 0; 
    sendmstamp = 0; 
    recQmstamp = 0;
    
    DDmaxloop = 2250; 
    
    //char* defaultcalib = "ECFF00001A00590062002A00000000000000E803D002"; 
    const char* defaultcalib = "010001000100DB00CC00E200FEFFFFFF0200E8033802"; 
    for (int i = 0; i < 22; i++)
        calibvalues[i] = (defaultcalib[i*2] <= '9' ? defaultcalib[i*2] - '0' : defaultcalib[i*2]-'A'+10)*0x10 + (defaultcalib[i*2+1] <= '9' ? defaultcalib[i*2+1] - '0' : defaultcalib[i*2+1]-'A'+10); 
}

// 2016-05-14 050006000200000000000000FFFFFEFF0100E8030000
// 2015-05-03 010001000100DB00CC00E200FEFFFFFF0200E8033802

void BNO055orientation::SetupSerial(HardwareSerial* lserial) 
{
    P("Setting up Serial for BNO055\n"); 
    serial = lserial; 
    serial->begin(115200, SERIAL_8N1); 
    calibstat = 0x00; 
    currentstate = 0; 
    remainingcalibrationreadingattempts = 0; 

    for (int i = 9; i < 64; i++)
        serringbuff[i] = 0; 
    serringbuffPtr = 0; 
}

void BNO055orientation::readserringbuff()  
{
    while (serial->available() != 0) {
        serringbuff[serringbuffPtr] = serial->read(); 
        serringbuffPtr++; 
        if (serringbuffPtr == 64)
            serringbuffPtr = 0; 
        serringbuffcount++; 
    }
}

void BNO055orientation::checkclearserbuff()  
{
    uint8_t n = 0; 
    while (serial->available() != 0) {
        serial->read(); 
        n++; 
    }
    if (n != 0) {
        P("cleared serbuff "); 
        P(n); 
        P("\n");
    }
    serringbuffcount = 0; 
}

// 0x01: WRITE_SUCCESS
// 0x02: READ_FAIL
// 0x03: WRITE_FAIL
// 0x04: REGMAP_INVALID_ADDRESS
// 0x05: REGMAP_WRITE_DISABLED
// 0x06: WRONG_START_BYTE
// 0x07: BUS_OVER_RUN_ERROR
// 0X08: MAX_LENGTH_ERROR
// 0x09: MIN_LENGTH_ERROR
// 0x0A: RECEIVE_CHARACTER_TIMEOUT
uint8_t BNO055orientation::checkEErecord()  
{
    uint8_t serringbuffPtrm2 = (serringbuffPtr + 62) % 64; 
    if (serringbuffcount < 2)
        return 0x00; 
    if (serringbuff[serringbuffPtrm2] != 0xEE)
        return 0x00; 
    serringbuff[serringbuffPtrm2] = 0; // kill the record
    uint8_t serringbuffPtrm1 = (serringbuffPtr + 63) % 64; 
    return serringbuff[serringbuffPtrm1]; 
}

bool BNO055orientation::checkBBrecord(uint8_t length, uint8_t* chdata) 
{
    if (serringbuffcount < length + 2)
        return false; 
    uint8_t serringbuffPtrm2 = (serringbuffPtr + 62 - length) % 64; 
    if (serringbuff[serringbuffPtrm2] != 0xBB)
        return false; 
    uint8_t serringbuffPtrm1 = (serringbuffPtrm2 + 1) % 64; 
    if (serringbuff[serringbuffPtrm1] != length) {
        P("length mismatch\n"); 
        return false; 
    }
    
    // copy bytes out
    for (int i = 0; i < length; i++) {
        uint8_t serringbuffPtrm0 = (serringbuffPtrm2 + 2 + i) % 64; 
        chdata[i] = serringbuff[serringbuffPtrm0]; 
    }
    serringbuff[serringbuffPtrm2] = 0; // kill the record
    return true; 
}


// on fail/false error code put into chdata[0]
// 0x02: READ_FAIL
// 0x04: REGMAP_INVALID_ADDRESS
// 0x05: REGMAP_WRITE_DISABLED
// 0x06: WRONG_START_BYTE
// 0x07: BUS_OVER_RUN_ERROR
// 0X08: MAX_LENGTH_ERROR
// 0x09: MIN_LENGTH_ERROR
// 0x0A: RECEIVE_CHARACTER_TIMEOUT

// 0xFF: response wait timeout
// 0xFE: length incorrect
// 0xFD: incorrect first byte
bool BNO055orientation::readdatastack(uint8_t reg, uint8_t length, uint8_t* chdata) 
{
    // clear the buffer before we write
    while (serial->available()) 
        serial->read(); 

    serial->write(0xAA); 
    serial->write(0x01); 
    serial->write(reg); 
    serial->write(length); 
    //serial->flush(); 

    long Dmaxloop = DDmaxloop+1000000L; 
    Dmaxloop = 100000L; 
    uint8_t ch = 0; 

    // find start of record
    for ( ; Dmaxloop != 0; Dmaxloop--) {
        if (serial->available()) {
            ch = serial->read(); 
            if ((ch == 0xBB) || (ch == 0xEE))
                break; 
        }
    }
    if (Dmaxloop == 0) {
        chdata[0] = 0xFF; 
        return false; 
    }
    if (!((ch == 0xBB) || (ch == 0xEE))) {
        chdata[0] = 0xFD; 
        return false; 
    }

    // get length of record
    uint8_t ch2 = 0; 
    for ( ; Dmaxloop != 0; Dmaxloop--) {
        if (serial->available()) {
            ch2 = serial->read(); 
            break; 
        }
    }
    if (ch == 0xEE) {
        chdata[0] = ch2; // error mode
        return false; 
    }
    if (ch2 != length) {
        chdata[0] = 0xFE; 
        if (length > 1)
            chdata[1] = ch2; 
        return false; 
    }
    for (int i = 0; i < ch2; i++) {
        for ( ; Dmaxloop != 0; Dmaxloop--) {
            if (serial->available()) {
                chdata[i] = serial->read(); 
                break; 
            }
        }
    }
    if (Dmaxloop == 0) {
        chdata[0] = 0xFF; 
        return false; 
    }
    return true; 
}

// 0x01: WRITE_SUCCESS
// 0x03: WRITE_FAIL
// 0x04: REGMAP_INVALID_ADDRESS
// 0x05: REGMAP_WRITE_DISABLED
// 0x06: WRONG_START_BYTE
// 0x07: BUS_OVER_RUN_ERROR
// 0X08: MAX_LENGTH_ERROR
// 0x09: MIN_LENGTH_ERROR
// 0x0A: RECEIVE_CHARACTER_TIMEOUT
// 0xFF: Timed out after looping 1250 times 
// 0xFD: Bad return byte
bool BNO055orientation::writedatastack(uint8_t reg, uint8_t length, uint8_t* chdata) 
{
    serial->write((uint8_t)0xAA); 
    serial->write((uint8_t)0x00); 
    serial->write((uint8_t)reg); 
    serial->write((uint8_t)length); 
    for (int i = 0; i < length; i++) {
        serial->write((uint8_t)(chdata[i])); 
    }
    //serial->flush(); 

    int Dmaxloop = DDmaxloop; 
    uint8_t ch = 0; 
    for ( ; Dmaxloop != 0; Dmaxloop--) {
        if (serial->available()) {
            ch = serial->read(); 
            if (ch == 0xEE)
                break; 
        }
        //delayMicroseconds(8); 
    }
    if (Dmaxloop == 0) {
        chdata[0] = 0xFF; 
        return false; 
    }
    if (!(ch == 0xEE)) {
        chdata[0] = 0xFD; 
        return false; 
    }
    uint8_t ch2 = 0; 
    for ( ; Dmaxloop != 0; Dmaxloop--) {
        if (serial->available()) {
            ch2 = serial->read(); 
            break; 
        }
    }
    if (Dmaxloop == 0) {
        chdata[0] = 0xFF; 
        return false; 
    }
    chdata[0] = ch2; 
    return (ch2 == 0x01); 
}

bool BNO055orientation::writedata8(uint8_t reg, uint8_t value) 
{
    uint8_t chdata[1]; 
    chdata[0] = value; 
    if (writedatastack(reg, 1, chdata))
        return true;  
    P("Bad writedata8 "); 
    if (chdata[0] == 0xFF) 
        P("timeout"); 
    else
        PH(chdata[0]); 
    P(" : "); 
    PH(reg); 
    P(" "); 
    PH(value); 
    P("\n"); 
    return false; 
}

uint8_t BNO055orientation::writedata8EE(uint8_t reg, uint8_t value, long timeoutms) 
{
    if (sendmstamp == 0) {
        checkclearserbuff(); 
        serial->write((uint8_t)0xAA); 
        serial->write((uint8_t)0x00); 
        serial->write((uint8_t)reg); 
        if ((reg == 0x55) && (value == 22)) {  // hack in calibration type
            serial->write((uint8_t)22); 
            for (int i = 0; i < 22; i++)
                serial->write((uint8_t)calibvalues[i]); 
        } else {
            serial->write((uint8_t)1); 
            serial->write((uint8_t)(value)); 
        }
        sendmstamp = millis(); 
        P("writing8EE "); 
        PH(reg); 
        P(":="); 
        PH(value); 
        P("\n"); 
    }
    readserringbuff(); 
    uint8_t res = checkEErecord(); 
    if ((res == 0) && (millis() < sendmstamp + timeoutms))
        return 0; 
    sendmstamp = 0; 
    P("writedata8EE got "); PH(res); P("\n"); 
    return (res == 0 ? 0xFF : res); 
}


int Dorienterrorscount = 0; 
bool BNO055orientation::CheckWorkingChipID(bool bprintrevision)
{
    uint8_t chdata[6]; 
    uint8_t ch; 
    while (true) {
        ch = readdatastackBB(0x00, 6, chdata, 10, 10); 
        if ((ch == 0x01) || (ch == 0xFF))
            break; 
    }
    if ((ch == 0x01) && (chdata[0] == 0xA0)) {
        if (bprintrevision) {
            P("CHIP_ID:"); 
            PH(chdata[0]); // must be 0xA0
            P(" ACC_ID:"); 
            PH(chdata[1]); 
            P(" MAG_ID:"); 
            PH(chdata[2]); 
            P(" GYR_ID:"); 
            PH(chdata[3]); 
            P(" SW_REV_ID: "); 
            PH(chdata[4]); 
            P("."); 
            PH(chdata[5]); 
            P("\n"); 
            // CHIP_ID:A0 ACC_ID:FB MAG_ID:32 GYR_ID:F SW_REV_ID: 8.3
        }
        return true; 
    }
    if (bprintrevision) {
        if ((Dorienterrorscount % 50) == 0) {
            P(Dorienterrorscount); 
            P(" OrientCheckWorkingChipIDFail: "); 
            PH(ch); 
            P("\n"); 
        }
        Dorienterrorscount++; 
    }
    return false; 
}


uint8_t configseq[] = { 0x3D, 0x00,     // config mode
                        0x3E, 0x00,     // PWR_MODE, normal
                        0x3B, 0x00,     // UNIT_SEL, celsius, UDegrees and m/s^2
                        // 0x3F, 0x80,   // SYS_TRIGGER, use external oscilator (is this right?) [No, we haven't got one!] 
                        // 0x40, 0x00   // TEMP_SOURCE, accelerometer (gyro=1)
                        0x3D, 0x0C      // back to NDOF mode
                      }; 

                    
// initialization states and running states
// currentstate
//   0      : startup
//   10-14  : setup modes
//   20     : setup done
//   30     : reading mode
//   40     : enter configmode
//   42     : request calibration settings
//   43     : exit configmode
//   51-59  : attempts at resetting
//   60     : enter configmode
//   62     : upload calibration
//   63     : exit configmode 
bool BNO055orientation::FetchOrientation()
{
    if (currentstate == 0) {
        //serial->begin(115200, SERIAL_8N1);  // this stops it working
        if (CheckWorkingChipID(true)) {
            currentstate = 10; 
        }
        return false; 
    }
    
    // initial configuration chain advancing state through the bytes that need to be sent
    if (currentstate < 20) {
        P("currentstate "); 
        P(currentstate); 
        P("\n"); 
        uint8_t ch = writedata8EE(configseq[(currentstate-10)*2], configseq[(currentstate-10)*2+1], 900); 
        if (ch == 0x01) {
            currentstate++; 
            if (currentstate == 14)  // 10+len(configseq)
                currentstate = 20; 
        }
        return false; 
    }
    
    if (currentstate == 20) {
        uint8_t chdata[1]; 
        uint8_t ch = readdatastackBB(0x34, 1, chdata, 20, 50); 
        if (ch == 0x01) {
            P("BNO055 temperature is: "); 
            P(chdata[0]); 
            P("\n"); 
            currentstate = 30; 
        }
        return false; 
    }
    
    //////////////////////
    // actual reading mode
    if (currentstate == 30) {
        uint8_t ch = ReadQLGTC(); 
        if (ch == 0x01)
            return true;  // the one true reading that says what we want (the orientation reading) is ready
        return false; 
    }
    
    // calibration reading sequence
    if ((currentstate == 40) || (currentstate == 60)) {
        uint8_t ch = writedata8EE(0x3D, 0x00, 900); // enter config mode
        if (ch == 0x01) {
            currentstate += 2; 
            remainingcalibrationreadingattempts = 5; 
        }
        return false; 
    }
    if ((currentstate == 42) || (currentstate == 62)) {
        if (currentstate == 42) {
            uint8_t ch = readdatastackBB(0x55, 22, calibvalues, 20, 250); 
            if (ch == 0x01) {
                P("calibration read "); 
                for (int i = 0; i < 22; i++) {
                    PH(calibvalues[i]); 
                    P("."); 
                }
                currentstate += 1; 
            }
            return false; 
        }
        if (currentstate == 62) {
            uint8_t ch = writedata8EE(0x55, 22, 900);     // special calibvalues values case (22 bytes) 
            if (ch == 0x01) {
                currentstate += 1; 
                return false; 
            }
        }
        
        P(remainingcalibrationreadingattempts); 
        P(currentstate == 42 ? " ncalibreadingerrs:" :  " ncalibwritingerrs:"); 
        P(calibvalues[0]); 
        P(" "); 
        if (calibvalues[0] == 0xFE) { 
            P(calibvalues[1]); 
            P(" "); 
        }
        remainingcalibrationreadingattempts--; 
        if (remainingcalibrationreadingattempts == 0) {
            P(" giving up on calibration work"); 
            currentstate += 1; 
        }
        return false; 
    }
    
    if ((currentstate == 43) || (currentstate == 63)) {
        uint8_t ch = writedata8EE(0x3D, 0x0C, 900); // leave config mode
        if (ch == 0x01)
            currentstate = 20; // back to temperature state
        return false; 
    }




    if ((currentstate >= 51) && (currentstate <= 59)) {
        uint8_t ch = writedata8EE(0x3F, 0x20, 900); // RST_SYS 
        if (ch == 0x01)  
            currentstate = 0;        // startup
        else if (currentstate < 59)
            currentstate++; 
        else
            currentstate = 0;        // goto startup anyway
        return false; 
    }
    
    return false; 
}


uint8_t BNO055orientation::readdatastackBB(uint8_t reg, uint8_t length, uint8_t* chdata, int reqQtimeout, int semdmstimeout) 
{
    long mstamp = millis(); 
    if (sendmstamp == 0) {
        if (mstamp < recQmstamp + reqQtimeout)
            return false; 
        checkclearserbuff(); 
        serial->write((uint8_t)0xAA); 
        serial->write((uint8_t)0x01); 
        serial->write((uint8_t)reg); 
        serial->write((uint8_t)length); 
        sendmstamp = mstamp; 
    }
    
    readserringbuff(); 
    uint8_t res = checkEErecord(); 
    if (res != 0x00) {
        sendmstamp = 0; 
        return res; 
    }
    if (checkBBrecord(length, chdata)) {
        mstampRgap = mstamp - recQmstamp; 
        mstampRsendgap = mstamp - sendmstamp; 
        sendmstamp = 0; 
        recQmstamp = mstamp; 
        prevcalibstat = calibstat; 
        calibstat = serbuff[21]; 
        return 0x01; 
    }
    if (millis() < sendmstamp + semdmstimeout) {
        return 0x00; 
    }
    sendmstamp = 0; 
    return 0xFF; 
}

uint8_t BNO055orientation::ReadQLGTC() 
{
    uint8_t ch = readdatastackBB(0x20, 22, serbuff, 7, 11);  
    if (ch == 0x01) {
        prevcalibstat = calibstat; 
        calibstat = serbuff[21]; 
    }
    if (ch == 0xFF)
        P("ReadQLGTC timeout\n"); 
    return ch; 
}

