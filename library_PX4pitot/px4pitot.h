
#ifndef PX4PITOT_h
#define PX4PITOT_h

// device is 45350; 5AI=5v A-type 10%-90% output I2Caddress=0x28; 001D=1psi pressure range Differential

class PX4pitot
{
public:
    uint8_t status; // 00 good, 10 stale data, 11 Fault detected (must to repower)
    uint16_t rawpress; 
    uint16_t rawtemp; 
    
    void setreadpitot(); 
    bool freadpitot();  
    int getrawscaled(); 

    float getdiffpressure(); 
    float getairspeed(float airdensity=1.2); 
    float gettemperature(); 
}; 


#endif
