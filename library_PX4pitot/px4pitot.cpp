// Copyright 2016 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"
#include <Wire.h>
#include "px4pitot.h"
#include <math.h>

void PX4pitot::setreadpitot() 
{
    Wire.beginTransmission(0x28);
    Wire.endTransmission();
}

bool PX4pitot::freadpitot() 
{
    Wire.requestFrom(0x28, 4);
    uint16_t pmsb = Wire.read();   // this first read take 0.5ms
    status = pmsb & 0xC0; 
    if (status == 0xC0)
        return false;   // fault

    // the later 3 readings take 0ms
    uint16_t plsb = Wire.read();
    rawpress = ((pmsb & 0x3F) << 8) | (plsb); 
    
    uint16_t tmsb = Wire.read();
    uint16_t tlsb = Wire.read();
    rawtemp = (tmsb << 3) | (tlsb >> 5); 
    
    return (status == 0); 
}
// the actual readings are scaled so that only 3n and 3n+1 readings are represented
int PX4pitot::getrawscaled()
{
    return (rawpress/3)*2 + (rawpress%3); 
}

float PX4pitot::getdiffpressure() 
{
    //return (((double)rawpress-819.15)/(14744.7)) - 0.49060678;  [original in code]
    // Along(InvAlong(rawpress, 0.1*0x3FFF, 0.9*0x3FFF), -1, 1)
    //return (m - 0.1*0x3FFF)/(0.8*0x3FFF)*2 - 1 
    float psi = ((float)rawpress)*(1.0/(0x3FFF*0.4)) - 1.25;  
    return psi*6894.75728;     // into pascals
}

float PX4pitot::getairspeed(float airdensity) 
{
    return sqrt(abs(2*getdiffpressure())/airdensity);
}

float PX4pitot::gettemperature()
{
    // Along(InvAlong(rawtemp, 0, 0x7FF), -50, 150)
    //return (double)rawtemp*(150 - -50)/0x7ff + -50; 
    return ((float)rawtemp)*(200.0/0x7FF) - 50; 
}


