
#include <Wire.h>
#include "px4pitot.h"

// how long does a reading take?
// how frequently can we read?
// set this whole thing up into a atmospheric sniffer
// including a barometer device (maybe all with own teensy)

PX4pitot px4pitot; 

#define P(X) Serial.print(X)

void setup(void)
{
    Serial.begin(9600);
    Wire.begin();
    delay(500);
}


int32_t singlereading()
{
    uint32_t t0 = micros(); 
    Wire.beginTransmission(0x28);
    Wire.endTransmission();

    // needs a delay of at least 400microseconds or the results are stale (from the previous reading)
    delayMicroseconds(500); // needs a delay of 500microseconds here
    uint32_t t1 = micros(); 
    Wire.requestFrom(0x28, 4);
    uint16_t pmsb = Wire.read();
    uint32_t t2 = micros(); 
    uint16_t stat = (pmsb & 0xC0) >> 6; 
    if (stat == 3) {
        P("fault "); 
        P(stat); 
        P("\n"); 
        return 0; 
    }
    uint16_t plsb = Wire.read();
    uint32_t t3 = micros(); 
    uint16_t rawpress = ((pmsb & 0x3F) << 8) | (plsb); 
    
    uint16_t tmsb = Wire.read();   // this takes 600ms
    uint32_t t4 = micros(); 
    uint16_t tlsb = Wire.read();
    uint32_t t5 = micros(); 
    uint16_t rawtemp = (tmsb << 3) | (tlsb >> 5); 
    
    //P(" raw: ");  P(rawpress);   P("  ");   P(rawtemp);   P(" stat=");   P(stat); 
    //P("  ts ");  P(t1 - t0);  P(" ");  P(t2 - t1);  P(" ");  P(t3 - t2);  P(" ");  P(t4 - t3);  P(" ");  P(t5 - t4); 
    //P("\n"); 
    return rawpress; 
}

int mstampdivider = 25; 
int prevmstampdivided = 0; 
void loop()
{
    int nmillis = mstampdivider*(prevmstampdivided+1); 
    while (millis()<nmillis)
        ;
    //delayMicroseconds(random(10000)); 
    prevmstampdivided = millis()/mstampdivider; 
    P(micros()); 
    P(" "); 
    P(singlereading()); 
    P("\n"); 
    return; 

    int32_t sraw; 
    int32_t srawL; 
    for (int i = 0; i < 16; i++) {
        srawL = singlereading(); 
        sraw += srawL; 
    }
    // this causes the reading
    P(sraw/16); 
    P(" "); 
    P(srawL); 
    P("\n"); 
}



