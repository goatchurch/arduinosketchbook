// Copyright 2016 Julian Todd <julian@goatchurch.org.uk> freesteel.co.uk
// This code is free, see COPYING.WTFPL for details.

#include "Arduino.h"

#include "SPI.h"       
#include "SdFat.h"

#include "SDloggingBase.h"
#include "SDloggingFat.h"

#define P(X) Serial.print(X)

// copied over from serialinputbuffer.cpp
#define SERIALINPUTBUFFER_BUFFSIZE 128 


SDloggerFat::SDloggerFat(uint8_t lchipselect, const char* dirname) :
    SDloggerBase(lchipselect, dirname)
{;}

void SDloggerFat::close()  {  sdfile.close();  sdstate = 1;  }
void SDloggerFat::flush()  
{
    if (pinLEDforflush != -1)
        analogWrite(pinLEDforflush, 100); 
    sdfile.flush();  
    mstamplastflush = mstamplaststartline; 
    Nlinessincelastflush = 0; 
    Ncharssincelastflush = 0; 
    if (pinLEDforflush != -1)
        analogWrite(pinLEDforflush, 0); 
}
bool SDloggerFat::writestring(const char* str) 
{
    return sdfile.write((uint8_t*)str, strlen(str)); 
}

size_t SDloggerFat::write(uint8_t v) 
{
    return sdfile.write(v); 
}

void SDloggerFat::SDprintStats()
{
    logfilename[nlogfileslash] = '\0';  // terminate the string at the slash
    uint32_t cardsize = SD.card()->cardSize();
    uint32_t sizeMB = 0.000512 * cardsize + 0.5;
    P("Card size: "); P(sizeMB);
    P("Volume is FAT"); P(int(SD.vol()->fatType()));  P("\n"); 
    P("Cluster size (bytes): ") << P(512L * SD.vol()->blocksPerCluster()); P("\n"); 
    P("Files found (name date time size):\n");
    sdfile = SD.open(logfilename, O_READ); 
    sdfile.ls(LS_R | LS_DATE | LS_SIZE);
    sdfile.close(); 
    logfilename[nlogfileslash] = '/';  
}

void SDloggerFat::SetupSD()  
{  
    pinMode(chipselect, OUTPUT);
    bool bsucc = SD.begin(chipselect, SPI_HALF_SPEED);
    sdstate = (bsucc ? 1 : 0); // 0 for dead 
    P("sdstate="); 
    P(sdstate); 
    P("\n"); 
}

bool SDloggerFat::openreplayfile()
{
    if (!SD.exists("REPLAY.TXT"))
        return false; 
    sdfile = SD.open("REPLAY.TXT", O_READ); 
    sdstate = 8; 
    return true; 
}

long SDloggerFat::replayfilesize()
{
    return max(1, sdfile.size()); 
}
 

int8_t SDloggerFat::readreplayfileintobuff(char* buff)
{
    if (sdstate != 8)
        return -1; 
    int8_t bufflen = 0; 
    buff[0] = '\0'; 
    while (bufflen < SERIALINPUTBUFFER_BUFFSIZE-3) {
        if (sdfile.read(buff+bufflen, 1) != 1) {
            sdstate = 0; 
            return -1; 
        }
        if (buff[bufflen] == '\n') 
            break; 
        bufflen++; 
    }
    buff[bufflen] = '\0'; 
    return bufflen; 
}


bool SDloggerFat::opennextlogfileforwriting(const char* pisodatetime, uint32_t mstamp)
{
    if (sdstate > 1)
        close(); 
    sdstate = 1; 
    logfilename[nlogfileslash] = '\0';  // terminate the string at the slash
    if (!SD.exists(logfilename)) {
        P("making directory: "); P(logfilename); P("\n"); 
        if (!SD.mkdir(logfilename)) {
            P("mkdir failed\n"); 
            return false; 
        }
    }
    logfilename[nlogfileslash] = '/'; 
    setlogfilename(logfilenumber);  
    while (SD.exists(logfilename)) {
        if ((logfilenumber % 10) == 0) {  P(logfilename);  P(" exists \n"); }
        logfilenumber++; 
        setlogfilename(logfilenumber);  
    }

    // reuse last file if size zero
    if (logfilenumber != 0) {
        setlogfilename(logfilenumber - 1);  
        sdfile = SD.open(logfilename, O_READ); 
        P(logfilename); P(" size: "); P(sdfile.size()); P("\n"); 
        if (sdfile.size() == 0) 
            logfilenumber--; 
        else 
            setlogfilename(logfilenumber);  
        sdfile.close(); 
    }

    Nloglines = 0; 
    Nloglinesfromserialbuff = 0; 
    Nloglinesfailed = 0; 
    Nlinessincelastflush = 0; 
    Ncharssincelastflush = 0; 
    mstamplastsuccess = mstamp; 
    mstamplaststartline = mstamp; 
    mstamplastflush = mstamp; 
    
    P("Openinglogfile: "); P(logfilename); P("\n"); 
    sdfile = SD.open(logfilename, O_RDWR | O_CREAT | O_APPEND); 
    
    if (pisodatetime != 0) {
        P("Setting file date: "); P(pisodatetime); P("\n"); 
        sdfile.timestamp(T_CREATE|T_WRITE|T_ACCESS, atoi(pisodatetime), atoi(pisodatetime+5), atoi(pisodatetime+8), atoi(pisodatetime+11), atoi(pisodatetime+14), atoi(pisodatetime+17)); 
    }

    sdstate = 4; 
    return true; 
}



void SDloggerFat::writeline(int offs)
{
    sddata[offs++] = '\n'; 

long m0 = millis();  
    if (sdfile.write(sddata, offs)) {
long mD = millis() - m0; 
if (mD > 10) {
    P("Long sdfile.write call "); 
    P(mD); 
    P(" logl "); 
    P(Nloglines); 
    P(" chrs "); 
    P(Ncharssincelastflush); 
    P("\n"); 
} 

        Nloglines++; 
        Nlinessincelastflush++; 
        Ncharssincelastflush += offs; 
        mstamplastsuccess = mstamplaststartline; 
    } else {
        Nloglinesfailed++; 
        if (Nloglinesfailed == 10) 
            P("At least 10 log lines failed\n"); 
        if ((Nloglinesfailed > 100) && (mstamplaststartline - mstamplastsuccess > 10000) && (mstamplaststartline - mstamplastflush > 10000)) {
            P("Attempting to reconnect to SD card file\n"); 
            SetupSD(); 
            if (SD.exists(logfilename)) {
                File lsdfile = SD.open(logfilename, O_READ); 
                P(logfilename); P(" size: "); P(lsdfile.size()); P("\n"); 
                lsdfile.close(); 
                P("Reopeninglogfile: "); P(logfilename); P("\n"); 
                sdfile = SD.open(logfilename, O_RDWR | O_CREAT | O_APPEND); 
                Nloglinesfailed = 0; 
            }
            mstamplastflush = mstamplaststartline; 
        }
    }
}

