#ifndef SDLOGGINGFAT_h
#define SDLOGGINGFAT_h


class SDloggerFat : public SDloggerBase
{
protected:
    SdFat SD;
    File sdfile;
    bool bopenlogfilefailed; 
public:
    virtual void close(); 
    virtual void flush(); 
    virtual bool writestring(const char* str);
    virtual void writeline(int offs); 
    virtual size_t write(uint8_t v); 
    
    SDloggerFat(uint8_t lchipselect, const char* dirname); 
    virtual void SetupSD(); 
    virtual void SDprintStats();  // if you call this after log file is open, it drops the handle
    
    virtual bool opennextlogfileforwriting(const char* pisodatetime, uint32_t mstamp); 
    bool openreplayfile(); 
    long replayfilesize();
    int8_t readreplayfileintobuff(char* buff); 
};



#endif
